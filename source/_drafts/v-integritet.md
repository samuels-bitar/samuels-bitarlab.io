---
title: Vänsterpartiet, integritet och övervakning
image: valborg.jpg
description: Hur ser Vänsterpartiets politik kring integritet och övervakning ut?
date: 2023-05-22 21:00
---

Motståndet mot massövervakningsförslaget Chat Control 2.0 har ökat och kritiken har inte varit nådig. Förslaget har kritiserats av [ministerrådets egna jurister](https://www.svt.se/nyheter/utrikes/omstridd-overvakning-sagas-av-eu-jurister), [Internetstiftelsen](https://www.linkedin.com/posts/carlpiva_angående-förslaget-till-europaparlamentets-activity-7056978434094153730-R5TG), [Svenska Journalistförbundet](https://www.journalisten.se/nyheter/journalistforbundet-chattkontroll-hotar-kallskyddet) och många fler.

Lördagen den 20 maj så anordnade [Kamratdataföreningen Konstellationen](https://konstellationen.org), [Föreningen för Digitala Fri- och Rättigheter (DFRI)](https:/dfri.se) och [5 juli-stiftelsen](https://femtejuli.se) en [demonstration mot Chat Control](https://konstellationen.org/2023/05/20/demo-chatcontrol-tal/). Det enda riksdagsparti som hade en talare på plats var Vänsterpartiet med riksdagsledamoten Andrea Andersson-Tay. (Hennes tal går att [läsa här](https://konstellationen.org/files/Tal-Andrea-Andersson-Tay.pdf)).

De så kallade "nätfrågorna" har inte diskuterats mycket senaste åren. Chat Control är ett dåligt förslag men om det är något positivt det har med sig så är det att frågorna om integritet, övervakning och mänskliga rättigheter på nätet återaktualiseras.

## Vänsterpartiet och Chat Control

* Vänsterpartiet sade [nej till Chat Control](https://www.etc.se/utrikes/v-saeger-nej-till-chat-control-enormt-intraang) 5 april. Intervju med Jessica Wetterling (V), ledamot i konstitutionsutskottet.
* Malin Björk (V), EU-parlamentariker, [debatterar Chat Control i SVT](https://www.svt.se/nyheter/svtforum/brottsbekampning-med-chat-control)

Vänsterpartiet lyfter fram 
