

Known CSAM, high accuracy, but risk for abuse.
New CSAM, Grooming, not high accuracy

Grooming, low accuracy, currently tools are for English mostly

Highly problematic for E2EE. 

"The commission uses Signal which is end to end encrypted"
Software will create new vulns. Hackers and nationstates might use it.
Can be used to detect other content. Opposition players could be target with these tools.

These tools are based on "security by obscruity" so it can not be reviewed by third parties


Proposal mandates scanning. Reported content will increase. There may be overreport.

Även om förslaget skulle kunna delvis skydda barn så kan förslaget också komma att skada deras yttrandefrihet.

Chilling effects expected for adult users. 

Förövare kan flytta över sin kommunikation till darkweb

---

N Vavoula

Även om förslaget kan få positiva effekter får barn kommer även barn att drabbas av:

- rätt till privatliv
- skydd av personlig data
- yttrandefrihet
- barnrättigheter

Europadomstolen har beslutat att generell övervakning bara kan godkännas om det är tillräckligt riktat.

https://techcrunch.com/2022/04/05/cjeu-serious-crime-data-retention/

"Scanning content on users' personal devices in E2EE communications violates the essence of the right to data protection"


Slutsats:

Effectiveness

It is unlikely that the CSA proposal will be effective, primarily, given that:

- Technologies to detect new CSAM and grooming are of low accuracy;
- Detecting CSAM in E2EE communication raises fundamental challenges with regard to privacy and creates substantial vulnerabilities for users;
- The detection orders for providers to detect, report and remove CSAM are disproportionate to the objective.

Efficiency:

Given the expected limited effectivness, it is difficult to assess expected efficiency. Nevertheless:
- Increased EU indepence (through EU Centre) in detection of CSAM would benefit efficiency


----

EU-kommissionens representant O Onidi.

Han gratulerade att de som gjorde rapporten kunde göra det på 2 månader. Det tog EU-kommissionen mer än 2 år att göra deras impact assessment.

Erkänner att han inte haft tid att läsa "the study".

Menar att förslaget syftar till att gå på ekonomiska aktörer. Plattformarna. Att få alla plattformar att engagera sig


"The margin of false positive even for new content is actually quite acceptable"

