---
title: Blogroll
date: 2024-08-26 10:00
---
En klassisk [blogroll](https://blogroll.org/what-are-blogrolls/)! Men jag länkar till lite mer grejer än bara bloggar. Det här är bara ett axplock.

För fler tips på bra bloggar, poddar och tidningar så kan jag rekommendera att kolla in [Mediakollen](https://mediakollen.org) och alla deras [flöden](https://mediakollen.org/feed).

## Bloggar

* Turist - https://www.turist.blog
* Svenssons Nyheter - https://blog.zaramis.se
* Gnomvid/Laia Odo-institutet - https://gnomvid.se
* Copyriot II - https://copyriot.se
* Det glada tjugotalet - https://detgladatjugotalet.se
* Fredrik Edin - https://fredrikedin.se
* Laserjesus - https://laserjesus.se
* Goda liv i kollapsen - https://prepp.fritext.org

## Poddar

* Café Bambino - https://mediakollen.org/feed/185/curated
* Apans anatomi - https://tidningenbrand.se/podd/apans-anatomi/
* GigWatch-podden - https://www.gigwatch.se/podd/
* Komintern - https://soundcloud.com/kominternpodd
* Radio åt alla - https://radio.alltatalla.se/
* Krakelpodden - https://shows.acast.com/krakelpodden
* The 404 media podcast - https://www.404media.co/the-404-media-podcast/
* Tech won't save us - https://www.techwontsave.us/
* Darknet Diaries - https://darknetdiaries.com/
* Hej (resten av) internet! - https://hejinter.net/