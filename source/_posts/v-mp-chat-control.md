---
title: V och MP erkänner miss - är fortsatt mot Chat Control
image: surveillance-eye.jpg
image_description: Bild på ett öga av <a href="https://pixabay.com/users/thedigitalartist-202249/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4518402">Pete Linforth</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4518402">Pixabay</a>
description: Chat Control var uppe på Justitieutskottets möte 18 juni. C och SD anmälde avvikande mening men inte V och MP. Förvirringen var stor men det var ett misstag säger V och MP nu.
date: 2024-06-19 17:00
---

**Chat Control var uppe på Justitieutskottets möte 18 juni. C och SD anmälde avvikande mening men inte V och MP. Förvirringen var stor men det var ett misstag säger V och MP nu.**

Emanuel Karlsten var först ut med att rapportera om att regeringen [stressade igenom stöd för ett Chat Control-förslag](https://emanuelkarlsten.se/regeringen-kuppar-igenom-chat-control-kan-klubbas-i-eu-redan-onsdag/) i riksdagen.

Karlsten sammanfattar:

1) Sveriges regering driver igenom Chat control utan att ha förankrat beslutet i EU-nämnden, vilket innebär att hållning är att EU-medborgares kommunikationer ska övervakas i jakt på brott.

2) Efter lång tystnad och debatt, och utan beslut i riksdagen, har den svenska regeringen nu tagit en positiv hållning till det belgiska kompromissförslaget om client-side-scanning, där medborgaren skannas innan ett meddelande skickas.

3) Med press från Belgien, har en snabb process inletts med COREPER II-möte den här 20 juni, där Sveriges regering sökt och fått stöd från justitieutskottet, vilket kan leda till att EU:s ministerråd antar en hållning, trots tidigare motstånd.


Belgien som har EU-ordförandeskapet har senaste veckorna samlat stöd för att ministerrådet ska ställa sig bakom ett kompromissförslag för Chat Control som har kvar de allvarliga massövervakningsförslagen som det ursprungliga förslaget från EU-kommissionen. Kompromissen innebär att meddelanden inte ska avlyssnas och avkrypteras när de skickas – utan innan de skickas. Detta kallas klientside-skanning. Och på så sätt menar man att man inte bryter kryptering och därmed respekterar rätten till privat kommunikation. Det är ju tekniskt sant att man inte kommer att bryta totalsträckskrypterade kommunikation (för det går inte, det är själva poängen) men resultatet blir detsamma: man kommer inte få ha sin kommunikation privat. Ett ny grej med förslaget är att användaren måste acceptera att få sin kommunikation skannad om den ska kunna skicka länkar, bilder och videos.

Mer om själva belgiska förslaget kan man läsa om här:

* [Be scanned – or get banned!](https://edri.org/our-work/be-scanned-or-get-banned/) av EDRi
* [EU Council Presidency’s Last-Ditch Effort For Mass Scanning Must Be Rejected](https://www.eff.org/deeplinks/2024/06/eu-council-presidents-last-ditch-effort-mass-scanning-must-be-rejected) av EFF

Det var alltså det belgiska förslaget som var uppe på Justitieutskottets möte 18 juni.

## Riksdagspartiernas hållning i Chat Control fram till EU-valet

V, C och MP har varit tydligast kritiker av Chat Control sen förslaget kom. Kamratdataföreningen Konstellationen sammanfattade partiernas hållning inför EU-valet med följande bild och [blogginlägg](https://konstellationen.org/2024/06/07/chatcontrol-riksdagspartier/):

![Partiernas hållning i Chat Control](/images/chatcontrol-partier.png)

Datan är helt baserad på granskning av partierna av den partipolitiskt obundna sajten [chatcontrol.se](https://chatcontrol.se/status/).

## Vänsterpartiet och Miljöpartiet missade anmäla avvikande mening - är fortfarande mot Chat Control

Men på Justitieutskottets möte så var det bara C och SD som anmälde en avvikande mening i protokollet. Många blev förvånade och oroade över att V och MP inte gjorde det och undrade om V och MP ändrat linje.

Men det visade sig att det var missförstånd och att V och MP missade att anmäla avvikande mening.

### Vänsterpartiet

Under tisdagskvällen kommenterade Gudrun Nordborg (V) själv frågan på [Facebook](https://www.facebook.com/100064726404851/posts/867345058766366/?rdid=q388RNSQYERMYmon), något som [Flamman skriver om](https://www.flamman.se/vansterpartiet-godkanner-kritiserad-lag-massovervakning/)

> ”Vänsterpartiet har inte ändrat sig om chat control. Vi är emot de förslag som innebär massövervakning och stödjer inte det belgiska förslag som regeringen nu vill ha”, skriver hon, och lägger till i en kommentar:
>
> ”Det stämmer att jag inte anmälde avvikande mening på utskottsmötet, det blev en miss. Jag beklagar det verkligen”.

Ilona Szatmári Waldau (V), ledamot i EU-nämnden, säger till [Dagens ETC](https://www.etc.se/inrikes/v-slaeppte-fram-chat-control-foerslag-som-de-sjaelva-aer-emot) att Vänsterpartiet inte har ändrat sig i frågan:

> – Vi tycker inte att det är ett bra förslag.
>
> Hade det inte då varit ett bra tillfälle att uttrycka detta under justitieutskottets sammanträde?
> 
> – Det är alltid lätt att vara efterklok, men vi har inte bytt ställning i frågan och det är det viktigaste, säger Illona Szatmári Waldau.
>
> Hon bekräftar att det verkar ha skett ett missförstånd  på Justitieutskottets möte. 
>
> – Det har inte påverkat utfallet och ledamöterna har inte röstat, men givet partiets position hade det varit tydligare att anmäla avvikande mening.
>
> Nu väntar man på att frågan kommer tillbaka till EU-nämnden.
>
> – Då kommer vi i Vänsterpartiet vara väldigt tydliga med vår kritik. Det är såklart otroligt viktigt att skydda barn mot sexuella övergrepp på nätet men det får inte innebära massövervakning. 

Även Daniel Riazat (V), [bekräftar på X](https://x.com/danielriazat/status/1803348395156738294) att Vänsterpartiets hållning i Chat Control inte har ändrats:

> Jag sitter i EU-nämnden tillsammans med några andra kamrater från V. Självklart röstar vi som vi aviserat sedan tidigare när förslag om Chat Control kommer upp. Vänsterpartiet har inte ändrat ställningstagande i frågan överhuvudtaget.

[Emanuel Karlsten](https://emanuelkarlsten.se/vansterpartiet-om-varfor-de-stottade-chat-control-det-skedde-en-miss/) har också skrivit om att V gjorde en miss.

### Miljöpartiet

Rebecka Le Moine (MP) [berättar i SvD](https://www.svd.se/a/1MOVgM/mp-om-chat-control-det-blev-fel) varför de gjorde fel:

> I utskottet var det bara SD och C som invände. Att även MP och V är emot det omstridda förslaget framgick inte, då de två partierna missade att redovisa en avvikande uppfattning.
>
> Enligt Rebecka Le Moine, som sitter i EU-nämnden för MP, skedde en sammanblandning av två olika förslag.
>
> – Det finns två olika versioner, dels rådets kompromissförslag som är så generellt i sina skrivelser att vi inte alls kan ställa oss bakom det.
>
> – Dels ett förslag där vi i den gröna gruppen i EU-parlamentet har arbetat fram olika krav som vi har fått gehör för. Det ställer vi oss bakom, säger hon.
>
> Det var alltså den förra som togs upp i utskottet.
>
> – Med facit i hand skulle vi ha lämnat en avvikande åsikt, men det gjorde vi inte utan det var en miss. Det är mänskligt att man ibland missar, säger Rebecka Le Moine.

### Lätt att göra fel uppenbarligen

Att V och MP gjorde en miss här är så klart inte bra. Men det är tydligt att det är lätt att göra fel. Även Centerpartiet gjorde en miss i höstas när frågan var uppe.

chatcontrol.se skriver om [Centerpartiet](https://chatcontrol.se/status/#centerpartiet-se):

> Vid justitie­utskottets sammanträde 2023-09-14 framförde Centerpartiets närvarande ledamot Ulrika Liljeberg dock ingen förslags­avvikande ståndpunkt. I en kommentar till [Dagens ETC](https://www.etc.se/inrikes/slutspurt-foer-chat-control) förklarade Liljeberg detta med att ”hennes partis brist på kritik i justitie­utskottet berodde på att regeringens handlingar kom in väldigt sent och att partiets ledamot inte fick tillräckligt med tid för att sätta sig in i ämnet” (2023-09-22).

## Förslaget smögs igenom

Niels Paarup-Petersen (C) är också kritisk mot hur förslaget klubbades igenom. Han säger att regeringen kom med förslaget till ”sittande bord.” Det vill säga utskottet hade inte fått förslaget innan och haft tid att sätta sig in i det. Han menar att regeringen gjorde det för att slippa diskussion.

IT-säkerhetsspecialisten Karl Emil Nikka är också kritisk till förslaget. De bakdörrar som skulle krävas att byggas in i användartjänsterna kan senare utvidgas till att skanna efter mer innehåll.

Han menar också att förslaget inte kommer att vara effektivt för att stoppa spridningen av barnpornografi. 

– Det här kompromissförslaget visar med all tydlighet att chat control aldrig hade något att göra med att skydda barnen. För vad kommer förövare svara på frågan om de godkänner att deras innehåll skannas? De kommer svara nej. Och så kommer de använda någon annan meddelandetjänst som inte omfattas av reglerna, säger han till Dagens ETC.


## Var på tårna inför EU-nämndens möte

Nu har följande partiet klargjort sin ståndpunkt mot Chat Control: V, MP, C och SD. Det är otroligt viktigt att man inte tabbar sig igen i den här frågan.

Och frågan måste nu ställas till Moderaterna och Liberalerna hur de kan föra ett budskap i EU-parlamentet och ett annat i Sverige i regeringsställning.

Det kan bli fel och det är olyckligt, speciellt kring sådana här viktiga frågor. Det belyser också problemen med många EU-förslag. Det är dåligt bevakat och svårt att få reda på information och hålla sig uppdaterad. Viktiga demokratiska principdebatter försvåras. Och det blir inte bättre när regeringen gör vad de kan för att köra fulknep för att smyga igenom förslag genom att ta saker vid "sittande bord".

Det är upp till alla oss är oroliga för massövervakningen att vara på i den här frågan. Stort tack till Emanuel Karlsten för att han har varit på i den här frågan.