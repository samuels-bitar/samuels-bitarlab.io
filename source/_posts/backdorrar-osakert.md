---
title: Hård kritik mot regeringens förslag om bakdörrar
image: fbi.jpg
image_description: Bild av <a href="https://pixabay.com/users/kirill_makes_pics-5203613/">kirill_makes_pics</a> från <a href="https://pixabay.com">Pixabay</a>
description: Regeringen vill kräva bakdörrar på totalsträckskrypterade appar men Försvarsmakten, Journalistförbundet, Internetstiftelsen, IT-säkerhetsexperter, med flera sågar förslaget
date: 2025-02-25 12:00
---

Nu försöker regeringen införa någon slags svensk Chat Control genom lagförslaget [Datalagring och tillgång till elektronisk information](https://www.regeringen.se/rattsliga-dokument/departementsserien-och-promemorior/2024/11/utkast-till-lagradsremiss-datalagring-och-tillgang-till-elektronisk-information/).

Regeringens syfte med med att införa bakdörrar i krypterade appar är att Säkerhetspolisen och Polismyndigheten ska kunna bedriva brottsbekämpning digitalt enklare. Myndigheterna ska då få möjlighet att begära in historik från en meddelandebank rörande enskilda personer.

Kritiken låter inte vänta på sig. It-specialisten Mikael Setterberg menar att det inte enbart är de kriminella som polisen vill komma åt som kommer bli påverkade om lagen träder i kraft.

– Det är en mänsklig rättighet att man ska kunna kommunicera hemligt. Många liv kommer att ödeläggas, [säger han.](https://www.svt.se/nyheter/inrikes/it-experten-man-vill-komma-at-kriminella-men-laglydiga-straffas)

Även Försvarsmakten är negativa och har nyligen uppmanat sin personal att börja använda Signal för att minska risken för avlyssning.

För oss som vill stoppa massövervakningshetsen så är det dags att bedriva opinion och kontakta riksdagspartierna igen och kräva svar på om de står upp för rätten att kommunicera privat.

## Lagförslaget

I utkastet till lagförslaget föreslår man:

> Tillhandahållare av nummeroberoende interpersonella kommunikationstjänster ska omfattas av regler om lagringsskyldighet och ska anpassa sin verksamhet så att hemliga tvångsmedel kan verkställas.

Eller med andra ord: man föreslår ett krav på bakdörrar, även för totalsträckskrypterade appar som Signal.

[Remisserna](https://www.regeringen.se/remisser/2024/11/remiss-av-utkast-till-lagradsremiss-datalagring-och-tillgang-till-elektronisk-information/) är otroligt kritiska.

IT-säkerhetsexperten Karl-Emil Nikka har djupdykt i remissvaren i Bli säker-podden och publicerar [resultatet på sin blogg](https://nikkasystems.com/2025/01/31/podd-285-forsvarsmakten-varnar-for-bakdorrar/). Nedan följer några utdrag från några av remissvaren.

> Utifrån sin sakkunskap genom uppdraget att leda och samordna signalskydds­tjänsten ser Försvarsmakten anledning att kommentera vad som anförs i utkastet om hur anpassnings­skyldigheten kan genomföras vid totalsträcks­krypterad kommunikation. Försvarsmakten bedömer att kravet på anpassnings­skyldighet av nummer­oberoende interpersonella kommunikations­tjänster inte kommer att kunna uppfyllas utan att införa sårbarheter och bakdörrar som kan komma att nyttjas av tredje part.

– [Försvarsmakten](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/forsvarsmakten.pdf)


> Detta innebär därmed att bakdörrar måste byggas in i kommunikations­tjänster. Bakdörrar är att likställa med sårbarheter ur ett riskhanterings­perspektiv, och sårbarheter får aldrig byggas in i tjänster enligt Netnod. Dessutom går det lagda förslaget stick i stäv med NIS2, speciellt skäl 98.

– [Netnod](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/netnod-ab.pdf)

> Enligt Truesec saknas tillräcklig sakkunnig expertis inom cyber och cybersäkerhet i utredningen. Detta gör att konsekvenserna för totalsträcks­krypterade s.k. nummer­oberoende interpersonella kommunikations­tjänster (”NOIK”) inte är tillräckligt utredda, särskilt hur förslaget kan leda till sämre säkerhet för samtliga användare.

– [Truesec](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/truesec-ab.pdf)

> Det är, rent tekniskt, så att anpassnings­skyldigheten är omöjlig att tillgodose utan att bryta totalsträcks­krypteringen. Därmed är lagförslaget att jämföra med att förbjuda totalsträcks­krypterade tjänster av den allmänt tillgängliga sorten.

– [DFRI, Isoc-SE och Snus](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/isoc-se-dfri-och-snus.pdf)

> Förslaget till reglering av nummer­oberoende interpersonella kommunikations­tjänster i utkastet till lagrådsremiss har mycket gemensamt med CSAM-förordningen, EU-förordningen som även kallas Chat control, eftersom båda förslagen skulle omöjliggöra fullsträcks­kryptering. Mot bakgrund av de negativa effekter förslaget skulle få för journalister och andra som har stort och legitimt behov att kommunicera anonymt avstyrker Journalist­förbundet förslaget.

– [Journalistförbundet](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/svenska-journalistforbundet-sjf.pdf)

> Vidare kan krav på att tillhandahålla bakdörrar och nyckel­deponering avseende totalsträcks­krypterade tjänster få motsatt effekt, eftersom det inte bara är brotts­bekämpande myndigheter som kan göra bruk av dessa utan även kriminella och andra hotaktörer. Detta påpekades t.ex. av Europol och EU:s cybersäkerhetsmyndighet ENISA i ett gemensamt yttrande 2016.

– [Stockholms universitet (Juridiska fakulteten)](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/stockholms-universitet-juridiska-fakulteten.pdf)

> Som vi framfört tidigare ifrågasätter Internetstiftelsen inte att tillgång till information och bevisning från elektroniska kommunikationer ofta är av stor betydelse i brottsutredningar. Men att bryta eller försvaga krypteringslösningar för att komma åt uppgifter kan få stora konsekvenser; det öppnar upp möjligheten för missbruk, både vid själva användningen och av tredje part, vilket äventyrar samtliga användares integritet och säkerhet.
> 
> Internetstiftelsen välkomnar förtydligande i utkastet till lagrådsremiss att det är av vikt att exempelvis totalsträckskrypterade tjänster alltjämt kan användas (s. 88) men noterar samtidigt att utredningen framhåller att med förslaget blir de aktuella tillhandahållarna skyldiga att lämna ut uppgifter till de brottsbekämpande myndigheterna ur den totalsträckskrypterade informationen. För Internetstiftelsens går resonemanget inte ihop eftersom hela syftet med totalsträckskrypterade tjänster är att ingen förutom mottagare och sändare ska kunna ta del av informationen

– [Internetstiftelsen](https://www.regeringen.se/contentassets/e22f777eb1964c258c5d9a21adb6a355/stiftelsen-for-internetinfrastruktur.pdf)

Nu säger [Signal att de kommer lämna Sverige](https://www.svt.se/nyheter/inrikes/signal-lamnar-sverige-om-regeringens-forslag-pa-datalagring-klubbas) om lagförslaget går igenom.

## Rapporten Kluvna tungor om Chat Control

När Chat Control var på tapeten sommaren/hösten 2024 så skickade Kamratdataföreningen Konstellationen ut en enkät till alla riksdagspartier med följande frågor (min fetning):

1. Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står ni bakom EU-parlamentets kompromissförslag?
3. Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. **Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?**
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle ni säga nej till förslaget?
6. **Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?**
7. Anser ni att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

V, C, MP och SD svarade alla "ja" på alla frågor. Frågorna och bakgrunden finns redovisade i rapporten [Kluvna tungor](https://konstellationen.org/2024/09/19/chatcontrol-rapport/).

Nu är det upp till bevis! Kommer partierna hålla fast vid ståndpunkterna de tog gällande Chat Control?

Extra intressant är det så klart vad SD gör eftersom de är en del av regeringsunderlaget. Kommer de att kompromissa bort tidigare ställningstagande för att gå resten av Tidölaget till mötes?

## Förslag på mailtext

Så här hade man kunnat skriva till V, C, MP och SD.

> Hej,
> 
> Nu försöker regeringen tvinga igenom bakdörrar för totalsträckskrypterade appar genom lagförslaget "Datalagring och tillgång till elektronisk information".
> 
> Lagförslaget har mötts med massiv kritik av remissinstanser som Försvarsmakten, Journalistförbundet, Truesec och Internetstiftelsen för att nämna några.
> 
> Ditt parti har tidigare tagit tydlig ställning mot EU-kommissionens massövervakningsförlag Chat Control. Det här lagförslaget från regeringen påminner väldigt mycket om Chat Control.
> 
> I rapporten Kluvna tungor svarade ditt parti ja på följande frågor:
> 
> * Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
> * Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
> * Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
> * Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?
> 
> Med det sagt undrar jag om ni kommer fortsätta stå upp för rätten till privat kommunikation och att totalsträckskrypterad kommunikation ska fortsätta vara tillåten utan att man försöker kringgå krypteringen med hjälp av exempelvis bakdörrar?
> 
> Vänlig hälsning,

## Kontaktuppgifter till V, C, MP och SD

Vänsterpartiet

* Officiell mail - info@vansterpartiet.se
* Gruppledare Samuel Gonzalez Westling - samuel.gonzalez.westling@riksdagen.se
* Ledamot i Justitieutskottet, Gudrun Nordborg - gudrun.nordborg@riksdagen.se

Centerpartiet

* Officiell mail - info@centerpartiet.se
* Gruppledare Daniel Bäckström - daniel.backstrom@riksdagen.se
* Ledamot i Justitieutskottet, Ulrika Liljeberg - ulrika.liljeberg@riksdagen.se

Mljöpartiet

* Officiell mail - info@mp.se
* Gruppledare Annika Hirvonen - annika.hirvonen@riksdagen.se
* Ledamot i Justitieutskottet, Ulrika Westerlund - ulrika.westerlund@riksdagen.se

Sverigedemokraterna

* Officiell mail - info@sd.se
* Gruppledare Linda Lindberg - linda.lindberg@riksdagen.se
* Justitieutskottets ordförande Henrik Vinge - henrik.vinge@riksdagen.se

## Vill Liberalerna fortfarande ha en massövervakningsstat?

Jag vet att jag borde sluta förvänta mig någon slags liberal politik alls från Liberalerna. Men så länge de har kvar partinamnet så kommer jag kräva svar på frågorna. Jag har bloggat tidigare om att [Liberalerna duckar frågorna](/duckade-chat-control-igen/).

Så det är återigen dags att ställa frågorna till dem:

1. Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står ni bakom EU-parlamentets kompromissförslag?
3. Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle ni säga nej till förslaget?
6. Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser ni att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Här är kontaktuppgifter till Liberalerna tagna från [riksdagens hemsida](https://www.riksdagen.se/sv/ledamoter-och-partier/partierna/liberalerna/):

* Officiell mail - info@liberalerna.se
* Partisekreterare Jakob Olofsgård - jakob.olofsgard@riksdagen.se
* Gruppledare Lina Nordquist - lina.nordquist@riksdagen.se
* Vice gruppledare Louise Eklund - louise.eklund@riksdagen.se
* Ledamot i Justitieutskottet, Martin Melin - martin.melin@riksdagen.se

## Namninsamling

Jag fick nys om [den här namninsamlingen](https://www.skrivunder.com/stoppa_lag_om_bakdorr_i_meddelandeappar) man kan skriva under om man vill protestera.

Jag har skrivit under. Gör du det med!