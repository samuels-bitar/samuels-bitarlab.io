---
title: Krönika - Hållbart nätanvändande
image: cat-laptop.jpg
description: En krönika om hållbart nätanvändande och sociala medier jag skrev i ETC Nyhetsmagasin
date: 2024-02-06 09:00
---

Jag har blivit krönikör i ETC Nyhetsmagasin! Superkul tycker jag. Jag hade bara tänkt att börja blogga men roligt att jag också kan skriva så att fler läser. I och med att jag får betalning för texterna så är det bra om jag kan vänta någon vecka innan jag lägger ut här på bloggen. Men här kommer första texten! Går också att läsa på [ETC.se](https://www.etc.se/kronika/aangestbefriade-kattbilder).

## Så får jag lagom med sociala kickar

**Under lång tid har jag haft svårt att se sådant jag faktiskt bryr mig om: inlägg från mina vänner och politiskt engagerade kamrater. Det är många som upplevt det här när de använder sig av Big Techs sociala medier.**

Chipsreklam, smörreklam, EU-reklam (what?) och propagandabilder från konspirationsteoretiker som tror att jorden är platt förpestar mitt Facebook-flöde tillsammans med en kloakfontän av sexism, rasism och homofobi. Under lång tid har jag haft svårt att se sådant jag faktiskt bryr mig om: inlägg från mina vänner och politiskt engagerade kamrater. Det är många som upplevt det här när de använder sig av Big Techs sociala medier.

Men som tur är tar Facebook upp en mindre del av min tid. Jag har nämligen börjat hitta till en närvaro på nätet som jag mår bra av.

En vanlig dag får jag tag på nyhetstips via [Mediakollen](https://mediakollen.org) (en hemsida som [Kamratdataföreningen Konstellationen](https://konstellationen.org), där jag är med, har skapat). Jag delar strunt och nyheter på Mastodon (ett öppet och fritt alternativ till Twitter), gruppchattar med föreningsaktiva i en rad olika rum på Matrix (öppet och fritt alternativ till Messenger/Slack/Discord) och läser nyhetsbrev från Skiftet och andra som kommer till min mejlbox. Jag tar del av bilder från systersonens kalas via familjens Signal-gruppchatt (alternativ till Whatsapp) och skickar – också via Signal – gulliga bilder på katten Morris till min fru som veckopendlar till Hudiksvall. När jag vill lägga ut texten om något skriver jag ett inlägg på min blogg.

Och jag får inte de där jobbiga ångestkänslorna som Facebook ger. Tvärtom, jag får lagom med sociala kickar. Jag har upptäckt att de mest sociala medierna är de mindre sammanhangen: de olika grupp-chattarna på Signal med familj och vänner.

Jag börjar närma mig ett nätanvändande jag vill ha. Ett nätanvändande på mina villkor, där jag använder plattformar som inte tjänar pengar på att jag ska vara kvar och där informationen om min dataaktivitet inte säljs till högstbjudande. Jag har inte brutit mig loss helt från sådana plattformar och får nog leva med att inte lyckas fullt ut. Men jämfört med för några år sedan är jag idag mera fri.

**Samuel Skånberg är civilingenjör i datateknik, webbutvecklare och integritetsaktivist.**


