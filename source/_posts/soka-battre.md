---
title: Krönika - Google förstör – men du kan söka bättre 
image: sign.jpg
image_description: <a href="https://unsplash.com/photos/a-road-sign-pointing-in-opposite-directions-in-the-desert-h1OhvEIIcxs">En bild på en skylt, Unsplash</a>. 
description: En krönika om hur sökmotorers innehåll förstörs av autogenererat AI-innehåll
date: 2024-03-25 09:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/tech).

**Tiden då man kunde förlita sig på ett stort tryggt företag med mottot ”Don't be evil” är förbi.**

Suger Google? Nätanvändare har under flera år klagat på att Googles sökresultat blivit sämre. [Nu bekräftar tyska forskare känslan](https://downloads.webis.de/publications/papers/bevendorff_2024a.pdf). Under ett år undersökte de mer än 7 000 produktgranskningar och recensioner på Google, Bing och Duckduckgo. Alla sökmotorer hade stora problem. De som la mycket krut på speciella sökord (sökoptimering) på sina sidor eller på annonssamarbeten med produkter, kom högre upp bland sökresultaten. Dessa sidor visade samtidigt tecken på lägre textkvalitet än webben i stort.

Även sökning efter nyheter blir sämre. Nättidningen 404 Media [rapporterade](https://www.404media.co/google-news-is-boosting-garbage-ai-generated-articles/) nyligen att Google News gynnar skräpartiklar som genererats av AI. Det innebär att sajter som automatiskt skrapar innehåll från nyhetssidor och skriver om dem med AI får sina ”nya artiklar” högt placerade på Google News. Google säger till 404 Media att de inte ämnar göra något åt detta.

Ett knep som nätanvändare använder för att få mer relevanta sökträffar är att lägga till ordet ”reddit” i frågan, för att få länkar till nätforumet Reddit. Användare på Tiktok söker direkt på plattformen för att snabbt få ett recept på en smarrig bolognese istället för att skickas till en sida där själva receptet kommer först efter flera stycken med text som bara är avsedd för sökmotorsoptimering. Nätanvändare vill ha resultat som människor skrivit, inte texter som sönderoptimerats eller hittats på av en AI.

Dansken Mikkel Denker är en av dem som tagit sökandet ännu längre. Som en del av sitt mastersarbete byggde han sökmotorn [Stract](https://stract.com/) och har [släppt](https://www.404media.co/this-guy-is-building-an-open-source-search-engine-in-real-time/) resultatet som öppen källkod. En egenskap som gör Stract speciell är att man kan ange att den bara ska visa resultat från forum eller oberoende bloggar. Det finns även betal-alternativ, som sökmotorn [Kagi](https://kagi.com/).

Tiden då man kunde förlita sig på ett stort tryggt företag med mottot ”Don't be evil” är förbi. Vi får helt enkelt lära oss att ändra våra vanor, byta tjänster när de börjar suga och förhoppningsvis se värdet av en mångfald av tjänster och öppna lösningar. 

Tills du är redo att överge Google kan du använda dig av enkla hack som ”reddit-sökningar”.