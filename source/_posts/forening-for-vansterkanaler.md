---
title: Starta förening för vänsterkanaler
image: forening.png
description: Vikten av att vänstern har egna kanaler
date: 2023-01-06 17:00
---

Jag har under en rätt lång tid funderat på att starta en förening för att drifta plattformar och kommunikationskanaler för den breda vänstern. Jag har bloggat om att [bryta sig loss från big tech sociala medier](https://samuels.bitar.se/bryta-sig-loss/) för jag har under lång tid pinats av att vara beroende av övervakningskapitalismens plattformar i form av t.ex. Google och Facebook.

Jag tog äntligen steget och [startade Mastodoninstansen Spejset](https://samuels.bitar.se/spejset-instansen/) där vänsterfolk kan känna sig som hemma. För den som inte känner till Mastodon så är det som ett öppet och decentraliserat alternativ till Twitter där Mastodon är ett nätverk av tusentals servrar/instanser pratar med varandra och varje instans bestämmer över sig själv och vilka regler som gäller på deras instans. Om man vill veta mer kan man läse mitt blogginlägg [Lite kort om Mastodon](https://samuels.bitar.se/lite-kort-om-mastodon/).

Alla som definierar sig som vänster och följer reglerna uppsatta är välkomna att skapa ett konto på [Spejset](https://social.spejset.org/about).

För att instansen ska driftas och styras demokratiskt så kommer jag att starta en förening.

## Vad ska föreningen göra?

Som ett första steg är tanken att föreningen helt enkelt ska ta över driften av Mastodoninstansen Spejset. (Eller om medlemmarna vill, skapa en helt annan distans). Men det finns **så mycket mer** man skulle kunna göra! Min tanke är ju att föreningen ska erbjuda flera olika plattformar och kanaler för vänstern. Mastodon är bara en plattform. 

Några saker som skulle kunna erbjudas är:

* **Eventplattformar** - Facebook är fortfarande använtbart för att skapa events. Exempel på andra lösningar man kan hosta själv: [Mobilizon](https://mobilizon.org/en/)
* **Bloggar** - Se till att ha reklamfria enkla bloggar där vänsterfolk kan blogga. Exempel: [WriteFreely](https://writefreely.org/), [Wordpress](https://wordpress.org/)
* **Forum** - Ibland krävs det klassiska forum. Exempel: [Discourse](https://en.wikipedia.org/wiki/Discourse_(software)).
* **E-postlistor** - Som [Robin Zachari](http://flamman.se/a/sluta-mema-borja-mejla) på Skiftet skrivit så är e-post ett utmärkt sätt.
* **Matrix-chatt** - Ett alternativ till Messenger eller WhatsApp. Bra krypterat. [Läs mer här](https://matrix.org/) eller på [Wikipedia](https://en.wikipedia.org/wiki/Matrix_(protocol)).
* **XMPP** - Ett alternativ till Messenger eller WhatsApp. Tidigare känt som Jabber. [Läs mer här](https://xmpp.org/) eller på [Wikipedia](https://en.wikipedia.org/wiki/XMPP).
* **Fillagring** - Viktigt att kunna lagra filer online. Alternativ till Google Drive. Exempel: [CryptPad](https://cryptpad.org/about/),[NextCloud](https://nextcloud.com/)
* **Enkäter, kalkylark, ordbehandling** - Idag använder många Google Drive och dess verktyg för att skriva, skapa kalkylark, göra formulär, osv. Exempel: [CryptPad](https://cryptpad.org/about/),[NextCloud](https://nextcloud.com/)

Det som begränsar är hur mycket pengar föreningen kan få in och hur många verktyg vi mäktar med att drifta. 

## Principer

För att vi verkligen ska ta kontrollen över vågen egen kommunikation, data och budskap så är det viktigt att ha några principer:

* **För vänstern** - Det är alltså inte verktyg som ska göras tillgängliga för alla. Vi har inget intresse att bygga upp en infrastruktur som nazister och blåbruna nyttjar.
* **Medlemsdemokrati** - Det är medlemmarna som bestämmer vad som ska gälla.
* **Open source / fri programvara** - Det är viktigt att så mycket som möjligt är fri programvara så att vi inte blir inlåsta. 
* **Kunna drifta långsiktigt** - Hellre att vi driftar några få tjänster men stabilt än många tjänster som blir svajiga.

## Första möte

Som tur är finns det bra dokumentation tillgänglig på nätet om hur man startar föreningar. Exempelvis [foreningsinfo.se](https://www.foreningsinfo.se/wiki/4-att-starta-forening/), [forening.se](https://forening.se/), [verksamt.se](https://www.verksamt.se/starta/valj-foretagsform/ideell-forening) och [Skatteverket](https://www.skatteverket.se/foreningar/driva/ideellforening/startaenideellforening.4.70ac421612e2a997f85800028338.html).

Allra mest sannolikt är att vi startar en ideel förening. [Det beskrivs bra här hur man gör](https://www.foreningsinfo.se/starta-ideell-forening/). Stegen är:

* Minst tre personer skriver ett utkast på bland annat syfte, namn och stadgar.
* Ett konstituerande möte hålls där beslut om bildande klubbas och stadgar antas. En styrelse väljs och i samband med detta brukar det första styrelsemötet hållas. 
* På styrelsemötet läggs en verksamhetsplan och budget fram inför nästkommande år som sedan fastslås på årsmötet.

Är du sugen på att vara med och se en förening bildas? Då är det här mötet för dig ;)

## Vilka kan hjälpa till?

Det finns mycket man skulle kunna hjälpa till med i föreningens verksamhet. Allt är ideellt. Man kan hjälpa till vare sig man är teknisk kunnig eller inte. Man kanske bara är föreningsnörd. Följande saker kommer behövas:

* **Vara betalande medlem** - Grunden för en förening är dess medlemmar. Ju fler medlemmar vi har, desto starkare är föreningen, speciellt ekonomiskt. 
* **Starta föreningen** - Det kommer krävas lite klurande i början för att få igång förening. Fixa med stagar, årsmöte. banken, osv.
* **Sitta i styrelsen** - Det kommer att behövas personer som sitter i styrelsen som träffas regelbundet.
* **Andra föreningsuppdrag** - Det kommer behövas revisorer, valberedning, osv också men som nog inte är så betungande.
* **Hjälpa till som moderator på mastodoninstansen Spejset**
* **Hjälpa till som administratör** - Det kommer att behöva administratörer som kan drifta Linuxservrar och installera programvara.
* **Utvecklare** - Beroende på vad vi gör med föreningen kan utvecklare också behövas. Primärt webbutvecklare.
* **Vara aktiv i samtalet på mastodoninstansen Spejset** - Det är ju ingen idé att ha en stor infrastruktur om ingen använder den. Att vara aktiv i de kanaler vi skapar är viktigt.

Är du intresserad att vara med på något sätt eller fortsätta få information? [Fyll då i det här formuläret](https://cryptpad.fr/form/#/2/form/view/kjfmevXFxJu85KXmHrhXKUNc2NraP2-tNFZhI8uqbPc/). Är du intresserad mer? Maila mig då på samuel@bitar.se.

