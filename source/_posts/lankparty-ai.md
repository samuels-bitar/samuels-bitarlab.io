---
title: Länkparty - Anonyma betalningar, möjligheter och begräsningar med AI/LLM, mindre datainsamling för Googles tjänster pga DMA
image: network-earth.jpg
description: Ett gäng intressanta länkar om teknik jag samlat på mig senaste tiden
date: 2024-01-23 09:00
---

Länkparty:

* [If you're in the EU, you can now decide how much data to share with Google](https://www.techradar.com/computing/cyber-security/if-youre-in-the-eu-you-can-now-decide-how-much-data-to-share-with-google)
* [New EU project NGI TALER will bring private and secure online payments to the Eurozone](https://taler.net/en/news/2024-02.html)
* [MobileCoin - Frequently Asked Questions](https://mobilecoin.com/learn/faq/)
* [Code Written with AI Assistants Is Less Secure](https://www.schneier.com/blog/archives/2024/01/code-written-with-ai-assistants-is-less-secure.html)
* [Hackers Break into AI Hiring Chatbot, Could Hire and Reject Fast Food Applicants](https://www.404media.co/hackers-break-into-hiring-ai-chat-bot-chattr/)
* [Open Source LLMs with Simon Willison](https://oxide.computer/podcasts/oxide-and-friends/1692510)

Den här gången blev det lite övervakning, anonyma digitala betalningar och lite AI-snack.

## Skippa dela data med Google

Om man bor i EU kan man nu bestämma mer över den datan som Google samlar in. Det är inget Google kommit på själv utan är ett resultat över att de rättar sig över EU-reglerna från [Digital Markets Act](https://en.wikipedia.org/wiki/Digital_Markets_Act) som började gälla i maj 2023. Det är en EU-reglering som inriktar sig på Big Techs stora plattformar (så kallade "gatekeepers") och ställer krav om t.ex. hur de får (eller inte får) kombinera data från olika tjänster (som Metas fall med Facebook och WhatsApp).

Från artikel:

 > You can exactly decide the amount of data you're comfortable to share with the big tech firm. As Google explains, in fact, "you can choose to keep all these services linked, choose to have none of these services linked, or choose which of these individual services you want to keep linked."
>
> The provider ensures that accepting to keep different Google services linked will not translate into sharing your data with third-party services. Yet, deciding to unlink them will nonetheless be beneficial in terms of privacy as these services will be prevented from sharing your personal information between each other. This will ultimately make online tracking a little bit more difficult as your Google activities won't be connected.

Läs mer: [If you're in the EU, you can now decide how much data to share with Google](https://www.techradar.com/computing/cyber-security/if-youre-in-the-eu-you-can-now-decide-how-much-data-to-share-with-google).

## GNU Taler används i EU-projektet NGI TALER

Vi har alltid kunnat betala anonymt i den fysiska världen med kontanter. Men hur gör vi för att betala anonymt på nätet? Kryptovalutor som [BitCoin](https://en.wikipedia.org/wiki/Bitcoin) som baserar sig på en [blockkedja](https://en.wikipedia.org/wiki/Blockchain) har länge varit den enda lösningen.

Men [GNU Taler](https://taler.net/en/index.html) löser anonyma betalningar fast utan en blockkedja. GNU Taler är fri programvara (alltså öppen källkod) är ett system för mikrobetalningar och elektroniska betalningar. GNU Taler använder inte blockkedja. De använder sig av så kallade ["blinda signaturer"](https://en.wikipedia.org/wiki/Blind_signature) för att skydda integriteten hos användarna. Det som är speciellt med GNU Taler är att det bara är den som betalar för något som är anonym, inte den som säljer. På så sätt lirar det här systemet bättre med nationalstaternas intresse av att kunna kontrollera och beskatta försäljningar.

Eftersom GNU Taler inte baseras på blockkedja som förlitar sig på [Proof of work](https://en.wikipedia.org/wiki/Proof_of_work) så drar den inte heller speciellt mycket energi.

Läs mer: [New EU project NGI TALER will bring private and secure online payments to the Eurozone](https://taler.net/en/news/2024-02.html).

## MobileCoin - Signals kryptovaluta

Det finns många [kryptovalutor](https://en.wikipedia.org/wiki/Cryptocurrency) som BitCoin, Ethereum, Tether, LiteCoin, Solana, Dogecoin och många fler.

Det finns mycket man kan säga om kryptovalutor, både ur ett tekniskt och samhälleligt perspektiv. Det får jag göra i ett annat blogginlägg.

Men det intressanta är att den säkra och krypterade chattappen [Signal](https://www.signal.org/) som många integritetsivrare förespråkar har utvecklat kryptovalutan [MobileCoin](https://mobilecoin.com/).

De säljer själva in MobileCoin att vara den första kryptovalutan som är designad för mobiltelefoner (för att kunna t.ex. snabbt kunna betala en kaffe) och samtidigt värna användarens integritet och har låg energiförbrukning.

Det som är intressant i sammanhanget är att det är integrerat i Signal-appen. Man behöver dock aktivera betalningar och skapa en digital plånbok för att komma igång.

Sannolikt så är för- och nackdelarna till stor del samma med den här kryptovalutan som med andra. Men jag tyckte det var intressant att Signal ändå satsade på detta. Är det ett sätt för att finansiera Signal-appen? Jag vet själv inte riktigt vad jag tycker i frågan. Jag tycker det är viktigt att kunna betala anonymt även på nätet. Men känner mig generellt skeptisk mot kryptovalutor. Jag tror jag föredrar upplägget med GNU Taler bättre. Men jag tyckte det var intressant.

Läs mer: [MobileCoin - Frequently Asked Questions](https://mobilecoin.com/learn/faq/)

## Mer kodbuggar med AI

AI slår igenom stort senaste åren. Eller stora språkmodeller som det egentligen handlar om. Själv tycker jag det är både spännande och läskigt. Det finns mycket man kan göra med AI men också mycket man inte kan göra eller det blir fel i alla fall.

AI-assisterat kodskrivande har funnits ett tag. [Github copilot](https://en.wikipedia.org/wiki/GitHub_Copilot) är kanske det mest välkända verktyget. Det är integrerat med kodutvecklingsmiljöer (IDE:er) som Visual Studio Code, Visual Studio, Neovim och JetBrains.

Nu har forskning kommit ut som visar att utvecklare som skrivit med AI-assistans skrivit mindre säker kod. Från abstracten:

> We conduct the first large-scale user study examining how users interact with an AI Code assistant to solve a variety of security related tasks across different programming languages. Overall, we find that participants who had access to an AI assistant based on OpenAI’s codex-davinci-002 model wrote significantly less secure code than those without access.

Läs mer: [Code Written with AI Assistants Is Less Secure](https://www.schneier.com/blog/archives/2024/01/code-written-with-ai-assistants-is-less-secure.html).


## Lura en AI

Det finns massvis med exempel på hur man kan lura en AI. Och det är inte så konstigt. AI:s är bara språkmodeller som är bra på att gissa vilken text som passar bra i följd med en annan text, grovt förenklat. För att AI:n inte ska uttrycka sig rasistiskt, hatiskt eller dumt på annat sätt så försöker man sätta upp säkerhetsräcken runt omkring. Men det går att komma runt genom så kallad "prompt injecttion". Ars Technica skrev hur [Microsofts AI Bing lurats](https://arstechnica.com/information-technology/2023/02/ai-powered-bing-chat-spills-its-secrets-via-prompt-injection-attack/).

Det kan få stora konsekvenser om man låter AI:n fatta en massa beslut åt en, speciellt beslut som har att göra med anställningar. 

Läs mer: [Hackers Break into AI Hiring Chatbot, Could Hire and Reject Fast Food Applicants](https://www.404media.co/hackers-break-into-hiring-ai-chat-bot-chattr/)


## Open source-AI och hur man lär sig AI:ns begränsningar

Jag lyssnade på ett väldigt intressant poddavsnitt från podden [Oxide and Friends](https://oxide.computer/podcasts/oxide-and-friends) om AI eller stora språkmodeller, Large Language Models (LLM), som de heter. Det här var en väldigt bra genomgång om vad AI:s kan göra och vad de inte kan göra. För AI är riktigt bra på vissa saker och superkassa på andra.

Simon Willison som intervjuas menar att det är lätt att hamna i ett av två diken. Det ena diket hamnar man i om man ställer en fråga till en AI som svarar fel och då tänker "det här är bara skit, kan inte hjälpa mig alls". Det andra diket hamnar man i när man ställer frågor och får väldigt bra svar och tror att AI:n är allvetande och kan lösa alla dina problem. Som oftast så är sanningen någonstans mitt emellan. En AI kan som sagt blåljuga en upp i ansiktet eller så att säga hallucinera sig fram till ett svar. Men en AI kan vara ett väldigt effektivt hjälpmedel. Det hela handlar om att lära sig om dess begränsning men även att alltid förhålla sig kritisk till vad den säger.

Läs/lyssna mer: [Open Source LLMs with Simon Willison](https://oxide.computer/podcasts/oxide-and-friends/1692510)