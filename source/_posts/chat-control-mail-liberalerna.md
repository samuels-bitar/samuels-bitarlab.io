---
title: Mail från Liberalerna om Chat Control
image: spying-eye.jpg
image_description: Bild av <a href="https://pixabay.com/users/tumisu-148124/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5279884">Tumisu</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5279884">Pixabay</a>
description: Mailväxling med Liberalerna om Chat Control
date: 2024-12-12 15:00
---

I augusti och september försökte jag få svar på [en enkät](https://konstellationen.org/2024/09/12/chat-control-enkat/) från alla riksdagspartierna gällande hur man ställde sig till Chat Control. Vi i Kamratdataföreningen Konstellationen redovisade svaren och vad som hänt i [rapporten Kluvna tungor](https://konstellationen.org/2024/09/19/chatcontrol-rapport/). Vi skrev också en [debattartikel](https://www.etc.se/debatt/varfoer-vill-m-l-kd-och-s-inte-prata-om-chat-control).

Inför att frågan skulle upp i Justitieutskottet igen så skickade jag ett mail till Liberalerna där jag uppmanade dem att rösta nej till att stödja Ungerns förslag. De gjorde inte som jag föreslog men svarade mig efter omröstningen.

Jag lägger mailväxlingen här för dokumentation. Först mitt mail, sedan deras svar och sist mitt andra svar. (Jag fick aldrig något mer svar efter det.)

Ännu vet vi inte hur S, M, L och KD svarar på frågorna vi ställde i vår enkät. Vi vet dock hur de röstade. Alla de som [röstade för Ungerns Chat Control-förslag](https://omni.se/strommer-regeringen-staller-sig-bakom-chat-control/a/5EjdeW) (S, M, L och KD) var också de som aldrig svarade på vår enkät.

## Mitt mail 2024-09-25

Hej Liberalerna,

Det är dags ännu en gång att rösta mot massövervakning.

Jag har läst att Ungerns Chat Control-förslag kommer upp torsdag 26 september i Justitieutkottet:

https://www.riksdagen.se/sv/aktuellt/kalendersida/kalenderhandelse/utskottens-kallelser-och-foredragningslistor/2024/sep/26/justitieutskottets-sammantrade_rdhca3juu3/

Ungerns förslag skiljer sig inte nämnvärt från Belgiens förslag som var uppe i utskottet i juni.

Men utifrån hur kritiska Liberalerna har varit i frågan vore det rimligt att istället kämpa för att utskottet tar ställning för EU-parlamentets kompromissförslag som värnar den personliga integriteten.

I SVT:s valkompass inför Europaparlamentsvalet svarade ni i Liberalerna att förslaget om att skanna privat kommunikation var "mycket dåligt":

https://valkompass.svt.se/eu-2024/fraga/all-privat-digital-kommunikation-ska-kunna-overvakas-i-brottsforebyggande-syfte/

Karin Karlsbro (L) skrev också ett väldigt kritiskt facebook-inlägg med rubriken NEJ TILL CHAT CONTROL:

https://www.facebook.com/karlsbroliberal/posts/pfbid02CKHivHcrBkwnWJCNTyJLWvGTS1GSLnjTU1rNetUoGqnH4BWxSYTcmHj6fNdS5exel


Kritiken mot Chat Control har varit öronbedövande.

Ministerrådets legal service menar att intrånget i den interpersonella kommunikationen utgör en särskilt allvarlig begränsning för rätten till integritet och skydd av personuppgifter enligt artiklarna 7 och 8 i EU-stadgan om de grundläggande rättigheterna (respekt för privatlivet och familjelivet samt skydd av personuppgifter).

https://aeur.eu/f/6ql

EU-parlamentets konsekvensutredning menar att förslaget effekter förväntas vara begränsade eftersom förövare flyttar över kommunikation till dark net / deep web. De uttrycker också oro över att skannandet av totalsträckskrypterad kommunikation (end-to-end-encryption - E2EE) kommer att göra tjänster som använder det (och deras användare) mer sårbara. De lyfter också att förslaget skulle bryta mot artiklarna 7 och 8 i EU:s stadga om de grundläggande rättigheterna. Att bryta mot förbudet mot allmän lagring av uppgifter och förbudet mot allmänna övervakningsskyldigheter kan inte motiveras menar dem. Men allt är inte dåligt i förslaget menar de och tar upp inrättandet av ett EU-center och att det positivt skulle påverka bekämpandet av CSAM.

https://www.europarl.europa.eu/RegData/etudes/STUD/2023/740248/EPRS_STU(2023)740248_EN.pdf

EU:s dataskyddsmyndighet anser att förslaget väcker allvarliga dataskydds- och integritetsproblem. De menar att förslaget behöver ändras så att det inte leder till att krypteringen försvagas eller försämras på en generell nivå.

https://www.edps.europa.eu/system/files/2022-07/22-07-28_edpb-edps-joint-opinion-csam_en.pdf

134 civilsamhälletsorganisationer skrev under ett öppet brev där de uppmanar EU-kommissionen att dra tillbaka förslaget. Undertecknarna kommer från ett brett spektrum av grupper som arbetar med mänskliga rättigheter – inklusive vuxnas och ungas digitala rättigheter, skydd av journalister och mediefrihet, advokater, visselblåsare, könsrättvisa, demokrati och fred, arbetare, barns hälsa med mera. De menar att EU-kommissionens förslag sannolikt skulle göra mycket mer skada än nytta. De menar att i den viktiga kampen mot sexuella övergrepp och utnyttjande av barn stödjer de åtgärder som är riktade, effektiva och proportionerliga. De skriver att många av dem har tidigare talat ut om hur man säkerställer att åtgärder för att hålla barn säkra på nätet görs i enlighet med befintliga mänskliga rättigheter och rättsstatsprincipen och har direkt erfarenhet av hur sådana regler och principer är väsentliga för att upprätthålla demokrati, tillgång till rättvisa och oskuldspresumtion.

https://edri.org/our-work/european-commission-must-uphold-privacy-security-and-free-expression-by-withdrawing-new-law/


Även i Sverige har kritiken varit hård. I skrivande stund har 39 svenska ledarsidor skrivit kritiskt om Chat Control, inklusive dagstidningar som DN, SvD, Expressen, Göteborgs-Posten och Dagens ETC, lokala tidningar som Corren, Arvika Nyheter och Eskilstunakurien samt profiltidningar som Syre, Dagen och Sändaren.

https://chatcontrol.se

Journalistförbundet varnade för intrånget i den personliga integriteten men också hur källskyddet riskeras.

https://www.dn.se/debatt/eus-nya-massovervakning-far-inte-forstora-kallskyddet/

Internetstiftelsen menar att det blir ett alltför stort ingripande i grundläggande fri- och rättigheter, i synnerhet rätten till skydd av privatlivet och därför inte är proportionerligt. De varnar också för att bakdörrar behöver installeras i totalsträckskrypterade tjänster vilket kan missbrukas.

https://www.linkedin.com/posts/carlpiva_ang%C3%A5ende-f%C3%B6rslaget-till-europaparlamentets-activity-7056978434094153730-R5TG

Det finns mycket mer att läsa om kritiken i denna rapport: https://konstellationen.org/files/kluvna-tungor-rapport.pdf


Journalisten Emanuel Karlsten har granskat förslaget från Ungern:

"Det nya förslaget är snarlikt det som Belgien la fram till övriga EU-länder innan sommaren. Till skillnad från EU-kommissionären Ylva Johanssons förslag så ska inte längre all kommunikation övervakas, utan ”bara” bilder och video. Krypteringen påstås heller inte brytas, utan istället ska bilderna och videon scannas innan det krypteras. Vilket i praktiken innebär att krypteringen är bruten. Detta trots att förslaget innehåller långa utläggningar om hur viktigt det är inte kompromissa med just helsträckskryptering.

Precis som tidigare finns också en formulering om att användaren ska godkänna att deras material scannas, men godkänner de inte får medborgare i EU inte längre använda bild eller video-tjänster på internet."

https://emanuelkarlsten.se/nya-forslaget-om-chat-control-lackt-sverige-maste-ge-besked-inom-12-dagar/


Samma sak konstaterar DN:s ledarsida:

https://www.dn.se/ledare/totalitar-overvakning-i-eu-om-regeringen-far-valja/

Och av The Brussels Times:

https://www.brusselstimes.com/1209673/controversial-chat-control-back-on-eu-agenda-this-week

Ungerns förslag delar fortfarande de stora problemen med EU-kommissionens ursprungliga förslag nämligen att det är massövervakning av alla användares kommunikation på en tjänst som åläggs att skanna material. Liksom Belgiens förslag innehåller Ungerns förslag fortfarande problemet att tjänster/appar med totalsträckskrypterad kommunikation kommer tvingas att installera bakdörrar för att skanna (så kallad klientsideskanning). Men i förslaget menar Ungern att man inte alls kommer kringgå totalsträckskryptering/helsträckskryptering utan att man kommer att skanna efter material genom så kallad uppladdningsmoderering. Men det som beskrivs är exakt klientsideskanning. De har bara kallat det något annat. Hela poängen med totalsträckskryptering/helsträckskryptering försvinner när meddelandet kan läsas av tredje part vilket blir fallet med Chat Control.

Kliensideskanning är exakt vad FN:s människorättskommissionär varnar för i en rapport:

"Encryption is a key enabler of privacy and human rights in the digital space, yet it is being undermined. The report calls on States to avoid taking steps that could weaken encryption, including mandating so-called backdoors that give access to people’s encrypted data or employing systematic screening of people’s devices, known as client-side scanning."

https://www.ohchr.org/en/press-releases/2022/09/spyware-and-surveillance-threats-privacy-and-human-rights-growing-un-report


I EU-parlamentets kompromissförslag däremot så tas de största bristerna bort från EU-kommissionens förslag. Det är bra!

I november 2023 gjordes ett genombrott när EU-parlamentet samlat en majoritet bakom en gemensam position som avvisade stora delar av kommissionens förslag.

Alla partigrupper stod bakom förslaget. I EU-parlamentets förslag hade de tagit bort de omtvistade och problematiska delarna, såsom masskanning av hela tjänster, även för totalsträckskrypterade tjänster. Istället lade de till mer effektiva, domstolssäkra och rättighetsrespekterande åtgärder till det ursprungliga förslaget för att hålla barn säkra på nätet. Med EU-parlamentets förslag kommer det alltså inte ske någon masskanning eller allmän övervakning och inte heller någon urskillningslös skanning av privat kommunikation eller införande av bakdörrar för att försvaga kryptering. Istället får bara övervakning ske av enskilda personer och grupper som misstänks vara kopplade till material med sexuella övergrepp mot barn.

https://www.europaportalen.se/2023/10/genombrott-i-kontroversiellt-forslag-om-kampen-mot-barnporr-pa-natet

https://www.europarl.europa.eu/news/en/press-room/20231110IPR10118/child-sexual-abuse-online-effective-measures-no-mass-surveillance

https://www.dn.se/varlden/eu-parlamentet-tar-strid-mot-den-kontroversiella-chat-control-lagen/


EU-parlamentets förslag tar itu med de problem som kritikerna lyft. Varken Belgiens eller Ungerns förslag gör det. Grundproblemen är kvar.

Det finns andra länder som tagit ställning mot Ungerns förslag. Nu senast Luxemburg. Men även Tyskland, Österrike, Polen, Nederländerna och Tjeckien har varit kritiska:

https://delano.lu/article/opposes-generalised-surveillan


Jag uppmanar er därför att:

* Rösta nej till Ungerns förslag
* Föreslå utskottet att ta ställning för EU-parlamentets förslag


Läs gärna mer om kritiken mot Chat Control från civilsamhälle, säkerhetsexperter och forskare i den här rapporten:

https://konstellationen.org/2024/09/19/chatcontrol-rapport/

Vänliga hälsningar,
Samuel Skånberg

## Liberalernas svar 2024-09-30

Hej Samuel,


tack för ditt mail.

Bakgrunden är, som du säkert vet, ett omfattande arbete med att stoppa sexuella övergrepp mot barn och pedofiler på nätet. För oss liberaler är det viktigt att det arbetet intensifieras samtidigt som den personliga integriteten skyddas.

Det finns ännu inget förslag att ta ställning till. Det är bara lösa utkast i en lång process mellan EU:s olika institutioner. Riksdagens beslut i Justitieutskottet är en del av denna process där det sades att förhandla vidare så att förslaget uppfyller de krav på integritet vi ställer. 

EU-parlamentet har bara en gång kunnat ta ställning till ett färdigförhandlat förslag kallat Chat Control 1. Liberalerna röstade då emot förslaget, precis som du skriver. Låt oss återkomma när det finns ett färdigt förslag att rösta om i EU. Karin Karlsbro är mycker engagerad i denna fråga.

Mvh

Lars Granath
Utredare
---------------------------------------------------
Liberalernas parti- och riksdagskansli
Riksdagen, 100 12 Stockholm
www.liberalerna.se

## Mitt svar 2024-12-30

Hej Lars,

Tack för återkopplingen! Jag delar inte din bild gällande vad partier i riksdagen kan göra. Det spelar roll vad Sverige har för ställning i ministerrådet. Där kan Justitieutskottet och EU-nämnden rimligtvis bidra till ett förslag som ligger mer i linje med EU-parlamentets förslag. De förslag som kommit från Spanien, Belgien och Ungern har varit väldigt oroväckande för oss som är engagerade i digitala fri- och rättigheter.

Detta är också en oro som ert ungdomsförbund har:

https://www.luf.se/pressmeddelande-fran-luf-angaende-chat-control-2-0/

Men nu när jag ändå har fått kontakt med någon från Liberalerna så skulle jag väldigt gärna få svar från de frågor som jag mailade å Kamratdataföreningen Konstellationens vägnar senaste månaden. Vi mailade flera olika mailadresser men fick aldrig något svar.

Vi presenterade svaren tillsammans med en genomgång av kritiken av Chat Control i vår rapport Kluvna tungor:

https://konstellationen.org/2024/09/19/chatcontrol-rapport/


Även om deadline passerat för enkäten så är vi fortfarande väldigt intresserade av vad ni tycker om frågor kopplade till övervakning och personlig. Om ni kan svara "ja" eller "nej" på följande frågor vore vi tacksamma. Ni kan även svara "vet inte / ingen åsikt / kan inte svara". Om ni inte svarar något så kommer det att räknas som "vet inte / ingen åsikt / kan inte svara".

Om vi skriver mer om frågan är det värdefullt att veta vad de olika partierna skrivit i frågan.

Vi uppskattar om ni kan svara på frågorna nedan innan den 8 oktober. Vi vill veta vad partierna tycker inför att ministerrådet diskuterar frågan.


---

Fråga 1

Bakgrund:

I maj 2022 lade EU-kommissionär Ylva Johansson fram förslaget som i folkmun kallas Chat Control 2.0. Förslaget är tänkt att förebygga och bekämpa sexuella övergrepp mot barn och sådant material (CSAM-material). Förslaget har fått kritik för att förslaget är framtaget på felaktiga antaganden kring hur meddelande­tjänster fungerar rent tekniskt och att det underminerar möjlighet till säker kommunikation.

---

Fråga: Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?

Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Fråga 2

Bakgrund:

I november 2023 röstade EU-parlamentet för ett rejält reviderat förslag. Förslaget innebär bland annat att det måste finnas en misstanke om brott och totalsträckskrypterade chattar skulle undantas från skanning. Det mest kritiserade delarna av Chat Control 2.0 togs alltså bort, som masskanning av hela tjänster.

Alla partigrupper i EU-parlamentet röstade för förslaget. Även alla svenska ledamöter röstade för förslaget.

Fråga: Står ni bakom EU-parlamentets kompromissförslag?

Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Fråga 3

Bakgrund:

En stor diskussion om Chat Control har handlat om att användares all kommunikation ska övervakas hos en tjänst om tjänsten får en spårningsorder mot sig, även om det inte finns brottsmisstanke mot den enskilda användaren. Effekten skulle bli att om t.ex. Facebook får en spårningsorder riktad mot sig ska alla användares kommunikation skannas, även privata chattmeddelanden mellan två personer som inte är misstänkta för brott.

Fråga: Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?

Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Fråga 4

Bakgrund:

Många kritiker har lyft problemet att skanningen även skulle omfatta totalsträckskrypterad kommunikation, alltså kommunikation där bara de som kommunicerar kan se kommunikationen i klartext. (Inte ens servrarna som används kan läsa innehållet i totalsträckskrypterad kommunikation.) Exempel på appar som använder totaltsträckskryptering är Signal, WhatsApp eller iMessage. För att man ska kunna skanna sådan kommunikation måste krypteringen kringås, t.ex. genom att apptillverkarna tvingas att installera bakdörrar i apparna så att kommunikationen skannas innan den krypteras och skickas iväg (så kallad klientside-skanning).

Fråga: Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?

Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Fråga 5

Bakgrund:

När Belgien hade ordförandeskapet i EU tog de fram ett kompromissförslag av Chat Control. En del av förslaget var att totalsträckskrypterade appar skulle skannas innan kryptering skedde, så kallad klientside-skanning. Användare skulle informeras om detta men om de sade nej så skulle appen vara stort obrukbar för en stor del av kommunikation, som delning av bilder, videor och länkar. Kritiker till Chat Control menade att Belgiens förslag i stort var samma massövervakningsförslag som EU-kommissionens ursprungsförslag.

Frågan var uppe på Justitieutskottets möte i riksdagen 18 juni. Det handlade om huruvida Sverige skulle stå bakom Belgiens kompromissförslag.

C och SD anmälde avvikande mening på mötet men efter mötet uttryckte V och MP att de också skulle ha gjort det. Inför EU-valet kampanjade flera partier för att stoppa Chat Control. Det blev alltså otydligt vad egentligen alla partier stod i frågan.

Fråga: Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle ni säga nej till förslaget?


Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Fråga 6

Bakgrund:

Många delar kring Chat Control är tekniska. Men i slutändan handlar det om synen på privat kommunikation och personlig integritet.

Fråga: Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?

Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Fråga 7

Bakgrund:

Många kritiker har uttryckt oro över att Chat Control skulle innebära inrättande av en övervakningsapparat som sedan skulle kunna användas för att skanna efter mer innehåll än CSAM-material. Begränsningen att söka efter CSAM-material är bara juridisk och politisk, inte teknisk. Man är orolig för ett sluttande plan där man först skannar efter CSAM-material för att senare utöka till att söka efter mer brottsligt material.

Fråga: Anser ni att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Ert svar:

( ) Ja
( ) Nej
( ) Vet inte / ingen åsikt / kan inte svara

---

Övriga kommentarer

Har ni några kommentarer kring era svar eller andra övriga saker ni vill säga kan ni skriva här:



Vänliga hälsningar,
Samuel Skånberg