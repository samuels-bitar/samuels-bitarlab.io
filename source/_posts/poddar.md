---
title: Poddtips om klimat, facklig kamp, tradwives, Rojava, Israel och Palestina
image: podcast-unsplash.jpg
description: Några tips från mig om poddar jag lyssnat på senaste tiden
date: 2023-11-17 14:00
---

*Bild av [C D-X](https://unsplash.com/@cdx2?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) på Unsplash.*

Jag tänkte försöka mig på att skriva ett kort blogginlägg som faktiskt blir av att jag skriver en långa som aldrig blir av. Så nu kör vi! Här kommer tips på några poddavsnitt jag lyssnat på senaste tiden. Allt är hämtat från [Mediakollen](https://mediakollen.org).

## Scocconomics

Scocconomics beskriv så här på [sin sida](https://scocconomics.captivate.fm/):

"Sandro Scocco och Jenny Lindahl pratar om samhällsekonomi. För dig som vill ha koll på debatten, nationalekonomin och politiken."

Podden finns så klart på Mediakollen och man kan se våra [utvalda avsnitt här](https://mediakollen.org/feed/113/curated) eller [alla avsnitt här](https://mediakollen.org/feed/113/all).

Jag vill lyfta fram två avsnitt:

* [Århundradets cagefight. Forbes topp-100 vs Svenska kollektivavtal.](https://scocconomics.captivate.fm/episode/rhundradets-cagefight-forbes-topp-100-vs-svenska-kollektivavtal). Här går de igenom kollektivavtalsfajten som IF Metall hat tagit med Tesla.
* [Regeringens klimatpolitik - förvärring av utsläppen men även dyrare drivmedel 2027](https://scocconomics.captivate.fm/episode/regeringens-klimatpolitik-forvarring-av-utslappen-men-aven-dyrare-drivmedel-2027). Här går de igenom regeringens kassa klimatpolitik. De går igenom biobränslen, skattesänkningar på drivmedel, om utsläppshandel inom EU, planerade slopade transportmål och mycket annat.

## Radio åt Alla

Förbundet Allt åt Alla driver en podd som de kallar Allt åt Alla. Det är väl snarare flera olika poddar men av folk som är engagerade i Allt åt Alla. Man kan se Mediakollens [utvalda avsnitt här](https://mediakollen.org/feed/28/curated) eller [alla avsnitt här](https://mediakollen.org/feed/28/all).

Här är några intressanta och/eller roliga avsnitt:

* [Eld och rörelse #123: Rasmus Canbäck är tillbaka!](https://radio.alltatalla.se/podcast/eld-och-rorelse-123/). Konfliktjournalisten Rasmus Canbäck har kommit tillbaka från Nagorno-Karabakh och intervjuas om konflikten mellan Armenien och Azerbajdzjan och hur den relaterar till konflikten i Israel och Palestina, med mera.
* [Rekreation / Avvikelse #3 / Destituerande makt](https://radio.alltatalla.se/podcast/rekreation-avvikelse-3/). I detta avsnitt talar de om Colectivo Situaciones och deras sätt att förstå politiska rörelser som inte kräver representation. Påminner mycket om deltagande undersökningar. Något som även podden Apans anatomi har tagit upp.
* [Vad händer, GBG? #28: ”Du tror väl inte det här är Dubai, eller?”](https://radio.alltatalla.se/podcast/vad-hander-gbg-28-du-tror-val-inte-det-har-ar-dubai-eller/). Detta är den bästa podden som tar upp kommunpolitik. Det handlar om Göteborg så jag kan varmt rekommendera den för alla göteborgare. 
* [Eld och rörelse #121: Henrik Bromander om Ljuset i Rojava, samt kort om Gaza](https://radio.alltatalla.se/podcast/eld-och-rorelse-121-henrik-bromander-om-ljuset-i-rojava-samt-kort-om-gaza/). Börjar med en uppdatering om situationen i Gaza och pratar framförallt om vad som kan legat bakom explosionen på al-Ahli al-Arab-sjukhuset. Huvuddelen av avsnittet är en samtal mellan Martin och Henrik Bromander, om den nya boken "Ljuset i Rojava: min tid som frivillig i YPG" som Henrik har skrivit tillsammans med den Rojava-frivillige Anton Nilsson.


## Studio Expo

Studio Expo är Stiftelsen Expos podd. Det som är najs med denna podden är att flera avsnitt är 20-30 minuter så passar bra på morgonpromenaden.

* [77. Tradwives & nazifluencers](https://sites.libsyn.com/412148/77-tradwives-nazifluencers). Den här podden är på engelska pga inbjuden gäst. "Women have always played an important role in the far right, despite it being a political environment filled with misogyny. As the mother of white children, the woman has had an elevated role within white supremacist groups - and even if that position remains, the role has also changed with the breakthrough of social media. Today, women have become influencers for the political cause and are used to attract new sympathizers with content about traditional lifestyles and conservative values."
* [79. Antisemitismen 85 år efter novemberpogromen](https://sites.libsyn.com/412148/79-antisemitismen-85-r-efter-novemberpogromen). Om novemberpogromerna 1938 mot judarna i Tyskland, det som kallas Kristallnatten. Om hur antisemitismen fortfarande är aktiv i världen och Sverige. De går också igenom om hur kritik mot staten Israel inte är anitsemitism men att det är viktigt att veta hur kritik mot Israel och mot sionism ofta kan utnyttjas av antisemitiska krafter.
