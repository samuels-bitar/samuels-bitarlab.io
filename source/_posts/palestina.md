---
title: Solidaritet med Palestina
image: palestina-demo.jpg
description: Bomber faller över Gaza, många tusen civila (flera tusen barn) dödas och högern tycker det är ett proportionerligt svar
date: 2023-12-15 16:00
---

Jag har tänkt länge jag ska skriva den här texten. Nu när jag sitter på ett tåg så kanske jag får till det.

Det är otroligt mycket källor och knepigt också med källor. Jag länkar så får ni själva bedöma källorna.

Efter Hamas brutala attack mot civila den [7 oktober där mer än 1200 dödades](https://www.svt.se/nyheter/inrikes/detta-har-hant-hamas-storskaliga-attack-mot-israel) så påbörjade Israel en stor militär insats mot Gaza som huvudsakligen drabbade (och drabbar) civila. Eftersom attackerna fortsätter så är det svårare att ge aktuell bild.

**Eld upphör nu!**

Israels attacker mot Gaza måste upphöra! De brutala bombningarna är med stor sannolikhet [brott mot folkrätten](https://www.svt.se/nyheter/utrikes/professorn-i-folkratt-pal-wrange-mycket-sannolikt-att-israel-bryter-mot-folkratten--5amv4d) och kan [även vara ett folkmord](https://www.ohchr.org/en/press-releases/2023/11/gaza-running-out-time-un-experts-warn-demanding-ceasefire-prevent-genocide).

## Några hårda och otäcka siffror

Men här är några länkar:

* Minst 18 787 dödade per den 15 december i Gaza, minst 289 dödade på Västbanken och runt 1200 dödade i Israel [enligt Al Jazeera](https://www.aljazeera.com/news/longform/2023/10/9/israel-hamas-war-in-maps-and-charts-live-tracker)
* 18 787 dödade per den 14 december i Gaza enligt [OCHAOPT](https://www.ochaopt.org/content/hostilities-gaza-strip-and-israel-reported-impact-day-69) (United Nations Office for the Coordination of Humanitarian Affairs - occupied Palestinian territory). OCHAOPT skriver också att det är runt 1,9 miljoner internflyktingar i Gaza.
* 240 israeler tagna som gisslan av Hamas [enligt New York Times](https://www.nytimes.com/article/israel-hostages-hamas-explained.html) och [The Guardian](https://www.theguardian.com/world/2023/dec/15/israeli-forces-recover-body-elya-toledano-hostage-gaza)
* 63 journalister dödade per den 15 december [enligt CPJ (Committe to Protect Journalists)](https://cpj.org/2023/12/journalist-casualties-in-the-israel-gaza-conflict/)

## Korta punkter från mig

Jag inser nu när jag börjar skriva att man fastnar i varje mening för det finns så många källor och så många siffror. Poängen med mitt blogginlägg är att tipsa om några poddar. Men det jag vill ha sagt är:

* Hamas attack den 7 oktober 2023 mot civila israeler är avskyvärd. Attacker mot civila vill jag aldrig ursäkta. De civila som drabbas förtjänar att sörjas och attacken förtjänar att fördömas.
* Israels respons, eller snarare hämnd, är inte proportionerlig. Och den är otroligt brutal och hänsynslös med attacker mot civila mål. De civila som drabbas förtjänar att sörjas och attackerna förtjänar att fördömas.
* Konflikten började inte i och med Hamas attack den 7 oktober. Konflikten började i och med [Nakba 1948](https://en.wikipedia.org/wiki/Nakba) där runt 700 000 palestinier fördrevs och flydde i och med staten Israels bildande. Konflikten har fortsatt sedan dess. Och hårdnat i och med Israels ockupation av Västbanken 1967 och det [apartheid-system som råder där](https://www.amnesty.se/aktuellt/israels-apartheid-mot-palestinier-ett-grymt-system-av-dominans-och-ett-brott-mot-manskligheten/).
* En fredlig lösning görs svårare och svårare i takt med att Israel i brott mot internationell rätt flyttar över sin befolkning till ockuperat område. Enligt Diakonia bor det [650 000 israeliska bosättare](https://www.diakonia.se/har-finns-vi/palestina/konflikt-och-rattvisa-i-israel-och-palestina/) i de ockuperade palestinska områdena.
* Hamas är inte samma sak som palestinier och inte samma sak som muslimer. Palestinier och muslimer kan inte som grupp utkrävas ansvar.
* Staten Israel är inte samma sak som israeler och inte samma sak som judar. Israeler och judar som grupp kan inte utkrävas ansvar.
* Det finns progressiva krafter i Palestina och de förtjänar vårt stöd.
* Det finns progressiva krafter i Israel och de förtjänar vårt stöd.
* När Israel-Palestina-konflikten blossar upp så ökar antisemitism och islamofobi. Det är viktigt att vara varsam mot rasism och markera att rasism inte hör hemma i solidaritetsrörelser

## Poddavsnitt

Här är några poddavsnitt som jag lyssnat på och tyckt varit intressanta:

* [Palestina vår framtid](https://soundcloud.com/apansanatomi/palestina-var-framtid) av [Apans Anatomi](https://mediakollen.org/feed/91/curated)
* [Dekolonial solidaritet](https://soundcloud.com/apansanatomi/dekolonial-solidaritet) av [Apans Anatomi](https://mediakollen.org/feed/91/curated)
* [Antisemitism och Israelkritik](https://shows.acast.com/krakelpodden/episodes/antisemitism-och-israelkritik) av [Krakelpodden](https://mediakollen.org/feed/34/curated)
* [Eld och rörelse #123: Rasmus Canbäck är tillbaka!](https://radio.alltatalla.se/podcast/eld-och-rorelse-123/) av [Radio åt alla](https://mediakollen.org/feed/28/curated)
* [Eld och rörelse #121: Henrik Bromander om Ljuset i Rojava, samt kort om Gaza](https://radio.alltatalla.se/podcast/eld-och-rorelse-121-henrik-bromander-om-ljuset-i-rojava-samt-kort-om-gaza/) av [Radio åt alla](https://mediakollen.org/feed/28/curated)
* [Avsnitt 200 - Israel/Palestina, med Michael Schulz](https://shows.acast.com/haveristerna/episodes/avsnitt-200-israelpalestina-med-michael-schulz) av [Haveristerna](https://mediakollen.org/feed/44/curated)
* [76. Hatet i spåren av krig och terror](https://sites.libsyn.com/412148/76-hatet-i-spren-av-krig-och-terror) av [Studio Expo](https://mediakollen.org/feed/124/curated)
* [79. Antisemitismen 85 år efter novemberpogromen](https://sites.libsyn.com/412148/79-antisemitismen-85-r-efter-novemberpogromen) av [Studio Expo](https://mediakollen.org/feed/124/curated)

## Mina reserapporter från när jag var i Israel och Palestina 2014

Jag åkte ut som ekumenisk följeslagare under tre månader hösten 2014 via ekumeniska följeslagarprogrammet. Så skrev jag de här reserapporterna:

* [Ekerö är långt från Stockholm!](https://foljeslagarprogrammet.se/rapporter/ekero-ar-langt-fran-stockholm/)
* [Lantbruk under ockupation](https://foljeslagarprogrammet.se/rapporter/lantbruk-under-ockupation/)
* [Hinder för ekonomisk utveckling](https://foljeslagarprogrammet.se/rapporter/hinder-for-ekonomisk-utveckling/)
* [Mellan muren och bosättningen](https://foljeslagarprogrammet.se/rapporter/mellan-muren-och-bosattningen/)
* [De israeliska protesterna](https://foljeslagarprogrammet.se/rapporter/de-israeliska-protesterna/)
* [Frågor och många svar](https://foljeslagarprogrammet.se/rapporter/fragor-och-manga-svar/)

## Den judiska vänstern är viktig

Avslutningsvis så vill jag lyfta en viktig sak. Jag har hört från flera judiska kamrater i kampen att de känner sig klämda, dels mellan vänstern (att man ska ta tydligare ställning mot Israel och för Palestina) och dels från sina församlingar (att man ska ta ställning mot Hamas och för Israel).

Vi ska inte avkräva judar ansvar när Israel begår hemska brott på samma sätt som att vi inte ska avkräva msulimer ansvar när jihadister begår terrorbrott.

Vapenvila nu! Häv ockupationen av Västbanken! Häv blockaden mot Gaza! Fred, rättvisa och jämlikhet för alla i Israel och Palestina.

Det var några korta ord från mig. 