---
title: Krönika - Sätt Magdalena Andersson i skolbänken 
image: social-media-pink.jpg
image_description: Bild av <a href="https://pixabay.com/users/alexandra_koch-621802/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=7647811">Alexandra_Koch</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=7647811">Pixabay</a>
description: En krönika om att det finns bättre sätt än förbud
date: 2024-07-14 16:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/saett-magdalena-andersson-i-skolbaenken).

**Jag är civilingenjör i datateknik och arbetar som webbutvecklare och vet att flera förslag som lagts fram den senaste tiden är ogenomförbara på en teknisk nivå. Det är frustrerande. Som tur är finns det ett bra svar på ogenomtänkta förslag: folkbildning!**

Vi har i flera år kunnat läsa om internets utmaningar: om hur vår privata data används för att påverka val, hur AI används i desinformationskampanjer och om negativa effekter av ungas sociala medievanor. Många är bekymrade och föräldrar är oroliga för sina barn. Själv är jag desto mer oroad över de förslag som politiker lägger fram för att lösa problemen.

IT-säkerhetsspecialisten Karl Emil Nikka nagelfar några av politikernas förslag gällande sociala medier i sin [krönika](https://content.bhybrid.com/publication/da4b7f07/mobile/) i Aktuell säkerhet, exempelvis Liberalernas förslag om stopp för sociala medier för barn efter ett visst klockslag och Socialdemokraternas förslag om att förbjuda anonymitet på sociala medier (något som både [Myra Åhbeck Öhrman](https://www.etc.se/kronika/bryr-sig-s-om-visselblaasaren) och [jag](https://www.etc.se/kronika/id-kontroller-paa-naetet-skyddar-de-starkaste) skrivit om). Nu toppar S med ännu ett förslag om att [förbjuda sociala medier för barn](https://omni.se/andersson-infor-aldersgrans-pa-sociala-medier/a/8qQwVQ).

Jag är civilingenjör i datateknik och arbetar som webbutvecklare och vet att flera förslag som lagts fram den senaste tiden är ogenomförbara på en teknisk nivå. Det är frustrerande. Som tur är finns det ett bra svar på ogenomtänkta förslag: folkbildning!

[Internetstiftelsen](https://internetstiftelsen.se/om-oss/) är en oberoende organisation som verkar för ett internet som bidrar positivt till människan och samhället. De ansvarar bland annat för internets svenska toppdomän .se. Intäkterna från affärsverksamheten finansierar flera utbildningssatsningar i hur man kan använda internet på ett säkert sätt. De producerar massvis med bra och korta guider (på flera språk), som [5 tips för ditt barns första mobiltelefon](https://internetkunskap.se/artiklar/foraldraskap/dags-for-barnen-att-fa-egen-mobil/), [Vanliga bedrägerier i barnens digitala värld](https://internetkunskap.se/artiklar/foraldraskap/vanliga-bedragerier-i-barnens-digitala-varld/) och [Avslöja falska profiler på sociala medier](https://internetkunskap.se/snabbkurser/sociala-medier/avsloja-falska-profiler-pa-sociala-medier/). De har även gjort en längre guide med titeln [Digitalt självförsvar](https://internetstiftelsen.se/docs/Digitalt-sjalvforsvar.pdf).

Så min uppmaning till alla politiker som vill göra något som är grundat i verkligheten blir: börja med att surfa in på internetstiftelsen.se och folkbilda dig. Då kommer du kunna lägga fram bättre förslag. Min uppmaning till alla nätanvändare är att göra det samma. Och passa på att även läsa EFF:s guide om [övervaknings-självförsvar](https://ssd.eff.org/) utifall att politikerna inte lyssnar utan istället vill kontrollera och läsa allt du skriver och gör på nätet.