---
title: Krönika - Låt inte Facebook övervaka din förening
image: lego.jpg
image_description: Bild av <a href="https://pixabay.com/users/eak_kkk-907811/">Eak K.</a> från <a href="https://pixabay.com">Pixabay</a>
description: En krönika om bra tjänster för föreningar
date: 2024-09-06 10:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/saa-engagerar-du-folk-utan-oevervakning).

**Samordnar ni och diskuterar via Facebook-grupper? Skapar ni evenemang med hjälp av Facebook? Som tur är finns det redan flera organisationer och tech-kollektiv som tillhandahåller allt som du behöver!**

Jag vet hur det är att organisera föreningsengagemang och partipolitiska aktiviteter. Då är Facebook ofta användbart. Kanske skickar din förening också ut formulär för vilka som kan delta på flygbladsutdelning eller julfesten via Google Forms? Skriver ni debattartiklar och motioner tillsammans med hjälp av Google Docs? Samordnar ni och diskuterar via Facebook-grupper? Skapar ni evenemang med hjälp av Facebook? Men tycker ni samtidigt att det inte känns bra i magen? Och är det inte så att fler och fler av era medlemmar faktiskt inte längre är aktiva på Facebook eller till och med valt bort det av personlig övertygelse? Vore det inte skönt att bli oberoende!?

Som tur är finns det redan flera organisationer och tech-kollektiv som tillhandahåller allt som du behöver!

[The Riseup Collective](https://riseup.net/), baserade i Seattle i USA, tillhandahåller e-post, mejllistor och wiki-hemsidor. [Disroot](https://disroot.org/en) är baserade i Amsterdam och erbjuder liknande tjänster. Organisationen [Framasoft](https://framasoft.org/en/) erbjuder öppna alternativ till stängda tjänster, såsom Framadate istället för Doodle, Framaforms istället för Google Forms och Mobilizon istället för Facebook Events. [CryptPad](https://cryptpad.fr/) är en fri och öppen programvara som ersätter mycket av Google-sviten som ordbehandling, kalkylark, formulär och bildspel. [Action Network](https://actionnetwork.org/) är en plattform för organisering och mobilisering som tillhandahåller tjänster som nyhetsbrev, fundraising och events. Italienska A/I (Autistici/Inventati) är ett projekt som [tillhandahåller](https://www.autistici.org/services/) e-post, nyhetsbrev och bloggar.

I Sverige har vi kooperativet [Collective Tools](https://collective.tools/) som erbjuder e-post och dokumentdelning för medlemmar. [Kamratdataföreningen Konstellationen](https://konstellationen.org/projekt/) (där jag är med) driver öppna och decentraliserade sociala medieplattformar som Mastodon och Lemmy utan kostnad.

Du och din grupp eller förening behöver så klart inte omedelbart sluta använda Big Tech-plattformarna. Men det är en bra riktlinje att medlemmar inte ska behöva skapa konton på stängda och övervakade plattformar bara för att kunna vara engagerad i föreningen. Nu har du fått några tips. Lycka till!