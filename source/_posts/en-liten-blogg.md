---
title: En liten blogg
image: polygon.jpg
description: En blogg med static site generatorn hexo
date: 2022-02-20 22:00
---
Tänkte det var väl dags för att göra mig en liten blogg för att blogga om politiska och andra grejer. 

Det här inlägget handlar mest om själva tekniken. Det är för andra som tycker det vore roligt att dra igång en blogg med hjälp av en static site generator till skillnad från t.ex. wordpress. Eftersom det är statiska filer så är det ingen backendkod som kan köras och heller ingen databas. Så rimligtvis säkrare och inte behöver lika mycket underhåll.

Så om man tycker det är roligare med politik jag skriver om kan man skippa detta inlägget.

## Plattform - hexo på gitlab

Det jag valde var verktyget [hexo](https://hexo.io). Kvittar egentligen vad tänker jag. Det finns jekyll och en massa annat men med hexo skriver man plugin och annat mha javascript. Jag använder mig av gitlab pages för att hosta innehållet.

Tanken med en static site generator är att man skriver markdown-filer (filer som slutar med .md) som påminner lite om wiki-syntax. Så man skriver sina inlägg lokalt, ser att de funkar, sen gör man en git commit och git push och sen så händer det lite magi hos gitlab och så är det uppe i molnet. Fiffigt!

