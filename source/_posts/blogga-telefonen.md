---
title: En bloggpost från Android
image: club-mate-phone.jpg
description: Den här bloggposten är skapad mha Android, termux, git, gitlab pages
date: 2023-08-03 14:00
---
Jag har funderat på hur man kan skapa ett blogginlägg smidigt från telefonen om jag vill. Det bästa vore att ha ett webbgränssnitt. Då kan man använda samma gränssnitt på desktop och mobilen. Men eftersom jag använder en static site generator (hexo) så har jag ingen backend.

Så nu har jag fixat så att min Android-telefon har termux installerat. Sen kör jag git clone mot gitlab. Det här inlägget har jag skrivit i vim i termux.

Det knepiga var att flytta över bilden till rätt katalog. Men det gick från termux.

![Termux och vim](./images/termux-vim.png)*Vim i termux*

Det vore nu najs att slippa sitta i vim för att skriva. Faaaaast det funkar :)

