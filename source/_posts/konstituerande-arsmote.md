---
title: Årsmöte för ny vänsterförening
image: video-conference.png
description: Konstituerande årsmöte 18 februari för ny vänsterförening
date: 2023-02-05 20:30
---
Nu är det äntligen dags för det konstituerande årsmötet för [den nya vänsterföreningen](/forening-for-vansterkanaler/) som ska tillhandahålla kommunikationskanaler för vänstern.

Mötet sker digitalt **lördag 18 februari kl 15:00**.

Vi har ett härligt gäng på uppstartsmötet 16 januari där vi tog fram förslag på stadgar och ändamål för föreningen. Nästa steg nu är det konstituerande årsmötet då föreningen bildas.

Eftersom vi ännu inte har någon bildad förening eller stadgar så har vi ännu inte heller några medlemmar. Därför är alla välkomna som gillar [tanken med föreningen](/forening-for-vansterkanaler/). Det är årsmötet som beslutar om namn, ändamål och stadgar. Det är först när föreningen är bildad som man kan bli medlem i föreningen.

**Vill du vara med på det konstituerande årsmötet?** Anmäl dig i [det här formuläret](https://cryptpad.fr/form/#/2/form/view/6OUJs7StbVK+FQC7S5mKQ-4hmKwj4G0OYqzLF-xgYxs/) så kommer du få länk till videomötet skickat till dig.

## Ändamål och stadgar

Hela förslaget till stadgar går att [läsa här](/files/stadgar.pdf).

Förslaget till ändamålsparagraf lyder:

> §2 Föreningens ändamål
> 
> Föreningen vilar på en demokratisk, socialistisk, feministisk och antirasistisk grund.
> Föreningen har som ändamål:
>
> - att tillhandahålla fria och öppna verktyg och digitala plattformar
> - att sprida information och bedriva utbildningsverksamhet
> - att verka för demokratisk medlemsstyrning av verktyg och plattformar

## Namn på föreningen

På mötet 16 januari så kom det fram ett antal förslag på namn men inget som fastnade. Så det är fritt fram och komma med förslag på mötet. Det finns redan ett gäng förslag som kommer att läggas fram.

## Beslut på mötet

De viktigaste besluten på mötet är:

* Antagande av stadgar och namn på föreningen
* Val av styrelse

Efter årsmötet (antagligen direkt efter) kommer styrelsen ha sitt konstituerande årsmöte. Då väljs firmateckningar och lite annat.

## Vill du sitta i styrelsen?

Om du vill sitta i styrelsen så är det bara att föreslå dig själv på mötet. Alla kan kandidera. Men om du vill får du väldigt gärna föranmäla ditt intresse till mig på samuel@bitar.se vilket är bra för mig att veta.

Välkomna till mötet! Och som sagt, [anmäl dig här](https://cryptpad.fr/form/#/2/form/view/6OUJs7StbVK+FQC7S5mKQ-4hmKwj4G0OYqzLF-xgYxs/).
