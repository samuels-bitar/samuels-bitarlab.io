---
title: Hur jag fixade egen Mastodon-instans
image: spejset_logo_2x1.png
description: Hur jag skapade instansen social.spejset.org
date: 2022-12-30 18:00
---

Jag har under ett tag funderat på att dra igång en mastodon-instans för någon slags bred vänster. Och för att jag vill göra det långsiktigt hållbart och demokratiskt så tänker jag att den ska styras av en förening. Men det är ett annat blogginlägg! För innan jag drar igång det så tänkte jag att det är bäst att fixa en egen instans för att testa. Så här är hur jag gjorde för att fixa igång instansen [social.plejset.org](https://social.spejset.org).

## Shoutout till Johan

Min vän Johan såg till att dra igång först med en billig instans och kunde lösa jobbiga grejer först och så var det enkelt för mig att bara kopiera :)

Det Johan (och jag) gjorde var att helt enkelt följa de anvisningar som finns på Mastodons hemsida på dokumentationssidan under [Running your own server](https://docs.joinmastodon.org/user/run-your-own/) och [Running Mastodon](https://docs.joinmastodon.org/admin/prerequisites/).

Det som jag behövde fixa innan var:

* Fixa domännamn
* Fixa VPS
* Fixa e-mail-provider
* INTE fixa S3 (kommer nog bita mig i röven)

## Fixa domännamn

Jag har sen tidigare använt domäner hos [njalla](https://njal.la/) som tillhandahåller domäner, vps:er och vpn.

![Skärmdump av olika VPS-alternativ hos HostUp](/images/njalla-screenshot.png)

De har [tre prisvarianter](https://njal.la/pricing/) för domännamn:

* 15 euro per år. Exempel: .art, .com, .net, .org, .re
* 30 euro per år. Exempel: .se, .nu, .social, .app, .cloud, .lol och väldigt många fler
* 45 euro per år. Exempel: .world, .cafe, .guru, .poker

Eftersom jag gillar att kostnadsminimera så tog jag .org för den blev billigast.

Det knepigaste var så klart att komma på ett bra namn och ett som var ledigt för den domänen. Jag valde spejset för det är lite mångbottnat: cyberspace, rymd, utrymme, plats och så svengelskt vilket är kul.

Njalla har också en bra DNS editor som är smidig och som de inte tar extra betalt för. Loopia t.ex. kan ha rätt billiga domäner men sen tar de extra betalt för att man ska kunna använda dem. Njalla drivs av Peter Sunde m.fl. så det är ju också kul. 

## Fixa VPS

Johan tipsade mig om [HostUp](https://min.hostup.se/?affid=149) (min affiliate-länk som ger mig rabatt) som har billiga VPS:er. Jag själv hade egentligen siktet redan inställt på [Contabo](https://contabo.com/en/) som har väldigt billiga och kraftfulla VPS:er och som jag har hört gott om. Men en annan kompis hade sagt att det krävdes att man skulle scanna in körkort eller pass. (Det är ett tyskt företag). Och eftersom han kommit igång smärtfritt så tänkte jag att jag kör på det också.

Jag hade funderat också på Njalla eftersom de har VPS:er och jag gillar dem. Men det hade varit lite för dyrt för mig i detta läge. 

De billigare alternativen som finns på HostUp kostade 50 kr, 100 kr eller 200 kr per månad. 

![Skärmdump av olika VPS-alternativ hos HostUp](/images/hostup-vps.png)

Jag valde den för 100 kr för månad. Jag slängde också på daglig backup (istället för veckovis backup) som kostade 120 kr extra per år.

Jag valde Debian 11 som också är rekommenderat från Mastodons officiella dokumenation.

## Fixa e-mail provider

Jag hade redan ett Mailgun-konto men i gratisversionen kunde man inte skicka från egen domän. Jag vill ju att det ska stå att mail kommer från t.ex. notifications@spejset.org. Men om man skulle uppgradera skulle det kosta minst 35 dollar per månad vilket var alldeles för dyrt för mig.

Men Johan hade tipsat om [Mailjet](https://www.mailjet.com/) som är en annan provider. Och där kunde man använda egen domän! Man får lägga till lite grejer på sin DNS så att man validerar att man äger domänen. [Det är beskrivet här](https://documentation.mailjet.com/hc/en-us/articles/360042561594-How-to-validate-an-entire-sending-domain-). Man behövde också generera API-nyckel och en secret till den nyckeln. Detta används senare som SMTP username och SMTP password när man confar mastodon. Man ska också ange SMTP server vilket är in-v3.mailjet.com. Detta finns [beskrivet här](https://dev.mailjet.com/smtp-relay/configuration/).

## INTE fixa S3 bucket

Okej, det här kommer jag säkert att få ångra. S3 buckets har man för att lagra media på (t.ex. bilder) så att man slipper fylla upp hårddisken (och ta slut på eventuell bandbredd) på sin VPS. Johan försökte med AWS men det strulade. Jag själv använder Digital Oceans alternativ [spaces](https://www.digitalocean.com/products/spaces) på jobbet så jag tipsade Johan om det. Det funkade fint för honom. Men när jag skulle kolla vad det kostade så var det i alla fall 5 dollar i månaden. Så därför tänkte jag att jag testar utan.

Vi får se när mitt hårddiskutrymme tar slut och vad jag ska göra då :) Om vi blir ett gäng på instansen så kommer jag nog att få göra om det här.

## Kostnad totalt

Tja, det blev då 1400 kr per år för VPS:en och ca 167 kr per år för domännamnet. Och än så länge ingenting för Mailjet (om det inte börjar skickas mycket mail). Totalt under 1600 kr per år. Rätt okej ändå.

## Nu då?

Ja, nu så tänkte jag köra lite på min instans, fixa serverregler, bjuda in lite folk som är intresserade. Men själva syftet är att testa lite innan jag på allvar drar igång en förening som driftar instansen. Är du intresserad? Följ mig då på [@samuel@social.spejset.org](https://social.spejset.org/@samuel). Eller släng iväg ett mail till samuel@bitar.se.