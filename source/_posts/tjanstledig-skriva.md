---
title: Tjänstledig ett år för att skriva
image: scifi-bg.jpg
image_description: Bild från <a href="https://pixabay.com/illustrations/background-background-image-epic-7855412/">Pixabay</a>
description: Jag är tjänstledig ett åt från mitt jobb som webbutvecklare/devops för att läsa en kurs i kreativt skrivande
date: 2024-07-15 18:00
---

Jag har lite roliga nyheter! Jag har fått tjänstledigt ett år från mitt jobb som webbutvecklare/devops för att läsa en kurs i kreativt skrivande på Jakobsbergs folkhögskola: [Skriv! – poesi, prosa, dramatik](https://www.jakobsbergsfolkhogskola.se/sv/vara-kurser/skriv-poesi-prosa-dramatik.aspx).

Vid nyår [skrev jag ett inlägg](/skriv-bara-skriv/) om mina tankar på skrivande. Jag hade då just fått en bok av min fru ("Skriv bara skriv") som visste att jag gått i de tankarna ett tag. Det var en väldigt givande bok och jag följde upp med ännu ett inlägg om [nyårslöfte kring skrivande och bloggande](/blogga-mera/). Mitt mål (för att det är kul att sätta upp något slags mål) var att blogga en gång i veckan med undantag för sommar och storhelger:

> Mitt nyårslöfte (nja, ambition snarare) är att blogga minst en gång per vecka. Men då undantar jag storhelger och sommar. Så med andra ord, de flesta vanliga jobbveckor så tänkte jag skriva minst ett blogginlägg i snitt. Så ungefär 35-40 inlägg under året borde det bli om jag skippar juni, juli, augusti och någon storhelg.

Redan nu är jag uppe i 24 inlägg i år (med detta inräktnat). Då är visserligen några av inläggen "bara" kopior av [mina teknikkrönikor på ETC Nyhetmagasin](https://www.etc.se/av/samuel-skaanberg).

Så det är roligt att jag kommit igång ordentligt med skrivandet redan innan kursen börjat.

Det som är kul också är att jag valde kreativt skrivande (poesi, prosa, dramatik) istället för facklitteratur som annars skulle legat närmare till hands. Men eftersom jag gärna vill ta ledigt och utforska något helt nytt så tyckte jag det kändes kul att lära mig en del tekniker kring att skriva skönlitterärt. Det blir ännu mer spännande så! Sen är också en anledning att jag är väldigt inspirerad av Cory Doctorow som både skriver facklitteratur och skönlitteratur. Han är sci-fi-författare så även hans skönlitteratur är politisk. Den får en att tänka kring vad som skulle hända om man tar en tendens man ser idag och extrapolerar den och funderar på vad som skulle hända om 10-20 år om man fortsätter på samma sätt.

Senaste boken av Doctorow jag läste var [The Lost Cause](https://en.wikipedia.org/wiki/The_Lost_Cause_(novel)). Den utspelar sig i en nära framtid i Kalifornien när klimatförändringarna härjar men samtidigt så har tidigare regeringar lyckats med en ordentlig Green New Deal med mycket progressiv politik. Men samtidigt finns det MAGA-folk kvar som är motståndare till de gröna satsningarna. Mycket spännande! Anledningen till att jag började läsa den var för att en kamrat startade igång en digital asynkron [bokklubb på Aggregatet](https://aggregatet.org/c/bokklubben).

Jag läser för närvarande [Walkaway](https://en.wikipedia.org/wiki/Walkaway_(Doctorow_novel)) av Doctorow. En spännande bok om skapande anarkistiska hackare som drar sig undan default (dvs normsamhället) för att skapa den värld de vill se utanför den kapitalistiska civilisationen. Det fria skapandet tas även upp i böcker som [Makers](https://en.wikipedia.org/wiki/Makers_(novel)) och [Pirate Cinema](https://en.wikipedia.org/wiki/Pirate_Cinema_(novel)).

Några saker jag funderar på i och med kursen jag ska läsa är:

* OMG! Vad kommer jag lära mig för saker? Sjukt spännande!
* Hmm, var ska jag lägga upp mina alster? Ska jag lägga upp mina texter någonstans? Eller ska jag bara ha dem privat? (Jag lutar åt att lägga ut det mesta). Kanske att jag skapar en ny hemsida med bara texter
* Hur ska jag rent tekniskt fixa skrivandet? Jag är sjukt sugen på att fixa en anpassad laptop som är distraktionsfri. Gärna med E-ink-skärm eller passiv LCD-skärm så jag kan sitta i solen.

Om dessa frågor kommer jag sannolikt blogga en del om också :)

Kommer jag att skriva en bok under året? Kommer jag bli författare? Jag vet inte! Jag har inga konkreta planer. Det enda jag vet är att det blir superkul att göra något annorlunda ett tag. Jag trivs väldigt bra med mitt jobb och mina arbetskamrater men det blir roligt att bryta av med något helt annat.

Det här blir kul!