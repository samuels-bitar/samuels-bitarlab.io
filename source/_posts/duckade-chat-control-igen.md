---
title: Vad tycker L om Chat Control?
image: laptop-lock.jpg
image_description: Bild av <a href="https://pixabay.com/users/albarus-1663074/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4736566">René</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4736566">Pixabay</a>
description: Vi lyckades ducka Chat Control ännu en gång. Men hur länge lyckas vi med det? Och vad tycker L om Chat Control?
date: 2024-12-12 16:00
---

Idag var frågan om Chat Control uppe i EU:s ministerråd. Det fattades inget beslut idag [rapporterar](https://digitalcourage.social/@echo_pbreyer/113640046073831978) den tyska piratpartisten Patrick Breyer.

Ungern, som har EU-ordförandeskapet just nu, lyckades inte samla en tillräckligt stor del av länderna för detta.

Dessvärre är Sverige ett av de länderna som sagt ja till Ungerns Chat Control-förslag.

I grunden är Ungerns Chat Control-förslag samma förslag som lagts fram av EU-kommissionären Ylva Johansson. Det är bara en fråga om mindre justeringar. Det handlar fortfarande om att massövervaka (i praktiken) allas kommunikation utan brottsmisstanke och det handlar om att förhindra (eller förbjuda) totalsträckskryptering genom att kräva leverantörer att de installerar bakdörrar. Eller sagt på ett annat sätt: chattappar som Signal, iMessage och WhatsApp ska inte tillåtas erbjuda privat kommunikation mellan sina användare.

Justitieminister Gunnar Strömmer (M) försöker slingra sig i en [intervju i Aftonbladet](https://www.aftonbladet.se/nyheter/kolumnister/a/63Vbmz/stor-intervju-med-justitieminister-gunnar-strommer-av-oisin-cantwell) där han säger att Moderaterna ju var emot EU-kommissionens förslag men att man kämpar med näbbar och klor för att värna integriteten i arbetet framöver.

Det är struntprat. I så fall hade man inte ställt sig bakom de förslag som lagts fram av Spanien, Belgien och Ungern där de stora problemen kvarstår. Om man tycker att EU-kommissionens förslag var så dåligt, då kan man ta ställning för det [förslag](https://www.pcforalla.se/article/2137224/eu-parlamentet-rostar-for-viktig-forandring-av-chat-control-2-0.html) som *samtliga grupperna i EU-parlamentet* ställt sig bakom. Det enda man vinner på att ställa sig bakom ett mer repressivt och integritetskränkande förslag från ministerrådet, är att stärka ministerrådets förhandlingspoistion i trilogsförhandlingar.

Vi i Kamratdataföreningen Konstellationen tog fram en rapport, [Kluvna tungor](https://konstellationen.org/2024/09/19/chatcontrol-rapport/), där vi redovisade hur de svenska riksdagspartierna har uttalat sig och agerat i riksdag och i media.

Därför ställde vi i Kamratdataföreningen Konstellationen följande frågor till riksdagspartierna:

1. Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står ni bakom EU-parlamentets kompromissförslag?
3. Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle ni säga nej till förslaget?
6. Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser ni att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

C, V, MP och SD svarade på enkäten. De svarade alla “ja” på våra frågor och tog därmed tydligt ställning mot Chat Control och massövervakning.

Men S, M, KD och L svarade inte ens på enkäten trots flera påminnelser.

Vi skrev därför en [debattartikel i Dagens ETC](https://www.etc.se/debatt/varfoer-vill-m-l-kd-och-s-inte-prata-om-chat-control) där vi avslutade med följande:

> S, M, KD och L svarade inte på enkäten trots flera påminnelser. Vi upplever att partierna duckar för frågorna. Vi är oroade över att de inte vill svara på om de anser att privat kommunikation är en mänsklig rättighet, vilket det är enligt Europakonventionen, EU-stadgan och FN:s deklaration om mänskliga rättigheter.
>
> Det är dags att sluta tala med kluven tunga.

Både M och L har haft ett högt tonläge i debatten och kritiserat Chat Control. Deras EU-parlamentariker har varit väldigt aktiva inför EU-valet vilket är något vi tar upp i rapporten.

## Liberalernas liberala värden?

Det är kanske dumt att hoppas på att Liberalerna faktiskt ska värna liberala värden. Men när man har ett sådant partinamn och faktiskt säger sig stå för värderingar om personlig integritet och individens frihet gentemot staten, då måste man vara tydlig.

Deras ungdomsförbund LUF har varit tydliga. De har deltagit på flera demonstrationer mot Chat Control som Kamratdataföreningen Konstellationen har varit med och anordnat. ([Maj 2023](https://konstellationen.org/2023/05/20/demo-chatcontrol-tal/) och [September 2023](https://konstellationen.org/2023/09/20/chatcontrol-pressmeddelande-efter-demo/))

Inför en omröstning om Ungerns förslag i Justitieutskottet 26 september så mailade jag partierna igen och uppmanade dem att rösta nej till förslaget. Några dagar efter att omröstningen var avklarad fick jag [ett mail från Liberalerna](chat-control-mail-liberalerna/). De svarar väldigt konstigt på varför de inte röstade nej:

> Bakgrunden är, som du säkert vet, ett omfattande arbete med att stoppa sexuella övergrepp mot barn och pedofiler på nätet. För oss liberaler är det viktigt att det arbetet intensifieras samtidigt som den personliga integriteten skyddas.
>
> Det finns ännu inget förslag att ta ställning till. Det är bara lösa utkast i en lång process mellan EU:s olika institutioner. Riksdagens beslut i Justitieutskottet är en del av denna process där det sades att förhandla vidare så att förslaget uppfyller de krav på integritet vi ställer. 
>
> EU-parlamentet har bara en gång kunnat ta ställning till ett färdigförhandlat förslag kallat Chat Control 1. Liberalerna röstade då emot förslaget, precis som du skriver. Låt oss återkomma när det finns ett färdigt förslag att rösta om i EU. Karin Karlsbro är mycker engagerad i denna fråga.

Jag svarade även på detta mail och förklarade varför det var helt felt:

> Tack för återkopplingen! Jag delar inte din bild gällande vad partier i riksdagen kan göra. Det spelar roll vad Sverige har för ställning i ministerrådet. Där kan Justitieutskottet och EU-nämnden rimligtvis bidra till ett förslag som ligger mer i linje med EU-parlamentets förslag. De förslag som kommit från Spanien, Belgien och Ungern har varit väldigt oroväckande för oss som är engagerade i digitala fri- och rättigheter.

Det var även den kritiken som Liberala ungdomsförbundet (LUF) framförde offentligt till liberalerna i ett [pressmeddelande](https://www.luf.se/pressmeddelande-fran-luf-angaende-chat-control-2-0/).

## Vad tycker egentligen Liberalerna?

Så frågorna kvarstår. Vad tycker egentligen Liberalerna? De har ännu inte svarat på vår enkät. Kan fler maila dem och be dem om svar.

Här är frågorna:

1. Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står ni bakom EU-parlamentets kompromissförslag?
3. Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle ni säga nej till förslaget?
6. Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser ni att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Här är kontaktuppgifter till Liberalerna tagna från [riksdagens hemsida](https://www.riksdagen.se/sv/ledamoter-och-partier/partierna/liberalerna/):

* Officiell mail - info@liberalerna.se
* Partisekreterare Jakob Olofsgård - jakob.olofsgard@riksdagen.se
* Gruppledare Lina Nordquist - lina.nordquist@riksdagen.se
* Vice gruppledare Louise Eklund - louise.eklund@riksdagen.se

Kanske någon annan kan få svar.