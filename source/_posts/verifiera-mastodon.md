---
title: Hur man verifierar sig på Mastodon
image: audit.jpg
description: Lite hederlig html på sin sida
date: 2022-11-13 16:55
---
Okej, fixar ett supersnabbt inlägg för jag märker att vissa lägger ett litet checkmark i sin profil. Men det betyder absolut ingenting för alla kan säga att man är Bill Gates och sätta ett checkmark på det.

På Mastodon kan man verifiera sin hemsida. Så om man har en websida som man har kontroll över så kan man bevisa på sin Mastodon att man har kontroll över den. Så här gjorde jag på mastodon.se. 

Jag surfade till https://mastodon.se/settings/profile (om man har annan instans så är det också till /settings/profile men på en annan webbplats).

Jag skrev in min webbsida, dvs https://samuels.bitar.se och fick då den här länken:

```
<a rel="me" href="https://mastodon.se/@samuel">Mastodon</a>
```

![Mastodon-profil](/images/mastodon-profile.jpg)

Sen tog jag den koden och lade in den längst ner på min hemsida https://samuels.bitar.se så då kommer det att stå "Mastodon" och vara en länk till min mastodon-profil.

Men eftersom jag inte ville att det skulle synas en länk där så gjorde jag den osynlig genom att lägga på ett style-attribut. Då blev koden så här:

```
 <a rel="me noopener" target="_blank" href="https://mastodon.se/@samuel" style="display: none">Mastodon</a>
 ```

 Så om man surfar in på hemsidan så ser man inget speciellt längst ner, bara den vanliga symbolen för atom:

 ![Atom-feed](/images/atom-feed.png)

Men om man kollar i källkoden så ser man den lilla länken till mastodon:

![Mastodon-länk](/images/mastodon-link-source.png)

Sen går man tillbaka till sin mastodonprofil och trycker på "Save settings". Då kommer Mastodon att se att det finns en länk från https://samuels.bitar.se till https://mastodon.se/@samuel och därmed förstå att jag har makten över länkarna på min hemsida och där med så får jag en fin liten länk:

![Verifierad hemsida](/images/verified-website.png)

Sådär! Så fixar man att verifiera sig på Mastodon.


