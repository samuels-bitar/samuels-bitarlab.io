---
title: Mastodon och Bluesky
image: mastodon-welcome.jpg
image_description: Bild från <a href="https://www.standard.co.uk/lifestyle/mastodon-twitter-killer-social-media-platform-b1038658.html">The Standard</a>
description: Mina tankar om Mastodon och Bluesky
date: 2025-01-15 17:00
---

Jag är ordförande för Kamratdataföreningen Konstellationen som är en socialistisk, feministisk och antirasistisk förening som driftar och utvecklar fria system och folkbildar om. En av sakerna vi gör är att drifta mastodoninstansen [Spejset](https://social.spejset.org). Igår hade vi styrelsemöte och hade en runda om hur vi tycker arbetet i föreningen gått och vad vi är glada för. Flera lyfte just Spejset som en riktigt lyckad satsning och något vi verkligen är glada för. Varför? Jo, för att det är social media som vi har kontroll över. Och vilka har kontroll över det? Medlemmarna i föreningen. Det är ändå smått fantastiskt! **Att användare på social media kan bli medlemmar i föreningen som driftar instansen och vara med om hur instansen ska driftas.**

Hur görs detta då? Jo, det görs genom programvaran Mastodon som är fri och öppen källkod. Vår lilla instans sammankopplas med tusentals andra instanser och tillsammans skapar vi ett fritt och öppet nätverk.

Senaste året har Bluesky seglat upp som en plattform som många joinar när de lämnar Twitter/X och Facebook. Vi som varit på Mastodon länge hoppades att folk skulle välja att komma till Mastodon. Det finns flera skäl till att vissa valde Bluesky framför Mastodon. Det kommer jag inte gå igenom i detta blogginlägget.

Men jag fick frågan från en kamrat om varför man skulle välja Mastodon framför Bluesky när jag tipsade om Mastodon. Så det här är mina spridda tankar om frågan.

## Mastodon och ActivityPub

Mastodon är en mikrobloggingplattform så det blir ett federerat, fritt och öppet alternativ till Twitter/X.

Den bygger på det federerade och decentraliserade protokollet [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub). Det innebär att hela nätverket består av tusentals servrar som pratar med varandra. På så sätt är det väldigt mycket som det federerade protokollet för e-post. En person som har en gmail.com-adress kan maila till en som har en hotmail.com-adress eller riksdagen.se-adress. De kan göra detta trots att de inte har konto på samma server. Genialt! En annan viktig sak med Mastodon och ActivityPub är att om man inte trivs på den instansen man har sitt konto så kan man flytta sitt konto. Och då kommer alla ens vänners konto uppdateras så att de automatiskt följer det nya kontot. Du kan flytta och dina vänner kanske inte ens märker det! På så sätt påminner det en del om hur telefonoperatörerna fungerar. Du kan ha ett nummer hos Telias men om du tycker det är för dyrt kan du byta till Comviq och ta med dig ditt nummer dit.

Det som är kul med Mastodon och ActivityPub är att det kräver inte så stora resurser. Så en liten förening som Kamratdataföreningen Konstellationen (som har ca 150 medlemmar) kan utan problem ha råd att drifta en instans.

Alla plattformar och instanser som implementerar ActivityPub-protokollet sammankopplas i detta stora nätverk. Man brukar kalla det för fediversum. Det finns t.ex. även [Pixelfed](https://pixelfed.org/) som är ett fritt och öppet alternativ till Instagram men som också implementerar ActivityPub.

Enligt [fedidb](https://fedidb.org/) så finns det drygt 10 miljoner användare på fediversum. Mastodonkonton står för 70% av alla konton på fediversum.

Några viktiga saker med Mastodon och andra plattformar på fediversum:

* Det består av flera tusen olika instanser så det är inte så sårbart
* Varje instans kan ha sina egna regler och moderering. Och även blocka andra instanser de inte vill ha något med att göra
* Användare på en instans kan prata med användare på andra instanser
* Användare kan flytta sitt konto från en instans till en annan och få med sig alla följare och även de som den följer
* Det finns ingen reklam på Mastodon. Man kan så klart sprida reklam i inlägg. Men det är inget som dyker upp bland ens inlägg för att själva plattformens ägare vill det, som är fallet på Twitter/X, Facebook, Instagram, osv.
* Det underliggande protokollet ActivityPub är standardiserat av W3C så det är inget enskilt företag som äger det
* Det är förhållandevist billig att drifta en Mastodon-instans



## Bluesky och AT Protocol

Även [Bluesky](https://en.wikipedia.org/wiki/Bluesky) är en mikrobloggingsplattform och är därmed ett alternativ till Twitter/X. Många som lämnade Twitter/X efter Elon Musks alla konstiga och obehagliga beslut om plattformen kom antingen till Mastodon eller till Bluesky. Eftersom de flesta sociala medier-plattformarna är centraliserade så är det för många enklare att välja ett centraliserat alternativ till Twitter/X för det är lättare att förstå. På Bluesky måste man inte välja vilken instans man ska skapa konto hos. Det är en centraliserad plattform och det som gäller är Blueskys instans.

Bluesky startade som ett projekt inom Twitter 2019. Syftet var att undersöka om det gick att decentralisera Twitter. Det blev självständigt från Twitter 2021 och drevs vidare av företaget Bluesky Social som var en [benefit corporation](https://en.wikipedia.org/wiki/Benefit_corporation), ett slags vinstdrivande företag som även har mål att ha positiv påverkan på samhället. Det innebär att företaget tillåts att använda vinsten för samhällspositiva saker och är inte tvingade att maximera aktieägarnas vinst. Företagets VD är [Jay Graber](https://en.wikipedia.org/wiki/Jay_Graber).

[Jack Dorsey](https://en.wikipedia.org/wiki/Jack_Dorsey) gick med i styrelsen för Bluesky Social 2022.

Utvecklingen tog fart efter att Elon Musk köpt Twitter 2022. Många ogillade Musk och hans politik och ville inte vara på en plattform där han hade otroligt stor makt. Bluesky började som invite-only februari 2023 och började med öppna registreringar februari 2024.

Jack Dorsey lämnade styrelsen för Bluesky Social i maj 2024 och uppmanade istället användare att använda Twitter/X.

Bluesky fick riskkapital från Blockchain Capital i oktober 2024.

Jay Graber har sagt att hon inte kommer "sugifiera nätverket med reklam".

Bluesky har ca [26 miljoner användare](https://backlinko.com/bluesky-statistics).

### Bluesky är inte federerat

Bluesky är baserat på [AT Protocol](https://en.wikipedia.org/wiki/AT_Protocol) är ett decentraliserat protokoll. Och på så sätt låter det som att det skulle vara samma sak som ActivityPub och att Bluesky är egentligen samma sak som Mastodon. Men så är det inte.

[Christine Lemmer-Webber](https://dustycloud.org), som är en av skaparna till ActivityPub, har analyserat Bluesky och AT Protocol för att se [hur decentraliserat Bluesky är](https://dustycloud.org/blog/how-decentralized-is-bluesky/) och menar att det inte är meningsfullt att kalla det decentraliserat. Hon pekar på att det skulle kosta otroliga mängder pengar att skapa alternativa instanser. Så som protokollet är uppbyggt går det att med inte alltför mycket pengar drifta vissa delar som alternativ till Blueskys centrala instanser. Men om man skulle göra det fullt ut skulle det kosta otroligt mycket pengar. Allt är i praktiken beroende på att Blueskys instans snurrar på. Om det går ner faller hela nätverket. Till skillnad från Mastodon där om den största instansten mastodon.social går ner, så fortsätter resten av nätverket att fungera.

Blogginlägget är otroligt tekniskt och detaljerat. Hon är samtidigt generös och lyfter några av styrkorna med Bluesky. Och hon avslutar också med en lista på saker hon tycker ActivityPub-utvecklarna och fediversum ska göra.

Även Cory Doctorow har skrivit flera blogginlägg om Bluesky. I sitt blogginlägg [Bluesky and enshittification](https://pluralistic.net/2024/11/02/ulysses-pact/#tie-yourself-to-a-federated-mast) skriver han om varför han inte i nuläget kommer skapa konto där.

Hans kritik går ut på att det inte går att migrera till en annan instans eller plattform.

> When a platform can hold the people you care about or rely upon hostage – when it can credibly threaten you with disconnection and exile – that platform can abuse you in lots of ways without losing your business. In other words, they can enshittify their service

Han skriver att Bluesky har utvecklat en del intressanta tekniker och att han respekterar Jay Graber och de andra som jobbar med Bluesky som han tror bara har goda avsikter. Men att det kvittar i slutändan

> But here's the thing: all those other platforms, the ones where I unwisely allowed myself to get locked in, where today I find myself trapped by the professional, personal and political costs of leaving them, they were all started by people who swore they'd never sell out. I *know* those people, the old blogger mafia who started the CMSes, social media services, and publishing platforms where I find myself trapped. I considered them friends (I still consider most of them friends), and I knew them well enough to believe that they really cared about their users.
>
> They *did* care about their users. They just cared about other stuff, too, and, when push came to shove, they chose the worsening of their services as the lesser of two evils.

Så när investerarna kommer och knackar på dörren och vill ha vinstutdelning så blir det svårt. Ska man skydda användarna eller göra investerarna glada?

> That's where federation comes in. On Mastodon (and other services based on Activitypub), you can easily leave one server and go to another, and everyone you follow and everyone who follows you will move over to the new server. If the person who runs your server turns out to be imperfect in a way that you can't endure, you can find another server, spend five minutes moving your account over, and you're back up and running on the new server

## Kan man rädda Bluesky och AT Protocol?

Cory Doctorow är en person som jag har stort förtroende för så jag lyssnar när han har något att säga. Nu har han skrivit under kampanjen/uppropet [#FreeOurFeeds](https://freeourfeeds.com/). Syftet är att samla in 30 miljoner dollar.

> With Zuckerberg going full Musk last week, we can no longer let billionaires control our digital public square.
>
> Bluesky is an opportunity to shake up the status quo. They have built scaffolding for a new kind of social web. One where we all have more say, choice and control.
>
> But it will take independent funding and governance to turn Bluesky’s underlying tech—the AT Protocol—into something more powerful than a single app. We want to create an entire ecosystem of interconnected apps and different companies that have people’s interests at heart.
>
> Free Our Feeds will build a new, independent foundation to help make that happen.

På frågan om hur kampanjen stöder standarden ActivityPub svarar de lite svävande om interoperabilitet:

> ActivityPub is a W3C standard that enables interoperability between platforms like Mastodon, Flipboard, Ghost, Threads and others. AT and ActivityPub should not be either/or options. Our vision is that interoperability between open social media platforms is achieved through collaboration. This campaign can organize developer communities, fund product development, support testing that improves bridging and native protocol interoperability.
>
> The Social Web Foundation convenes the network of platforms connected via ActivityPub. SWF's executive director is involved in the #FreeOurFeeds campaign as a custodian and you can read more about in her blog post [here](https://socialwebfoundation.org/2025/01/13/free-our-feeds-and-algorithmic-pluralism/).

I en [artikel](https://www.forbes.com/sites/emilybaker-white/2025/01/03/jay-graber-bluesky-elon-musk-x/) förklarar Blueskys VD Jay Graber om hur hon vill säkerställa att Bluesky inte kommer sluta som X. Mycket låter bra men det framgår också att de inte har en affärsmodell klar för sig. De säger att de inte ska ha förpestande reklam. De funderar på olika prenumerantprodukter. Men ska de då vara stängda och inte open source? Hon öppnar för att testa viss reklam.

Cory Doctorow berättar om varför han skrev under kampanjen i blogginlägget [Billionaire-proofing the internet](https://pluralistic.net/2025/01/14/contesting-popularity/#everybody-samba). Där tar han upp flera intressanta aspekter som är värda att fundera över.

Men kanske att Cory Doctorow är väl positivt inställd till en reformering av Bluesky och AT Protocol. Finns det verkligen något intresse från Bluesky att bli av med den makten de har över sina användare? Det vore toppen om Bluesky och AT Protocol ändras så att användare bara kan byta till en annan likvärdig instans men som driftas av något annat företag, en organisation eller olika techkollektiv så fort de inte trivs. Men varför skulle ett vinstdrivande företag göra detta?

#FreeOurFeeds syftar till att samla in 30 miljoner dollar. Det är mycket pengar. Utvecklingen av tjänster och plattformar på fediversum är viktig. Hur mycket skillnad skulle inte 30 miljoner dollar göra i organisationer som jobbar på verktyg och plattform baserad på ActivityPub. Som t.ex. [IFTAS](https://about.iftas.org/) som jobbar med att ta fram verktyg och arbetssätt för moderering. Eller [Nivenly Foundation](https://nivenly.org/) som stöttar open source-projekt och bidrar med hållbar organisering. Eller varför inte stödja med pengar så att man får fler utvecklare som kan knacka kod för Mastodon, Pixelfed, Mobilizon eller någon annan befintlig plattform som idag försöker utgöra alternativ till Big Tech sociala medier.

Mastodon-kontot Mastodon Migration kommenterar Cory Doctorows inlägg och ställer följdfrågor. [Tråden](https://mastodon.online/@mastodonmigration/113829076223876008) är väl värd att läsa.

[Christine Lemmer-Webber fick frågan](https://social.coop/@cwebber/113821963645421468) om vad hon tyckte om Free Our Feeds och hon svarade:

> My main thoughts about Free Our Feeds are I wonder if anyone launching this read my recent blogposts which analyzed ATProto and its ecosystem IN DEPTH and determined that it's quadratic to scale towards decentralization in its present form, which nobody has really disputed since https://dustycloud.org/blog/how-decentralized-is-bluesky/ https://dustycloud.org/blog/re-re-bluesky-decentralization/
>
> So is Free Your Feeds hoping to move towards actual decentralization, or are they hoping to just set up a *second* major relay and appview

Hon verkar ödmjuk och har avböjt att tala på olika konferenser för att kritisera AT Protocol för att istället fokusera på sitt arbete med ActivityPub och samla in pengar till organisationen [Spritely](https://spritely.institute/). Att fokusera på att bygga nytt istället för att lägga fokus på att kritisera annat är otroligt svårt och jag är imponerad över att hon gör det. Det måste vara frestande att tycka en massa om AT Protocol, sitta på nätet och rätta folk. Men hon gör inte det. (Vilket för övrigt inspirerar mig till att i framtiden kanske lägga mer fokus på att drifta grejer än att blogga. Men samtidigt är det ju kul att blogga.)

## Bättre med Bluesky än X och Meta

Jag föredrar Mastodon och fediversum med ActivityPub-protokollet och tror att det är det strategiska valet för att få till bättre social media där användarna har mer makt över sin data och kommunkation. Nu har vi äntligen ett alternativ till vinstdrivande Big Tech som suger. Det är värt att fira och vara glad över.

Men jag vill verkligen poängtera att allt är bättre än X. Här är ett axplock om varför X blivit skit och varför Elon Musk inte är en stabil person att ratta en av världens största plattformar:

* [Elon Musk har censurerat mitt konto på X](https://www.etc.se/ledarkolumn/elon-musk-har-censurerat-mitt-konto-paa-x)
* [Elon Musk uttrycker sitt stöd för ytterhögern i Europa](https://www.svt.se/nyheter/utrikes/elon-musk-uttrycker-sitt-stod-for-ytterhogern-i-europa)
* [Oron över Musks stöd till Europas ytterhöger: ”Kan bli sista knuffen”](https://www.etc.se/inrikes/oron-oever-musks-stoed-till-europas-ytterhoeger-kan-bli-sista-knuffen)
* [Brittiska ministrar hukar för Musk – rädda för konflikt med USA](https://www.etc.se/utrikes/ministrar-hukar-foer-elon-musk)
* [Elon Musk har byggt en desinformationsmaskin](https://www.etc.se/ledare/elon-musk-har-byggt-en-desinformationsmaskin)

Det har funnits en kort diskussion om vänstern bör lämna Twitter/X. Dagens ETC ställde frågan till en rad vänsterskribenter om [vänstern bör lämna X](https://www.etc.se/inrikes/svenska-debattoerer-saa-hanterar-vi-kontroversiella-plattformen-x). Det verkar finnas konsensus om att det är skit att Elon Musk har sådan makt och förstör plattformen. Men många är oroliga över att inte nå ut till läsare.

Kalle Söderberg från förbundet Allt åt alla skrev en text som fick den något provocerande rubriken [Vänstern hör inte hemma i ett kuddrum](https://www.flamman.se/vanstern-hor-inte-hemma-i-ett-kuddrum/). Eftersom Jesper redan skrivit ett [blogginlägg där han svarar på texten](https://www.turist.blog/2024-11-24-veckans-lankar/) så behöver jag inte kommentera mer.

Sedan Trump vann valet så har även Mark Zuckerberg sagt att han ska ändra Metas plattformar på olika sätt. Detta gäller både [faktagranskningsinformation och modereringsregler](https://www.businessinsider.com/mark-zuckerberg-donald-trump-moderation-2025-1?op=1). Jonas Lundström har skrivit om att det [kanske är dags att även lämna Metas plattformar](https://www.patreon.com/posts/om-att-lamna-och-119976705) och om sina tankar kring det. Även om Mark Zuckerberg inte är skogstokig som Elon Musk så är det tydligt att han bara bryr sig om vinstmaximering. Om det innebär att anpassa Metas plattformar så att det behagar Trump så kommer han göra det så att han inte drar på sig presidentens vrede.

Det finns mycket goda skäl att lämna Twitter/X och bygga upp nätverk på andra ställen. Även Meta finns det skäl att lämna om man har möjlighet. Det allra viktigaste är kanske inte att lämna rakt av. Men det viktiga är att andra plattformar stärks genom din närvaro där. Plattformar som inte kan tas över av narcisistiska miljardärer med storhetsvansinne och högerextrema åsikter.

Om valet är mellan att stanna kvar och lägga fokus och energi på Twitter/X och Meta eller att skifta över till Bluesky så säger jag byt till Bluesky direkt!

Men om valet står mellan Bluesky och Mastodon hoppas jag verkligen man väljer Mastodon.

**Men man måste inte välja!** Det går alldeles utmärkt att ha ett konto på Mastodon och ett konto på Bluesky. Jag ser mycket hellre än sådan utveckling än att folk ska tvingas välja.

Men så som det ser ut i dagsläget så tycker inte jag att Bluesky är det långsiktigt strategiska valet. Det är ett vinstdrivande företag som driver Bluesky. När det väl gäller så kommer vinst att prioriteras över användarnas makt och frihet. Om en federering och decentralisering värd att tala om kommer att hota Blueskyes vinster så kommer företaget att välja vinsterna.

## Korta avslutande tankar

Det här långt blogginlägg men hoppas du fick ut något av det. Jag kommer nog skriva något mer senare om att bidra till en mer inkluderande kultur på Mastodon och fediversum någon gång. För teknik är inte allt. Vi är sociala varelser och det är viktigt att tänka på. Man vill inte komma till ett ställe där man känner att man gör fel om man gör eller säger fel, om saker man inte alls tänkt på. Men som sagt, ett annat blogginlägg.

Här är mina takes:

* Jag hoppas så många som möjligt lägger mindre tid (eller rent ut av tar bort konto) på Twitter/X och Metas Facebook och Instagram
* Jag hoppas så många som möjligt upptäcker Mastodon och fediversum
* Jag kommer lägga mitt fokus på att ha kul och spännande diskussioner på Mastodon snarare än att försöka lista ut vad "nästa stora plattform" är för att vara där
* Om folk vill vara på Bluesky får de gärna vara där. Om mina farhågor om att Blueskys kommande sugifiering infrias så hoppas jag så klart att användare väljer Mastodon eller annan plattform på fedeiversum istället för att ödsla tid på att bygga upp nätverk på Bluesky och om något år upptäcka att det var bortkastad tid
* Om Bluesky kommer slå världen med häpnad, växa gigantiskt, bidra till att Twitter/X förtynar och dessutom senare blir decentraliserat och mer makt kommer internetanvändarna till del så är jag bara glad att få fel. Ja, då tar jag nog och startar ett konto där också :)
* Fram tills dess att det finns skäl att tro att Bluesky verkligen kommer vilja och kommer kunna anpassa Bluesky och AT Protocol så att en äkta federering (med allt det innebär) är möjlig, så kommer jag uppmuntra folk att välja Mastodon och andra ActivityPub-plattformar på fediversum
* Det är häftigt och kul med tekniker som bryggan Mastodon-Bluesky-bryggan [Bridgy Fed](https://fed.brid.gy). Det visar också på att man rätt snabbt kan bygga fungerande verktyg. Men eftersom jag tror risken är större att Blueskys användarbas har större social dragkraft än Mastodons användarbas så tror jag att bryggan snarare kommer skapa mer FOMO för användare på Mastodon än vad användare på Bluesky kommer känna gentemot Mastodon. Så när/om bryggan strular eller inte kan funka lika bra och en användare känner att den inte pallar med bryggan, tror jag fler kommer sticka till Bluesky. Själv är jag på Mastodon för att hänga med andra här för att jag har det supertrevligt och för att det är så coolt med vår egen sociala media som vi själva kan kontrollera. Därför har jag inte aktiverat brygging och har dessutom tystat bryggan för mitt egna konto
* Jag kommer inte skamma eller klaga på folk som hänger på Bluesky. De får göra det om de vill. Om frågan om Bluesky vs Mastodon dyker upp kommer jag väl mest hänvisa till detta blogginlägg
* Inspirerad av Christine Lemmer-Webber så kommer jag hellre försöka vad jag kan för att göra Mastodon (och mer specifikt Spejset-instansen) mer roligt och välkomnande istället för att snacka om varför Bluesky är skit

Det var det hela!