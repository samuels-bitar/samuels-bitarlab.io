---
title: Mysigt med Mastodon
image: mastodon_wallpaper.png
description: Jag skriver om mina erfarenheter av Mastodon och Facebook och hur Kamratdataföreningen Konstellationen bildades
date: 2024-01-05 16:00
---

Som läsare av den här bloggen vet så [ogillar jag Big Tech sociala medier](/bryta-sig-loss/). Med det lite slarviga uttrycket syfter jag på de sociala medie-plattformarna som drivs av vinstdrivande företag som t.ex. Facebook, Instagram, Twitter/X, Youtube och TikTok. Så min kritik riktar sig huvudsakligen mot de vanligaste plattformarna. Det finns visserligen andra företag som har sociala medie-plattformar som de tjänar pengar på men problemet blir tydligast med jättarna. (Med [Big Tech](https://en.wikipedia.org/wiki/Big_Tech) så avser man oftast Google/Alphabet, Facebook/Meta, Amazon, Apple och Microsoft och jag är medveten om att inte alla har sociala medie-plattformar som sin primära verksamhet).

## Alternativ till Big Techs sociala medier

Men det finns som tur är alternativ till Big Techs sociala medier! Innan Facebooks intåg så fanns det en levande bloggosfär och webforum. Dessa medier finns fortfarande kvar men i minskad omfattning. Vänstern var speciellt aktiv på dessa plattformar, något som bl.a. [Anders Svensson](https://blog.zaramis.se/2023/12/28/en-gang-hade-vanstern-en-egen-infrastruktur-pa-natet/) bloggat om.

I somras var jag och ett gäng anarkister, socialister och vänsterdatafolk på programpunkten [Vilda vänstern och Internet - Vad gör vi när sociala medier dör?](https://www.anarchistbookfair.se/index.php/sv/program-2023) på Anarkistiska bokmässan på Cyklopen i Stockholm. I texten till eventet kan vi läsa:

> Under Internets ”nybyggar-era” var den vilda vänstern bäst på Internet. Det var i våra IRC-kanaler som piratrörelsens verktyg växte fram. Det var vi som skapade oberoende medieplattformar och det var i våra forum som nya kontakter och idéer tog form. I bloggosfären hade högern inte en chans att hänga med. Men plötsligt slukades hela vilda vänstern upp av de stora plattformarna. Vi lärde att anpassa oss efter Facebook, Instagram och Twitter. Och med det försvann också vår initiativförmåga.
>
> Nu håller dessa jättar på att antingen implodera eller vissna bort. Vilket öde väntar då vilda vänstern på Internet? Kan vi ta tillbaka initiativförmågan? Kan vi göra Internet öppet, enkelt och roligt igen? 

Om detta rundabordssamtalet skrev [Rasmus Fleischer om här](https://www.etc.se/kronika/den-vilda-vaenstern-kom-tillbaka). Han skriver (mina länkar):

> Vi är många som fortsätter logga in på Facebook enbart för att få inbjudningar till evenemang. Att hitta alternativ till detta borde vara en hög prioritet, såväl för den vilda vänstern som för oberoende kulturaktörer.
>
> Ett försök i denna riktning är kalendariet [Gnistor.se](https://www.gnistor.se/), som sammanställer inbjudningar till studiecirklar, demonstrationer och andra vänsterevenemang. En länk till varje grej postas sedan automatiskt av ett annat projekt, [Kamratpostaren](https://kamratpostaren.se), på Mastodon och Twitter.

Efter rundabordsamtalet satte ett gäng av oss vid ett bord och pratade vidare. Vi beslutade att anordna en temakväll med rubriken [Bortom Big Tech sociala medier - hur kan vänstern vara online?](https://konstellationen.org/2023/07/05/mastodonfest-rss-blogg/).

![Gryta med ris](/images/konstellationen-event-mat.jpg)*Gryta med ris*

En sammanfattning av kvällen skrevs av [Judith Kahrer från Konstellationen](https://konstellationen.org/2023/08/20/intresset-for-alternativ-stort/). Det serverades ris och gryta, var en dragning av mig själv om Konstellationen och om vår Mastodonsinstans Spejset (mer om det snart), smågruppssamtal och en dragning av Robin Zachari från Skiftet om varför mail är oslagbart och varför man ska [skaffa en mejlstrategi](https://www.flamman.se/kamma-er-snyt-er-och-skaffa-en-mejlstrategi/).

## Mastodon

Jag har redan skrivit [Lite kort om Mastodon](/lite-kort-om-mastodon/). Men kort och gott kan man säga att Mastodon är ett open source-alternativ till Twitter/X som är federerat.

Det är den sista biten om federerat som verkligen gör Mastodon annorlunda. Federering är en slags decentralisering. Det finns inte bara en stor server utan tusentals servrar. Servrar kallas ofta instanser.

![Olika nätverksmodeller](/images/network-models.jpg)*Olika nätverksmodeller: centraliserat, decentraliserat, federerat*

Det betyder att man kan skapa ett konto på en Mastodonserver och sen prata med andra konton på samma instans eller på andra instanser. På det sättet är det som med e-post. Jag kan t.ex. från min e-post samuel@bitar.se skicka tips på nyheter till tipsa@etc.se. Vi kan kommunicera med varandra utan att ha konto på samma server.

Varför ska man ens ha tusentals servrar kanske man undrar. Varför inte bara ha en? Jo, för att sprida ut makten över nätverket. Man försvårar också en "hostile takeover". Vi kan alla se hur skit Twitter blev när Elon Musk köpte upp det. Även om Mastodon är open source så är det i sig inte tillräckligt för att förhindra att plattformen skulle kunna köpas upp om det bara funnits en server. Men med flera tusentals servrar så försvinner det problemet för om "huvudservern" [mastodon.social](https://mastodon.social) skulle köpas upp så kan nätverket fortsätta utan den. Och ja, användarna på [mastodon.social](https://mastodon.social) kan flytta över sina konton (med följare) till en annan mastodoninstans. (För den som vill nörda ner sig i techpolitik så kan man läsa om den "hostile takeover" som gjordes mot Freenode [här](https://www.vice.com/en/article/m7ev8y/freenode-open-source-korea-crown-prince-takeover), [här](https://arstechnica.com/gadgets/2021/05/freenode-irc-has-been-taken-over-by-the-crown-prince-of-korea/) och [här](https://hackaday.com/2021/05/20/freenode-debacle-prompts-staff-exodus-new-network/)).

En fördel med att ha olika servrar är att varje server kan ha sin uppsättning med regler som gäller på servern. En vanlig uppsättning regler är att inte tillåta rasism, sexism, homofobi och transfobi. Om en användare missköter sig kan en annan användare anmäla. Admin (eller snarare moderatorerna) för servern får då avgöra om användaren brutit mot reglerna och om användaren ska varnas, inlägg tas bort eller om användaren direkt ska petas.

![Skärmdump av Spejsets regler](/images/spejset-regler.jpg)*Skärmdump av [Spejsets regler](https://social.spejset.org/about)*

Men eftersom programvaran är fri kan vem som helst starta en instans. Så ja, det finns även en rad riktiga skitservrar där extremhöger hänger. De brukar hänga på servrar som säger att de värnar yttrandefrihet och kallar sig "free speech absolutists" men det är bara ett kodord för att tillåta rasism, homofobi och annan skit.

De flesta admins vill inte ha med nazister att göra så därför blockas ofta skitservrarna. The Verge skrev artiklen [How the biggest decentralized social network is dealing with its Nazi problem](https://www.theverge.com/2019/7/12/20691957/mastodon-decentralized-social-network-gab-migration-fediverse-app-blocking) där de tar upp den stora alt-right-servern Gab som migrerade från sin egen plattform till att gå över till Mastodon. Väldigt många Mastodonservrar blockade Gabs server så den är mest sin egen lilla ö.

## Mastodonservern Spejset föds

Egentligen så går det alldeles utmärkt att skapa ett konto på den största Mastodonservern [mastodon.social](https://mastodon.social). Men styrkan i Mastodon blir större som sagt ju fler servrar det finns, så att inte all makt, data och sårbarhet hamnar på en server. Det fanns redan en rad andra svenska Mastodonservrar, t.ex. mastodon.nu och mastodonsweden.se.

![Spejsets logga](/images/spejset_logo_2x1.png)*Spejsets logga*

Så därför [startade jag själv Mastodonservern Spejset](/spejset-instansen/). Anledningen var att jag ville skapa en plats där svenskspråkiga vänstermänniskor skulle känna sig hemma. Kul att Rasmus Fleischer inom kort skrev krönikan [låt mig tipsa om vänsterns server](https://www.etc.se/kronika/laat-mig-tipsa-om-vaensterns-server).

## Kamratdataföreningen Konstellationen

En tanke jag hade redan från början var att [starta en förening för vänsterkanaler](/forening-for-vansterkanaler/) där drift av en Mastodonserver skulle vara en del av verksamheten. Jodå, sagt och gjort, vi var ett gäng som tillsammans startade [Kamratdataföreningen Konstellationen](https://konstellationen.org/2023/02/25/en-forening-ar-fodd/). Föreningen vilar på en demokratisk, socialistisk, feministisk och antirasistisk grund.

Föreningen har som ändamål:

* att främja och tillhandahålla fria och öppna verktyg och digitala plattformar
* att sprida information och bedriva utbildningsverksamhet
* att verka för demokratisk medlemsstyrning av verktyg och plattformar

![Kamratdataföreningen Konstellationens logga](/images/konstellationen_logo_with_text.png)*Kamratdataföreningen Konstellationens logga*

Vi fick låna servrar av ETC gratis så vi migrerade Spejset från min privata server till föreningens servrar.

Anledningen till att jag hellre ville att Spejset skulle driftas av en förening var för att jag såg hur vissa Mastodonservrar hade lagt ner sin verksamhet eller tagit konstiga beslut eftersom all makt över driften och reglerna låg hos administratören. Men jag tycker det är viktigt med medlemsdemokrati och då är det självklart att det är bättre för användarna om de har möjlighet att påverka. (Det finns mycket man kan säga om hur Mastodonservern mastodon.se lades ner och effekterna det fick men det får bli ett annat blogginlägg).

Det är frivilligt för varje användare på Spejset att bli [medlem i Konstellationen](https://konstellationen.org/bli-medlem/). Det kostar 100 kr per år. Men om man är medlem så har man också möjlighet att vara med och påverka beslut som har med föreningen att göra och driften av Mastodonservern Spejset.

## Mediakollen

Jag vill också bara lite kort lägga till att Konstellationen har andra [projekt](https://konstellationen.org/projekt/). T.ex. så driftar vi det minimalistiska medieprojektet [Mediakollen](https://mediakollen.org/) som är ett slags [omni för vänstern](https://www.flamman.se/vansterns-omni-vill-samla-rorelsens-medier/). Syftet är att göra det enklare att hitta vänsternyheter på ett och samma ställe.

Vi anordnade i höstas [ett hackathon](https://konstellationen.org/2023/10/15/hackathon-rapport/) där en stor del av tiden gick ut på att koda på Mediakollen.

## Varför är det mysigt (för mig) på Mastodon?

Många av de som kommer till Mastodon tycker att det är mysigt och trevligt. Vänsterbloggaren Anders Svensson skriver en del om sina tankar i ett [blogginlägg](https://blog.zaramis.se/2024/01/05/sociala-medier-mastodon-trevligare-an-de-andra/):

> Jag har också skaffat konton på sociala medier som Mastodon, Bluesky och Threads. Syftet är främst det samma som med X/Twitter. Att diskutera mina inlägg på bloggen med andra och att få läsare till bloggen. Men på Mastodon har det blivit lite annorlunda. Där deltar jag mer i diskussioner med andra, kommenterar deras inlägg och länkar. Det är helt enkelt mer socialt för mig än vad Facebook, X/Twitter, Bluesky och Threads är.
>
> Jag upplever att är ett sundare diskussionsklimat på Mastodon och att det är lättare att få tag på intressanta personer att följa och inlägg att förhålla sig till. Det finns inte heller en massa reklam och högertextremisterna lyser med sin frånvaro. De är visserligen sällsynta även på Bluesky och Threads.

Varför är det så? Jo, men en anledning kan vara att nazister och troll blockas rätt snabbt, antingen deras konton eller servern om serverns admin inte tar ansvar för att städa upp. 

Själv så har jag använt Mastodon i flera år. I övrigt använder jag bara Facebook som sociala medier, mest för att jag känner att jag måste vara där för att hålla kontakt med andra vänsterpartister och kunna få tag på folk. Jag har nog aldrig på riktigt haft som ambition att helt ersätta Facebook med Mastodon. Men jag ser till att försöka ha mina diskussioner på Mastodon och dela bilder och tankar på Mastodon.

En stor anledning till att jag undviker Facebook är att jag mår lite dåligt varje gång jag delar något på Facebook, jag liksom föder Metas monster med min data och med min närvaro. Därför är det alltid en besk eftersmak oavsett vad jag delar. Men Mastodon har jag ingen sådan eftersmak.

## Inte bara rosor

Men det finns också problem med Mastodon. På samma sätt som att det finns rasism, sexism, homofobi, transfobi och annan skit i samhället så finns det även på Mastodon.

Många icke-vita personer (huvudsakligen svarta från USA) har påtalat att de utsatts för rasism men många användare svarar att det inte stämmer för de inte har sett något. Kritiker menar att Mastodon är väldigt vitt och att det råder en vithetsnorm där. Även på "svenska Mastodon" så verkar det mest vara vita personer.

Mastodon har även sin beskärda del av folk som klagar på andra, att man gör fel, att man borde göra ditt eller datt. Det kommer väl alltid vara så att när många personer med olika kulturer, erfarenheter, intressen och åsikter interagerar så kommer det att uppstå konflikter och friktion. Men många personer som kommit från Twitter för att nosa på Mastodon har upplevt att det fått många pekpinnar riktade mot sig. Det är nog mer än fråga om att odla en trevlig kultur och inte något man kan eller bör försöka slå fast i serverns regler.

Så även om Mastodon i mångt och mycket är bättre än Twitter och Facebook gällande samtalsklimatet så kan man som serveradmin inte slappna av.

Alla serveradmins som på allvar vill ta itu med rasismen i samhället och även på Mastodon bör vara ödmjuka och inlyssnande. 

Mer att läsa om kritik finns här:

* [Erin Kissane - Blue Skies Over Mastodon](https://erinkissane.com/blue-skies-over-mastodon)
* [Erin Kissane - Mastodon Is Easy and Fun Except When It Isn’t](https://erinkissane.com/mastodon-is-easy-and-fun-except-when-it-isnt)
* [The Whiteness of Mastodon](https://www.techpolicy.press/the-whiteness-of-mastodon/)
* [Is Mastodon The Social Media Platform Black Twitter Needs?](https://peopleofcolorintech.com/articles/is-mastodon-the-new-twitter-heres-everything-you-need-to-know-about-the-social-networking-site/)

## ...men mysig och trevlig ton ska inte få hålla folk borta

Jag tycker Mastodon som det är nu är mysigt och trevligt. Men jag vill att fler ska upptäcka Mastodon och få nytta av det. Speciellt marginaliserade grupper. För min del får gärna diskussionerna på Mastodon bli mer politiska och jag tycker det är okej att de kan bli upprörda och spetsiga.

Tonmoderering (tone policing) har använts ofta mot icke-vita personer när de påtalar rasism för att man tycker att de för fram kritik "på fel sätt". Jag själv försöker att vara konstruktiv i mina samtal och ha en god ton själv. Men jag försöker verkligen att undvika att påtala att andra ska ha det om de framför legitim kritik, speciellt om personen är från en marginaliserad grupp.

Så mysigheten på Mastodon är najs. Men jag är okej om den minskar om det är för att fler personer är där. Men reglerna och modereringen på servrarna ska i alla fall göra så att hatiska diskussioner försvåras och att sådana konton blockas.

## Ingen reklam

En annan sak som är najs med Mastodon är att det inte finns reklam. Eller influencers som vill kränga en massa grejer. Det finns däremot "personliga profiler" på Mastodon och företag och organisationer har konton med många tusen följare. 

![Stopp för reklam](/images/stop.jpg)*Stopp för reklam*

## Inga algoritmer - rak tidslinje

En annan sak med Mastodon är inlägg inte filtreras bort. Det är nog det jag är mest trött på när det gäller Facebook, att en massa inlägg som jag vill läsa från mina vänner och sidor jag följer, de visas inte. Istället visas reklam för andra sidor eller reklam för prylar att köpa.

Jag är däremot inte en motståndare mot "algoritmer" (kanske en av de mest missbrukade termerna det här årtusendet). Algoritmer kan vara superbra! Jag hade gärna haft fler algoritmer som hade hjälpt mig att hitta fler konton som skriver om sådant jag är intresserad av. Här tror jag att det faktiskt vore bra med mer algoritmer på Mastodon.

Men fördelen som det är nu är att inlägg som jag vill läsa inte filtreras bort.

## Vad väntar du på? Skapa ett konto!

Om du inte redan har skapat ett konto så har jag en guide här om att [komma igång med Mastodon](https://wiki.konstellationen.org/sv/guider/mastodon) där du vägleds pedagogiskt (hoppas jag) med att starta ett konto på Kamratdataföreningen Konstellationens egna Mastodonserver [Spejset](https://social.spejset.org/).

När du väl har skapat ett konto så kan du titta på den här [listan av konton att följa](https://wiki.konstellationen.org/sv/guider/mastodon-hitta-folk).

När du gjor det så kan du läsa mina korta tips om hur man [har det trevligt på Mastodon](https://wiki.konstellationen.org/sv/guider/tips-mastodon).

Just ja, och se till att [följa mig](https://social.spejset.org/@samuel) :)