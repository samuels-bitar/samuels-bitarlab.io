---
title: Maktanalys för kommunikation och sociala medier
image: social-media2.jpg
image_description: Image by <a href="https://pixabay.com/users/webtechexperts-10518280/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5187243">Joseph Mucira</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5187243">Pixabay</a>
description: Vänstern och andra progressiva rörelser behöver uppdatera sina strategier för sociala medier och kommunikation och införa en maktanalys.
date: 2024-11-07 18:00
---

**TLDR; Vänstern och andra progressiva rörelser behöver uppdatera sina strategier för sociala medier och kommunikation och införa en maktanalys. Det är en risk att förlita sig på stängda plattformar som låser in en och som har makten över ens data och kommunikation.**

Jag har tänkt länge på frågan om hur vänstern och andra progressiva rörelser bör tänka kring sociala medier och kommunikation. Det kan väl knappast gått någon förbi att sociala medier som drivs av stora vinstdrivande företag förstör mycket genom att övervaka, shadowbanna, blocka och bidra till polarisering. Om inte, så kolla in några av blogginläggen som Kamratdataföreningen Konstellationen skrivit på sin hemsida:

* [En förening är född](https://konstellationen.org/2023/02/25/en-forening-ar-fodd/)
* [Att bekämpa massövervakning är en feministisk ödesfråga](https://konstellationen.org/2023/03/08/debattartikel-8-mars-om-massovervakning/)
* [Intresset för alternativ till stora plattformar är stort](https://konstellationen.org/2023/08/20/intresset-for-alternativ-stort/)

Eller läs:

* Omis utmärkta [blogginlägg](https://autonomin.fritext.org/hejda-facebook/) om varför hon lämnade Facebook
* Henriks inlägg om hur [hans inlägg markerades som spam av Facebook](https://www.hemrin.com/ord-words/mitt-inlaegg-bedoems-som-skraeppost)
* Eller mitt eget om varför [Progressiva bör oroa sig för massövervakningen](/progressiva-bor-oroa-sig/)

Det finns många aspekter att tänka på om hur man som enskild, förening, parti, tidning, nätverk, bokklubb eller whatever bör agera på de olika plattformarna. Vilka plattformar bör man vara på? Varför? Vilka fördelar finns? Vilka risker? Missar jag något på att inte vara där?

Jag menar inte att jag sitter på alla svaren. Jag har starka värderingar och tankar kring de här sakerna. Samtidigt kan jag vara rätt pragmatisk och tänker att alla inte måste göra allt. Jag hoppas att vänstern och andra progressiva rörelser kan fundera mer kring de här frågorna och att det här blogginlägget kan vara en del av den diskussionen.

## Makt att nå ut till medlemmar och sympatisörer

Den stora grejen man tänker på när man är på sociala medier eller använder olika kommunikationsverktyg är så klart att man lyckas nå de man vill nå. När man skickar iväg ett nyhetsbrev vill man att folk ska läsa. När man skriver ett inlägg på Facebook vill man att folk ska läsa. Man vill engagera läsare, medlemmar och sympatisörer. Men framför allt vill man att de som följer en ska se vad man skriver.

Så är det inte längre på big tech sociala medier.

I början var Facebook ett fantastiskt verktyg för partier, organisationer och nätverk att organisera sig. Det var lätt som en plätt att få direktkontakt med de som var intresserade av ens frågor.

Sen kom sugifieringen.

Sugifieringen eller "[enshitification](https://en.wikipedia.org/wiki/Enshittification)" är ett bergrepp myntat av Cory Doctorow som är författaren, journalist och aktivist. Så här beskriver han det hela:

> Here is how platforms die: first, they are good to their users; then they abuse their users to make things better for their business customers; finally, they abuse those business customers to claw back all the value for themselves. Then, they die. **I call this enshittification**, and it is a seemingly inevitable consequence arising from the combination of the ease of changing how a platform allocates value, combined with the nature of a "two-sided market", where a platform sits between buyers and sellers, hold each hostage to the other, raking off an ever-larger share of the value that passes between them.

Vissa plattformar lever vidare som zombies och kanske inte riktigt dör. Så på så sätt innebär kanske inte sugifieringen nödvändigtvis död för plattformen. Men nog blir den allt sämre.

För det mesta är det plattformens algoritmer som skapar det här problemet. Trots att man följer en viss sida eller person på Facebook så får man inte ta del av innehållet. Facebooks algoritmer bestämmer vad som ska visas i ens flöde. En del av innehållet är så klart från en del av de man följer men vad som premieras bestäms av plattformen.

Demokratinätverket Skiftet blev [blockerat från att annonsera](https://www.etc.se/inrikes/vaensterorganisationen-blockades-av-facebook) på Facebook utan någon förklaring skriver Dagens ETC.

Det är lurigare med det som händer som man kanske inte märker. Visst innehåll döljs helt eller delvis av plattformerna utifrån vilket innehåll. Innehåll på Facebook och Instagram blir ofta smygblockerat (shadow banned), deras inlägg döljs helt eller delvis för deras följare. Bland annat Human Rights Watch har visat hur detta [drabbar pro-palestinska röster](https://www.hrw.org/news/2023/12/20/meta-systemic-censorship-palestine-content). Även nu inför valet i USA blev innehåll smygblockerat, något som [instagramprofilen Arielle Fodor](https://www.washingtonpost.com/technology/2024/10/16/instagram-limits-political-content-shadowban-election-posts/) fick erfara när hon skrev om bland annat aborträtt och Trump.

## Makt att inte bli blockad/kickad

I vissa fall är det inte premieringsalgoritmerna som är problemet. Det kan vara att man blockas. Det här är det mest uppenbara. När man loggar in möts man av information om att man blivit blockad. Det här är ett stort problem och det påminner en om vilken makt plattformen har över ens kommunikation och nätverk. Men det är i alla fall tydligt.

Eftersom det inte är någon mänsklig rättighet att ha ett konto på Facebook så kan företaget agera mer som det vill. Plattformen drivs av ett vinstdrivande företag som inte har skyldigheter mot användarna som man skulle haft om staten hade bedrivit plattformen. (Jag tror dock inte det skulle bli speciellt bra om staten skulle tagit över driften heller.)

Man får helt enkelt hålla tummarna. Frågan om "rent mjöl i påsen" och vad Facebook och de andra plattformarna tycker är okej gör att många självcensurerar och dessutom överkompenserar. För den som inte är ett dugg intresserad av politik så kanske den inte märker av det direkt. Men för politisk aktiva, fackliga aktivister, hbtqi+-personer, minoriteter och andra utsatta och normbrytande grupper så kan det vara oerhört stressande och göra att man inte kan skriva det man vill även om det är helt lagligt.

## Makt att behålla nätverken och kontakterna

Okej, men om man till slut känner för att dra från skitplattformar. Antingen för att man själv vill göra det eller för att man kommer bli kickad, eller tror man kommer att bli kickad, hur gör man då?

Om man har byggt upp en följarskara eller många vänner på många tusen så kan det vara svårt att lämna och att man då stannar kvar på plattformen trots att man hatar den.

Vissa gör det ändå. Myra Åhbeck Öhrman skrev krönikan [Jag flydde X – snälla gör det du också](https://www.etc.se/kronika/se-dig-sjaelv-i-xpegeln). Det är inte lätt att göra det. Hon avslutar sin krönika om att hon lämnat X med:

> Hälsar,
>
> Myra, som visserligen saknar sina 26 000 följare men som mår så mycket bättre utan att doomscrolla.

Myra lämnade till slut. Men om det hade gått att flytta med sina följare så hade det gått så mycket lättare att flytta och hon kanske hade flyttat tidigare. Det är detta som kallas interoperabilitet, dvs att plattformar kan prata med varandra och utbyta information.

![Bild på person med megafon som sitter på ett e-postmeddelande](/images/email-megaphone.jpg)*Bild på person med megafon som sitter på ett e-postmeddelande*

Det bästa exemplet om hur man kan ta med sig sina följare är det klassiska epost-nyhetsbrevet. Folk kan skriva upp sig via något formulär på ditt nyhetsbrev. Oavsett om det är [Mailchimp](https://mailchimp.com/) eller något av [alternativen](https://alternativeto.net/software/mailchimp) som [MailerLite](https://www.mailerlite.com/), [ListMonk](https://listmonk.app/), [ButtonDown](https://buttondown.com/), [FluentCRM](https://fluentcrm.com/), [PhpList](https://www.phplist.com/) eller [ActiveCampaign](https://www.activecampaign.com/) så kommer man alltid ha listan på e-postadresser man samlat in. Om tjänsten man har blir alltför dyr eller köps upp av en högerextrem galning så är det bara att exportera sin data och flytta.

Det är därför plattformar som Mastodon är så sympatiska och påminner om e-post. Där sitter man inte fast. Det går att exportera alla sina följare. Ja, det går till och med att skapa ett nytt konto på en ny mastodoninstans och så uppdateras alla ens följare automatiskt när man genomför en migrering. Så de som följer en behöver inte ens märka att personen de följer helt plötsligt bor på en annan instans.

## Delta i diskussionen

När det kommer till deltagandet i "den allmänna debatten" så ser behovet väldigt olika ut om man är ett parti, en tidning, en aktivistgrupp, en bokklubb eller whatever. De allra flesta vill vara "där alla andra är" vilket inte är så konstigt. Man tänker att om man är där andra är så får ens budskap större spridning även om det sker på plattformar som är riktigt skit, som t.ex. Elon Musks avloppsfontän X. Men genom att man stannar kvar på en plattform så legitimerar den också och gör det samtidigt svårare och mer kostsamt för andra att flytta.

![Grupp av personer som sitter vid laptops](/images/digital-prepping.jpg)*Bild på personer som sitter vid sina laptops*

Men här förstår jag att det är svårare för vissa grupper att undvika vissa forum. Just gällande X så tänker jag så här:

* Det är en skitplattform som är riggad mot vänstern. The Guardian har visat att [Trumps presidentvalskampanj har jobbat tillsammans med X](https://www.theguardian.com/us-news/2024/oct/12/x-twitter-jd-vance-leaked-file) för att förhindra att information om vicepresidentkandidat JD Vance ska spridas. Elon Musk som äger X har offentligt tagit ställning för Trump. Finns det någon som tror att vänstern har en chans på X?
* Det skrivs fortfarande väldigt mycket på X som är av intresse för journalister och därför kan man som journalist vilja vara kvar för att bevaka. Då tycker jag man ska ha ett tyst/passivt konto som man har bara för att läsa. Men att man inte postar eller interagerar med det.
* Man kan fasa ut sitt engagemang på X. Det kan ske i flera steg. Börja med att tala om att du successivt drar ner på ditt enagemang och vart man kan följa dig istället. Stäng sedan av aviseringar på appen på telefon. Stäng av alla möjliga e-post-aviseringar också. Ta sedan och avinstallera appen på telefonen. Gå vidare med att primärt dela innehåll på andra plattformar. Helst via Mastodon, blogg, nyhetsbrev men går också med andra plattformar. Gör sedan så att du bara loggar in på webben någon gång i veckan. Sedan någon gång i månaden. Sedan loggar du inte in. Du kan även ta bort ditt konto.
* Allt är bättre än X. Helst Mastodon. Men även BlueSky kan vara okej. (Men det finns goda anledningar att skippa BlueSky också vilket Cory Doctorow skriver om [här](https://pluralistic.net/2023/08/06/fool-me-twice-we-dont-get-fooled-again/) och [här](https://pluralistic.net/2024/11/02/ulysses-pact/)) Jag gillar inte Facebook, Instagram, TikTok eller Threads heller. Men allt är bättre än X.

Men kommer då inte högerpopulisterna och fascisterna ta över X då? Jo, kanske. Men låt dem göra det då. De lever dessutom på polarisering och konflikterna mest av alla. Om ingen är där som de kan mobba eller hota och få reaktioner för så förlorar en stor del av varför de är där.

## Behovet av säkra och trygga rum

Vi har olika kommunikation för olika tillfällen i våra föreningar, partier, nätverk och kompisgäng. Ett parti har extern kommunikation som ofta är inriktad till väljare. De har också kommunikation som kan vara riktad till tidningar, andra partier eller till sociala rörelser. Så även den externa kommunikationen kan ha olika syften. Men sen har man också en annan kommunikation internt till medlemmar och valarbetare. Sen har man kommunikation med sina meningsmotståndare. Det kan vara via debattartiklar, partiledarsnack, kommentarsfajter på sociala medier och så vidare.

Det finns även en poäng att ha kommunikation som är helt intern. Där man kan känna att man kommunicerar i trygga rum. Att man kan lufta tankar och oro, vare sig det handlar om samtiden eller om partiets eller föreningens politik eller verksamhet men helt enkelt kommunikation som man inte vill ska vara extern och tillgänglig för andra.

Om man anordnar aktioner, t.ex. civil olydnadsaktioner, då är det ofta man verkligen vill att saker ska ske i privata och trygga rum. Det har blivit hårdare tag mot aktivister. Men de som drabbas allra hårdast av detta är de som organiserar aktioner.

## Resiliens i kommunikation

Oavsett vad man valt för primär kommunikationsplattform är det bra att en rörelse, parti, organisation eller nätverk kan omgruppera på en annan plattform om det behövs. Om man då har all kommunikation på t.ex. Facebook (som jag tror kan vara den vanligaste plattformen) så blir man väldigt utsatt om ens Facebook-sida eller grupp helt plötsligt blir nedstängd eller shadowbannad.

Även om man använder öppna plattformar så kan de stängas plötsligt av överbelastningsattack eller intrång i systemen. För egen del använder jag gärna följande sätt att kommunicera med folk:

* Mastodon
* Blogg
* E-post
* Telefonnummer
* Signal
* Matrix
* XMPP (planerar i alla fall att göra det framöver)

Andra sociala medier-plattformar kan så klart också vara en backup.

Gällande fria och säkra chattalternativ så kan man läsa [det här inlägget](https://wiki.konstellationen.org/sv/guider/chat-alternativ) som beskriver Sigal och Matrix.

## Hur kan man hjälpa till?

Jag hoppas att big tech-plattformarna bränns ner och att fria, öppna, decentraliserade och säkra plattformar ersätter dem. Men det kommer nog inte göras närmaste tid. Det finns olika nivåer på hur "mycket" man vill förändras. Här är några varianter på roller jag hittat på. Olika sätt man kan hjälpa till i att skapa levande rum utanför big tech-plattformarna.

![Bild på många legogubbar](/images/lego.jpg)*Bild på många legogubbar*

* **Superaktivisten**. De som går före och lär sig de nya plattformarna och kan utbilda och välkomna nya. De skyr big tech-plattformarna som pesten. De påminner sitt parti, sina föreningar och nätverk att se till att information inte låses in på stängda plattformar som Facebook. De uppmanar till att skapa nyhetsbrev och egna hemsidor. I bästa fall kan de också hjälpa till.
* **Content- och trevlighetsskaparen**. Det finns en poäng att det skapas content och sociala nätverk som enart finns på de öppna plattformarna, som Mastodon eller olika chattgrupper på Matrix eller Signal. De håller igång samtalet. De delar intressanta inlägg. De interagerar med sina kamrater, fokuserar på att välkomna nya.
* **Den vanliga användaren**. Om det enda man känner man orkar med är att vara en helt vanlig användare så är det också en insats. Det är så mycket roligare att drifta tjänster om man vet att de är till nytta för folk.
* **Brobyggaren**. Det finns en poäng för vissa att vara kvar på de gamla big tech-plattformarna. Som informerar om hur man går tillväga för att komma igång på de nya öppna plattformarna. De kan också dela inlägg till bloggar och andra sajter som bidrar till att skapa nya nätverk utanför big tech-plattformarna.
* **Organisatören**. Mycket bra saker kan hända helt organiskt, "av sig självt", men ofta krävs det att några går sammans och funderar, planerar och anordnar events, startar plattformar, tjänster, hemsidor, osv, för att lyfta frågorna. Då kan man kandidera till styrelsen för föreningar som pysslar med sådant (som Kamratdataföreningen Konstellationen) eller vara aktiv i en av deras arbetsgrupper.
* **Moderatorn**. Det kan behövas arbete som att hjälpa till med moderering eller skriva nyhetsbrev. Detta behöver man sällan vara speciellt teknisk för men det är ett viktigt arbete som behöver göras.
* **Hackaren**. Ibland behövs det någon eller några som hjälper till att programmera nya system. Eller så behövs det någon admin som vet hur man driftar en system och installerar olika tjänster.

Det finns alltså väldigt många sätt man kan hjälpa till på. Alla måste inte göra allt. Men ofta kan man hitta något litet sätt.

## De första stegen för en förening

Här är de första sakerna jag tycker man ska göra:

* **Hemsida.** Det viktigaste man kan göra är att se till att ens förening har en hemsida. Det är inte dyrt att köpa en domän och webbhotell. Det kan man göra på t.ex. [Inleed](https://inleed.se/). Om man inte vill spendera en krona kan man skapa en gratisblogg på https://wordpress.com. Om man är lite tekniskt lagd kan man kika på hur man skapar hemsida med Static Site Generators (SSG). Info finns [här](https://wiki.konstellationen.org/sv/guider/skriva-blogginlagg-med-gitlab-pages) och [här](https://fritext.org/). Se till att din hemsida har stöd för RSS så att andra kan prenumerera på din hemsida och nya inlägg. De allra flesta hemsidor har detta inbyggt. Men värt att se till att det funkar. Läs mer om RSS [här](https://konstellationen.org/2024/10/31/facebook-till-rss/) och [här](https://wiki.konstellationen.org/sv/guider/rss-intro)
* **Starta ett nyhetsbrev.** Det allra enklaste tror jag är att dra igång nyhetsbrev med [MailChimp](https://mailchimp.com). Gratisvarianten är upp till 500 kontakter och man kan skicka 1000 mail per månad. Man kan också kika på [phplist](https://www.phplist.com) som använder ett open source-verktyg och där det verkar vara gratis för de första 500 kontakterna upp till 300 mail. [MailerLite](https://www.mailerlite.com/pricing-plans) verkar också vara gratis upp till 1000 kontakter och upp till 12 000. Om du är tekniskt lagd kan du drifta open source-verktyg själv som t.ex. [listmonk](https://listmonk.app/).
* **Starta ett konto på Mastodon.** Det finns [tusentals servrar/instanser](https://joinmastodon.org/servers) att välja på. Men för att förenkla för dig föreslår jag de här svenska instanserna: [Spejset](https://social.spejset.org/about) (Kamratdataföreningen Konstellationens instans), [Mastodonsweden](https://mastodonsweden.se/about), [Mastodon.nu](https://mastodon.nu/about). Om man vill köra på riktigt stora servrar kan man välja mellan t.ex. [mastodon.social](https://mastodon.social/about) (den "officiella" servern) eller [kolektiva.social](https://kolektiva.social/about) (en slags anarkistisk aktivistinstans). Sen kan man läsa dessa guider för att komma igång: [Kom igång med Mastodon](https://wiki.konstellationen.org/sv/guider/mastodon), [Hitta folk att följa](https://wiki.konstellationen.org/sv/guider/mastodon-hitta-folk) och [Tips på hur man har det trevligt med andra på Mastodon](https://wiki.konstellationen.org/sv/guider/tips-mastodon).
* **Korsposta.** Om det är så att ni nu kommunicerar via t.ex. Facebook så se till att lägga ut samma info på hemsida, mastodon och nyhetsbrev.

## Några peppiga länkar

Här är lite inspirerande och framåtblickande länkar:

* [Vänstern måste skapa och bygga upp sina egna mediekanaler](https://blog.zaramis.se/2024/11/07/vanstern-maste-skapa-och-bygga-upp-sina-egna-mediekanaler/) från Anders Svenssons blogg
* [Gästbloggare - Från Facebook till algoritmfritt flöde](https://konstellationen.org/2024/10/31/facebook-till-rss/) från Kamratdataföreningen Konstellationens hemsida
* [Ett annat internet är möjligt](https://konstellationen.org/2023/06/12/ett-annat-internet-ar-mojligt/) från Kamratdataföreningen Konstellationens hemsida
* [Om att bygga publik, nätverk och räckvidd](https://www.turist.blog/2024-07-25-om-att-bygga-publik-natverk-och-rackvidd/) av Turist/Blog
* [Minst tre brott har begåtts av sociala medier!](https://www.turist.blog/2024-06-05-minst-tre-brott-har-begatts-av-sociala-medier/) av Turist/Blog
* [Mysigt med Mastodon och interoperabilitet](https://www.turist.blog/2023-01-06-mysigt-med-mastodon-ochinteroperabilitet/) av Turist/Blog
* [Hur får man en behaglig nätvistelse?](https://gnomvid.se/2023/08/03/hur-far-man-en-behaglig-natvistelse/) av Gnomvid

## Call to action

Om man trots detta blogginlägg inte vet vad man ska göra kan man:

* [Bli medlem](https://konstellationen.org/bli-medlem/) i Kamratdataföreningen Konstellationen eller [skänka en gåva](https://konstellationen.org/stod-oss/).
* [Bli medlem](https://www.dfri.se/bli-medlem/) i Föreningen för Digitala Fri- och Rättigheter (DFRI) eller [skänka en gåva](https://www.dfri.se/donera/).

Lycka till!

![Bild på ett gäng arbetare som håller en stor nätverkskabel](/images/konstellationen-klibba.png)*Bild på ett gäng arbetare som håller en stor nätverkskabel*