---
title: Länkparty - Open source AI, zrok istället för ngrok, PWA-revolution och Big Tech-böter
image: network-chain.jpg
description: Ett gäng intressanta länkar om teknik jag samlat på mig senaste tiden
date: 2024-01-12 09:00
---

Länkparty:

* [zrok - som ngrok men open source](https://zrok.io/)
* [llamafile: bringing LLMs to the people, and to your own computer](https://future.mozilla.org/blog/introducing-llamafile/)
* [GPT4All - A free-to-use, locally running, privacy-aware chatbot. No GPU or internet required.](https://gpt4all.io/index.html) 
* [Why Are Tech Reporters Sleeping On The Biggest App Store Story?](https://infrequently.org/2024/01/the-web-is-the-app-store/)
* [Big Tech has already made enough money in 2024 to pay all its 2023 fines](https://proton.me/blog/big-tech-2023-fines-vs-revenue)

## Zrok istället för ngrok

Om du är webbutvecklare och ska tillgängliggöra en grej som du har lokalt på datorn och inte pallar lägga ut grejer på en server. då har du säkert kommit i kontakt med [ngrok](https://ngrok.com/). Du tillgängliggör en port på din dator och ngrok skapar en tunnel och skickar tillbaka en extern url som du kan skicka till folk.

Mycket smidigt att ha men dessvärre inte open source.

Nu finns det ett fritt alternativ som heter [zrok](https://zrok.io/). Det är baserat på något som kallas [OpenZiti](https://openziti.io/) som syftar till att "bringing zero trust networking principles directly into any application". 

Spännande! Hoppas att zrok är decentraliserat så att man kan använda zrok med fler hosting providers är zrok.io

## Open source, lokal och internetlös AI

Det har skrivit mycket om AI senaste åren. De flesta är säkert lite trötta på det.

Själv så har jag varit rätt kluven till AI. Mest för att det känns som att mer makt flyttas till stora företag som skapar monopol som man blir beroende av. Sen finns det en massa problem med AI som har med arbetsmiljö att göra för att träna AI:n och bristerna med AI gällande fördomar, osv.

Därför blev jag väldigt glad när jag läste att Mozilla släppte [llamafile](https://github.com/Mozilla-Ocho/llamafile). I sitt [blogginlägg](https://future.mozilla.org/blog/introducing-llamafile/) skriver Mozilla:

> Introducing the latest Mozilla Innovation Project llamafile, an open source initiative that collapses all the complexity of a full-stack LLM chatbot down to a single file that runs on six operating systems. Read on as we share a bit about why we created llamafile, how we did it, and the impact we hope it will have on open source AI.
>
> Today, most people who are using large language models (LLMs) are doing so through commercial, cloud-based apps and services like ChatGPT. Many startups and developers are doing the same, building applications or even entire companies on top of APIs provided by companies like OpenAI. This raises many important questions about privacy, access, and control. Who is “listening” to our chat conversations? How will our data be used? Who decides what kinds of questions a model will or won’t answer?

Men Mozilla är inte de enda som gör open source AI. [GPT4All](https://gpt4all.io) gör något snarlikt.

[HuggingFace](https://huggingface.co/datasets/nomic-ai/gpt4all-j-prompt-generations) har fria dataset. De har även [HugginChat](https://huggingface.co/chat) online om man vill använda den istället för OpenAI:s chatt.

Man pratar ofta om [Large Language Models (LLM)](https://en.wikipedia.org/wiki/Large_language_model) när man pratar om AI för text. Det är enkelt uttryckt så att LLM:s lär sig statistiska relationer av text genom att tränas på en massa data. Man använder sig av artificiella neuronnät (Artificial neural network) för detta.

Ofta nämns också [LLaMA (Large Language Model Meta AI)](https://en.wikipedia.org/wiki/LLaMA) som är en familj av LLM:s som Meta släppt. Men dataseten är inte helt open source. Det finns andra LLM:s som är open source som [Mistral 7B, GPT-J och Pythia](https://en.wikipedia.org/wiki/Open-source_artificial_intelligence).

Det finns massvis med olika slags verktyg för AI. Här är några tips:

* [Open Source AI Projects and Tools to Try in 2023](https://www.freecodecamp.org/news/open-source-ai/)
* [10 Top Open Source AI Platforms and Tools to Try Today](https://blog.hubspot.com/marketing/open-source-ai)

## Webb-appar utmanar app stores

I artikeln [Why Are Tech Reporters Sleeping On The Biggest App Store Story?](https://infrequently.org/2024/01/the-web-is-the-app-store/) så tar författaren upp webbläsaren som en stor utmanare till appar och app stores.

Webbläsare har under flera års tid kunnat göra i princip det som vanliga native-appar kan göra (appar för Android och iOS t.ex.) och i och med så kallade [Progressive Web Apps (PWA)](https://en.wikipedia.org/wiki/Progressive_web_app) så blir skillnaden ännu mindre i och med att man kan installera webb-appar så att de för användaren ser ut och beter sig som vanliga native-appar.

Från artikeln:

> The tech news is chockablock with antitrust rumblings and slow-motion happenings. Through eagle-eyed press coverage, regulatory reports, and legal discovery the shady dealings of Apple and Google's app stores are now comprehensively documented and pressure for change has built to an unsustainable level. Something's gotta give.
>
> This is the backdrop to the biggest app store story nobody is writing about: on pain of steep fines, gatekeepers are opening up to competing browsers. This, in turn, will enable competitors to replace app stores with directories of Progressive Web Apps. Capable browsers that expose web app installation and powerful features to developers can kickstart app portability, breaking open the mobile duopoly.

För webbutvecklare som vill lära sig mer kan man kolla på [Mozillas hemsida](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps).


## Böterna mot Big Tech är minimala

I blogginlägget [Big Tech has already made enough money in 2024 to pay all its 2023 fines](https://proton.me/blog/big-tech-2023-fines-vs-revenue) så skriver Proton att Big Tech-företagen betalar sina böter med kaffepengar i sammanhanget:

> Last year, Big Tech companies (Alphabet, Amazon, Apple, Meta, and Microsoft) received about $3.04 billion in fines for breaking laws on both sides of the Atlantic. As of seven days and three hours into 2024, they had already earned enough revenue to pay it all off.

Det här innebär att om man vill få storföretagen att ändra sitt beteende så kommer låga böter inte verka avskräckande. Vad behövs då enligt Proton?

> If we want to take back control of the internet, several things must happen. 
>
> * First, governments must combat Big Tech. They must impose fines that get Big Tech’s attention for breaching user privacy and locking competitors out of the market.
> * But fines can only be part of the solution. Regulators, armed with strong antitrust legislation, must strongly challenge mergers that create massive monopolies. They must encourage competition and empower people to choose the service that is best for them. 
