---
title: Lite kort om Mastodon
image: mastodon_wallpaper.png
description: Ett öppet och federerat alternativ till Twitter men vad betyder det?
date: 2022-11-08 13:55
---

Helt plötsligt rasslade det till och en hel drös personer blev trötta på Twitter och stack till det öppna och decentraliserade alternativet [Mastodon](https://en.wikipedia.org/wiki/Mastodon_(software)). Det var efter att Elon Musk tagit över Twitter och [kickat halva personalstyrkan](https://uk.pcmag.com/social-media/143711/mastodon-reaches-1-million-active-users-after-tumultuous-week-at-twitter) som det verkligen tog fart.

Politiska debattörer och tyckare tog skuttet bara för någon dag sen. Exempelvis kan vi se att senaste fyra dagarna har [Anna Ardin](https://mastodon.nu/@annaardin), [Kawa Zolfagary](https://mastodon.nu/@zolfagary), [Helle Klein](https://mastodon.nu/@helle), [Anders Lindberg](https://mastodon.nu/@anderslindberg), ["rektor Linnea"](https://mastodon.nu/@rektor_linnea), [Andreas Gustavsson](https://mastodon.social/@a_gustavsson) och [Greta Thunberg](https://mastodon.nu/@gretathunberg) anslutit sig till Fediversum.

När Greta Thunberg anslöt sig så gick den snabbt ökande servern/instansen mastodon.nu ner:

![Mastodon.nu sänkes efter högt tryck](/images/mastodon_admin.png)

Vänstermedia var snabba på att hoppa på tåget. Både [Dagens ETC](https://mastodon.se/@dagensetc) och [Arbetaren](https://mastodon.nu/@tidningen_arbetaren) har gått med. 

Det skrivs mycket i internationella medier om flykten från Twitter till Mastodon. Twitter kommer med stor sannolikhet finnas kvar och ha många användare. Den stora nyheten är snarare att en decentraliserad och öppen sociala medier-plattform nu verkligen fått luft under vingarna.

Jag har tidigare skrivit ett blogginlägg om att [Bryta sig loss från big tech sociala medier](https://samuels.bitar.se/bryta-sig-loss/). Det är många som tröttnat på att vinstdrivande amerikanska företag ska ha kontroll över information och kommunikation. Det är också en risk om vänstern inte skapar sina egna alternativ. Robin Zachari från Skiftet förordar därför vänstern att ta kontrollen över kommunikationen istället för att använda centraliserade lösningar som Facebook. Han förespråkar bland annat nyhetsbrev.

## Vad är speciellt med Mastodon?

Vad är det då som gör att Mastodon skiljer sig från Twitter. Några saker:

* Programvaran som driver det är öppen och fri mjukvara som alla kan använda gratis
* Mastodon är ett federat nätverk
* Kulturen på Mastodon är annorlunda, inte lika "toxic" som på Twitter

Men hur ser det ut då? Jo, så här t.ex.

![Mastodon i webbvyn på desktop](/images/mastodon_desktop.png)

Eller på mobilen:

![Mastodon på Android i klienten Tusky](/images/mastodon_tusky_mobile.png)

Det ser alltså rätt så mycket ut som Twitter i sitt gränssnitt.

## Vad betyder federerat

Det enklaste sättet att förklara federation och Mastodon är att likna det med e-post. Det finns en massa olika mailservrar som är helt fristående men som kan prata med andra mailservrar. Eller på ett annat sätt: användaren bob som har ett mailkonto på gmail.com kan maila till alice som har ett mailkonto på hotmail.com. Alltså bob@gmail.com kan maila alice@hotmail.com.

På samma sätt är det med Mastodon. Användaren @samuel som har ett mastodonkonto på mastodon.se kan följa och skicka meddelande till användaren @gargron som har ett mastodonkonto på mastodon.social. Alltså, [@samuel@mastodon.se](https://mastodon.se/@samuel) kan göra poster som läses av [@gargron@mastodon.social](https://mastodon.social/@Gargron).

Man pratar om centraliserade nätverk, decentraliserade och federerade. Federerade nätverk är en sorts decentraliserade nätverk. För de av oss som kommer ihåg IRC så är det ett exempel på decentraliserade nätverk. Det fanns många olika IRC-servrar man kunde ansluta till. Men de kunde dock inte prata med varandra. Federerade nätverk kopplar samman de olika servarna så att användarna på de olika servrarna kan prata med alla, inte bara med dem på samma server.

![Centraliserat vs federerat](/images/federated.png)

## Hur hittar jag folk?

Ja, Mastodon har något av en inlärningskurva. Men i ärlighetens namn så var det samma sak med Twitter när det var nytt. 

Men hur gör man då för att hitta intressanta personer? Tja, du kan söka på hashtags du är intresserade av, t.ex. #svpol eller #klimat. Och så följer du de personer där som skriver intressanta saker. Du kan också titta på din mastodon-instans vilka som finns där. Din klient har en flik för att hitta flödet för din lokala instans. På Tusky på Android så klickar man på ikonen med två figurer:

![Flödet för din instans](/images/mastodon_tusky_local.png)

Det finns dock lite verktyg som kan hjälpa dig att migrera från Twitter, t.ex. [Twitodon](https://twitodon.com/).

Du kan också börja med att följa mig på Mastodon och se vilka jag följer och vilka boosts ("retweets") jag gjort. Jag finns här: [@samuel@mastodon.se](https://mastodon.se/@samuel).

## Läs mer

Många har skrivit mycket om Mastodon. Här är ett gäng artiklar:

* [Mastodon quick start guide](https://blog.joinmastodon.org/2018/08/mastodon-quick-start-guide/)
* [Mastodon på Wikipedia](https://en.wikipedia.org/wiki/Mastodon_(software))
* [What is Mastodon — the hottest Twitter alternative explained](https://www.tomsguide.com/reference/what-is-mastodon-why-people-are-fleeing-elon-musks-twitter-for-this-social-network)
* [How to Find Your Twitter Friends on Mastodon](https://www.wired.com/story/how-to-find-twitter-friends-on-mastodon/#intcid=_wired-bottom-recirc_e8b180bf-5ed0-404b-8897-4f5e9d74ba2d_entity-topic-similarity-v2-reranked-by-vidi)
* [Mastodon Is Like Twitter Without Nazis, So Why Are We Not Using It?](https://www.vice.com/en/article/783akg/mastodon-is-like-twitter-without-nazis-so-why-are-we-not-using-it)

Lycka till!




