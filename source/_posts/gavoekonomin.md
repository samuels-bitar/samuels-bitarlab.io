---
title: Krönika - Gåvoekonomin gör mig delaktig 
image: digital-gift.png
image_description: Bild av <a href="https://pixabay.com/users/mohamed_hassan-5229782/">Mohamed Hassan</a> från <a href="https://pixabay.com">Pixabay</a>
description: En krönika om gåvoekonomin på nätet
date: 2024-08-13 20:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/gaavoekonomin-goer-mig-delaktig).

---

Jag läste nyss ut Cory Doctorows sci-fi-roman [Walkaway](https://en.wikipedia.org/wiki/Walkaway_(Doctorow_novel)) som är en slags utopi och dystopi på samma gång. Den handlar om en massrörelse för att lämna ett hyperkapitalistiskt samhälle med klimatförändringar och miljöförstörelse för att leva i anarkistiska gemenskaper utanför samhället. Genom förbättrade 3D-skrivare, ömsesidig hjälp och gåvoekonomi så kan de leva gott utanför samhället, något som ägandeklassen inte gillar och de försöker därför krossa rörelsen med våld.

Även vi lever som bekant i ett kapitalistiskt samhälle och det kan vara svårt att se att gåvoekonomi och ömsesidig hjälp skulle fungera i någon större skala. Men det finns gott om exempel även i nutid om hur man kan ta del av något utan krav på motprestation. Fri progamvara som operativsystemet Linux och kunskapsdatabaser som Wikipedia eller festivalen Burning Man är nutida exempel där man inte behöver betala för att ta del av något, men donationer och gåvor uppmuntras.

Gåvoekonomin utvecklas även på nätet. Många poddar, mastodoninstanser (decentraliserat alternativ till Twitter/X) och fri programvaruprojekt förlitar sig på donationer för att täcka omkostnaderna. Det finns nu en uppsjö av olika plattformar där man kan stödja artister, designers, poddare, utvecklare och andra skapare där de populäraste är [Liberapay](https://en.wikipedia.org/wiki/Liberapay), [OpenCollective](https://en.wikipedia.org/wiki/Open_Collective), [Mirlo](https://mirlo.space/), [Patreon](https://www.patreon.com/), [Ko-Fi](https://ko-fi.com/) och [Buy Me a Coffee](https://buymeacoffee.com/). Plattformen Bandcamp säljer musik och kan därför inte klassas som gåvor. Jag köpte för några veckor sen alla album av artisten [LukHash](https://lukhash.bandcamp.com/). Det var väldigt billigt men det gick att betala mer för att stödja artisten mer, så jag gjorde det.

Jag prenumererar på en rad olika vänstertidningar men ser det inte som att jag köper produkter jag ska konsumera. Jag ser det som att jag stödjer viktiga verksamheter. Den känslan blir ännu större av att donera till en verksamhet som inte kräver pengar. Att komma bort från tankesättet [vad fan får jag för pengarna?](https://www.aftonbladet.se/nyheter/a/kqz5k/svenskt-naringslivs-ordf-vad-fan-far-jag) gör mig mer benägen att faktiskt göra mig av med dem.

Så nästa gång du skänker en slant till Wikipedia eller din favoritpodd, kom ihåg att du bidrar till att stärka gåvoekonomin.