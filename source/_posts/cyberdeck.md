---
title: Cyberdeck för att skriva i solen
image: laptop-typewriter.jpg
image_description: Bild från <a href="https://pixabay.com/illustrations/laptop-typewriter-screem-paper-5496983/">Pixabay</a>
description: Idéer på olika datorer man kan bygga för att kunna skriva i solen
date: 2024-07-28 17:00
---

Som jag skrev i [förra inlägget](/tjanstledig-skriva/) så ska jag vara tjänstledig för att läsa en kurs i kreativt skrivande.

Just nu är det semester och solen skiner utomhus. Egentligen vill jag sitta där ute och götta mig med en kopp kaffe eller ett glas saft men jag vill samtidigt blogga! Hur ska man kombinera detta? Jo, med en [cyberdeck](https://hackaday.com/category/cyberdecks/).

## Vad är en cyberdeck?

En cyberdeck är en slags moddad dator som skiljer sig från en vanlig laptop. Konstigt nog hittar jag ingen wikipedia-sida för det. Men själv skulle jag säga att cyberdeckbyggen idag är inspirerade av specialiserade datorer från cyberpunk-litteratur som William Gibsons [Neuromancer](https://en.wikipedia.org/wiki/Neuromancer).

Det kan vara datorer som är wearables, alltså sitter på kroppen (t.ex. Fallout-spelens [Pip-Boy](https://fallout.fandom.com/wiki/Pip-Boy) som är en dator på handleden), eller datorer med extra skärmar eller speciella på något sätt.

Här kommer ett gäng olika cyberdecks.

### Raspberry pi 4 CyberDeck

![Raspberry pi 4 CyberDeck](/images/cyberdeck1.jpg)
[Länk till projektet](https://www.printables.com/de/model/310480-raspberry-pi-4-cyberdeck).

### Adafruit CYBERDECK HAT for Raspberry Pi 400

![Adafruit CYBERDECK HAT for Raspberry Pi 400](/images/cyberdeck2.jpg)
[Länk till bild](https://www.flickr.com/photos/adafruit/51023951112/).

### TRS-80-inspirerad Cyberdeck

![TRS-80 Model 100 Inspires Cool Cyberdeck Build, 40 Years Down The Line](/images/cyberdeck3.jpg)
[Länk till blogginlägg](https://hackaday.com/2023/03/04/trs-80-model-100-inspires-cool-cyberdeck-build-40-years-down-the-line/).

Det finns många många fler! Nätsök på cyberdeck eller kolla på [Hackadays tagg cyberdecks](https://hackaday.com/category/cyberdecks/).


## Färdiga produkter för fokuserat skrivande

Det finns redan lösningar för distraktionsfria laptops. Några kan användas i solen direkt, andra är mer som en avskalad laptop med vanlig skärm. Men många är rätt så dyra och kostar runt 500 USD. Vissa av de här produkterna är ännu inte i produktion.

### FreeWriter

![FreeWriter traveller](/images/freewriter1.jpg)

[FreeWrite](https://getfreewrite.com/) är ett företag som säljer färdiga produkter. Finns många olika modeller.

### BYOK

![BYOK](/images/byok.jpg)

En liten enhet med LCD-display som man kan koppla tangentbord till. [Blogginlägg här](https://boingboing.net/2024/04/18/tiny-e-ink-display-hooks-up-to-a-keyboard-and-shows-only-one-thing-four-lines-of-text.html).

### Alpha portable

![Alpha portable](/images/alpha-portable.jpg)

En enhet med LCD-display. [Läs mer här](https://thegadgetflow.com/product/alpha-portable-distraction-free-writing-device-helps-you-focus-on-daily-writing-habits/).

### Pomera DM 30

![Pomera DM 30](/images/pomera-dm30.jpg)

En japansk enhet med tangentbord och e-ink-skärm som är ihopvikbar. Läs mer [här](https://boingboing.net/2019/11/16/i-found-the-perfect-distractio.html) och [här](https://goodereader.com/blog/reviews/digital-memo-pomera-dm30-e-ink-typewriter-review).

### Scripto

![Scripto](/images/scripto.jpg)

[Scripto](https://www.hackster.io/news/scripto-is-a-distraction-free-raspberry-pi-powered-writing-device-65916a3e5bb7) är en projekt som ännu inte verkar produceras men som ska fokusera på distraktionsfritt skrivande.

## DIY-projekt för distraktionsfritt skrivande

Det finns även en uppsjö av hack och DIY-produkter som antingen bygger upp något själv eller hackar befintlig hårdvara.

### ZeroWriter

![ZeroWriter](/images/zerowriter.jpg)

Läs mer [här](https://liliputing.com/zerowriter-open-source-diy-e-ink-typewriter-that-costs-about-200-or-less-to-build/) eller på [hackaday.io](https://hackaday.io/project/193902-zerowriter).

### TypeWryter

Utvecklaren utgick från ZeroWriter och gjorde anpassningar.

![TypeWryter](/images/typewryter.jpg)

Läs mer på [github](https://github.com/RyWhal/TypeWryter) och på [reddit](https://www.reddit.com/r/writerDeck/comments/1alauai/my_slate_writerdeck_i_call_it_my_typewryter/).

### MicroJournal

Utvecklaren har gjort flera olika iterationer. Man kan läsa om alla på [github-repot](https://github.com/unkyulee/micro-journal).

![MicroJournal rev 2](/images/microjournal-rev2.png)
[Micro Journal - Rev. 2](https://github.com/unkyulee/micro-journal/blob/main/micro-journal-rev-2-raspberypi/readme.md)

![MicroJournal ESP32](/images/microjournal-esp32.jpg)
[Micro Journal - ESP32 writerDeck](https://github.com/unkyulee/micro-journal?tab=readme-ov-file#third-iteration-immeidate-power-on-and-writing)

![Micro Journal Rev.6](/images/microjournal-tindie.jpg)
[Länk till Tindie-shop](https://www.tindie.com/products/unkyulee/micro-journal-rev6-vivian-in-new-york/).

### Beepy

Beepy har samma formfaktor som en BlackBerry men kör Linux. Egentligen vet jag inte om jag skulle använda en sådan här för man behöver skriva med tummarna. Men den var för söt för att inte ha med.

![Beepy](/images/beepy_outside.jpg)

Läs mer på [hackaday](https://hackaday.com/2023/08/07/review-beepy-a-palm-sized-linux-hacking-playground/) om Beepy. Och kika på [glowfire](https://github.com/hack-shack/glowfire) för någon slags miljö.

### KoboWriter

Det finns även flera projekt som går ut på att hacka eboks-läsare och koppla tangentbord till. KoboWriter tar en Kobo-läsplatta.

![KoboWriter](/images/kobowriter.jpg)
[Läs mer på github-repot](https://github.com/olup/kobowriter).

### Ultimate Writer

En enhet som använder en e-ink-skärm som är något större. Dock i en väldigt stor låda i övrigt :)

![Ultimate Writer](/images/ultimate-writer.jpg)

Läs mer på [github-repot](https://github.com/picnoir/ultimate-writer)

### PaperTTY

PaperTTY är ett projekt för att koppla in en e-ink-skärm som terminal till en Linux-burk (t.ex. en Raspberry PI).

![PaperTTY](/images/papertty_detail.jpg)

Läs mer på [hackaday](https://hackaday.com/2018/08/14/run-a-linux-terminal-on-cheap-e-ink-displays/) och på [github-repot](https://github.com/joukos/PaperTTY).

## Subreddit w/writerDeck

Efter lång research såg jag att det fanns en subreddit som heter just [r/writerDeck](https://www.reddit.com/r/writerDeck/) där det är cyberdecks men fokuserade på skrivande. Några bra inlägg var:

* [A List of Every DIY WriterDeck](https://www.reddit.com/r/writerDeck/comments/ux4s7o/a_list_of_every_diy_writerdeck/)
* [DIY WriterDecks](https://www.writerdeck.org/list-of-diy-writerdecks.html)
* [Commercially Available WriterDecks](https://www.reddit.com/r/writerDeck/comments/uw6vzw/commercially_available_writerdecks/)

## Slutsatser

Det här är bara i början av mitt lilla projekt. Men det finns redan rätt mycket jag har kommit fram till:

* Det finns färdiga bra projekt som man kan köra på eller utgå ifrån. Närmast till hand ligger nog **TypeWryter** som ju kör Linux och har e-ink-skärm i tillräckligt stor storlek. Eller **Ultimate Writer** som har en rejält stor e-ink-skärm. Men jag kommer behöva fixa ett bätre case.
* Att bara beställa hem en e-ink-skärm som stöds av **PaperTTY** är nog det bästa sättet att börja på.
* Det vore najs att hitta fler LCD-skärmar som man kan använda i solljus. De uppdaterar sig snabbare än e-ink-skärmar. Men detta kanske blir mindre viktigt i och med att många e-ink-skärmar har bättre förmåga att uppdateras snabbt.
* Jag kommer behöva fixa något tangentbord. Jag gillar tangentbordet som **ZeroWriter** har. Jag kanske får göra ett blogginlägg för bara tangentbord :) Men annars kan man köpa random bluetooth-tangentbord.
* Jag kommer med största sannolikhet att köra en Raspberry Pi Zero W. Dels för att den passar bra, kör Linux som standard, har WiFi och är liten. Och så har jag redan en. ESP32-varianten med **Micro Journal** var visserligen kul men kommer vara för begränsande för mig.

Har du du tips? Peta på mig på [mastodon](https://social.spejset.org/@samuel).