---
title: Chat Control lever igen
image: surveillance-cctv.png
image_description: Bild på datorskärm och övervakningskamera av <a href="https://pixabay.com/users/mohamed_hassan-5229782/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=6078897">Mohamed Hassan</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=6078897">Pixabay</a>

description: Vi trodde att massövervakningsförslaget Chat Control blivit avklarat men det lever igen
date: 2024-05-27 08:00
---

I oktober så hände det otroliga: alla partigrupper i EU-parlamentet slutade upp bakom ett [gemensamt förslag från EU-parlamentet](https://www.europaportalen.se/2023/10/genombrott-i-kontroversiellt-forslag-om-kampen-mot-barnporr-pa-natet) som sade nej till massövervakningsdelarna i Chat Control:

> En av förslagets högljuddaste kritiker har varit den tyske piratpartisten Patrick Breyer som förhandlar för den gröna gruppen.
>
> – Vi [tog] bort de omtvistade och problematiska delarna, såsom masskanning av hela tjänster, även för totalsträckskrypterade tjänster, obligatorisk ålderskontroll för alla kommunikationstjänster eller att utesluta alla barn under 16 år från vanliga appar. Istället lade vi till mer effektiva, domstolssäkra och rättighetsrespekterande åtgärder till det ursprungliga förslaget för att hålla barn säkra på nätet, sade Patrick Breyer.
>
> Den tyska vänsterpartisten Cornelia Ernst gick hårt åt kommissionens ursprungsförslag.
>
> – Detta är ett slag i ansiktet på kommissionen [som] inte fokuserade på att skydda barn utan ville ha massövervakning, sade Ernst.
>
> Enligt Zarzalejos kommer ingen massövervakning ske om deras linje blir lag och att totalsträckskrypterad kommunikation undantas från de föreläggande som domstolar han utfärda nätföretag att genomföra.
> 
> – Det sker ingen masskanning eller allmän övervakning av webben. Det finns ingen urskillningslös skanning av privat kommunikation eller bakdörrar för att försvaga kryptering, sade Javier Zarzalejos.

EU-parlamentet ville istället satsa på förebyggande av barnporr på olika sätt.

I Sverige så behövs också mer resurser till Polisen att kunna utreda barnporr. [Tusentals tips](https://www.svt.se/nyheter/inrikes/tusentals-bilder-med-misstankta-overgrepp-har-samlats-pa-hog-polisen-startar-sarskild-insats) samlades på hög och de utredningar som väl sker [tar lång tid](https://www.svt.se/nyheter/lokalt/uppsala/hundratalet-barnpornografiutredningar-pa-hog-utredning-tog-nastan-fyra-ar).

Jag själv [trodde att Chat Control hade fallit](/chatcontrol-faller-eller/) men ropade hej för tidigt. Nu har det belgiska ordförandeskapet i EU gjort nytag och tagit fram ett [nytt kompromissförslag](https://www.svd.se/a/W0Oqpg/nytt-forsok-att-enas-om-chat-control) för att få EU-länderna att enas om massövervakningsförslaget Chat Control. 

Den tyska piratpartisten Patrick Breyer är fortfarande kritisk och säger:

> Det utläckta belgiska förslaget innebär att huvuddelen av EU-kommissionens extrema och aldrig tidigare skådade chat-kontroll skull införas oförändrat.

Om förslaget får tillräckligt stöd blir det sedan förhandlingar mellan Ministerrådet, EU-parlamentet och EU-kommissionen. Tyvärr blir [ofta resultatet av förhandlingar sämre](https://maxandersson.eu/spannande-uppgifter-om-chatcontrol/) än det som EU-parlamentet kommit fram till i förhandlingarna.

**Och hur ställer sig den svenska regeringen i frågan? Detta är fortfarande oklart.**

Jag har bloggat om det tidigare [här](/vad-blir-framtidens-brott/), [här](/progressiva-bor-oroa-sig/) och [här](/chatcontrol-faller-eller/).

En kort sammanfattning av den huvudsakliga kritiken kan man läsa på [Kamratdataföreningen Konstellationens hemsida](https://konstellationen.org/2023/09/14/chatcontrol-pressmeddelande/) där jag är ordförande.

Det finns också massvis med kritiska artiklar som man kan hitta på [chatcontrol.se](https://chatcontrol.se). Här är ett litet urval:

* [Journalistförbundet: ”Chattkontroll hotar källskyddet” ](https://www.journalisten.se/nyheter/journalistforbundet-chattkontroll-hotar-kallskyddet)
* [Utgivarna uppmanar regeringen att protestera mot Chat control](https://www.journalisten.se/nyheter/utgivarna-uppmanar-regeringen-att-protestera-mot-chat-control)
* [Nej, vi behöver inget övervakningssamhälle](https://www.dagensarena.se/opinion/nej-vi-behover-inget-overvakningssamhalle/)
* [Johanssons förslag hotar privatlivet, inte pedofiler](https://www.expressen.se/ledare/johanssons-forslag-hotar-privatlivet-inte-pedofiler/)

## Vänsterpartiet säger nej till Chat Control

Det finns tre riksdagspartier som konsekvent sagt nej till Chat Control: V, MP och C.

Det är bara Socialdemokraterna som sagt ja till Chat Control i Sveriges riksdag och i EU. M, SD, KD och L har haft bättre hållning i EU-parlamentet men är otydliga i Sverige. Läs mer detaljerat på [chatcontrol.se](https://chatcontrol.se).

Vänsterpartiet har publicerat sin ståndpunkt sin hemsida [2023-04-06](https://www.vansterpartiet.se/nyheter/vansterpartiet-sager-nej-till-chat-control/) och de har fortsatt att ta ställning mot förslaget, senast under justitie­utskottets sammanträde [2023-09-14](https://handlingar.se/request/protokoll_fran_justitieutskottet#incoming-3279).

Vänsterpartiet hade en EU-parlamentariker under mandatperioden 2019-2024: Malin Björk. Hon satt i Vänstergruppen i Europa­parlamentet. Hon kritiserade förslaget i en debatt i [SVT Forum](https://www.svt.se/nyheter/svtforum/brottsbekampning-med-chat-control) (2023-04-26) och publicerade en [kritisk debattartikel](https://www.sydsvenskan.se/2023-06-04/chat-control-oppnar-for-massovervakning-av-sallan-skadat-slag) i Sydsvenskan tillsammans med andra vänsterpartister (2023-06-04).

Vänsterpartiet nämner EU-förslagen om övervakning utan brottsmisstanke i dokumentet [Vänsterpartiets prioriteringar i EU-valet 2024](https://www.vansterpartiet.se/wp-content/uploads/2024/05/v-prioriteringar-eu-val-2024.pdf):

> Vi ser förslag att övervaka länders invånare utan brottsmisstanke. Vänsterpartiet arbetar för att bryta den nedåtgående spiralen.

Vänsterpartiet nämner Chat Control 2.0 specifikt i ett av svaren på ”Vad tycker Vänsterpartiet om… A-Ö” som de länkar till från sin webbsida [EU-val 2024](https://www.vansterpartiet.se/euval2024/politik/).

Vänsterpartiets lista toppas av Jonas Sjöstedt som var [öppet kritisk mot ursprungsförslaget på Twitter](https://twitter.com/jsjostedt/status/1717427739626532942) (2023-10-26). Inför Europaparlamentsvalet bekräftade han sin syn på Chat Control 2.0 i [ett inlägg på Bluesky](https://bsky.app/profile/jonassjostedt.bsky.social/post/3kt6kive7n22r) (2024-05-23) där han skrev att ”massövervakning utan brottsmisstanke hör inte hemma i en rättsstat”.

## Rösta mot massövervakning i EU-valet

Nu är det EU-val. Själva valdagen är 9 juni men du kan redan förtidsrösta nu. Se till att rösta för ett parti som står upp för rätten till personlig integritet och skyddet av demokrati, pressfrihet och rättssäkerheten.