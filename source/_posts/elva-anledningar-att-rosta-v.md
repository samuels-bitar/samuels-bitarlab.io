---
title: Elva anledningar att rösta på Vänsterpartiet
image: valgranskning.png
description: Vänsterpartiet rankas bäst i många granskningar
date: 2022-09-04 22:20
---
Vänsterpartiet är ett [socialistiskt och feministiskt parti på ekologisk grund](https://www.vansterpartiet.se/resursbank/partiprogram/). Vänsterpartiet driver en politik som minskar klyftorna, satsar på välfärden, stoppar mäns våld mot kvinnor, är en tydlig röst mot rasismen, säkerställer att klimatomställningen sker och att den är rättvis. Plus mycket mycket mer. 

Vänsterpartiet är det självklara valet för den som vill se en rödgrön regering som driver vänsterpolitik, satsar på välfärden och genomför en rättvis klimatomställning.

Här är en liten sammanfattning av vad andra säger om Vänsterpartiet.

## jagvillhabostad.nu

![jagvillhabostad.nu:s granskning](/images/jagvillhabostadnu.png)*jagvillhabostad.nu:s granskning*

jagvillhabostad.nu är en ideell organisation som kämpar för ungas rätt till en trygg bostad. De har genomfört en [granskning av partiernas politik](https://jagvillhabostad.nu/valet2022/). Vänsterpartiet hamnar högst i rankingen med 9,5. I botten av rankingen hamnar Centerpartiet och Moderaterna med 1,5 poäng, och Liberalerna med 0,5 poäng.

## Hyresgästföreningen

![Hyresgästföreningens granskning](/images/hyresgastforeningen.jpeg)*Hyresgästföreningens granskning*

Hyresgästföreningen har granskat riksdagspartiernas bostadspolitik. Vänsterpartiet får bäst betyg. (S och MP kommer på andra plats). Frågorna de ställer är:

* Ekonomiskt stöd och mer inflytande för hyresgäster vid renovering?
* Ökat statligt stöd till bostadsbyggande med rimliga hyror?
* Utjämna skatterna mellan hyrt och ägt boende?

[Läs mer här](https://www.hyresgastforeningen.se/var-politik/hyresgasternas-val/vad-tycker-partierna-i-bostadspolitiken/).

## Cykelfrämjandet

![Cykelfrämjandets valgranskning](/images/cykelframjandet.jpg)*Cykelfrämjandets valgranskning*

Vänsterpartiet får full poäng av Cykelfrämjandet. De bad samtliga riksdagspartier att ta ställning till tio cykelfrämjande förslag:

* Ökade resurser till cykling i den nationella planen för transportinfrastruktur
* Sänk bashastigheten i tätort till 30 km/h
* Prioritera hållbar mobilitet framför stillastående trafik
* Inför ett nationellt investeringsstöd till nationella och regionala cykelleder
* Inför aktiv mobilitetsundervisning i grundskolan
* Enklare regler i korsningar
* Tillåt cykling mot enkelriktat
* Tillåt friliggande statliga cykelvägar
* Möjliggör statliga medel till steg 1- och 2-åtgärder
* Handlingsplan för nationella cykelmål

[Här kan man läsa mer om resultatet](https://cykelframjandet.se/nyheter/2022/08/22/vagvalet-2022/).

## Klimatkollen

![Klimatkollens granskning"](/images/klimatkollen.jpg)*Klimatkollens granskning*

Inför valet genomförde Klimatkollen en [granskning av riksdagspartiernas klimatpolitik](https://klimatkollen.se/partier). Bakom projektet står forskarnätverket Researchers’ Desk, Världsnaturfonden WWF, Våra barns klimat och ClimateView, i samarbete med PwC och Naturskyddsföreningen.

Analysen visar att bara två partiers klimatmål, Vänsterpartiets och Miljöpartiets, är nära Parisavtalets 1,5-gradersmål.

## Naturskyddsföreningen

![Naturskyddsföreningens granskning av partierna 2018-2022](/images/naturskyddsforeningen-sagt-och-gjort.jpg)*Naturskyddsföreningens granskning av partierna 2018-2022*

Naturskyddsföreningen har gjort rapporten [Kastvindar i miljöpolitiken](https://www.naturskyddsforeningen.se/artiklar/utvardering-av-svensk-miljopolitik-2018-2022/) som blickar bakåt och ser hur partierna presterat i en rad frågor under mandatperioden.

Vänsterpartiet hade i början av mandatperioden sagt att man skulle utföra 18/18 av de frågor som Naturskyddsföreningen tar upp. Vänsterpartiet har sedan agerat och fått 17 "gröna ljus" och 1 "rött ljus". Miljöpartiet kommer sedan. De har lovat lika mycket, 18/18 frågor. De får sedan 13 "gröna ljus", 4 "gula ljus" och ett "rött ljus". Men i den samlade bedömningen så rankar Naturskyddsföreningen ändå Vänsterpartiet på andra plats. Något som motiveras med att "Under den sista delen av mandatperioden har partiet dock haft en mycket lägre miljöprofil i debatten och dessutom lagt förslag om att sänka priset på bensin och diesel". Vänsterpartiet fick också kritik mot beslutet kring Cementa, något som Miljöpartiet dock inte fick sämre betyg av.

## Naturskyddsföreningen Stockholms län

![Naturskyddsföreningens Stockholms läns granskning av partierna](/images/v-mp-mitti.jpg)*Naturskyddsföreningens Stockholms läns granskning av partierna*

Vänsterpartiet och Miljöpartiet är de "grönaste" partierna i Region Stockholm enligt svaren i den [klimatenkät som Naturskyddsföreningen i Stockholms län](https://www.mitti.se/nyheter/v-och-mp-mest-grona-enligt-naturskyddsforeningen/repvhr!SGfEftaLKxwgcM119WF8Q/) skickat ut till partierna i regionfullmäktige.

Moderater, Kristdemokrater och Sverigedemokrater säger samtliga nej till i princip alla klimatförslag i enkäten.

Frågorna var:

* För att nå klimatmålen måste transportortutsläppen minska med 70 procent till 2030 räknat från 2010 års nivåer. Vill ert parti arbeta för att minska trafikmängden för att nå det målet? 
* Anser ert parti att trängselskatten ska utökas och omfatta fler kommuner än i dag? 
* Anser ert parti att det ska byggas mer havsbaserad vindkraft i regionen?

## Världsnaturfonden WWF

![WWF:s granskning av partierna](/images/wwf.jpg)*WWF:s granskning av partierna*

[Världsnaturfonden WWF](https://www.wwf.se/wwfs-valgransking-2022/#granskning) har ställt 24 frågor i sin valenkät. När enkäten gjordes så svarade Vänsterpartiet i enlighet med WWF i 20 frågor. Men några kommentarer kring det:

Fråga 16 lyder "Kommer ni att agera för att endast bilar som enbart går på el, vätgas eller biogas säljs i Sverige från 2025 genom att justera för beskattning". Vänsterpartiet svarade "Vi är för ett förbud mot nybilsförsäljning av fossildrivna bilar 2025 men inte att fossilfria laddhybrider förbjuds". Jag skulle gissa att WWF:s avsikt inte var att fossilfria laddhybrider skulle förbjudas.

En annan sak är att Vänsterpartiet efter enkäten gick ut med en [storsatsning på att skydda skog, natur och återställa våtmarker](https://www.aftonbladet.se/debatt/a/wOy4PL/v-fem-5-miljarder-kr-mer-per-ar-pa-biologisk-mangfald):

"Vi vill satsa minst fem miljarder kronor mer per år jämfört med dagens nivå för att skydda värdefull natur och återställa våtmarker."

## Svensk mat och miljöinformation

![Svensk- mat och miljöinformations valgranskning"](/images/vegoval.jpg)*Svensk- mat och miljöinformations valgranskning*

Svensk- mat och miljöinformation har genomfört en [valenkät om riksdagspartiernas arbete med hållbar mat och minskad köttkonsumtion](https://smmi.se/valenkat-om-riksdagspartiernas-arbete-med-hallbar-mat-och-minskad-kottkonsumtion/).

Det var flera saker som SMMI analyserade i svaren till sin enkät:

* Matproduktionens klimatpåverkan
* Ökad matproduktion i Sverige
* Ekologisk odling
* Djurhållning och biologisk mångfald
* Mål måste åtföljas av politik för att möta målen
* Att styra konsumtionen
* Konsekvenserna av att inte agera

Annika Carlsson-Kanyama, klimatforskare och tidigare medlem i regeringens vetenskapliga råd för hållbar utveckling, kommenterade för SMMI:s räkning partiernas svar. Vänsterpartiet fick bäst omdöme:

"Vänsterpartiet är en tungviktare när det gäller klimatpolitik och mat. Som enda parti har man satt ett mätbart mål på hur mycket köttkonsumtionen bör minska. Man har också motionerat om att den offentliga sektorn bör gå före när det gäller omställningen till en mer hållbar kost där växtbaserat är grunden och vill ha klimatskatt på mat.

Jämfört med alla andra partier är V (tillsammans med MP) de enda två partierna som har en politik värd namnet när det kommer till mat och klimat."

## Funktionsrätt Sverige

![Funktionsrätt Sveriges partianalys"](/images/funktionsratt-sverige.png)*Funktionsrätt Sveriges partianalys*

I [Funktionsrätt Sveriges ranking](https://funktionsratt.se/press-hur-bra-ar-riksdagspartierna-pa-funktionsratt/) kommer Vänsterpartiet på första plats och Moderaterna sist. Analysen bygger främst på deras valenkät med 23 frågor inom prioriterade samhällsområden.

Så här skriver Funktionsrätt Sverige om Vänsterpartiets svar:

"Vänsterpartiet får en självklar förstaplats.

Partiet svarar ja på samtliga frågor och ger också initierade och underbyggda motiveringar.

V visar därmed höga ambitioner. Ska vi låta förra mandatperioden styra våra förväntningar kan vi känna förtroende.

Vänsterpartiet svarar redan nu upp i form av motioner bland annat när det gäller att tänderna ska betraktas som en del av kroppen och därmed ingå i samma regelsystem som annan vård samt kring skärpningar av rättskyddet mot hatbrott för personer med funktionsnedsättning."

## Byggnads

![Byggnads sammanfattning av partiernas politik](/images/byggnads.jpg)*Byggnads sammanfattning av partiernas politik*

Fackförbundet Byggnads har gjort en sammanställning av vad partierna tycker i en rad frågor som är viktiga för deras medlemmar. Vänsterpartiet tycker som Byggnads. Det här var frågorna som man skulle ta ställning till:

* Begränsa antalet UE-led vid offentlig upphandling
* Inför marknadshyror
* Stoppa vinstjakten i välfärden
* Återinför investeringsstöd för att bygga fler billiga hyresrätter
* 1 000 kronor mer i plånboken för de pensionärer som har det tuffast
* Höj A-kassan
* Återinför Lex Laval
* Arbetskraftsinvandring får endast ske till bristyrken

Det går att läsa mer på [Byggnads hemsida](https://www.byggnads.se/om-oss/vi-tycker/val-2022/).

## Kvinna till kvinna

![Kvinna till kvinnas valkompass](/images/kvinna-till-kvinna.jpg)*Kvinna till kvinnas valkompass*

I [Kvinna till kvinnas valkompass](https://kvinnatillkvinna.se/feministisk-valkompass/) kommer Vänsterpartiet på första plats med 7 godkänt, 0 varning, 0 underkänt.

Frågorna de ställer är om följande:

* Aborträtt - rätten till sin egen kropp
* Mäns våld mot kvinnor
* HBTQI-personers rättigheter
* Jämställd familjepolitik
* Exploatering av kvinnors kroppar
* Global solidaritet
* Global jämställdhet och feministisk utrikespolitik

## Och mycket mer...

Ja, det finns många andra enkäter. Men någon gång måste jag sluta om det här inlägget ska bli klart innan valet :)