---
title: Bryta sig loss från big tech sociala medier
image: social-media.jpg
description: Min hatkärlek till facebook och försök att hitta nya platser
date: 2022-08-01 21:40
---
Jag gillar verkligen inte Facebook. Ändå använder jag det väldigt mycket. Det här är ju inget nytt och jag är långt ifrån den enda som har ett dubbelt förhållande till Facebook. Det finns så otroligt många anledningar till att inte bidra till övervakningskapitalismen även om "gratis är gott".

För att bara ge ett litet smakprov av anledningar till varför Facebook och andra övervakningsföretag är skadliga:

* [Facebook–Cambridge Analytica data scandal](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal)
* [Facebook and Google surveillance is an ‘assault on privacy,’ says Amnesty International](https://www.theverge.com/2019/11/20/20974832/facebook-google-surveillance-data-assault-privacy-amnesty-international)
* Krönika av [Rasmus Fleischer: Nätjättarnas politiska makt får mig att känna stor oro](https://www.etc.se/kronika/overvakning-ar-politik)
* Dagens ETC skriver: [Så tar vi upp kampen mot techjättarna](https://www.etc.se/inrikes/sa-tar-vi-upp-kampen-mot-techjattarna)
* [Demokratinätverket Skiftet blev blockat från att annonsera på Facebook](https://www.etc.se/index.php/inrikes/vaensterorganisationen-blockades-av-facebook)

Men varför är jag då kvar på Facebook? För min egen del så är det för att "alla andra är där". Och för min del handlar det mindre om det sociala. Samtal med vänner och familj har jag i flera små grupper på [Signal](https://en.wikipedia.org/wiki/Signal_(software)) (som visserligen skulle kunna ersättas till något bättre) men samtal med andra politiskt engagerade har jag på Facebook i olika grupper och direkt via min egen vägg.

# Vänstern måste ha egna kanaler

Robin Zachari från Skiftet förordar vänstern att ta [kontrollen över kommunikationen](http://flamman.se/a/sluta-mema-borja-mejla) istället för att använda centraliserade lösningar som Facebook. Mail är det uppenbara alternativet. Det finns stora problem med att vi låter ett vinstdrivande övervakningsföretag (Facebook/Meta) vara vår primära kommunikationsform.

Det finns två delar som jag tror är viktigt att tänka kring kommunikation:

* Intern diskussion inom partiet/föreningen/nätverket
* Extern kommunikation / propaganda / info till allmänheten och intresserade

Det finns flera gruppen inom Vänsterpartiet som använder Facebook-grupper eller Messenger-trådar som primär intern kommunikationskanal och sedan Facebook-sidor som primär extern kommunikation. Jag har själv bidragit till det här när jag var aktiv i kommunpolitiken. Vi hade inga bra digitala kanaler och facebook-grupper var det snabbaste att få igång. Så jag vet att det är lätt hänt.

De flesta partiföreningar har visserligen också en hemsida och det är väldigt bra för då har man makten över sin data och sin kommunikation.

Vi går mot ett val då brunhögern riskerar att vinna valet. Och tonläget har hårdnat, inte bara från Sverigedemokraterna utan från de andra högerpartierna också. [Moderaterna vill se hårdare straff för klimataktivister](https://www.svt.se/nyheter/inrikes/m-vill-se-hardare-straff-mot-klimataktivister). Och [Säpochefen vill göra det straffbart att demonstrera för PKK](https://www.sydsvenskan.se/2022-07-22/sapochefen-vill-gora-det-straffbart-att-demonstrera-for-pkk).

Att vänsterorganisationer, klimatnätverk, asylaktivister och antirasister skulle övervakas eller vilja stängas ner snabbt är faktiskt en reell risk inom överskådlig framtid. Det tycker jag inte vi ska bjuda på genom att all kommunikation sker på t.ex. Facebook. Som tur är finns det redan några som jobbar med att erbjuda alternativ:

* [riseup.net](https://riseup.net) - "Riseup provides online communication tools for people and groups working on liberatory social change. We are a project to create democratic alternatives and practice self-determination by controlling our own secure means of communications."
* [Disroot](https://disroot.org/en) - "Disroot is a platform providing online services based on principles of freedom, privacy, federation and decentralization."

Några av de saker som erbjuds är:

* E-post
* Mailinglistor
* Molnlagring
* Forum
* Chat via XMPP
* Videokonferens
* Röstchatt

# Gammalt och beprövat eller nytt och coolt?

E-post, mailinglistor och rss-flöden är beprövade tekniker som har funnits under lång tid. Samma sak med webbforum och chattar. Går det att få över politiskt engagerade till de tjänster som erbjuds av riseup.net och Disroot? Och i så fall, är det bara de mest engagerade och de som inser risken för sig själva, eller kommer man kunna få med sig en bred massa?

# Centraliserat, decentraliserat, federerat, distribuerat

Om man vill ha makten över sin kommunikation så kan det vara dumt att förlita sig på helt centraliserade lösningar. Även om man skulle hitta ett fritt och öppet alternativ till Facebook t.ex. så finns möjligheten för en aktör att stänga ner eller ta över den. Det kan vara en framtida fascistoid regering eller hackare. Därför är det bra om man kan använda tekniker som inte är så sårbara. Några exempel på olika nätverkslösningar:

* **Centraliserat**. Det finns en central nod som alla ansluter till och blir därmed sårbart. Man kan inte flytta sin data till en annan server utan sitter fast. Exempel: Facebook, Instagram, Tiktok, Slack, Signal.
* **Decentraliserat**. Det finns inte en central nod utan man kan ansluta till olika centrala noder. En organisation eller nätverk kan sätta upp sina egna servrar som bara de har tillgång till med tillgänglig mjukvara. Man kan flytta sin data till en annan server. Exempel: egna [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat)-servrar, egna webbforum (som [Discourse](https://en.wikipedia.org/wiki/Discourse_(software))).
* **Federerat**.  Är också decentraliserat. Det finns inte en central nod utan flera som kopplas ihop med varandra så att användare kan prata med varandra. Det kallas också ["Fediverse"](https://en.wikipedia.org/wiki/Fediverse). Exempel: [Mastodon](https://en.wikipedia.org/wiki/Mastodon_(software)), [Diaspora](https://en.wikipedia.org/wiki/Diaspora_(social_network)), [Peertube](https://en.wikipedia.org/wiki/PeerTube), [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)), [Frendica](https://en.wikipedia.org/wiki/Friendica), [IndieWeb](https://en.wikipedia.org/wiki/IndieWeb). 
* **Distribuerat**. Man är inte beroende av någon enda central nod. Exempel: blockkedjan (Bitcoin t.ex.), [Jami](https://jami.net/)

![Centraliserat, decentraliserat, distribuerat](/images/centralized-distributed-web.jpeg)*Centraliserat, decentraliserat, distribuerat. Bild: CC-BY av www.cs-ib.net*

Det är inte helt uppenbart för mig alla gånger om man ska sätta exemplena ovan under "decentraliserat" eller "federerat". Men det mest uppenbara för mig är att det är säkrare att använda sig av decentraliserade eller federerade alternativ.

Med alla federerade tekniker så behöver man hitta en nod/instans/server som man registrerar konto hos. Men sen kan man prata med alla andra på nätverket, även om de har konto på en annan instans. På så sätt är det exakt som e-post. Om man har ett konto på gmail.com kan man maila någon på hotmail.com eller på riksdagen.se.

Man kan se en lista på de olika projekten i "Fediverse" på den [the-federation.info/](https://the-federation.info/)

# Gamla och nya alternativ

Här listar jag några alternativ som jag själv ser potential hos.

De äldre och beprövade:

* **E-post**. Har varit med länge och väl. Finns många bra leverantörer. En snab sökning ger t.ex.: [Proton Mail](https://proton.me/mail), [Fastmail](https://www.fastmail.com/), [Tutanota](https://tutanota.com/), [mailbox.org](https://mailbox.org/en/)
* **Webbforum**. Har också varit med länge. De mest populära webbforumen idag i Sverige är nog [Familjeliv](https://www.familjeliv.se/forum) och [Flashback](https://www.flashback.org/). Exempel på programvara: [Discourse](https://en.wikipedia.org/wiki/Discourse_(software))
* **Bloggar/hemsidor**. Antingen så sätter man upp servern själv eller så använder man en färdig hosting. Några exempel på gratis hosting som man kan uppgradera: [WriteFreely](https://writefreely.org/), [Wordpress](https://wordpress.org/hosting/) eller om man är mer tekniskt lagd så kan man fixa egen blogg på [Github](https://medium.com/flycode/how-to-deploy-a-static-website-for-free-using-github-pages-8eddc194853b), [Gitlab](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html), [Netlify](https://www.netlify.com/blog/2016/10/27/a-step-by-step-guide-deploying-a-static-site-or-single-page-app/). Kan vara värt att betala en peng för att få ett eget hostname (t.ex. som jag har gjort med bitar.se). Då kan man byta hostingleverantör. Jag har använt [Njalla](https://njal.la/) men det vanligaste i Sverige är nog [Loopia](https://www.loopia.se/)
* **RSS**. [RSS](https://en.wikipedia.org/wiki/RSS) är ett sätt för att "prenumerera" på hemsidor. Man behöver en RSS-läsare där man skriver in sina prenumerationer och så kan man titta där direkt och se när nytt inehåll kommer upp. Den här sidan kan man också prenumerera på. Gå längst ner på sidan och klicka på symbolen.

De lite nyare

* [Mastodon](https://en.wikipedia.org/wiki/Mastodon_(software)). Ett alternativ till Twitter. [Jag finns själv här på mastodon.se-instansen](https://mastodon.se/@samuel)
* [Diaspora](https://en.wikipedia.org/wiki/Diaspora_(social_network)). Ett alternativ till Facebook. Det har funnits med ett tag nu, sen 2010. Utseendemässigt ser det mer ut som Facebook än andra alternativ. Mer n
* [PeerTube](https://en.wikipedia.org/wiki/PeerTube). Ett alternativ till Youtube eller Vimeo.
* [PixelFed](https://pixelfed.org/). Ett alternativ till Instagram.
* [XMPP](https://en.wikipedia.org/wiki/XMPP). Ett alternativ till Messenger eller WhatsApp. Tidigare känt som Jabber. 
* [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)). Ett alternativ till Messenger eller WhatsApp. Ett nytt protokoll som är på väg uppåt.

![Skärmdump av Mastodon](./images/mastodon-screenshot.png)*Skärmdump av Mastodon*

![Skärmdump av Diaspora](./images/diaspora-screenshot.png)*Skärmdump av Diaspora. CC-BY-SA 3.0, Wikipedia*

Det var lite saker jag har gått och grubblat på. Jag hoppas att vi i vänstern kan se till att finnas på fler plattformar som inte ägs och styrs av vinstdrivande företag. Det innebär inte att vi måste stänga ner våra konton på Facebook, Instagram, Twitter och TikTok. Men det innebär att vi tar mer kontroll över vår egen kommunikation och får mer frihet.

Vad tänker du?

