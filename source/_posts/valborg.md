---
title: Valborgssånger
image: valborg.jpg
description: En lista på valborgssånger
date: 2023-04-30 23:00
---

Här är en lista på valborgssånger.

## Vårvindar friska

Vårvindar friska leka och viska
lunderna kring likt älskande par.
Strömmarna ila, finna ej vila
förrän i havet störtvågen far.

Klappa mitt hjärta, klaga och hör,
vallhornets klang bland klipporna dör.
Strömkarlen spelar, sorgerna delar
vakan kring berg och dal. X 2

Hjärtat vill brista! Ack, när den sista
gången jag hörde kärlekens röst:
avskedets plåga, ögonens låga,
mun emot mun och klappande bröst.

Fjälldalen stod i grönskande skrud,
trasten slog drill på drill för sin brud.
Strömkarlen spelar,
Sorgerna delar,
Vakan kring berg och dal. X 2

## Längtan till landet (Vintern rasat ut)

Vintern rasat ut bland våra fjällar
drivans blommor smälta ned och dö
Himlen ler i vårens ljusa kvällar
solen kysser liv i skog och sjö
Snart är sommarn här i purpurvågor
guldbelagda, azurskiftande
ligga ängarne i dagens lågor
och i lunden dansa källorne
Snart är sommarn här i purpurvågor
guldbelagda, azurskiftande
ligga ängarne i dagens lågor
och i lunden dansa källorne

Ja, jag kommer! Hälsen, glada vindar
ut till landet, ut till fåglarne
att jag älskar dem, till björk och lindar
sjö och berg, jag vill dem återse
se dem än som i min barndoms stunder
följa bäckens dans till klarnad sjö
trastens sång i furuskogens lunder
vattenfågelns lek kring fjärd och ö
se dem än som i min barndoms stunder
följa bäckens dans till klarnad sjö
trastens sång i furuskogens lunder
vattenfågelns lek kring fjärd och ö

## Vårsång (Glad såsom fågeln)

Glad såsom fågeln i morgonstunden
Hälsar jag våren i friska natur’n,
Lärkan mig svarar och trasten i lunden,
Ärlan på åkern och orren i fur’n.

Glad såsom fågeln i morgonstunden
Hälsar jag våren i friska natur’n,
Lärkan mig svarar och trasten i lunden,
Ärlan på åkern och orren i fur’n.

Se, hur de silvrade bäckarna små
Hoppa och slå,
Hoppa och slå
Vänliga armar kring tuvor och stenar!
Se, hur det spritter i buskar och grenar
Av liv och av dans,
Av liv och av dans
I den härliga vårsolens glans!

Se, hur de silvrade bäckarna små
Hoppa och slå,
Hoppa och slå
Vänliga armar kring tuvor och stenar!
Se, hur det spritter i buskar och grenar
Av liv och av dans,
Av liv och av dans
I den härliga vårsolens glans!
uti vårsolens glans!

## Blåsippor

Blåsippan ute i backarna står,
niger och säger att nu är det vår.
Barnen de plocka små sipporna glatt,
rusa sen hem under rop och skratt.

Mor, nu är våren kommen, mor
Nu får vi gå utan strumpor och skor.
Blåsippor ute i backarna stå,
har varken skor eller strumpor på.

Mor i stugan, hon säger så:
–  Blåsippor aldrig snuva få.
Än fä ni gå med strumpor och skor,
än är det vinter kvar, säjer mor.

## Majsång (Sköna maj, välkommen)

Sköna maj, välkommen
till vår bygd igen!
Sköna maj, välkommen,
våra lekars vän!
Känslans gudaflamma
väcktes vid din ljusning;
jord och skyar stamma
kärlek och förtjusning;
sorgen flyr för våren,
glädje ler ur tåren

Nu ur lundens sköte
och ur blommans knopp
stiga dig till möte
glada offer opp.
Blott ditt lov de susa,
dessa rosenhäckar,
till din ära brusa
våra silverbäckar,
och med tacksam tunga
tusen fåglar sjunga
liksom vi: Välkommen, sköna maj!

## Uti vår hage

Uti vår hage där växa blå bär.
Kom hjärtansfröjd!
Vill du mig något, så träffas vi där.
Kom liljor och akvileja, kom rosor och salivia!
Kom ljuva krusmynta, kom hjärtansfröjd!

Fagra små blommor där bjuda till dans.
Kom hjärtansfröjd!
Vill du, så binder jag åt dig en krans.
Kom liljor och akvileja, kom rosor och salivia!
Kom ljuva krusmynta, kom hjärtansfröjd!

Kransen den sätter jag sen i ditt hår.
Kom hjärtansfröjd!
Solen den dalar, men hoppet uppgår.
Kom liljor och akvileja, kom rosor och salivia!
Kom ljuva krusmynta, kom hjärtansfröjd!

Uti vår hage finns blommor och bär.
Kom hjärtansfröjd!
Men utav alla du kärast mig är.
Kom liljor och akvileja, kom rosor och salivia!
Kom ljuva krusmynta, kom hjärtansfröjd! 

## Nu grönskar det

Nu grönskar det i dalens famn,
nu doftar äng och lid.
Kom med, kom med på vandringsfärd
i vårens glada tid!
Var dag är som en gyllne skål,
till brädden fylld med vin.
Så drick, min vän, drick sol och doft,
ty dagen den är din.

Långt bort från stadens gråa hus
vi glatt vår kosa styr,
och följer vägens vita band
mot ljusa äventyr.
Med öppna ögon låt oss se
på livets rikedom
som gror och sjuder överallt
där våren går i blom! 