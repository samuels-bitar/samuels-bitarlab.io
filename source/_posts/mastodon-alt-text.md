---
title: Mastodon, bilder och alt-text
image: mastodon-welcome-2.png
image_description: Bild från <a href="https://renken-sebastian.ca/wp-content/uploads/2023/02/Mastodon-Welcome-800x455.png">https://renken-sebastian.ca</a> 
description: Hur skapar vi en välkomnande miljö? Tankar om alt-text för bilder på Mastodon
date: 2025-02-13 21:30
---

Jag tycker Mastodon och resten av Fediversum är väldigt trevligt! Det är väldigt annorlunda från andra sociala medier jag använt och det är det jag gillar. Jag har tidigare bloggat om att det är [Mysigt med Mastodon](/mysigt-med-mastodon/) och skrivit en guide på Kamratdataföreningen Konstellationens wiki om [Tips på hur man har det trevligt med andra på Mastodon](https://wiki.konstellationen.org/sv/guider/tips-mastodon).

Jag hoppas fler kan upptäcka Mastodon och få det lika trevligt som jag har!

Mastodon och resten av Fediversum utvecklades av personer som lagt stor vikt vid inkludering på olika sätt. Det kan man märka om man följer vissa admins för Mastodon-instanser. Därför är kulturen emellanåt lite annorlunda på Mastodon. Vissa normer är starkare här än på andra socmed-plattformar. Läs gärna t.ex. [A (partial) queer, trans, and non-binary history of Mastodon and the fediverse](https://wedistribute.org/2023/06/a-partial-queer-trans-and-non-binary-history-of-mastodon-and-the-fediverse/).

Det som är fiffigt med Mastodon är att varje instans har en uppsättning regler för instansen. Så om man följer de reglerna så är man "safe" så att säga. Exempelvis så har Konstellationens instans Spejset [följande regler](https://social.spejset.org/about):

* Ingen rasism, sexism, homofobi, transfobi eller högerextremism
* Ingen uppmaning till våld, varken mot enskild person eller folkgrupp
* Inga trakasserier eller mobbning
* Dela inte medvetet vilseledande information
* Dela inte innehåll som uppenbart är olagligt enligt svensk lag
* Ingen spam och reklam som vi inte tycker är produktiv
* Inget pornografiskt material
* Botkonton som regel godkänns inte. Hör av dig till moderatorer om du ändå vill skapa
* Saker som vi inte vill se på den offentliga tidslinjen (men som är OK för bara följare): inlägg från botkonton

Utöver det har vi inga krav på hur man "bör" kommunicera. Men jag tänker att det kan vara bra att veta vilka specifika normer som är vanliga här på Mastodon/Fediversum. Då kan man välja om man vill följa dem eller om man inte vill följa dem. Men det är ändå bra om det är medvetet.

Utöver att Mastodons federerade arkitektur så finns det andra tekniska verktyg som hjälper till att skapa och upprätthålla en inkluderande miljö. Det är:

* Alt-text för bilder
* Content Warnings (CWs)

I det här inlägget tänkte jag prata om alt-text.

## Alt-text för bilder

Alt-text för bilder är helt enkelt en beskrivning av en bild. Den texten kan visas när man av olika anledningar inte kan se själva bilden.

Alt-text har funnits i [HTML sedan 1993](https://en.wikipedia.org/wiki/Alt_attribute). Så det är inget nytt påfund.

Själva syftet med att ha alt-text för bilder är bland annat för att:

* Underlätta för blinda och andra med nedsatt syn som använder sig av så kallade skärmläsare/[screenreaders](https://en.wikipedia.org/wiki/Screen_reader). En skärmläsare är ett program som omvandlar text och bilder som syns på en bildskärm. Resultatet kan skickas till talsyntes och läsas upp eller överföras till en punktskriftsdisplay/[braille display](https://en.wikipedia.org/wiki/Refreshable_braille_display). Utan alt-text vet skärmläsaren inte vad den ska generera för ord för bilden.
* Underlätta för besökare på en sida när man t.ex. har dålig nätverksuppkoppling eller bilder av andra anledningar har problem att laddas.

![Punktskriftsdisplay bredvid ett vanligt tangentbord](./images/braille-display.jpg)*Punktskriftsdisplay bredvid ett vanligt tangentbord*

## Hur man lägger till alt-text för en bild

### Webbgränssnitt

![Fönstret för att lägga upp post](./images/alt-text-1.jpg)*Fönstret för att lägga upp post*

![Uppladdad bild, ingen alt-text](./images/alt-text-2.jpg)*Uppladdad bild, ingen alt-text*

Nu kan man klicka på "Redigera"

![Fönster för att redigera media och sätta fokusplats för bilden](./images/alt-text-3.jpg)*Fönster för att redigera media och sätta fokusplats för bilden*

Nu kan man klicka där man vill fokus på bilden ska vara (om inte hela platsen får plats när den visas) och vad alt-texten ska vara. Här skrev jag "En katt i en soffa med en tv-spelskontroll". Sen klickade jag på "Verkställ".

![Fönstret för att lägga upp post](./images/alt-text-4.jpg)*Fönstret för att lägga upp post*

Nu är posten klar. Nu kan jag publicera.

![Fönstret för att lägga upp post](./images/alt-text-5.jpg)*Fönstret för att lägga upp post*

Inlägget med bilden kan man se [här på Mastodon](https://social.spejset.org/@samuel/113998108073905988).


## Kulturen på Mastodon

På de flesta socmed-plattformar finns möjligheten att lägga till alt-text när man laddar upp en bild. Men på Mastodon så är normen starkare att göra det. Och gränssnitt och appar uppmuntrar aktivt att man ska göra det. Detta är för att skapa en så inkluderande miljö som möjligt, att de med nedsatt syn ska kunna vara med i samtalen även när bilder används.

Flera användare på Mastodon känner starkt för frågan och lägger aldrig upp bilder utan att ange en alt-text. Många undviker till och med att boosta (dela vidare) bilder som de ser för att de saknar alt-text.

Jag gjorde en [omröstning](https://social.spejset.org/@samuel/113991359019065484) på Mastodon och ungefär hälften avstår ibland, sällan eller aldrig för att boosta om alt-text inte finns.

![Omröstning om hur folks boostande påverkas av frånvaron av alt-text](./images/alt-text-vote.jpg)*Omröstning om hur folks boostande påverkas av frånvaron av alt-text*

Vissa personer kan kommentera och säga "hörru, ska du inte lägga till en alt-text till bilden". Det finns även användare som hjälper till att skriva en alt-text och tagga #Alt4You. Sidan Fedi.tips har ett [blogginlägg](https://fedi.tips/how-do-i-make-posts-more-accessible-to-blind-people-on-mastodon-and-the-fediverse/) där de tipsar om hur man kan göra.

## Vem bryr sig?

Många bryr sig inte. De vill dela bilder och pallar inte sitta och skriva alt-text. De vill bara visa bilder för sina kompisar och de känner ingen som (vad de vet) är i behov av alt-text. Jag tycker att det är okej. Bilderna kanske bara visas för följare och inte ens går att boosta.

Men för mig och många andra så vill man gärna att ens inlägg ska få spridning. Det är supernajs att få gilla-markeringar. Ännu roligare med kommentarer. Och det är kul när folk boostar så att ännu fler ser bilderna. Och jag skulle verkligen bli glad om personer med screen readers kan ha nytta av alt-texterna till mina bilder.

Jag tänker att speciellt tidningar, föreningar, opinionsbildare och andra som vill nå så långt som möjligt och är beredda att lägga lite tid och energi på det tjänar på att lägga alt-text på sina bilder för att helt enkelt ha större chans att fler delar.

## Pekpinnar eller välkomnande?

Det finns alltid risk när man vill upprätthålla vissa normer eller värderingar att det upplevs som pekpinnar. Det tror jag sällan blir bra. Det är bättre tror jag att föregå med gott exempel. Men för att man ens ska kunna föregå med gott exempel måste folk veta om att det här ens är en fråga. Och det är lite därför jag skrev inlägget.

Erin Kissane är en person som är väldigt aktiv på Mastodon och har bloggat om hur det kan bli när man misslyckas med att skapa en inkluderande och välkomnande miljö trots att man kanske ville. Hon skrev blogginlägget [Mastodon Is Easy and Fun Except When It Isn’t](https://erinkissane.com/mastodon-is-easy-and-fun-except-when-it-isnt). Där går hon igenom en undersökning hon gjorde med folk som först testade Mastodon men sedan valde att lämna för Bluesky av en rad anledningar.

Hon delade in svaren i följande kategorier:

* got yelled at, felt bad (mest aktuell i frågan om alt-text enligt mig)
* couldn’t find people or interests, people didn’t stay
* too confusing, too much work, too intimidating
* too serious, too boring, anti-fun
* complicated high-stakes decisions

Det är värt att påpeka att den här undersökningen gjorde på folk som *lämnade* Mastodon. Så det finns så klart många fler som stannade kvar. För egen del tycker jag det är tråkigt om folk lämnar Mastodon på grund av att de känner att de "gör fel" på olika sätt.

## Vill men glömmer bort?

Kanske du vill lägga till alt-text men du glömmer alltid bort? Då kan du följa bot-kontot [@alt_text@mastodon.social](https://mastodon.social/@alt_text) så kommer kontot automatiskt påminna sig om att lägga till alt-text om du glömt.

## Vill men det tar för mycket tid

Du kanske tänker att det här låter ju bra men det tar för lång tid att skriva en uppsats för varje bild? Du behöver inte skriva en superlång text för varje bild. Om vi tar bilden ovan på katten i soffan med tv-spelskontroll. Att skriva "gamer-katt" är bättre än att inte skriva något alls. Även att bara skriva "katt" eller "gamer" är bättre än att inte skriva något alls.

För egen del tänker jag att det inte måste vara svart-vitt. Börja med att skriv alt-text för de bilder du orkar. Om du delar en meme med en massa text som är jobbig, ja då kanske det inte är den bilden du kommer börja med att skriva alt-text för. Men om man börjar smått med alt-text för bilder på sin katt så märker man att det inte är så jobbigt.

## Hur jag gör

Jag försöker själv att sätta alt-text på mina bilder.

Jag delar gärna vidare bilder jag gillar. Om det är alt-text på den så delar jag vidare ofta direkt. (Om jag inte känner jag spammat mina följare allt för mycket på siston.e) Men om jag ser en bild som jag gillar men som inte har alt-text så är det inte säkert jag delar vidare. Men jag skulle nog inte kommentera och säga "hörru, sätt alt-text på din bild". Jag skulle bara nöja mig med en gilla-markering.

Men för min del tycker jag det det är viktigtast att folk lämnar Big Tech-plattformarna. Så om folk lämnar Twitter/X, Facebook och Instagram för att komma till Mastodon/Fediversum så blir jag jätteglad. Att sätta upp allt för många regler och pekpinnar om hur man "måste" göra på Mastodon/Fediversum tror jag blir fel. Att däremot hjälpa till med en bra onboarding tror jag är bra. Tips på guider och visa folk när de vill veta. Men jag tror att kultur, normer och värderingar måste vara grundat i vad man vill göra.

Hur som helst, hoppas ni hade glädje av detta inlägg!