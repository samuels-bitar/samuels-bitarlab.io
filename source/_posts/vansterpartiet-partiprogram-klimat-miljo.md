---
title: Klimat och miljö i Vänsterpartiets partiprogram
image: ende-gelande.jpg
image_description: Klimataktivister på väg ner i en kolgruva för att ockupera den. <a href="https://flickr.com/photos/350org/19982832093/">Bild av 350.org</a>
description: Vad står det i Vänsterpartiets partiprogram om klimat och miljö? Det hittar du här!
date: 2024-05-14 09:00
---

Vänsterpartiet tog ett [nytt partiprogram](https://www.vansterpartiet.se/resursbank/partiprogram/) 12 maj 2024. Men vad står det om klimat och miljö? Det har jag sammanställt i detta inlägg.

Jag skulle dock rekommendera alla att läsa hela partiprogrammet. Politiken håller ihop och det blir lättare att förstå även klimat- och miljöpolitiken om man har läst hela partiprogrammet. Vi är ett socialistiskt parti med marxistisk analys. Det är viktigt att förstå kapitalets del i hur klimat- och miljöförstöring fungerar. Det är också det som skiljer Vänsterpartiet från andra gröna partier. Och det gör också att det är lättare att ge svar på frågan "men varför händer inget när forskningen har visat under många år att klimatkrisen är på riktigt och att vi måste minska utsläppen nu".

Med det sagt, här kommer en massa text om klimat och miljö. Jag kan ha missat något så klart. Jag har kopierat hela stycken och inte klippt ut enstaka meningar.

## Inledning

Vänsterpartiet är ett socialistiskt, feministiskt och antirasistiskt  parti på ekologisk grund som samlar människor för att bygga ett  bättre samhälle, där alla kan leva i trygghet, frihet och jämlikhet. Vi  vill se en politik som sätter människor i arbete med att lösa de stora  samhällsproblemen. Genom att skapa välfungerande arbetsplatser,  fasa ut fossilberoendet och ta bättre hand om varandra skulle det gå  att ta historiska kliv framåt.

[...]

Vi vill frigöra ekonomin från den belastningen. När samhället tar  över verksamheter kan vi driva dem med bättre mål. Vi  behöver skolor som alltid ser till elevernas bästa och industrier som  är ärligt engagerade i att ställa om från det fossila. Vi behöver  arbetsplatser som respekterar sina anställda, med kortare  arbetsdagar som låter oss leva friare liv.

## Arbete och gemenskap

Det är mycket mer som förenar oss människor än som skiljer oss åt.  Vi behöver alla värme och mat, närhet och omsorg, hem att bo i  och natur omkring oss. Vi behöver kunskap och kultur. Vi  behöver personer som finns där för oss och ser oss för dem vi är. Vi  behöver frihet att uttrycka oss och forma våra liv.   

[...]

Genom arbetet stöper vi om den värld vi lever i. Det har långsiktiga  konsekvenser, eftersom vi griper in i historiska processer och  relationer – allt från ekosystem till tilliten mellan människor. Vare  sig vi tar hand om barn eller byggnader, skriver recept eller  programkod, behöver vi en gedigen förståelse av det vi gör och  omsorg om helheten.

[...]

## Frihetens hinder

#### Låsningar på arbetsplatserna

[...]

Den enögdheten kan driva dem att förstöra biologisk mångfald  och ekosystem, vars långsiktiga bärkraft betyder lite för deras  lönsamhet men mycket för framtida generationers liv på denna  planet. Den får dem att låsa in kunskap som skulle göra långt  större nytta om den kunde användas fritt. Den leder dem till att  slösa enorma resurser på konflikter med varandra i form av allt från  marknadsföring till juridiska tvister.

[...]

#### Kapitalets instabilitet

[...]

I många företag märks kapitalets instabilitet när ägandet skiljs från  verksamheten och besluten börjar fattas i styrelserum med väldigt  svag koppling till det verkliga arbete företaget bygger på. Den syns  i hur ägare kan ta stora risker och låta andra stå för notan när ett  bolag förstör naturresurser eller går i konkurs. Att vinstdrivande  företag ensidigt fokuserar på sin egen lönsamhet skapar en  kultur av ointresse för samhällsekonomins komplexitet, som också  kommit att prägla andra institutioner.

[...]

### Ekologiska kriser

Vi lever i en tid av historiska ekologiska kriser.  Klimatförändringarna ruckar på stabiliteten i de grundläggande  livsvillkor vi behöver för att bygga goda liv. Trängda ekosystem  gör att vi förlorar delar av jordens biologiska arv i snabb takt. Flera  kriser förstärker varandra, med oöverblickbara konsekvenser för de  livsmiljöer och den natur vår frihet vilar på. Världen står inför  vägval som mänskligheten kommer att se tillbaka på långt in i  framtiden.  

De ekologiska kriserna går långt tillbaka i tiden. Mänsklighetens  begränsade tekniska förmåga och sätt att organisera arbetet har lett  till att kretslopp överbelastats och naturresurser förbrukats i  snabbare takt än de förmått återhämta sig. Den storskaliga  förbrukningen av fossila bränslen är på många sätt en brytpunkt,  med djupa konsekvenser på global nivå. Vi behöver utveckla nya,  hållbara sätt att arbeta på en lång rad områden, som bygger på en  genomtänkt omsorg om livets villkor. Det är en stor och krävande  uppgift för vilket samhälle som helst. Varje politisk kraft som gör  anspråk på att leda samhället behöver kunna svara på hur den  omställningen ska se ut.  

Kapitalets brist på helhetssyn är en av grundorsakerna till att  situationen gått så långt som den gjort. Mycket av dagens problem  hade gått att undvika till små kostnader, men vinstmaximerande  bolag kan inte annat än att i varje läge prioritera den egna  avkastningen. Den oförmågan gör det omöjligt för dem att ta  verkligt ansvar.   

Trots allt vi vet om klimatförändringarna kommer kapitalet att  fortsätta bränna fossil energi så länge det är kortsiktigt lönsamt.  Trots att vi behöver ta bättre hand om ekosystemen för att inte  förstöra deras bärkraft tvekar inte kapitalet att riva sönder kretslopp  och lämna problemen till andra.  

Trots att vi behöver hushålla med begränsade resurser drar sig  inte kapitalet för att slösa med dem. När produkter designas för  att säljas, snarare än för att användas, leder det ofta till att de inte  håller och är svåra att reparera. Det är ofta lönsamt att förstöra  överblivna varor hellre än att låta dem komma till nytta. Mängder  av resurser förbrukas på marknadsföring människor gör allt för att  undvika. Till det kommer det slöseri som bottnar i den absurda  fördelningen av vinsterna, som gör att privatjet och uppassning  åt de rikaste prioriteras före samhällets verkliga behov.  

Vinstdrivande företag har svårt att driva den tekniska  utveckling vi behöver, eftersom de inte ser till helheten. Ny  kunskap har ett enormt samhällsekonomiskt värde för världen, men  det är bara en bråkdel av det värdet som går att tjäna pengar på.  Kapitalet prioriterar istället andra investeringar, stänger sina  utvecklingsprocesser för omvärlden och låser in den kunskap det  tar fram bakom patent.   

Det är en utmaning för stater att reglera de här problemen, eftersom  de flesta företag har ägandeformer som gör dem ovilliga att  samarbeta. Det har ofta lönat sig för vinstdrivande företag att  tvärtom motverka demokratiskt fattade beslut genom att bryta mot  lagar, driva politisk lobbying eller flytta produktion till länder utan  effektiva regelverk.   

Det blir särskilt tydligt för fossilkapitalet, de bolag som äger fossila  tillgångar och djupt fossilberoende industrier. Varje omställning,  hur vi än genomför den praktiskt, innebär att deras tillgångar blir  värdelösa. Därför har de länge finansierat politiska krafter som  försenar det skiftet.  

Andra företag hamnar i andra roller. Vissa har ett intresse av att  driva på omställningen, som till exempel de som bygger  vindkraftverk eller eldrivna fordon. Ibland kan företag göra stor  skillnad utan att det nödvändigtvis kostar dem särskilt mycket och  motivera det som marknadsföring. En del ställer om för att  fossilberoende blivit en företagsekonomisk risk i en värld som  inte kommer acceptera det hur länge som helst.   

Däremot har kapitalet samlade politiska intressen som försvårar  världens omställning. Det har till exempel mycket att förlora på  skiften mot en starkare demokrati som tar en större roll i de stora  investeringsbesluten. Ett samhälle som medvetet tar oss ur  beroendet av icke-förnybar energi är ett samhälle som också är  redo att ta sig an fler problem i konflikt med starka  kapitalintressen.   

Det finns en lång historia av människor som tillsammans försvarat  de naturvärden vi bygger våra liv kring. Det har gått att organisera  politiska krafter som varit starka nog att utmana ägarintressen och  öppna nya vägar framåt. Vi har kunnat bygga upp lagstiftning som  begränsar utrymmet för kortsiktig exploatering och lägger tillbaka  en del av de samhällsekonomiska kostnaderna på företagen.  

Klimatforskningens genomslag kom i en omtumlande tid då stater  drog sig tillbaka från sitt ansvar att styra samhällets utveckling,  ivrigt pådrivna av kapitalet. I praktiken betydde det låga offentliga  investeringar, attacker på välfärden och sämre jobbmöjligheter. Det  skiftet drevs igenom med en ideologisk strömning full av politisk  uppgivenhet, fokus på privat konsumtion och skuldbeläggning av  enskilda människor för strukturella problem. Allt det här kom att  prägla diskussionerna om klimatpolitiken på ett sätt som ofta ledde  till låsningar. För att bygga upp stödet för omställningen kommer  vi att behöva förhålla oss till den historien och utforma politiska  projekt som många kan sluta upp bakom.   

Klimatkrisen öppnar för nya, bättre sätt att organisera samhället.  Det går att sänka utsläppen effektivt genom nya tekniska  innovationer, beteendeförändringar, minskad konsumtion och  förändrad efterfrågan. Att bevara skog och återställa våtmarker  betyder också mycket.  

Investeringar, innovation och storskaliga samhälleliga projekt  kan ge de största effekterna. Det handlar om att ställa om  produktionen och satsa på förnybar energi, järnväg,  kollektivtrafik och renoveringar av miljonprogrammet. Det  behövs strategier som utgår från forskning, ser till att  omställningen genomförs rättvist och bygger upp ett starkt  folkligt stöd.

### En ojämlik värld

Jakten på större vinster har lett kapitalet att söka jorden runt efter  arbetskraft och naturresurser att exploatera. Historiskt har det ofta  tagit sig formen av annekteringar, tvångsarbete och kolonialt  förtryck. Flera av västvärldens stater byggde imperier som sträckte  sig över stora delar av världen.

Världens stormakter har länge drivit destruktiva geopolitiska  konflikter om kontrollen över strategiska råvaror, teknologi och  finansiella flöden. Det har stärkt antidemokratiska krafter i många  länder och etablerat en kostsam vapenindustri med starka politiska  intressen. Det driver fram en farlig dynamik, där mindre stater dras  in och hamnar i kläm. Särskilt utvecklingen av kärnvapen ställer  världen inför oacceptabla risker. 

Historien av kolonialism och imperialism formade en förödande  ekonomisk underordning av stora delar av världen. Den syns  fortfarande i form av utländskt ägande av nyckelsektorer, ensidiga  resursflöden, korruption och djup skuldsättning av många stater.  Det ger kapitalet starka verktyg att pressa fram politik. Den  självständighet befrielserörelserna vann återstår till stor del att  förverkliga.  

Kapitalet har dragits till underordnade länder för att det har varit  platser där det inte behövt respektera de krav på grundläggande  rättigheter som människor kunnat driva igenom i mer utvecklade  ekonomier. Det har kunnat förlägga tungt arbete och miljöskadlig  produktion där, istället för att investera i teknikskiften som löser  problemen på riktigt.

[...]

## Våra svar

#### Organisering för förändring

[...]

Människors gemenskap har lagt grunden för många starka  svar på rasistiska strategier uppifrån. I föreningslivet,  fackföreningarna och vänkretsarna odlar vi en  sammanhållning som inte låter sig luras så lätt. Vi vill vara  med och bygga en bred antirasistisk rörelse som förstärker  sammanhållningen och skapar tryck på förändring.   Många starka rörelser utgår från de platser där vi bor tillsammans.  Vi ser det när grannar går samman för att sätta tyngd bakom  sina krav som hyresgäster. Vi ser det i allt från förortsorganisering  till byalag, från arbetet för lokala skolor och sjukhus till försvaret  av naturen omkring oss. I det arbetet märks det att det handlar om  gemensamma frågor, som vi kan driva bättre när vi väver ihop det  som händer över hela landet. Det finns mycket som förenar  Sveriges förorter, landsbygd, glesbygd och bruksorter.   

Klimatfrågan öppnar nya politiska möjligheter, eftersom en verklig  omställning bara kommer att kunna ske i samhällen som tar ett fast,  demokratiskt grepp om sin ekonomiska utveckling. Arbetet med  den sortens långsiktiga framtidsfrågor har sina egna utmaningar,  bland annat för att det ofta är långt till synliga resultat. Med en  stark motståndare som fossilkapitalet blir det desto viktigare att  samla människor brett och ge alla möjlighet att känna sig hemma i  rörelsen.  

Vänsterpartiet arbetar för att förändra samhället genom breda  folkrörelser och parlamentariska beslut. Demokrati är  samtidigt mer än lagar och regler. Civil olydnad är en yttersta  möjlighet för människor att försvara folkligt förankrade  värden som mänskliga rättigheter och naturresurser. Den  grundar sig på principer om icke-våld och öppenhet och är en  viktig del av demokratin.

[...]

#### Arbeta strategiskt

[...]

När vi tar fram förslag drar vi konfliktlinjerna medvetet för att  kunna samla människor brett. Där det går försöker vi isolera  aktörer som spelar en destruktiv roll, som fossilkapitalet eller  tobaksbolagen. Framstegen behöver komma alla till del och  samtidigt leda till jämlikhet och sammanhållning. Gemensamma  erfarenheter av segrar, av att framgångsrikt driva sina  intressen tillsammans, formar starka rörelser.

[...]

### Strategiska reformer

[...]

Vi vill se en samlad strategi för full sysselsättning som bygger på  stora, långsiktiga samhällsekonomiska investeringar. Det handlar  om att bygga upp ny infrastruktur och nya industrier, som tar oss ur  fossilberoendet och kärnkraftsberoendet. Det handlar om att  skapa ett starkt utbildningssystem, som ser till att alla människor  kan det de behöver på sina arbetsplatser och i livet i stort. Det  handlar om att bygga bostäder, som ökar människors frihet att bo  och leva som de vill.

[...]

#### Demokratiska ägandeformer

Vi vill att fler av de beslut som har stora, komplexa konsekvenser  för samhället ska fattas med ett genomtänkt helhetsperspektiv.  I nyckelsektorer som finans, infrastruktur, bostäder och energiförsörjning vill vi kraftigt stärka det gemensamma ägandet,  eftersom det är verksamheter där besluten i alla led behöver väga in  långsiktiga samhällsekonomiska mål. I vård, skola och omsorg  vill vi överhuvudtaget inte se några aktörer som är där för att  göra vinster i välfärden.

[...]

Världen behöver gå i riktning mot att upplösa den illegitima  politiska makt som till exempel fossilkapitalet sitter på. Det är inte  långsiktigt hållbart att ett litet fåtal samlar på sig absurda  förmögenheter, byggda med andra människors arbete. Varje genuint  demokratisk kraft behöver ta på sig uppgiften att förändra det.  

Vi vill utveckla mer övergripande demokratisk samordning av den  riktning ekonomin utvecklas i. De stora besluten om vår  gemensamma framtid behöver fattas demokratiskt, med en genuin  förståelse för hur människor, samhällen och ekosystem fungerar.

#### Ut ur fossilberoendet

Vi vill sätta fasta ramar för att ta Sverige ur fossilberoendet, med  en utsläppsbudget och ett omställningsprogram som kan ta oss hela  vägen till utsläpp nära noll. Det kommer att behövas stora  investeringar, ett aktivt gemensamt ägande och en beredskap att ta  konflikt med fossila ekonomiska intressen för att genomföra det.  Vänsterpartiet avvisar planerna på att bygga ut kärnkraften  som en väg ut ur fossilberoendet.   

En politisk strategi för omställning behöver lägga fokus på åtgärder  som både har stora utsläppseffekter och kan vinna brett politiskt  stöd. Den behöver bygga på förtroende för att de flesta människor  gör sitt bästa i vardagen och se till att det blir så enkelt som  möjligt. Den behöver en trygg och stabil takt i omställningen, där  vi tillsammans ser till att de tekniska skiften vi genomför fungerar  för alla. Den behöver bygga på rättvisa och tydlighet i att de  som står för de största utsläppen är först med att förändra sina  vanor.   

Vi vill visa att det går att förena en radikal omställning med att  bygga ett samhälle som är enklare att leva i, med full  sysselsättning, välfärd och trygghet. Den politiska verktygslåda  som behövs för att fasa ut det fossila kommer också att kunna  användas för att lösa andra stora samhällsproblem.

#### Sverige i världen

Stora samhällsekonomiska investeringar, gemensamt ägande och  jämlikhet skulle betyda att Sverige kan stå starkt också i tider av  konflikt i omvärlden. Vi vill utveckla ett bättre internationellt  samarbete mellan länder, som skapar ett större oberoende från  stormakternas geopolitiska spel. Tillsammans kan vi stärka alla  länders möjlighet att besluta om sin produktion, sina finansiella  flöden och sina naturresurser.

Vi vill att Sverige utmanar EU:s överstatliga fördrag och lagar  när de sätter kapitalets intressen före jämlikhet och nationellt  självbestämmande. Vi lägger fram förslag i konflikt med den  sortens EU-lagstiftning för att i förlängningen vidga utrymmet  för progressiv politik.   

Vi vill att Sverige ska vara ett land som deltar helhjärtat i arbetet  med de historiska utmaningar världen står inför, som att ta  människor ur fattigdom och genomföra klimatomställningen.  Sverige behöver vara en stadig röst i världen som talar för mänskliga  rättigheter, fred och nationellt självbestämmande.   

#### Frigörelse

[...]

Det är det vi tänker på som socialism. Att slå in på en socialistisk  väg framåt handlar om att organisera arbetet på bättre sätt, för att  bygga ett samhälle där vi kan leva friare liv. Där det finns bra  skolor, bostäder och möjligheter i livet för alla. Där det finns gott  om tid för kärlek och omsorg om varandra. Där ekosystemen har  utrymme att återhämta sig. Där vi går med sträckta ryggar på  arbetsplatsen. Där vi fattar de avgörande besluten tillsammans.  Vi vill lämna maktstrukturerna bakom oss och utveckla ett  jämställt och socialistiskt samhälle, byggt av var och en efter  förmåga, åt var och en efter behov.

[...]

## Vänsterpartiets grundsyn i sakfrågor

### Ägande med helhetssyn

Vi vill att de verksamheter vi äger tillsammans drivs med fokus på  den långsiktiga samhällsnyttan. Det handlar både om att ta fram  kloka ägarstrategier och att odla en kultur av långsiktighet och  helhetsperspektiv i styrningen av välfärd, myndigheter och statliga  bolag. Det betyder till exempel att besluten i alla led ska utgå från  en strategi för klimatomställning och präglas av genuin omsorg om  människor och natur. Vi vill utveckla dem till föregångare som  arbetsplatser, som sätter en ny och högre standard för till exempel  arbetsmiljö och jämställdhet.

Det skiftet mot att fatta beslut med helhetssyn behövs i fler  verksamheter än de som ägs gemensamt idag. Vi vill steg för steg  stärka det gemensamma ägandet i ekonomins nyckelsektorer  och därmed öka de demokratiska beslutens räckvidd. Det  handlar bland annat om strategiska naturtillgångar,  energiförsörjning, infrastruktur, bostadsproduktion och den  finansiella sektorn. Företag vars beslut har stora konsekvenser för  alla som bor här behöver ägandeformer som motiverar dem att lyfta  blicken och se det. Det skulle innebära nya möjligheter till en klok  samhällsekonomisk utveckling. Med ett bredare gemensamt ägande  kommer bättre möjligheter att samordna den ekonomiska  utvecklingen, till exempel kring de skiften i teknik och arbetssätt vi  behöver i klimatomställningen. Det ger också fler verktyg för att  skapa en regionalt balanserad ekonomisk utveckling.

Gemensamt ägda verksamheter kan användas för att driva  förändring också i privata företag. Deras inköp av varor och  tjänster är ett viktigt strategiskt verktyg för att genomföra  samhällsekonomiskt betydelsefulla skiften och etablera standarder.  På marknader som präglas av övervinster eller dåliga avtalsvillkor  kan gemensamt ägda företag sätta tryck genom att erbjuda bättre  villkor.  

Redan idag är det gemensamma ägandet större än vad många  inser. Genom pensionsfonderna äger vi tillsammans en  betydande del av börsbolagen. Vi vill använda den  ägandemakten till att aktivt styra Sveriges ekonomiska  struktur mot samhälleliga mål som långsiktig hållbarhet och  ordning och reda på arbetsmarknaden.

[...]

### Stora investeringar

Det är människors arbete som driver den ekonomiska utvecklingen.  Vi vill bygga ett samhälle där det finns bra och meningsfulla jobb  till alla. Omfattande investeringar för att ta itu med de stora  samhällsutmaningarna bildar kärnan i den samlade strategi för full  sysselsättning vi vill se. Riksbanken behöver demokratiseras och  driva en penningpolitik som prioriterar sysselsättningen högt.  

Vi vill att regeringen arbetar med en utsläppsbudget som knyter  ihop klimatmålen med konkreta, siffersatta förslag. Det gör det  möjligt att bygga upp strategier med fokus på det som är mest  avgörande.  

Sverige behöver också ett medvetet och heltäckande arbete med att  skapa goda villkor för rika ekosystem, bland annat genom att  återställa natur och kompensera för förlorade naturvärden.  Våtmarker spelar en särskild roll för att sänka klimatutsläppen.  Vi vill se ambitiösa miljömål som följs upp med skarp lagstiftning  och praktiskt arbete.   

För att ta tag i de stora samhällsutmaningarna kommer  skattenivåerna att behöva öka igen, framför allt på stora  förmögenheter, fastigheter, arv och höga inkomster. Vi vill se ett  enkelt, stabilt och solidariskt skattesystem där de rikaste åter börjar  betala sin del. Skatterna bidrar på många sätt till ett tryggt, jämlikt  och jämställt samhälle, särskilt om de utformas progressivt och  används till saker alla har användning av.  

Skatter kan spela en viktig kompletterande roll för att driva på  klimatomställningen. De behöver utformas klokt för att få verklig  effekt och vinna politiskt stöd. Kostnaderna för utsläpp behöver  öka långsiktigt på ett sätt som är tryggt och förutsägbart, i takt med  att samhället bygger upp nya, bättre alternativ.

### Seriöst företagande

Vi vill se goda villkor för sunda företag, som tar tillvara på  människors drivkrafter att bygga upp egna verksamheter och riktar  dem mot samhällsekonomiska behov. Lagar och sammanhang kan  göra stor skillnad för att företagandet ska ta sig positiva uttryck. De  investeringar vi vill se i utbildning, infrastruktur och trygghet  skulle skapa goda förutsättningar att starta nya, seriösa  arbetsplatser.  

Sverige är ett land med goda förutsättningar för industriproduktion.  Vi har välutbildad arbetskraft, god tillgång till förnybar energi och  värdefulla råvaror. Sveriges export kan fylla viktiga funktioner i  den globala klimatomställningen.   

Vi vill att Sverige beslutsamt genomför de strategiska teknikskiften  som behövs i industrin för att komma ur fossilberoendet. Staten har  viktiga roller att spela i det, i allt från forskning till elproduktion,  infrastruktur, utbildning och ett ekonomiskt ramverk som driver på  utvecklingen. Det är en nyckeldel i ett bredare skifte till  cirkulär ekonomi och långsiktig hållbarhet i industrin.   

Vi vill gå mot ett större gemensamt ägande av gruvbrytningen i  Sverige för att säkra ett seriöst arbete och fördela vinsterna till hela  befolkningen. Den behöver drivas med ett långsiktigt  helhetsperspektiv som ser till såväl de globala behoven som lokala  naturvärden och den ekonomiska utvecklingen i närområdet.  

Jordbrukets långsiktiga hållbarhet är viktig för både ekosystemen  och Sveriges ekonomiska självständighet. Staten har ett särskilt  ansvar att se till att landet är tillräckligt självförsörjande med  livsmedel. Vi kommer att behöva ett långsiktigt skifte mot mer  hållbara matvanor för att klara omställningen. All djurhållning ska  utgå från att djur är kännande varelser som ska leva goda liv.  

Hur vi använder skogen har stora och komplexa konsekvenser, som  bland annat handlar om hur vi binder kol i naturen och i produkter.  Det behövs ett starkt skydd av skog med rika ekosystem och en  genomtänkt reglering av skogsbruket, som utgår från en  helhetssyn och inte ensidiga vinstintressen. Biomassan är en  begränsad resurs, särskilt i relation till världens klimatomställning.  Det kommer att vara viktigt att prioritera användningen av den  klokt.

## Infrastruktur och bostäder

En radikal utbyggnad av den förnybara elproduktionen är  avgörande för Sveriges klimatomställning, krisberedskap och  ekonomiska utveckling. Det möjliggör en elektrifiering som kan ta  landet ur beroendet av importerad fossil energi. Vi vill se en  energipolitisk strategi som bygger på förnybar el och låga  klimatutsläpp. Nya projekt behöver jämföras i miljökonsekvenser  och säkerhet över hela sin livscykel. Vi ser el och värme som en del  av Sveriges infrastruktur, där investeringar och priser behöver utgå  från samhällsekonomiska mål.   

Vi vill bygga ett samhälle där varje människas rätt att ha  någonstans att bo är verklighet. Mot det står de ägarintressen som  vinner på bostadsbristen genom höga hyror och bostadspriser.  Hyresgäster behöver ett starkt stöd i lagstiftningen för att  tillsammans stå starka och försvara sina intressen.   

Vi vill se en generell bostadspolitik med goda och jämlika villkor  för alla boendeformer. Staten behöver ta huvudansvaret för att  stärka investeringarna i byggande, upprustning och  energieffektivisering av hyresbostäder. Vi vill prioritera att  bygga hyresrätter med rimliga hyror. Den gemensamt ägda  Allmännyttan behöver stärkas. Allmännyttan ska i första hand  ha ett samhällsansvar och inte styras av affärsmässiga  principer.   

Bostäderna behöver vara en del av ett större samhällsbygge,  där vi skapar goda boendemiljöer med skyddande grönska,  mötesplatser, service och arbetsplatser. Planeringen behöver en  genuin demokratisk förankring och utgå från människors kärlek till  platsen man bor på. Vi vill bryta upp segregationen och skapa en  blandning av hustyper och upplåtelseformer, för att bygga städer  där människor möts i vardagen.   

Det gemensamma ägandet av bostäder och mark ger starka verktyg  för stadsplaneringen. Byggande, förnyelse och förvaltning fungerar  bättre utan ensidigt vinstsyfte. Allemansrätten, som ger var och  en fritt tillträde till naturen utan att behöva äga den, och  kulturmiljölagen, som skyddar vårt kulturarv, är viktiga  för att alla människor ska kunna leva goda liv. Samma sak  gäller strandskyddet, som ger allmänheten tillträde till kuster  och sjöar, bevarar känsliga djur- och växtliv och stärker  motståndskraften mot klimatförändringarnas konsekvenser.  

Vi vill se en samhällsplanering som så långt som möjligt undviker  att låsa människor i bilberoende. Det behöver bli enklare att röra  sig i städerna och närmare till välfärd och service på landsbygden.  Vi vill se en sammanhållen strategi för elektrifiering av  transporterna, där stadsplanering, kollektivtrafik och elfordon alla  har viktiga roller att spela. Det behövs en omfattande och  långsiktig investeringsplan för det, som bland annat innehåller  en utbyggnad av järnvägen. Gemensamma ägandeformer och  samordning är avgörande för att trafiken och underhållet ska  fungera bra.   

Flygtrafiken är en av de stora utmaningarna i omställningen.  Eftersom möjligheterna till större tekniska skiften i flyget ligger så  långt i framtiden är det viktigt att bygga upp ekonomiska ramverk  där flyget börjar stå för sina samhällsekonomiska kostnader. Det  skulle sänka utsläppen och samtidigt stärka jämlikheten, eftersom  de ohållbara resvanorna framför allt finns i samhällets toppskikt.

### Kultur, media och föreningsliv

[...]

Idrott och friluftsliv spelar stor roll för många människors glädje,  välmående och sociala sammanhang. För att deltagandet ska vara  öppet för alla behöver dess organisationer vara förankrade i  föreningsdemokrati och stå starka mot marknadsintressen. En god  samhällsplanering som ser till att människor har nära till  idrottsanläggningar och naturupplevelser gör också stor skillnad.

[...]

### Alla människors lika värde

[...]

Den svenska staten har genom historien utsatt urfolk och  nationella minoriteter för allvarliga oförrätter. Antisemitiskt  tankegods och andra rasistiska föreställningar var länge uttryckliga  delar av myndigheters syn på människor. Exploateringen av  norra Sverige är en del av en lång kolonial historia. Sverige  har en särskild uppgift att stärka urfolks och de nationella  minoriteternas rättigheter till identitet, språk, kulturarv och religion  som behöver ses i ljuset av den historien. Det kräver också en  särskild medvetenhet om markanvändningens betydelse.

[...]

### Brottsbekämpning

[...]

Ekonomisk brottslighet i företag leder till allvarliga konsekvenser,  bland annat i form av farlig arbetsmiljö och miljöförstöring. Det  fifflet leder till att seriösa företag slås ut och skatteintäkter  uteblir. Det handlar i hög utsträckning om överlagda beslut, där  risken för upptäckt kan spela stor roll. Vi vill gå mot en större  samhällelig insyn i företag där det behövs för att stävja fusk med  till exempel skatter och anställningar. Det behövs en effektiv  uppföljning av den organiserade ekonomiska brottsligheten för att  förhindra att den också börjar ta sig uttryck i till exempel  korruption eller våld.

### Internationella relationer

Vi vill se ett robust samhälle som är byggt för att klara olika sorters  kriser. Investeringar i samhällets kärnverksamheter, gemensamt  ägande i nyckelsektorer och medvetna strategier för landets  ekonomiska utveckling är viktiga delar av det. Sverige har goda  möjligheter att bli mer självförsörjande på energi och livsmedel.

[...]

Människor världen över har gemensamma intressen av ekonomisk  och demokratisk frigörelse. Det syns till exempel i arbetet för att  säkra generiska läkemedel eller mot handelsavtal som inskränker  utrymmet för demokratiska beslut. Vi vill att Sverige verkar för  skuldavskrivning, solidariskt bistånd och rättvisa handelsvillkor.  Sverige behöver utveckla ett konstruktivt samarbete med andra  länder i klimatomställningen. Vi vill att Sverige ska spela en  pådrivande roll för långtgående internationella klimatavtal, som  både underlättar omställningen och ställer krav på takten.  Tekniköverföring och klimattullar är viktiga verktyg för det. De  länder som historiskt stått för de största utsläppen har ett särskilt  ansvar att ta. Omställningen behöver gå hand i hand med att ta  världen ur fattigdom.  

Väl fungerande internationella samarbeten behöver byggas med  den nationella självbestämmanderätten som grund. Alla länder och  regioner ska ges möjlighet att kontrollera sin produktion, sina  finansiella flöden och sina naturresurser. Folkrätten behöver  försvaras och fördjupas. På områden där det finns specifika behov  av bindande samarbeten är mellanstatligt överenskomna  minimiregler vanligtvis en bättre väg att gå än överstatliga beslut.  

Vi vill se ett tätt samarbete mellan de nordiska länderna och ett gott  samarbete mellan de europeiska länderna. Det är särskilt viktigt på  områden som klimat, miljö, energi, transporter och krishantering.  Genom att agera tillsammans ökar också ländernas möjligheter att  reglera finansiella flöden och internationella storföretag. Den  europeiska konventionen om skydd för de mänskliga rättigheterna  och de grundläggande friheterna är en bra grund för samarbete.  

[...]

De framsteg som trots förutsättningarna gjorts inom ramen för EU  är viktiga att försvara och bygga vidare på. Det handlar bland annat  om utbyte mellan länder och vissa klimat- och miljöregleringar.  Ibland uppstår komplexa situationer där en kortsiktig framgång  riskerar att leda till långsiktiga problem, för att frågor börjar  avgöras i EU istället för i ländernas långt mer demokratiska  processer. Vi agerar pragmatiskt för att både förändra inom ramen  för EU:s strukturer och utmana dem, beroende på situationen.

[...]


## Läs hela programmet

Avslutningsvis vill jag återigen uppmana att [läsa hela programmet](/vansterpartiets-partiprogram/).