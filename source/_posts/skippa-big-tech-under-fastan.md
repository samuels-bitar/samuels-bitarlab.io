---
title: Skippa Big Tech under fastan
image: mobilizon-wall.jpg
image_description: Bild från <a href="https://joinmobilizon.org/en/">Join Mobilizon</a>
description: Jag gör ännu ett försök med att försöka få svar från Liberalerna på rätt enkla frågor.
date: 2025-03-03 12:00
---

Jag älskar [fastan](https://sv.wikipedia.org/wiki/Fasta)! Det kan vara för att jag älskar rutiner, system, högtider och traditioner. På samma sätt älskar jag midsommar och jul. Men något som är extra najs med fastan är att man under begränsad tid kan testa på att göra saker annorlunda.

Inom den ortodoxa fastan avstår man från animalier, dvs man käkar veganskt under fasteperioden. I den muslimska fastan ramadan fastar man under dagen från mat och vatten. I vanlig svenskkyrklig eller frikyrklig tradition så innebär fastan inte så himla mycket i vad man "borde göra". Man får liksom göra lite vad man känner för. Men det har blivit vanligare bland kristna i protestantiska kyrkor att även avstå från vissa saker under fastan.

Själv har jag fastat från alkohol och från animalier tidigare fasteperioder. Det har också varit min "inkörsport" till nykterism och veganism. Men det är bra att ha en begränsad tid då man testar något. Alltså mellan askonsdagen (dagen efter fettisdagen) och påsk.

Vänsteraktivisten Jonas Lundström har [bloggat intressant om fasta](https://www.patreon.com/posts/fasta-och-119474647). Trots att han inte längre är kristen ser han flera positiva saker med fastan i olika religioner.

## Fasta från Big Tech

Min uppmaning till vänner och kamrater som läser detta är att genomföra en Big Tech-fasta!

**Man måste så klart inte vara religiös för att fasta!** Alla kan göra det.

Det kan bestå av att man antingen avstår från något eller att man tillför något. Därför kommer mina spridda tips här:

* **Skippa Facebook, Instagram, X och Tiktok**. Om du måste använda i jobbet så undvik att använda dessa Big Tech sociala medierna till privata och sociala saker.
* **Ta bort Facebook, Instagram, X och TikTok från mobilen**. På så sätt slipper man det slentrianmässiga kollandet på telefonen.
* **Skapa ett Mastodon-konto**. Skapa ett Mastodon-konto ([guide finns här](https://wiki.konstellationen.org/sv/guider/mastodon)) och hitta ett gäng [konton att följa](https://wiki.konstellationen.org/sv/guider/mastodon-hitta-folk).
* **Be föreningar skaffa nyhetsbrev**. Är du medlem i en förening som informerar allt som händer via Facebook? Be dem att informerar via nyhetsbrev. Passa gärna på att tipsa dem om att man kan använda https://organisera.org för att skapa Facebook-events. Om man vill få intro till det här kan man läsa [detta blogginlägget](https://konstellationen.org/2025/01/24/organisera-mobilizon/).
* **Flytta chattgrupper till Signal**. Har du massa chattgrupper via WhatsApp eller Messenger? Kanske några kompisar eller familjemedlemmar? Skapa en ny grupp på Signal och bjud in dem där istället. Signal funkar bra för personer som är tekniskt ovana. Det är enkelt att få folk över, speciellt om man har använt WhatsApp tidigare.
* **Testa decentraliserade chattappar**. Du kanske redan har koll på Signal och gillar det men är orolig för att det är centraliserat? Det är lättare att stänga ner en plattform eller nätverk om den är centraliserad och så som det ser ut i USA och EU så finns det ändå en risk att man försöker stänga ner Signal. Testa då några fria och decentraliserade alternativ! Det finns en uppsjö! Läs mer längr ner
* **Starta en enkel hemsida**. Det är väldigt bra att ha en egen hemsida av en rad olika orsaker. En viktig grej är att man där kan lägga information om hur man kontaktar en. Som ett slags digitalt visitkort. Men man kan också använda den för att blogga så klart vilket är superkul! Jag har flera olika hemsidor och bloggar. Min enklaste är min "visitkortshemsida" https://bitar.se. Sen har jag även bloggarna https://samuels.bitar.se och https://text.bitar.se. Se längre ner för mer info om hur man gör.
* **Skapa nästa event på Organisera.org**. Är du aktiv i en förening som anordnar olika events? Kanske det brukar skapa events på Facebook där folk kan läsa mer och anmäla sig? Se till att nästa event görs på en fri plattform! [Organisera.org](https://organisera.org/) är en Mobilizon-instans som Kamratdataföreningen Konstellationen driftar för att erbjuda alternativ till Facebook events. [Lär mer här](https://konstellationen.org/2025/01/24/organisera-mobilizon/).

## Decentraliserade chattappar

Här nedan beskriver jag några appar som är öppen källkod, decentraliserade och har stöd för totalsträckskryptering. Totalsträckskryptering innebär att bara du och den/de du pratar med kan läsa innehållet. Inte ens servern som ni använder kan se vad ni skriver.

**Matrix/Element** är ett bra alternativ till Discord, Slack eller Messenger. Läs [guide här](https://wiki.konstellationen.org/sv/guider/chat-alternativ) och skicka gärna ett meddelande till mig på `@samuel.skanberg:matrix.org`). På Matrix kan man skapa chattrum och skicka enskilt till olika personer. Det har snygg grafiskt gränssnitt som känns modernt och har det mesta man förväntar sig. 

Du kan också testa **XMPP** som är ett stabilt och beprövat alternativ. Det är standariserat protokoll som funnits lämge. [Kolla här](https://xmpp.org/getting-started/) för hur man kommer igång med det. Man behöver hitta en server där man kan skapa konto. Det finns en lista på servrar [här](https://providers.xmpp.net/) och [här](https://list.jabber.at/). Jag har ett konto på linuxkompis.se som är ett svenskt alternativ. Man kan [läsa mer här](https://linuxkompis.se/2021/12/16/en-introduktion-till-snikket.html). Du kan kontakta mig på `samuel@snikket.linuxkompis.se` eller `samuel.skanberg@disroot.org`.

**DeltaChat** är också spännande. Det som är kul med den är att appen använder sig av den befintliga e-post-infrastrukturer. Ja, den skickar mail :) Väldigt fiffigt! Man kan använda sin egen befintliga e-post om den [stöds](https://providers.delta.chat/). Annars kan man enkelt skapa ett nytt konto med hjälp av de så kallade [chatmail server](https://delta.chat/en/chatmail) som finns tillgängliga och är anpassade för att köra DeltaChat. Du kan skriva till mig på `samuel@fripost.org`.

## Starta en hemsida

Att starta hemsida kan vara superenkelt om man vill! Och det finns massvis med olika lösningar vilket jag [bloggat om här](https://samuels.bitar.se/jakten-pa-bloggen/).

Om du bara vill ha en enkel statisk sida föreslår jag du kollar in [guiden här](https://fritext.org/). Om du vill kunna blogga och göra det gratis kan du använda en så kallad Static Site Generator (SSG). [Här finns en guide](https://wiki.konstellationen.org/sv/guider/blogg-med-gitlab) om hur man startar en sådan sida. [Och här](https://wiki.konstellationen.org/sv/guider/skriva-blogginlagg-med-gitlab-pages) kan man läsa om hur man skriver blogginlägg.

Självklart kan man också bara skapa en gratis sida på https://wordpress.com men får ta vara beredd på reklam.

Om du vill göra det hela ordentligt och vill ha det enkelt så föreslår jag att du betalar för en egen domän (t.ex. `annashemsida.se`) och [webbhotell på Inleed](https://inleed.se/webbhotell). Domän och webbhotell kostar runt 500 kronor per år.

## Testa och se!

Man behöver inte ta bort sitt Facebook-konto. Jag har själv [tagit bort mitt Facebook-konto](https://samuels.bitar.se/befrielsen-fran-facebook/) men vissa har svårt för att man använder det i sitt jobb. Det måste inte vara allt eller inget. Man kan de steg man känner man klarar av just nu.

Det här inlägget handlar om att under en tid på allvar utforska alternativen till Big Tech sociala medier. Vem vet, du kanske upptäcker att det ju faktiskt var lätt och himla trevligt att hänga med oss utanför storföretagens kontroll.