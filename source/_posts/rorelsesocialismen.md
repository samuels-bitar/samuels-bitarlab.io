---
title: Rörelsesocialism, engagemang och en massa tankeprojekt
image: lego.jpg
image_description: Bild av <a href="https://pixabay.com/users/eak_kkk-907811/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1044891">Eak K.</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1044891">Pixabay</a>
description: Jag skriver om boken Rörelsesocialism, om mitt eget engagemang inom Vänsterpartiet, om Kamratdataföreningen Konstellationen och olika projekt jag funderar på
date: 2025-01-07 19:00
---

Nu har jag läst boken [Rörelsesocialism: Tjugosju teser om samhällsförändring underifrån](https://verbalforlag.se/bocker/rorelsesocialism/) av Toivo Jokkala och Per-Anders Svärd och uppskattade den verkligen. Jag tänkte skriva lite om den, om mitt engagemang inom Vänsterpartiet och om andra engagemang.

Om jag skulle sammanfatta boken skulle jag säga att bokens poäng är att politisk förändring sker genom politiskt tryck underifrån. Det innebär inte per automatik att parlamentarism eller politisk förändring genom staten är dåligt, onödigt eller ointressant men snarare att även de som vill förändra med hjälp av staten så kommer de misslyckas om de inte lyckas mobilisera politiskt tryck.

Boken landar väl i mig och jag inser att jag nog törstat efter något sådant här.

## Vänsterpartiet

Jag har sedan 2014 engagerat mig politiskt i Vänsterpartiet på klassiskt partipolitiskt sätt: jag har delat ut flygblad, suttit i styrelser inom Vänsterpartiet, suttit i kommunfullmäktige, suttit i nämnder och beredningar, engagerat mig i nätverk inom Vänsterpartiet, deltagit på flertalet distriktsårskongresser och centrala partikongresser och även suttit i partistyrelsen. I mångt och mycket har det varit bra och jag har lärt mig otroligt mycket och fått många kamrater och vänner. 

För min del har jag varit engagerad huvudsakligen i klimat- och miljöfrågor och i någon mån djurrätt och frågor om övervakning och digitalisering. När jag engagerade mig i partiet kände jag mig verkligen hemma politiskt. Jag höll kanske inte med om allt men om det allra mesta. Jag var så glad över politiska företrädade som Jonas Sjöstedt, Jens Holm och Malin Björk. Och i Vänsterpartiet Storstockholm var Elena Karlström distriktsordförande och var inte rädd för att driva frågor om klimat- och miljöfrågor och att koppla det till civil olydnad. Jag kände mig trygg med att bli uppbackad av partiet när jag deltog i [aktionerna mot brunkol genom Ende Gelände](https://www.etc.se/klimat-miljo/massiv-klimataktion-mot-tyskt-brunkol). Andra kamrater från partiet var nere och skrev resedagbok på [Vänsterpartiet Storstockholms hemsida](https://storstockholm.vansterpartiet.se/2018/11/07/ende-gelande/).

Några fler saker jag skrivit om:

* [Klimataktivismen frodas i Vänsterpartiet](https://haninge.vansterpartiet.se/2018/08/17/klimataktivismen-frodas-i-vansterpartiet/)
* [Klimatengagemanget har växt enormt i partiet](https://storstockholm.vansterpartiet.se/2020/01/21/klimatengagemanget-har-vaxt-enormt-i-partiet/)

Nu när jag sitter och går igenom gamla blogginlägg så skälver hjärtat till för jag kommer ihåg vilken känsla det var när jag själv och partiet gick i samma riktning utan att det skavde.

Men sen blev mer utmanande att varit engagerad i Vänsterpartiet om man kommit in som jag med frågor om klimat och miljö och civil olydnad. Jonas Sjöstedt slutade som partiordförande och Nooshi Dadgostar tog över. Strategier och kommunikation blev på ett annat sätt. Jonas var ju personligt engagerad i klimat- och miljöfrågor och på så sätt hade jag väl mer tur än andra då det var mina hjärtefrågor också. Jag har förståelse för att Nooshi hade en egen ledarstil och satte sin prägel på partiet. Inget konstigt med det. Och vem vet, förändringen kanske redan hade pågått ett tag men det blev tydligt med den nya partiledningen.

Det var flera saker som förändrades i kommunikation och inriktning. Vissa grejer kanske var rimligt att förändra, andra ostrategiska om man inte i onödan ville reta upp klimat- och miljörörelsen. Här är några artiklar som tar upp debatten:

* [Splittring bland V:s klimataktivister – är tillväxt bra eller dåligt?](https://www.flamman.se/splittring-bland-vs-klimataktivister-ar-tillvaxt-bra-eller-daligt/)
* [Moderat halsbandstrick förebild – så vill V växa](https://www.svd.se/a/OrXeQl/nya-v-direktivet-undvik-att-prata-kottkritik-och-bilskam)
* [Kritik mot vänsterns SD-flört: ”Stryker industrin medhårs”](https://www.aftonbladet.se/nyheter/a/wOkVlo/kritik-mot-nooshi-dadgostars-sd-flort)

Vissa diskussioner var nog att man pratade förbi och misstolkade varandra. Och vissa initiativ lades av media in i ett narrativ, som att satsningen på bruksorter och landsbygd bara skulle vara för att locka SD-väljare. Det blev också en ihopblandning av satsningen på bruksorter och klimatinvesteringsprogrammet på 700 miljarder kronor. Det var saker som kunde överlappa men var olika saker.

Hur som helst, många klimat- och miljöengagerade blev oroade inom partiet på grund av hur kommunikationen förändrades och hur vissa frågor tonades ner eller glömdes bort medan det blev väldigt starkt fokus på andra. Jag och några andra klimat- och miljöprofilerade kamrater kandiderade till partistyrelsen och blev valda. Jag hoppas och tror att vi kunde bidra till att undvika missförstånd mellan klimat- och miljörörelsen brett och partiet.

Det var svårt att sitta i partistyrelsen. Man förväntades att backa upp fattade beslut och inte vara alltför kritisk till beslut som man inte höll med om. Det blev en dragkamp mellan att stå upp för det man tror på och samtidigt respektera att partistyrelsen är ett kollektiv och om alla skulle ut och veva i frågor man inte helt håller med om skulle det bli rätt rörigt. Jag lyckades kanske inte hitta den perfekta balansen utan kanske självcensurerade något mer än vad jag kände att jag borde ha gjort. Men å andra sidan så kanske de andra kamraterna i partistyrelsen tog mig mer på allvar då? Om jag var en lagspelare? Det kanske låter banalt men det var rätt jobbigt. Och inte blev det lättare när Ukrainakriget kom och frågan om att exportera vapen till Ukraina kom upp. Extra dålig start på den nya partistyrelsen. Vi hade nyss blivit valda och hade inte ens hunnit ha ett vanligt partistyrelsemöte. Frågan dök upp snabbt, regeringen skulle snabbehandla den och vi hade tajt med tid. Det var upplagt för att bli riktigt dåligt. Vi hade ett långt och ångstfyllt teams-möte och landade i att inte rösta för att exportera vapen. Det fick vi mycket kritik för, både från riksdagsgruppen (där många inte dök upp på omröstningen eller lade ner sina röster) och sedan kallades det till ett nytt partistyrelsemöte där vi bytte fot och sedan var fr vapenexport till Ukraina. För mig var hela den vändan en smärtsam process, inte minst för att jag varit aktiv i Kristna Fredsrörelsen och varit engagerad i ickevåld som metod.

### Stå upp för kollektivt fattade beslut och självcensur

Det var så klart även givande att sitta i partistyrelsen. Det är ett förtroende och det var lärorikt. Men jag lade ner otroligt mycket tid på att lägga förslag för att förändra olika skrivningar och beslut. Men den absoluta majoriteten av mina förslag röstades ner. Det tog också rätt hårt och jag funderade på om jag verkligen skulle sitta kvar. Vad var poängen att jag satt där? Lyssnade de andra ens på vad jag sa? Ja, det fanns en handfull andra som jag oftade tänkte liknande som mig och som ofta röstade på mina förslag men det var en minoritet. Men det är kanske ett väldigt egocentriskt sätt att se det på. Det kanske var så för de flesta andra också. Jag hade en uppfattning om hur ofta mina förslag röstades ner (ofta) men hade ingen direkt uppfattning om hur det var för andra. Så jag kanske inte ska tycka så synd om mig som jag gjorde då. Men det var i alla fall rätt kämpigt.

Vad gör man då när man känner att man inte blir lyssnad på i det sammanhang där man har mest makt? Och när man i andra sammanhang helst ska vara tyst om vad man egentligen tycker? Ja, då blir det inte jättemycket. Så mitt engagemang minskade. Jag tappade nog en del energi. Sen tog partistyrelsen så pass mycket tid om man skulle vara förberedd inför mötena (vilket jag försökte vara) så det var svårt med så många andra engagemang. Jag lade ner en del tid speciellt inför valen att blogga om vår politik och det var saker där jag kände att jag själv kunde välja vad jag ville skriva om.

Det var en del där jag kände att jag faktiskt kunde bidra med. Så det blev t.ex. dessa blogginlägg:

* [Lokal klimat- och miljöpolitik av Vänsterpartiet](https://samuels.bitar.se/lokal-klimatpolitik/)
* [Elva anledningar att rösta på Vänsterpartiet](https://samuels.bitar.se/elva-anledningar-att-rosta-v/)
* [Peppad efter Vänsterdagarna](https://samuels.bitar.se/vdagarna/)
* [Vänsterns svar på klimatkrisen, en välbehövlig boost](https://samuels.bitar.se/vansterns-svar-pa-klimatkrisen/)

Jag försökte också fortbilda inom partiet så att man förstod hur det låg till med klimatet och var utsläppen kom ifrån. Det blev:

* [Var kommer utsläppen från?](https://samuels.bitar.se/klimat-sektorer/)
* [Klimatkrisen och förlusten av den biologiska mångfalden hänger ihop](https://samuels.bitar.se/klimatet-naturen/)
* [Hur får vi ner utsläppen från resor och transporter](https://samuels.bitar.se/klimatet-resor-och-transporter/)

Under partikongressen 2024 skulle ombuden ta ett nytt partiprogram. Det var programkommissionen som hade tagit fram ursprungsförslaget (alltså inte partistyrelsen) och det var programkommissionen som svarade på motionerna.

I partistyrelsen fick vi dubbla budskap om hur vi skulle agera. Å ena sidan skulle partistyrelsen inte fatta beslut om vad vi tyckte om olika motioner, det var programkommissionens förslag. Å andra sidan skulle vi gå upp och tala för partistyrelsens linje. Jag vet inte riktigt vad det innebär och vi var flera som var förvirrade. Så jag talade utifrån vad jag tyckte var rätt tydligt, om strategidokumementet (som tagits på tidigare kongresser) och hur jag tyckte motionerna lirade om det. Jag skrev inför kongressen en lista på [mina favvo-motioner](https://samuels.bitar.se/klimatmotioner-vkongress-2024/).

Flera motioner röstades igenom som gjorde förslaget ännu bättre. Och i slutändan blev jag själv rätt så nöjd med resultatet. Partiprogrammet finns att läsa på [Vänsterpartiets hemsida](https://www.vansterpartiet.se/resursbank/partiprogram/) (eller [kopia här](https://samuels.bitar.se/vansterpartiets-partiprogram/)). Eftersom fokus för min del låg på klimat och miljö gjorde jag en snabbgenomgång efteråt om vad [partiprogrammet säger om klimat och miljö](https://samuels.bitar.se/vansterpartiet-partiprogram-klimat-miljo/).

Det hade som sagt varit rätt jobbigt att sitta i partistyrelsen så jag kandiderade inte till omval. Andra bra kamrater blev invalda.

### Inte alla ägg i en korg

Under tiden som jag satt i partistyrelsen så drog jag och andra goa kamrater (huvudsakligen utanför partiet) igång [Kamratdataföreningen Konstellationen](https://konstellationen.org/2023/02/25/en-forening-ar-fodd/). Det tror jag var väldigt viktigt för mig. Jag kände att var svårt när alla mina "engagemangsägg" låg i samma korg: Vänsterpartiet. Det blev lättare att hantera motgångar eller svårigheter i partiet när jag visste att Konstellationen inte påverkades av det. Istället blev det ett sätt att få utlopp för energi. Jag deltog också på några möten och events med Allt åt alla i Stockholm, något som kunde vara roligt att återuppta.

Det var liksom inte en helt stabil strategi att ha alla ägg i en korg. Och jag tror flera andra kamrater i partiet drabbades hårdare av motgångar. Dels för att de kanske hade starkare känslor kring dem. Men också för att deras engagemang huvudsakligen var Vänsterpartiet.

Det är bra att det finns en interndebatt i ett parti och det är viktigt att inte alla som är kritiska mot ledningens beslut lämnar partiet. Men samtidigt var det otroligt mycket energi som gick åt att kritisera ledningen. Jag kunde flera gånger instämma i kritiken men jag tvivlar att så mycket kom ut ur det. Många tappade energin också när man kunde bli kritiserad hårt och rätt okamraligt om man försvarade ledningens beslut.

Jag tror mer positivt hade kommit mer ut av den besvikelsen om några av kritikerna (inte alla) hade lagt den energin på annat. Kanske tagit en timeout från partiet, eller arrangerat bra saker i sin egna partiförening. Vänsterpartiet är uppbyggt på en solid medlemsdemokrati. Men det är represenativt och vad ledningen tycker och gör spelar stor roll. Därför så blir väldigt mycket fokus på vad man kan göra eller säga för att ledningen ska agera/säga på ett annat sätt. Det känns som raka motsatsen till anarkisternas "handling utan ombud".

### Framåt då?

Men har jag då lämnat Vänsterpartiet? Nej, det har jag inte. Jag engagerade mig inför EU-valet och efter det bra valresultatet vi gjorde försökte jag säga hej då från min tid i partistyrelsen med den något optimistiska och framåtblickande texten i Flamman: [Vänsterpartiet har underskattat folkets klimatengagemang](https://www.flamman.se/vansterpartiet-har-underskattat-folkets-klimatengagemang/).

Men däremot så känner jag för tillfället att jag har svårt att hitta energin till engagemanget just nu genom Vänsterpartiet. Så jag stannar kvar som medlem, är fortfarande med i styrgruppen för klimat- och miljöpolitiska nätverket där jag mest hjälper till genom att vara tekniskt support när vi anordnar webbinarier.

Jag tror jag kommer återkomma till partiet framåt. Och jag är inställd på att göra ett arbete under valrörelsen. Men mitt primära engagemang lägger jag på andra ställen.

Det finns massvis med engagerade kamrater som jag tycker gör ett fantastiskt jobb. Om de har ork och energi så tycker jag de gör bra arbetet där de är. Men för egen del så tror jag inte jag kommer hitta energin just nu. Det kanske är så enkelt att jag tar en paus några år, eller blir inspirerad av en ny kampanj som partiet anordnar eller att det kommer in nytt folk med spännande idéer och driv.

Och Vänsterpartiet är utan konkurrens det parti som ligger mig närmast och det parti jag uppmanar alla att rösta på.

## Engagemang utanför partiet

Mitt engagemang just nu kan sammanfattas i:

* Kamratdataföreningen Konstellationen
* Kollektivhuset (matlag, cafégrupp, osv)
* Teknisk support för Vänsterpartiets- klimat och miljönätverk

Mitt politiska engagemang är väl huvudsakligen Konstellationen. Där känner jag också att jag gör skillnad. Att kombinera politik med teknik är något som behöver göras och som det visar sig att många andra håller med om.

Det är också så himla kul att göra grejer med Konstellationen! Lite kort:

* Kul att drifta [mastodoninstansen Spejset](https://social.spejset.org/) där jag snackar med vänsterfolk och många andra
* Kul att vidareutveckla [Mediakollen](https://mediakollen.org), a.k.a. "vänsteromni"
* Kul att anordna [hackathons](https://konstellationen.org/2024/11/14/hackathon-2024-rapport/) och [AW-hackathons](https://konstellationen.org/2024/11/14/aw-hackathons-2024/)
* Meningsfullt att opinionsbilda mot Chat Control genom [rapporter](https://konstellationen.org/2024/09/19/chatcontrol-rapport/), debattartiklar och [demonstrationer](https://konstellationen.org/2024/06/08/picknick-pressmeddelande/)
* Himla fint att vi anordnar [studiecirklar](https://konstellationen.org/2024/10/08/studiecirkel/)
* Fint att få ta del av [medlemmars bloggande](https://konstellationen.org/2024/10/31/facebook-till-rss/)

## Att vara en del av en rörelse

Konstellationen är partipolitiskt obunden. Men [vi är](https://konstellationen.org/om-oss/) en tydligt politiskt förening:

> Föreningen vilar på en demokratisk, socialistisk, feministisk och antirasistisk grund.
>
> Föreningen har som ändamål:
> * att främja och tillhandahålla fria och öppna verktyg och digitala plattformar
> * att sprida information och bedriva utbildningsverksamhet
> * att verka för demokratisk medlemsstyrning av verktyg och plattformar

Våra medlemmar består av socialister, kommunister, syndikalister, anarkister eller folk som är allmänt vänster men ser behovet av att vänstern och andra sociala rörelser tar tillbaka makten över sin kommunikation, data och infrastruktur på nätet. På så sätt är vi en del av en rörelsesocialism.

Man behöver inte vara tekniskt kunnig för att vara med i Konstellationen men många är det. Vi skapar forum där folk kan träffa likasinnade. Superroligt om vi tillsammans kan göra saker i föreningens namn men om det innebär att vi kan föra samman kamrater som annars inte skulle träffas så är mycket vunnet.

## Parlamentarism eller anarkism

Genom Konstellationen och mitt häng på Mastodon så har jag kommit att hänga ännu mer med kamrater inom den utomparlamentariska vänstern. Det i sig behöver inte betyda att de är anarkister eller anti-parlamentarister. Men de har sitt engagemang utanför parlamenten och ägnar inte dem alltför stor tankekraft i sitt arbete. Det har varit fantastiskt skönt tycker jag!

När man lever med en blåbrun regering som försämrar sak efter sak och slaktar välfärden, public service, folkbildning, och... ja, listan kan göras lång, då kan man lätt bli paralyserad. Och det kan hända att man inte gör något mer än att bara klagar över att allt går åt helvete. Här har jag fått energi och förebilder i kamrater från den utomparlamentiska vänstern. De gör saker. VI gör saker. Här och nu. Är man ledsen åt att regeringen förstör? Ja, självklart! Men genom att arbeta och kämpa på så sätter man själv dagordningen för sitt liv och för sina tankar. Det behöver inte betyda att man blundar för allt som har med partipolitik att göra eller vad regeringen pysslar med. Man kan självklart delta på demonstrationer, aktioner, namninsamlingar, osv för att påverka parlamenten. Men det är inte där all energi hamnar.

Jag är inte anarkist. Men jag är intresserad av anarkistiska praktiker och principer. Och många anarkister har inte mycket till övers för staten men de föredrar sannolikt att fascister inte har regeringsmakten. Hur och när en stat eventuellt avskaffas eller "vittrar bort" är för mig en fråga som ligger långt bort i tiden. Fram tills dess så tror jag att det finns otroligt mycket att vinna på att stötta varandra för mer jämlika och fria samhällen.

Det största boken Rörelsesocialism gjort är att bejakat den här hållningen jag nog länge haft eller velat ha: att det måste inte vara antingen eller. Man kan vara en del av en bredare socialistisk rörelse. Under resans gång kanske jag vissa perioder sätter större tilltro till parlamentarismen och andra gånger hellre fokuserar på anarkismens tankar och praktiker. Men oavsett så skapar vi rörelse, ett tryck underifrån. Och oavsett om man är socialdemokrat, kommunist eller anarkist så förstår de flesta att man behöver engagerade människor, som bryr sig och vill förändra.

## Ett axplock av vad jag har lust att göra

Jag brukar prata om saker jag har lust att göra som tankeprojekt. Det är saker som eventuellt kan bli av någon gång men som är riktigt roligt att tänka på och klura på. Att starta en förening var ett tankeprojekt som faktiskt blev av. Och det var nog för att jag funderade mycket på det. Och det var väldigt kul under tiden jag funderade :)

Att få tänka stort och visionärt är kul! Att få bygga saker eller bara fundera på att bygga saker är kul. Här följer en lista på saker jag skulle vilja göra. Några saker blir kanske av, andra inte. Men hjärtat öppnas när jag tänker på det.

* Starta ett café som drivs som ett kooperativ
* Starta en webbyrå som drivs som ett kooperativ
* Flytta ut i någon mindre ort och dra igång en kulturförening
* Starta en socialistiskt camping
* Flytta ut på landet och bygg en himla massa tinyhouses med hjälp av CNC och enkla färdiga modeller
* Göra en tävling inför valet 2026 med politiska webbläsarspel
* Starta en vänsterteknikpodd med någon/några goa kamrater
* Skapa konst som uppmanar till kamp och sätt upp på alla elskåp
* Göra en drönare som kan spreja broar med politiska budskap automatiskt
* Skapa en infrastruktur med hemmaservrar med andra goa kamrater så man kan drifta webbplatser, poddar, bloggar, osv med redundans
* Skriv ett medborgarförslag och få till gratis arrende så man kan skapa kollektiv odling i staden. (Eller bara joina [Odla ihop](https://www.odlaihop.se/))
* Starta en podd, blogplattform eller annat som kräver en liten liten betalning och fixa betalningen med [Open Collectives fiscal hosting](https://opencollective.com/fiscal-hosting) istället för med en bank
* Skapa ett hackerspace i Stockholm och [köp lokalen som Uppsala Makerspace gör](https://www.uppsalamakerspace.se/2025/01/07/informationsmote-om-lokalen-12-januari/). (Jepp, vet att [Stockholm Makerspace](https://www.makerspace.se/) finns men är lite för dyrt och har lite mer grejer än vad jag behöver)
* Skapa ett eget LoRa-nätverk (eller liknande som [Meshstatic](https://meshtastic.org/)) i Stockholm så man kan kommunicera med folk utan att använda internet
* Skriva bok om exempel på hur olika sorters kooperativ och kollektiv funkar

Listan kan göras längre. Men det här var några saker jag har funderat på. Det är kul att klura på och ger energi. Och vissa saker är sådant som man kan göra med andra och det vore så najs!

Vad har du själv för saker du går och tänker på? Kanske är du sugen på att joina Konstellationen och knacka kod? Eller så vill du göra konst? Skriva noveller? Bilda ett band med några kamrater? Bygga en bastu med grannarna? Eller varför inte anordna en studiecirkel i Rörelsesocialism? ;)

## Länkar

Några länkar vidare för att ta sig vidare:

* [Toivo Jokkala om boken Rörelsesocialism - tjugosju teser om samhällsförändring underifrån](https://www.youtube.com/watch?v=CI3Ub7nIxhw&ab_channel=Internationalen) - Videointervju
* [Köpa boken](https://www.bokus.com/bok/9789189524835/rorelsesocialism-tjugosju-teser-om-samhallsforandring-underifran/)
* [Rörelsesocialism - 2 februari Göteorg](https://www.gnistor.se/event/2025-02-02-rorelsesocialism-tjugosju-teser-om-samhallsforandring-underifran/)
* [Rörelsesocialismen - 11 januari Stockholm](https://www.gnistor.se/event/2025-01-11-rorelsesocialismen/)