---
title: Krönika - Hacka din chefs övervakning
image: file-listing.jpg
image_description: Bild av <a href="https://pixabay.com/users/joffi-1229850/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1685092">joffi</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1685092">Pixabay</a>
description: En krönika om övervakningen av oss på arbetsplatserna
date: 2024-06-16 21:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/hacka-din-chefs-oevervakning).

**Min kompis berättade för några år sen att hans chef hade installerat mjukvara på hans jobbdator för att se att han jobbade. Jag blev bestört! Men han tog det med ro. Programmet var lätt att lura och min kompis kollade på Youtube i protest utan att chefen kunde se det. Voila! På det sättet lyckades chefen effektivt sänka arbetsmoralen och få mindre arbete utfört.**

Numera finns det mer avancerade övervakningssystem. Programmen har tillgång till vad som händer på skärmen, kan registrera tangenttryckningar och musklick, användaren kan spelas in via webbkamera och AI kan analysera tonfall i kundtjänstsamtal.  

Arbetare i klassiska LO-yrken har länge fått dras med arbetsplatsövervakning. LO skrev en [rapport](https://www.lo.se/home/lo/res.nsf/4c5cb03d0b41dae4c1257672003b2b12/3b047b31787611e1c1257b4f004691c6/$FILE/Overvakning_kontroll_arb.pl.pdf) redan 2012 om problemet. GPS-övervakning av hemtjänstpersonal, kameraövervakning av busschaufförer och drogtester är några exempel. För gig-arbetare är övervakningen inbyggd genom de appar som används. I takt med att även kontorsarbetare övervakas har frågan fått en större uppmärksamhet.

Den som läser om arbetares rättigheter på [Integritetsmyndighetens hemsida](https://www.imy.se/verksamhet/dataskydd/dataskydd-pa-olika-omraden/arbetsliv/kontroll-och-overvakning-av-anstallda/) riskerar att bli besviken. Även om arbetsgivaren ska ta hänsyn till personlig integritet kan den komma undan med mycket så länge den informerar om att övervakning sker. Tankesmedjan Futurion skriver att [tre av fyra](https://futurion.se/det-nya-overvakningssamhallet/) anser att digital övervakning är ett intrång på den personliga integriteten. Övervakningen skapar också [misstro](https://kollega.se/arbetsmiljo/daniel-boden-overvakning-skapar-misstro-bland-anstallda).

Men på nätforum ger arbetare varandra tips om hur man [slår tillbaka mot övervakningen](https://fredrikedin.se/2024/04/11/stjala-tid-fran-jobbet/). Exempelvis utvecklas programvara som arbetare kan använda för att få kommunikationsverktygen att alltid visa att man är aktiv.

Lösningen ligger självklart i [organisering genom facken](https://kollega.se/arbetsmiljo/astra-zeneca-overvakar-personalens-datorer), för att vägra sådana här kränkningar av den personliga integriteten och för att skapa opinion för lagar som förbjuder detta.

Men tills dess att det är fixat kommer här ett erbjudande: vill du ha hjälp med att hacka din chefs övervakningsförsök? Skriv ett inlägg på webbforumet [aggregatet.org](https://aggregatet.org/c/integritet) så är vi några där som kan försöka hjälpa dig.