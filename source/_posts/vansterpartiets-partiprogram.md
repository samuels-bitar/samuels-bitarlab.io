---
title: Vänsterpartiets partiprogram
image: v-kongress.jpg
image_description: Foto på händer som håller i röstlappar. Bild, Vänsterpartiet
description: Här är Vänsterpartiets partiprogram som webbsida istället för pdf
date: 2024-05-13 23:00
---

UPPDATERING: Partikansliet var snabba! [Partiprogrammet finns på Vänsterpartiets hemsida](https://www.vansterpartiet.se/resursbank/partiprogram/). Läs där istället :)

Det här är en webbversion av redaktionskommitténs förslag till partiprogram som blev antaget på kongressen 12 maj 2024. Den ursprungliga pdf-filen går att ladda ner på [Vänsterpartiets hemsida](https://www.vansterpartiet.se/wp-content/uploads/2024/05/programforslaget-med-redaktionskommittens-forslag-v4.pdf) eller [från denna hemsidan](/files/vansterpartiet-partiprogram-redkom.pdf). Men om man tycker det är jobbigt att läsa pdf:er på telefonen så kan denna sida kanske vara till hjälp.

Det här dokumentet är konverterat från pdf till text med lite linuxhackande. Eventuella felaktigheter är mitt och ingen annans. Jag har tagit bort de motionsnummersreferenser och fetmarkeringar som fanns med i redaktionskommitténs förslag.

# Programförslaget med redaktionskommitténs förslag

## Innehållsförteckning

- Innehållsförteckning
- Inledning
- Arbete och gemenskap
- Frihetens hinder
    - Klassamhället och kapitalet
        - Låsningar på arbetsplatserna
        - Arbetarklassen och det skiktade samhället
        - Kapitalets instabilitet
        - Överklassens politiska roll
    - Kvinnors underordning
        - Konservativa intressen
        - Socialisering
        - Ekonomiskt beroende
        - Press och våld
        - Konflikt och frigörelse
    - Ekologiska kriser
    - En ojämlik värld
    - Rasism
    - Auktoritära högerkrafter
- Våra svar
    - Sammanhållning
        - Organisering för förändring
        - Arbeta strategiskt
    - Strategiska reformer
        - Arbete ger utveckling
        - Trygghet och självständighet
        - Demokratiska ägandeformer
        - Ut ur fossilberoendet
        - Sverige i världen
        - Frigörelse
- Vänsterpartiets grundsyn i sakfrågor
    - Stärk den politiska demokratin
    - Ägande med helhetssyn
    - Välfungerande arbetsplatser
    - Stora investeringar
    - Starka försäkringar
    - Välfärdens verksamheter
    - Seriöst företagande
    - Infrastruktur och bostäder
    - Utveckla tekniken
    - Kultur, media och föreningsliv
    - Alla människors lika värde
    - Brottsbekämpning
    - Internationella relationer
    - Vänsterpartiet

## Inledning

Vänsterpartiet är ett socialistiskt, feministiskt och antirasistiskt  parti på ekologisk grund som samlar människor för att bygga ett  bättre samhälle, där alla kan leva i trygghet, frihet och jämlikhet. Vi  vill se en politik som sätter människor i arbete med att lösa de stora  samhällsproblemen. Genom att skapa välfungerande arbetsplatser,  fasa ut fossilberoendet och ta bättre hand om varandra skulle det gå  att ta historiska kliv framåt.  

Det finns alla möjligheter att göra det. Det som står i vägen är  framför allt att kapitalet sätter press på samhället att gå i en helt  annan riktning, som istället handlar om kortsiktig vinstjakt. Många  människor som skulle kunna arbeta fokuserat med de stora  utmaningar vi har framför oss, jobbar idag i verksamheter där  vinstmaximeringen krockar med samhällsnyttan.  

Vi vill frigöra ekonomin från den belastningen. När samhället tar  över verksamheter kan vi driva dem med bättre mål. Vi  behöver skolor som alltid ser till elevernas bästa och industrier som  är ärligt engagerade i att ställa om från det fossila. Vi behöver  arbetsplatser som respekterar sina anställda, med kortare  arbetsdagar som låter oss leva friare liv.   

Den sortens skiften flyttar makt från ett fåtal rika kapitalägare  till demokratiska strukturer, där alla är med och bestämmer  om vilken riktning vi går i. Politik handlar inte bara om god vilja,  utan också om att ta konflikt när det behövs.   

De bra saker vi byggt upp i Sverige blev verklighet just för att  arbetarrörelsen, kvinnorörelsen och många andra stod upp för sin  sak. Vi är många, många fler än de som gynnas av en politik som  utgår från kapitalets intressen. Vi kan använda demokratin för att  genomföra det vi vill.  

För att göra det behöver vi hålla ihop. Vi behöver frigöra oss från  de gamla, orättvisa strukturer som delar upp oss. Alla ska kunna  forma sina liv fritt, oavsett kön. Alla ska mötas med respekt, var vi  än föddes. Alla ska få vardagsekonomin att gå ihop, vart livet än  har tagit oss. Alla ska känna att samhället finns där, var i  landet vi än bor.   

De allra flesta vinner på den politik för trygghet, frihet och  jämlikhet vi arbetar för. Vi vill bygga ett samhälle som frigör  sig från kapitalismen – ett som fungerar för alla, inte bara för  några få.   

## Arbete och gemenskap

Det är mycket mer som förenar oss människor än som skiljer oss åt.  Vi behöver alla värme och mat, närhet och omsorg, hem att bo i  och natur omkring oss. Vi behöver kunskap och kultur. Vi  behöver personer som finns där för oss och ser oss för dem vi är. Vi  behöver frihet att uttrycka oss och forma våra liv.   

Det sitter djupt i den mänskliga naturen att svara på de här behoven  tillsammans. Vi dras till att ta hand om varandra och bygga bättre  liv. Vi känner empati när någon behöver stöd, nyfikenhet när vi  kan lära oss något nytt, beslutsamhet när vi behöver samla kraft.  När vi gör nytta i våra sammanhang skapar det mening, självbild  och riktning i livet. Vi har en stark drivkraft att arbeta och skapa  nytt.   

Genom arbetet stöper vi om den värld vi lever i. Det har långsiktiga  konsekvenser, eftersom vi griper in i historiska processer och  relationer – allt från ekosystem till tilliten mellan människor. Vare  sig vi tar hand om barn eller byggnader, skriver recept eller  programkod, behöver vi en gedigen förståelse av det vi gör och  omsorg om helheten.  

Ett välfungerande samhälle fångar upp vår vilja att arbeta och  kanaliserar den för vår gemensamma utveckling och för  individens frihet. Just för att vi människor kan utvecklas åt så  olika håll gör genomtänkta former för samarbete stor skillnad. Vi  behöver sammanhang där vi kan vara trygga med att mötas med  omtanke och förtroende. Det lägger grunden för friheten att  utvecklas i sin egen riktning. Med den friheten i ryggen kan vi  mötas som jämlikar, som självständiga individer med stark  integritet.   

Formerna för hur vi arbetar tillsammans bildar grundläggande  maktstrukturer som kommer att prägla hela samhällets utveckling.  Det säger mycket om ett samhälle hur fria eller auktoritära  arbetsplatserna är, hur genomtänkt eller kortsiktigt arbetet utförs  och hur ägandeförhållandena ser ut.  

Vi ser flera problem med hur arbetet organiseras idag. På  arbetsplatserna är det vanligt att vi inte är tillräckligt fria att  använda vårt yrkeskunnande och att arbetet läggs upp efter andra  mål än att möta människors behov. Långa arbetsdagar gör att tiden  inte alltid räcker till för det arbete vi gör hemma, för det vi  skapar i kultur- och föreningslivet eller för den vila vi behöver.  Maktstrukturerna står i vägen för det arbete vi behöver göra för att  kunna leva friare liv.   

Eftersom vi hela tiden utvecklar nya sätt att arbeta är de  strukturerna alltid i rörelse. Kan de gamla formerna för att  organisera arbetet inte hantera de nya utmaningarna öppnar sig nya  möjligheter till politisk frigörelse. Genom historien har också de  starkaste maktstrukturer kunnat utmanas av politiska strömningar  som sett de öppningar som uppstått och agerat på dem.  

Det är alltid ytterst vi människor som bestämmer vilka samhällen vi  utvecklar. Varje steg vi tar i att frigöra oss från maktstrukturerna  är ett steg mot att fatta de besluten mer medvetet och demokratiskt.  Genom att tillsammans rikta vårt arbete mot våra mänskliga behov  kan vi bygga ett samhälle där alla kan leva friare.   

## Frihetens hinder

### Klassamhället och kapitalet

Den värld vi lever i har en lång historia av klassamhällen.  Människor har tvingats arbeta hårt för att ett fåtal furstar, slavägare  och patroner ska kunna bygga upp makt och ägande. Den kedjan av  olika klassamhällen fortsätter hela vägen till idag. Många av dagens  miljardärer sitter på förmögenheter som grundlades långt innan de  demokratiska genombrotten.  

Med industrisamhällets framväxt förändrades klassamhällena  dramatiskt. Arbetet blev långt mer produktivt. Ägandet började  organiseras i bolag som drevs att ständigt försöka sänka sina  kostnader och vara steget före varandra. Den kapitalistiska  dynamiken formade mer och mer av samhället.   

Det lade grunden för två nya samhällsklasser: en klass av  kapitalägare och en klass av arbetare. När människor knöts ihop  kring den storskaliga produktionen i städerna öppnades helt nya  möjligheter till organisering. I land efter land växte starka  arbetarrörelser fram, tillsammans med en uppsjö av andra folkliga  rörelser. De drev igenom en lång rad krav på demokrati, rättigheter  och socialistiska lösningar trots hårt motstånd från överklassen.  För första gången kunde vanliga människor organisera sig så brett  att de kunde göra anspråk på att själva leda samhället.   

Många politiska motsättningar handlar i grunden om  intressekonflikten mellan alla oss som arbetar och skapar värde,  och å andra sidan det fåtal som äger de stora företagen. Hur mycket  av det värde vi skapar de tar i vinst och hur mycket som går tillbaka  till oss är en fråga om makt.  

Deras behov av att höja sina vinstmarginaler och bygga upp  sina maktpositioner ger dem starka intressen i hur hela  samhället är uppbyggt. Det har format mycket av den moderna  historien. Alla politiska rörelser som vill förbättra människors  livsvillkor krockar på ett eller annat sätt med kapitalismens  dynamik. Det gör att vitt skilda sakfrågor knyts samman i ett  gemensamt intresse av att stärka de demokratiska krafterna  och göra bättre vägval möjliga.   

Överklassen är beroende av det arbete vi utför, och det ger oss  förhandlingsstyrka. Det ligger i deras intresse att försöka hålla  tillbaka den. Arbetarrörelsens uppgift är tvärtom att organisera den  till en kraft som kan förändra samhället.   

#### Låsningar på arbetsplatserna

En arbetsplats där människor har bra sammanhållning, löser  problem gemensamt och känner sig trygga att säga vad de  tycker är en produktiv arbetsplats. Det är också en arbetsplats där  människor kommer att ha lätt att bygga upp förhandlingsstyrka  tillsammans.  

Just därför driver ägarnas intresse av att hålla nere lönerna ofta  fram dåliga företagskulturer. Utbytbarhet och kontroll sänker  människors förhandlingsstyrka och pressar fram hårdare  arbetsdagar. Individuella löner ställer människor mot varandra.  Samtidigt punkteras det engagemang som lägger grunden för att  arbetsplatser och samhällen ska fungera ordentligt.   

Det är ett grundläggande problem med ägandeformer som bygger  på vinstmaximering. Från ett samhällsperspektiv är det en enorm  förlust varje gång en människa tappar sin genuina drivkraft att göra  ett gott arbete i yrkeslivet. För det enskilda företaget kan det  tvärtom vara lönsamt att slita ut människor och säga upp dem  istället för att lägga tid på återhämtning, kompetensutveckling  och att skapa bra arbetsplatser. Vinstdrivande företag är inte  gjorda för att lyfta blicken och se de större konsekvenserna av sina  beslut.   

Den enögdheten kan driva dem att förstöra biologisk mångfald  och ekosystem, vars långsiktiga bärkraft betyder lite för deras  lönsamhet men mycket för framtida generationers liv på denna  planet. Den får dem att låsa in kunskap som skulle göra långt  större nytta om den kunde användas fritt. Den leder dem till att  slösa enorma resurser på konflikter med varandra i form av allt från  marknadsföring till juridiska tvister.   

En ekonomi där de här tendenserna är utbredda leder till ett  samhälle som hela tiden tvingas hanka sig fram genom små och  stora kriser. Med gemensamma och demokratiska  ägandeformer skulle vi kunna förebygga problem innan de  växer sig stora. Fler människor skulle kunna gå till jobbet med en  känsla av att vara en viktig och uppskattad del av en arbetsplats  världen behöver. Fler uppgifter skulle utföras av någon som är fri  att göra ett bra arbete. Samhällen där människor möts med respekt  på sina arbetsplatser fungerar långt bättre.   

#### Arbetarklassen och det skiktade samhället

Lönearbete betyder i praktiken olika saker för olika  människor. Livs- och arbetsvillkoren skiljer sig drastiskt på  djupt orättvisa sätt.  

För arbetarklassen är lönearbetets grundvillkor att det bara är  genom att sälja sitt arbete det går att försörja sig.  Arbetsdagarna präglas av ett begränsat inflytande över sitt  eget och andras arbete. En oorganiserad arbetarklass är enkelt  utbytbar och tvingas oftare möta den kalla kärnan i kapitalets  dynamik. En organiserad arbetarklass har å andra sidan en  stark förhandlingsposition gentemot kapitalet. Många har  kritiska positioner i produktionskedjan eller för att samhället  ska fungera.  

Andra grupper av löntagare är i annorlunda positioner. Bland  tjänstepersoner har det funnits bättre möjligheter att säkra  rimliga villkor genom individuella förhandlingar. Samtidigt  har många av de yrkesgrupperna proletariserats så att  villkoren mer kommit att likna arbetarklassens. Det ger bred  organisering en än större betydelse. När vi står upp  tillsammans, som kollektiv, är vi långt mindre utbytbara och  kan ta tillbaka makten.   

Ju större personliga konsekvenser det skulle få för människor att  förlora jobbet, desto starkare är ägarnas övertag på arbetsplatsen.  Det ger dem ett intresse av ett samhälle med djup fattigdom och  utbredd ekonomisk stress. Ju tryggare och friare vi är, desto mer  förhandlingsstyrka har vi.  

Därför har den ägande klassen också en i grunden kluven syn på  arbetslöshet. I ett samhälle med stora investeringar, där det finns  gott om jobb att söka, kommer vi att kunna ställa krav på bra löner  och arbetsvillkor. Att Sverige under flera årtionden drev en politik  för full sysselsättning skapade en situation på arbetsplatserna som  gjorde att arbetarklassen kunde ta stora steg framåt. Det kom att  prägla hela samhällsutvecklingen.  

Det var viktigt för kapitalägarna att bryta med det och tvärtom  normalisera en hög arbetslöshet. Trots det ofattbara slöseri  med människors arbetsförmåga det inneburit, har det lönat sig  för dem eftersom makten på arbetsplatserna förskjutits till  deras fördel. Från deras perspektiv kan lägre produktion vara  ett pris värt att betala för att säkra en mer orättvis fördelning  av kakan.   

Att människors fattigdom sänker förhandlingsstyrkan betyder också  att kapitalet inte är särskilt intresserade av att samhället stödjer  människor att ta sig ur den. När bostadsbolagen höjer hyrorna eller  butikerna höjer matpriserna ökar pressen på människor att behålla  sina jobb även om villkoren är orimliga. Ett samhälle där  människor fastnar i djupa skulder är både lönsamt för bankerna och  fördelaktigt för kapitalet i stort.  

Stadsdelar, förorter, bruksorter, småorter och landsbygd lämnas  med färre möjligheter till utveckling när människor görs utbytbara  och arbetslösa. Det kan handla om stängda skolor och avsaknad av  service, eller om en självförstärkande kombination av fattigdom,  brott och skuldbeläggning. Istället för att bygga upp starka  lokalsamhällen med resurser att lösa de problemen, drar sig den  ekonomiska eliten hellre undan till välbärgade områden.  

Överklassen har ett intresse av att stärka de skillnaderna. De har  större möjligheter att vinna stöd för sin sak i ett samhälle där  chefer, politiker och andra i maktställning lever på helt andra  villkor än de flesta. Sammanhållning och jämlikhet betyder tvärtom  att vanliga människors livsvillkor hamnar i centrum.   

Ett samhälle med djupa klasskillnader kommer inte att hänga ihop.  Människor har enklare att förstå varandra, bygga förtroende och  arbeta tillsammans ju mer vi kan mötas på samma villkor.  

#### Kapitalets instabilitet

Skiktningen av samhället är ett av flera exempel på hur  kapitalismens dynamik leder till en grundläggande instabilitet i  samhället och ekonomin. Vi har länge sett en ohållbar utveckling  där alltmer kapital koncentrerats på allt färre händer. Det kommer  att behövas aktiv politisk handling för att bryta mönstret att mer  och mer makt samlas hos miljardärer, som utövar den helt  godtyckligt.   

I många företag märks kapitalets instabilitet när ägandet skiljs från  verksamheten och besluten börjar fattas i styrelserum med väldigt  svag koppling till det verkliga arbete företaget bygger på. Den syns  i hur ägare kan ta stora risker och låta andra stå för notan när ett  bolag förstör naturresurser eller går i konkurs. Att vinstdrivande  företag ensidigt fokuserar på sin egen lönsamhet skapar en  kultur av ointresse för samhällsekonomins komplexitet, som också  kommit att prägla andra institutioner.  

I ekonomin i stort syns den instabiliteten bland annat i djupa kriser  som kan förlama många länders ekonomiska utveckling i åratal.  Oförmågan att sätta människor i arbete leder till en  nedåtgående spiral där många tvingas dra ner på sina utgifter  och därmed driver än fler företag till uppsägningar och  konkurs.   

Kapitalet behöver politisk stabilitet och statliga investeringar i till  exempel utbildning för att stå starka i konkurrensen på sikt.  Samtidigt kan enskilda kapitalintressen sällan hålla sig från att i  varje läge driva sina ekonomiska intressen så hårt de kan. När  särintressen som skolbolagen köper politiska tjänster och sänker  standarden i viktiga verksamheter blir det särskilt tydligt.  Kapitalet undergräver ständigt både sina långsiktiga förutsättningar  och sin egen legitimitet.   

#### Överklassens politiska roll

Överklassen omsätter sin ekonomiska makt i politisk makt genom  tankesmedjor, lobbyister, medieägande och högerpartier. Det är en  fortsättning på en lång historia av maktutövande uppifrån. Många  av dagens politiska idéer och institutioner växte en gång i tiden  fram utifrån intressena hos gamla tiders överhet. Det har tagit tid  och kraft att förändra inriktningen på till exempel utbildnings- och  rättsväsendet i en mer demokratisk riktning.  

Med demokratins genombrott uppstod en grundläggande oro i den  ägande klassen för att den politiska sfären skulle ha för stort  handlingsutrymme. Det har varit viktigt för dem att försöka snäva  in demokratins ramar för att begränsa utrymmet för människor att  driva igenom sina egna intressen. Den ekonomiska eliten behöver  ett politiskt etablissemang som står nära dem och lever långt från  vanliga människors vardag.   

De senaste decennierna har präglats av att de förstärkt sin  politiska makt och använt den till att sätta press på den välfärd  arbetarrörelsen har byggt upp. Minskade resurser från staten  och utförsäljning av en lång rad verksamheter har lett till en  lång rad problem. Välfärden har urholkats och klyftorna i  samhället har ökat. Politikens möjligheter att styra de  gemensamma verksamheterna har beskurits och grov  kriminalitet har letat sig in i ägarstrukturerna.   

Ju mer kapitalet förmår forma samhället, desto mer kommer  högerns politiska projekt att behöva handla om att hantera olika  sorters kriser. Deras ideologiska behov handlar därför till stor del  om att undvika konstruktiva, demokratiska processer, som skulle  synliggöra hur deras misslyckade politik gång på gång krockar med  större samhällsintressen.  

De behöver uppgivenhet och misstroende, alarmism och  syndabockar. Deras klassförakt syns i en dömande människosyn,  som handlar om att människor är lata, själviska och behöver  pressas att göra nytta. Tanken att varje människa är värd trygghet,  frihet och jämlikhet är fortfarande radikal.  

Ensidig vinstmaximering fungerar sämre och sämre i den  komplexa samhällsekonomi som växer fram. När vi löser olika  samhällsproblem på bättre sätt svarar det inte bara på  situationen här och nu – det lägger också grunderna för ett  nytt, friare och mer demokratiskt samhälle. På samma sätt som  kungar och drottningar inte längre styr världen kommer också  kapitalismen en dag vara historia.   

### Kvinnors underordning

Den gamla patriarkala maktordningen har tagit sig många olika  uttryck genom tiderna. Ett grundläggande mönster är att kvinnors  liv, sexualitet och arbete begränsas och styrs på olika sätt.  Strukturen upprätthålls med sociala förväntningar, ekonomiskt  beroende och ytterst mäns våld mot kvinnor.  

Varje samhälle behöver former för hur nya generationer kommer  till världen och växer upp. En röd tråd som löper genom de  patriarkala samhällena är att kvinnor på olika sätt hindras från att  bestämma över sitt liv och sitt föräldraskap, med såväl lagar som  strikta normer. Det har bland annat tagit sig uttryck i försök att  hålla kvar kvinnor i hemmen för att ta ansvar för familjen. De  strukturerna har betytt att män har haft stark kontroll över kvinnors  kroppar och arbete.   

Människor förväntas främst leva ut kärlek och sexualitet genom att  bilda heterosexuella par, som leder till föräldraskap i traditionella  kärnfamiljer. Kvinnors frigörelse har varit en kraft som öppnat nya  möjligheter och ifrågasatt det drag av ägande och instängdhet som  levt kvar i synen på familjer. De juridiska, ekonomiska och sociala  möjligheterna att välja sina relationer är större idag, även om gamla  normer också lever kvar.  

För att samhället ska fungera behövs ett omfattande omsorgsarbete,  där vi tar hand om varandra. Det handlar till exempel om  föräldraskap, hushållsarbete och att ta ansvar för nära relationer.  Kvinnor har länge stått för större delen av det arbetet, samtidigt  som värdet av det inte erkänts ekonomiskt eller kulturellt. Att en  del av det arbetet professionaliserades med framväxten av  skola, vård och omsorg för alla var ett stort genombrott för  kvinnors möjligheter till ekonomisk självständighet.   

#### Konservativa intressen

Könsmaktsordningen har påtagliga fördelar för män. Den ger dem  större tillgång till maktpositioner och bygger upp vardagliga sociala  överlägen i relationerna till kvinnor. Män förväntas odla särskild  gemenskap och lojalitet med andra män, som utesluter kvinnor. Det  formar en social kraft med gott om maktpositioner att agera från.  När världen förändras, och särskilt när kvinnor ställer uttryckliga  krav på jämlikhet, möts det ofta av en konservativ reflex hos män  som upplever sin position hotad. Den tar sig uttryck i såväl  medvetna politiska projekt som att passivt undvika att lösa de  problem kvinnor möter.   

Den överordnade roll män växer upp i är både bekväm och ihålig  samtidigt. Att undvika det som påstås vara kvinnligt blir att  undvika delar av vad det är att vara människa. Att inte träna sig på  omsorgsarbete utan förlita sig på att kvinnor står för det leder till  färre nära relationer och större ensamhet i livet.  

Klassamhället förstärker könsmaktsordningen på flera sätt.  Kapitalet drar nytta av det obetalda och lågavlönade arbetet och har  därför ett direkt intresse av att hejda reformer som bygger upp  kvinnors förhandlingsstyrka.  

Precis som med andra resurser har kapitalet en tendens att försöka  pressa fram så mycket betalt och obetalt omsorgsarbete det går,  utan något större intresse för de långsiktiga konsekvenserna. Vi  märker det i den ibland omöjliga situation människor sätts i när de  förväntas kombinera till exempel föräldraskap med förvärvsarbete  på heltid. Klassamhället har också inneburit att omsorgsarbetet inte  bara fördelats utifrån kön, utan också utifrån klass och etnicitet.  

#### Socialisering

Redan från uppväxten präglas våra liv av hur människor delas  upp i två kön, med varsin uppsättning förväntningar för hur vi  ska tänka, se ut och bete oss. Det handlar bland annat om  förväntningar på människors sexualitet, som heteronormen,  och uppfattningen att det bara skulle finnas två kön,  tvåkönsnormen. Allt det framstår ofta som självklart och givet,  på ett sätt som osynliggör andra möjligheter. Det är en del i  hur vi formar våra självbilder, vad vi lägger vår tid på och  vilka roller vi vänjer oss vid att ta.  

De uppdelningarna gör både kvinnlighet och manlighet hårt  villkorad. Människor sätts i olika positioner i  könsmaktsordningen beroende på hur väl de motsvarar  förväntningarna.  

Ordningen upprätthålls genom allvarliga och direkta  konsekvenser för den som bryter mot normen, till exempel  genom sin sexualitet eller sitt kön. Det tar sig uttryck i allt från  sociala bestraffningar till kontroll och i värsta fall våld. Nya  generationer har allt eftersom kunnat bryta upp många av de  ramar som begränsar våra liv.   

Utrymmet för olika sätt att vara människa har på många sätt varit  snävare för kvinnor än för män. Det finns fortfarande förväntningar  på kvinnor att ta mindre plats, anpassa sig och gå över sina egna  gränser för att ge andra omsorg. Den feministiska rörelsen har varit  avgörande för att synliggöra och komma ur de mönstren.  

#### Ekonomiskt beroende

Arbeten som gav mycket förhandlingsstyrka var länge uttryckligt  reserverade för män. Kvinnorörelsen har kommit långt på vägen  med att bryta upp de strukturerna. Många juridiska hinder är  avskaffade, diskriminering är förbjuden och sociala murar har rivits  ner. Trots det är många yrken än idag tydligt mans- eller  kvinnodominerade och kvinnors arbete värderas generellt lägre än  mäns arbete. I många kvinnodominerade omsorgsyrken  försvagas förhandlingsstyrkan av att fackliga stridsåtgärder  riskerar att drabba människor som är djupt beroende av det  arbetet.   

De långa och utmattande arbetsdagar som präglar många  arbetsplatser krockar med det obetalda omsorgsarbete kvinnor  förväntas stå för. Det är ett av skälen till att deltidsarbete är utbrett i  många kvinnodominerade yrken. Det leder i sin tur till en svagare  förhandlingsposition och att många som söker heltidsarbete  förvägras det.   

Den sortens strukturer betyder att kvinnor överlag har en svagare  ekonomisk självständighet än män, trots att de sammantaget arbetar  mer. Både i hemmet och på arbetsplatserna är det vanligt att  kvinnor i någon mening är ekonomiskt beroende av goda relationer  till män. Goda möjligheter till förvärvsarbete har alltid betytt  mycket för de feministiska framstegen.  

#### Press och våld

De traditionella förväntningarna på kvinnor att träna sig i  omsorgsarbete under uppväxten och på män att bygga upp  auktoritet lever fortfarande kvar. I skarp kontrast till kvinnorollens  betoning av värme och omtanke, innehåller idealen för män ofta en  romantisering av maktutövning och emotionell avstängdhet. Det är  ett gammalt mönster som delvis kommer ur klassamhällets behov  av hårda hierarkier.  

En av konsekvenserna av det är fysiskt och psykiskt våld. Mäns  våld mot kvinnor tvingar kvinnor att förhålla sig till allvarliga  risker i en lång rad situationer och förskjuter därmed makt i  samhället i stort. Mäns våld mot andra män leder till en betoning av  våldskapital i mansrollen. Mäns våld mot de människor som bryter  med könsnormerna snävar in allas frihet att uttrycka sina olika  sidor.  

Auktoritära mansroller leder också till en normalisering av att gå  över kvinnors gränser, som börjar etableras långt innan vuxen  ålder. Det tar sig sedan uttryck i allt från förminskanden till  kontrollerande mönster i relationer och sexuella trakasserier. Det  skapar också ett utrymme för övergreppen i porrindustrin, sexköp  och människohandel.  

Att kvinnorörelsen steg för steg kunnat sänka acceptansen för både  fysiskt och psykiskt våld har lett till att samhället sakta börjat se  brottens allvar och omfattning. Det har också betytt att fler växer  upp med att respekteras i sin integritet och att vi tagit flera steg mot  en samtyckeskultur.  

#### Konflikt och frigörelse

Fortfarande genomsyras mycket av det vi gör tillsammans av ett  strukturellt övertag som män har över kvinnor. Det sker både öppet  och dolt, medvetet och omedvetet. När vi sätter ord på det som  händer framträder mönstren. De syns i personliga erfarenheter och i  statistik. De syns i familjer och på arbetsplatser, i myndigheter och  i föreningsliv. De syns i en kultur som ofta sätter män i centrum  och kvinnor i bakgrunden.   

Konservativa och auktoritära krafter försöker aktivt förstärka de  mönstren och riva upp de rättigheter vi vunnit. Det gör det viktigt  att försvara tidigare framgångar.  

Samtidigt lever vi mitt i en radikal frigörelseprocess som pågår  världen över. Industrisamhällets framväxt öppnade helt nya  möjligheter till organisering och politiskt arbete också för kvinnor.  Förvärvsarbetet lade grunden för ett skifte som bara har börjat.  

### Ekologiska kriser

Vi lever i en tid av historiska ekologiska kriser.  Klimatförändringarna ruckar på stabiliteten i de grundläggande  livsvillkor vi behöver för att bygga goda liv. Trängda ekosystem  gör att vi förlorar delar av jordens biologiska arv i snabb takt. Flera  kriser förstärker varandra, med oöverblickbara konsekvenser för de  livsmiljöer och den natur vår frihet vilar på. Världen står inför  vägval som mänskligheten kommer att se tillbaka på långt in i  framtiden.  

De ekologiska kriserna går långt tillbaka i tiden. Mänsklighetens  begränsade tekniska förmåga och sätt att organisera arbetet har lett  till att kretslopp överbelastats och naturresurser förbrukats i  snabbare takt än de förmått återhämta sig. Den storskaliga  förbrukningen av fossila bränslen är på många sätt en brytpunkt,  med djupa konsekvenser på global nivå. Vi behöver utveckla nya,  hållbara sätt att arbeta på en lång rad områden, som bygger på en  genomtänkt omsorg om livets villkor. Det är en stor och krävande  uppgift för vilket samhälle som helst. Varje politisk kraft som gör  anspråk på att leda samhället behöver kunna svara på hur den  omställningen ska se ut.  

Kapitalets brist på helhetssyn är en av grundorsakerna till att  situationen gått så långt som den gjort. Mycket av dagens problem  hade gått att undvika till små kostnader, men vinstmaximerande  bolag kan inte annat än att i varje läge prioritera den egna  avkastningen. Den oförmågan gör det omöjligt för dem att ta  verkligt ansvar.   

Trots allt vi vet om klimatförändringarna kommer kapitalet att  fortsätta bränna fossil energi så länge det är kortsiktigt lönsamt.  Trots att vi behöver ta bättre hand om ekosystemen för att inte  förstöra deras bärkraft tvekar inte kapitalet att riva sönder kretslopp  och lämna problemen till andra.  

Trots att vi behöver hushålla med begränsade resurser drar sig  inte kapitalet för att slösa med dem. När produkter designas för  att säljas, snarare än för att användas, leder det ofta till att de inte  håller och är svåra att reparera. Det är ofta lönsamt att förstöra  överblivna varor hellre än att låta dem komma till nytta. Mängder  av resurser förbrukas på marknadsföring människor gör allt för att  undvika. Till det kommer det slöseri som bottnar i den absurda  fördelningen av vinsterna, som gör att privatjet och uppassning  åt de rikaste prioriteras före samhällets verkliga behov.  

Vinstdrivande företag har svårt att driva den tekniska  utveckling vi behöver, eftersom de inte ser till helheten. Ny  kunskap har ett enormt samhällsekonomiskt värde för världen, men  det är bara en bråkdel av det värdet som går att tjäna pengar på.  Kapitalet prioriterar istället andra investeringar, stänger sina  utvecklingsprocesser för omvärlden och låser in den kunskap det  tar fram bakom patent.   

Det är en utmaning för stater att reglera de här problemen, eftersom  de flesta företag har ägandeformer som gör dem ovilliga att  samarbeta. Det har ofta lönat sig för vinstdrivande företag att  tvärtom motverka demokratiskt fattade beslut genom att bryta mot  lagar, driva politisk lobbying eller flytta produktion till länder utan  effektiva regelverk.   

Det blir särskilt tydligt för fossilkapitalet, de bolag som äger fossila  tillgångar och djupt fossilberoende industrier. Varje omställning,  hur vi än genomför den praktiskt, innebär att deras tillgångar blir  värdelösa. Därför har de länge finansierat politiska krafter som  försenar det skiftet.  

Andra företag hamnar i andra roller. Vissa har ett intresse av att  driva på omställningen, som till exempel de som bygger  vindkraftverk eller eldrivna fordon. Ibland kan företag göra stor  skillnad utan att det nödvändigtvis kostar dem särskilt mycket och  motivera det som marknadsföring. En del ställer om för att  fossilberoende blivit en företagsekonomisk risk i en värld som  inte kommer acceptera det hur länge som helst.   

Däremot har kapitalet samlade politiska intressen som försvårar  världens omställning. Det har till exempel mycket att förlora på  skiften mot en starkare demokrati som tar en större roll i de stora  investeringsbesluten. Ett samhälle som medvetet tar oss ur  beroendet av icke-förnybar energi är ett samhälle som också är  redo att ta sig an fler problem i konflikt med starka  kapitalintressen.   

Det finns en lång historia av människor som tillsammans försvarat  de naturvärden vi bygger våra liv kring. Det har gått att organisera  politiska krafter som varit starka nog att utmana ägarintressen och  öppna nya vägar framåt. Vi har kunnat bygga upp lagstiftning som  begränsar utrymmet för kortsiktig exploatering och lägger tillbaka  en del av de samhällsekonomiska kostnaderna på företagen.  

Klimatforskningens genomslag kom i en omtumlande tid då stater  drog sig tillbaka från sitt ansvar att styra samhällets utveckling,  ivrigt pådrivna av kapitalet. I praktiken betydde det låga offentliga  investeringar, attacker på välfärden och sämre jobbmöjligheter. Det  skiftet drevs igenom med en ideologisk strömning full av politisk  uppgivenhet, fokus på privat konsumtion och skuldbeläggning av  enskilda människor för strukturella problem. Allt det här kom att  prägla diskussionerna om klimatpolitiken på ett sätt som ofta ledde  till låsningar. För att bygga upp stödet för omställningen kommer  vi att behöva förhålla oss till den historien och utforma politiska  projekt som många kan sluta upp bakom.   

Klimatkrisen öppnar för nya, bättre sätt att organisera samhället.  Det går att sänka utsläppen effektivt genom nya tekniska  innovationer, beteendeförändringar, minskad konsumtion och  förändrad efterfrågan. Att bevara skog och återställa våtmarker  betyder också mycket.  

Investeringar, innovation och storskaliga samhälleliga projekt  kan ge de största effekterna. Det handlar om att ställa om  produktionen och satsa på förnybar energi, järnväg,  kollektivtrafik och renoveringar av miljonprogrammet. Det  behövs strategier som utgår från forskning, ser till att  omställningen genomförs rättvist och bygger upp ett starkt  folkligt stöd.   

### En ojämlik värld

Jakten på större vinster har lett kapitalet att söka jorden runt efter  arbetskraft och naturresurser att exploatera. Historiskt har det ofta  tagit sig formen av annekteringar, tvångsarbete och kolonialt  förtryck. Flera av västvärldens stater byggde imperier som sträckte  sig över stora delar av världen.  

Världens stormakter har länge drivit destruktiva geopolitiska  konflikter om kontrollen över strategiska råvaror, teknologi och  finansiella flöden. Det har stärkt antidemokratiska krafter i många  länder och etablerat en kostsam vapenindustri med starka politiska  intressen. Det driver fram en farlig dynamik, där mindre stater dras  in och hamnar i kläm. Särskilt utvecklingen av kärnvapen ställer  världen inför oacceptabla risker.  

Historien av kolonialism och imperialism formade en förödande  ekonomisk underordning av stora delar av världen. Den syns  fortfarande i form av utländskt ägande av nyckelsektorer, ensidiga  resursflöden, korruption och djup skuldsättning av många stater.  Det ger kapitalet starka verktyg att pressa fram politik. Den  självständighet befrielserörelserna vann återstår till stor del att  förverkliga.  

Kapitalet har dragits till underordnade länder för att det har varit  platser där det inte behövt respektera de krav på grundläggande  rättigheter som människor kunnat driva igenom i mer utvecklade  ekonomier. Det har kunnat förlägga tungt arbete och miljöskadlig  produktion där, istället för att investera i teknikskiften som löser  problemen på riktigt.  

Den hårda press det kunnat sätta på människor har lett till stora  sociala kostnader och tragedier, samtidigt som det betytt att väldigt  lite av alla de rikedomar som skapats där stannat hos befolkningen.  Människor har fastnat i fattigdom i generationer, trots att små  investeringar i trygghet och utbildning hade kunnat bryta det  mönstret. Vi lever i en värld där många stukas av hårt arbete från  tidig ålder, när vi hade kunnat leva i en där alla får utveckla sina  förmågor.  

Vi människor har allt att vinna på att samarbeta och  tillsammans bygga en bättre värld i fred och frihet. Det är  genom gemensam organisering i politiska partier,  fackföreningar, kvinnorörelser och internationella  solidaritetsrörelser vi kan frigöra oss från fattigdom och  ojämlikhet.   

Varje gång människor någonstans tar ett steg framåt, stärks också  vi. Internationell solidaritet handlar om att se sig som en del av en  större helhet och försvara varandras möjligheter att utvecklas fritt.  

### Rasism

Människor har genom historien levt i mångfald av alla möjliga  slag. Samtidigt har vi också många gånger fastnat i att dra  streck mellan oss och avhumanisera andra, inte minst när det  varit politiskt användbart för olika överheter.  

Att kolonialismen slog sönder och underordnade samhällen  världen över lade grunden för mycket av de rasistiska  strukturer vi ser idag. Den ägande klassen satte i system att  pressa fram arbete med våld och övergrepp. Många av de  rasistiska idéer som uppstod för att legitimera att människor  förvägrades grundläggande rättigheter lever fortfarande  kvar idag.  

Med nationalstaternas framväxt skapades starka identiteter  som delade upp människor i olika folk. De byggde ofta på  föreställningar om homogena grupper och kunde lägga stor  vikt vid vem som passade in och inte. Nationalismen visade sig  användbar för de som ville utesluta människor från  gemenskapen och elda på konflikter mellan länder.  

De rasistiska strukturer som skiljer människor åt i vardagen  byggs upp i historiska processer, där fattigdom, segregation  och lagstiftning lägger grunden för olika idéer om varför de  uppdelningarna finns. De processerna har satt djupa, tragiska  spår i människor och samhällen.  

Vi såg det när Sverige formades som nation och vi har sett det  ske många gånger sedan dess. Vi ser det med nationella  minoriteter och andra som levt här hela sina liv. Vi ser det med  människor som kommit hit för att arbeta och människor som  behövt fly.  

Hur de uppdelningarna tagit form har sett väldigt olika ut för  olika grupper. Därför behöver vi specifika historiska  kunskaper om olika sorters rasism för att förstå och bryta  mönstren.   

Kapitalet drar nytta av de strukturerna och idéerna för att  normalisera orimliga arbetsvillkor, sänka stödet för att ta hand om  varandra eller försvara imperialistiska krig. Det har alltid dragits till  fattigdom och rättslöshet eftersom det undergräver människors  förhandlingsstyrka. Det syns till exempel i exploateringen av  migrantarbetare som saknar grundläggande juridiska rättigheter i  det land de bor i. En del av de rasistiska strukturerna är att vissa  människor sätts i den sortens situationer, där handlingsalternativen  är hårt begränsade.   

De allra flesta människor har tvärtom ett intresse av att alla har  ordentliga rättigheter, skyddsnät och förhandlingsstyrka eftersom  det lyfter nivån på hela arbetsmarknaden. Varje gång ett samhälle  accepterar usla villkor för vissa riskerar det att börja gälla fler och  fler.  

Rasismen används för att dölja det. Den låter överklassen påstå  att det ligger i alla andras intresse att trampa på vissa  människor. Ju mer skiktat samhället är, desto fler kommer  dras till att se på sig själva som överordnade de som pekas ut  och misstänkliggörs. När de sämre villkoren sedan breder ut  sig ligger det nära till hands att skylla på dem som angreps  först istället för att se vad det är som händer.   

Strategisk rasism har länge varit en del av högerns retoriska och  politiska verktygslåda. Ju mer segregerat ett samhälle är, desto  enklare är det att sprida misstänksamhet mellan människor som har  all anledning att hålla ihop. En regering som stadigt förbättrar  människors livsvillkor kan försvara alla människors lika värde,  medan en som tvärtom pressar människor att arbeta hårdare  till lägre lön behöver syndabockar. Det är upp till de  progressiva rörelserna att sätta ord på varför högern använder  den strategin, försvara alla människors värdighet och föra tillbaka  diskussionen till de verkliga konfliktlinjerna.   

Rasistiska tankemönster fungerar ofta mer eller mindre omedvetet.  Däremot kommer de till stor del ur den långa historien av medvetet  skapade rasistiska berättelser och myter. Eftersom gamla tiders  rasläror blivit svåra att försvara är många av de negativa  stereotyper som sprids idag istället knutna till etnicitet, religion och  kultur.   

Trots att de rasistiska idéerna satt sig djupt visar historien tydligt att  det finns alla möjligheter att göra upp med det arvet. Det arbete  rasifierade personer står för ger också förhandlingsstyrka.  Tålmodigt, organiserat arbete för gemensamma intressen har betytt  mycket för att bryta upp de här strukturerna och skapa genuin  sammanhållning.   

Människor föredrar i grunden gemenskap. Det tar energi att försöka  hålla oss uppdelade. I ett samhälle där vi möts på mer jämlika  villkor blir det lättare att lägga det rasistiska idéarvet bakom  oss, och omvänt: gör vi oss av med rasistiskt tankegods står vi  starkare i arbetet för jämlikhet och frigörelse.   

### Auktoritära högerkrafter

Maktstrukturerna skapar en grogrund för ett förakt mot andra  människor. För en del är det en tilltalande tanke att få ingå i dem,  stå över andra och rikta sin frustration mot någon som inte kan  försvara sig. Auktoritära högerkrafter bygger en världsbild på  fördomar, konformism och aggression riktad neråt, mot människor  de uppfattar som annorlunda och underordnade.  

Deras bittra kamp för gamla hierarkier och lydnadskultur passar på  många sätt överklassens intressen. Det ställer människor som  egentligen har samma intressen mot varandra. Det öppnar för  direkta angrepp på vissa gruppers rättigheter och  förhandlingsstyrka. Det leder det politiska fältet bort från  demokratiska processer och sakliga diskussioner.  

Den auktoritära högern har blivit reella hot mot demokratin i  situationer där den ekonomiska eliten börjat uppfatta dem som  användbara verktyg, gett dem finansiering och öppnat politiskt  utrymme för dem i etablissemanget. Ofta har det handlat om att  överheten försökt komma ur sina egna legitimitetskriser.  

Samtidigt är auktoritära högerkrafter vid makten ett recept för  stormiga politiska år, långt från den stabilitet kapitalet vanligtvis  föredrar. Den hårda press på människor det kan dra nytta av  kommer med ett stort mått godtycke, tumult och nyckfulla agendor  som kan krocka med viktiga kapitalintressen. Den ekonomiska  eliten kan upptäcka att de är förpassade till passagerarsätet på en  färd med en aggressiv, bångstyrig förare.  

Den auktoritära högerns grundläggande svaghet är att deras  faktiska politik går emot de flestas intressen. De kan fånga upp  människors frustrationer med samhället, men inte bygga något  bättre. De kan spela på tendenser att vilja trampa på andra, men den  avhumaniseringen färgar också i praktiken samhället i stort. De kan  erbjuda ett träsk av bitterhet att vada i, men de kan inte svara mot  människors behov av fungerande sammanhang.  

I tider där samhället präglas av större trygghet och sammanhållning  pyser luften ofta ur dem. Den som är en del av ett organiserat  politiskt sammanhang som ger ett reellt hopp om förändring  kommer långt mer sällan att fastna i aggressivt misstroende.  Välfungerande fackföreningar och andra sociala rörelser har därför  en nyckelroll i det långsiktiga arbetet mot auktoritära och  fascistiska krafter.  

Vi svarar på deras angrepp med ett brett försvar av mänsklighet och  sammanhållning, som gång på gång för tillbaka diskussionen till de  verkliga konfliktlinjerna i samhället. Genom att bygga hopp om  verkliga lösningar på konkreta problem kan vi göra dem irrelevanta  för de flesta människor.  

## Våra svar

### Sammanhållning

De stora samhällsproblemen har en sak gemensamt: de handlar i  grunden om makt. För att lösa dem behöver vi en rörelse med  kraft nog att ta konflikt med starka intressen.   

Möjligheterna att bygga den förhandlingsstyrkan kommer framför  allt ur det arbete människor gör. Kvinnors obetalda omsorgsarbete  är avgörande för att samhället överhuvudtaget ska fungera. Många  LO-arbetare står för arbetsuppgifter som tas för givna men snabbt  blir synliga den dag de inte längre utförs. Kapitalismen är helt  beroende av att människor fortsätter arbeta. Det ger oss en  oerhört stark förhandlingsposition.   

En och en kan vi inte göra särskilt mycket med den  förhandlingsstyrkan. Organiserar vi oss och agerar tillsammans i  solidaritet kan vi däremot sätta ett tryck på förändring som i  längden är svårt att hejda. Allmän rösträtt, fackliga rättigheter,  förskolor och pensioner är alla exempel på politiska segrar som har  vunnits genom att människor har hållit ihop kring ett gemensamt  mål. Vi vet av erfarenhet att bred organisering kan skapa starka  politiska krafter.  

Kapitalismen har en grundläggande svaghet i att ett fåtal  kapitalägares intressen ständigt krockar med de stora  samhälleliga behoven. De allra flesta har allt att vinna på att  frigöra sig från det. Människors förmåga att samarbeta är därför  avgörande för att driva igenom förändring. Vi vill odla förtroende  och sammanhållning i en bred arbetarklass som tillsammans står  upp för sina intressen.   

Det är ofta en utmaning, för vi behöver samla människor vars  bakgrund, arbetsdagar och vardagsliv ser olika ut. Ett framgångsrikt  politiskt projekt behöver bygga på förslag som trots det är relevanta  för många samtidigt och formar gemensamma intressen.  

De problem arbetare och tjänstepersoner möter på arbetsplatserna  tar sig olika uttryck, men kommer till stor del ur samma grund. Vi  kan se att i de länder där arbetarrörelsen stått starkare mot kapitalet  lever också akademiker tryggare liv, med mänskligare  arbetsplatser. Den generella välfärden i Sverige är ett viktigt  historiskt exempel på hur vi har kunnat bygga bred solidaritet.  

Segregationen mellan människor gör det lätt att uppfatta oss som  uppdelade i grupper snarare än att se alla skäl vi har till att hålla  ihop. Den som letar efter någon att se ner på kan enkelt väva ihop  rasism och klassförakt. Däremot blir det långt mindre relevant om  det känns som att riktiga framsteg finns inom räckhåll. När vi  bestämmer oss för att ta strid för våra gemensamma intressen, blir  det också uppenbart att det bara är den ekonomiska eliten som  tjänar på en politik som trampar på människor.  

Genom historien har många män fastnat i att försvara det gamla när  kvinnor visat att det behövs förändring. Det har varit ett hinder för  både det feministiska arbetet och organisering överhuvudtaget.  Samtidigt kan vi se att de framsteg kvinnorörelsen drivit igenom  ofta vunnit brett stöd i efterhand, trots att de en gång i tiden varit  djupt kontroversiella.  

Att bygga förtroende mellan människor är en grundläggande  strategisk uppgift för Vänsterpartiet. För att hålla ihop och agera  tillsammans behöver vi kunna mötas som jämlikar, förstå varandras  perspektiv och ge plats för olika erfarenheter. Då kan vi också lyfta  blicken och se det större sammanhanget. Vi arbetar för att fler ska  se styrkan i breda strategier, som knyter samman intressen.  

Det här samhället har präglat oss alla. Det är inte alltid så enkelt att  ta sig ur de mönster vi är uppväxta med, men ärliga försök att förstå  andras perspektiv tar oss långt. Ju bättre vi är på att möta varandra  där vi är, desto större möjligheter har vi att komma framåt. Vi vill  bygga trygga, varma sammanhang där människor från alla delar av  samhället agerar tillsammans på jämlika villkor.  

#### Organisering för förändring

Alla viktiga politiska framsteg har byggt på att människor  organiserat sig och drivit sina intressen tillsammans. Att bygga  och stötta självständiga och demokratiska folkrörelser är  därför en grundpelare i vår politiska strategi. Det är styrkan i  rörelserna som avgör vad som är politiskt möjligt i samhället.  När de gror och vinner segrar växer också människors självförtroende och tillit till den gemensamma solidariteten.   

Vänsterpartiet är en del av en större politisk rörelse med många  olika delar. Den hänger ihop för att varje del kommer att ha större  möjligheter att komma framåt ju bättre det går för de andra delarna.  Ju större medvetenhet om det vi kan skapa, desto starkare står vi  tillsammans.   

Den fackliga rörelsen spelar en nyckelroll i att organisera  människor gentemot kapitalet i arbets- och samhällslivet. Att  vara med och bygga upp starka, levande fackföreningar på  arbetsplatserna är en av de strategiska huvuduppgifterna för  Vänsterpartiet och våra medlemmar. Ett brett, aktivt deltagande är  avgörande för att försvara och förstärka de framsteg vi drivit  igenom i lag och kollektivavtal. Det är framför allt genom att  tillsammans ta strider i vardagen vi skapar stark facklig  sammanhållning. En avgörande insikt i det arbetet är att varje  spricka i den gemensamma förhandlingsstyrkan sakta sprider sig  om vi inte lagar den. Vi behöver gemensamt ta oss an den större  uppgiften att organisera alla som lönearbetar.   

Kvinnors organisering – politiskt, fackligt och socialt – har varit  den starkaste kraften bakom den historiska frigörelse vi sett från det  traditionella patriarkatet, mot större frihet och jämlikhet. Den har  utmanat gamla strukturer på många plan samtidigt och stärkt  kvinnors makt över sina kroppar, sitt arbete och sitt liv. Vi vill  vara med och skapa en bred, målmedveten feministisk rörelse, som  både arbetar med praktisk frigörelse i vardagen och sätter tryck på  politiska reformer.   

Människors gemenskap har lagt grunden för många starka  svar på rasistiska strategier uppifrån. I föreningslivet,  fackföreningarna och vänkretsarna odlar vi en  sammanhållning som inte låter sig luras så lätt. Vi vill vara  med och bygga en bred antirasistisk rörelse som förstärker  sammanhållningen och skapar tryck på förändring.   Många starka rörelser utgår från de platser där vi bor tillsammans.  Vi ser det när grannar går samman för att sätta tyngd bakom  sina krav som hyresgäster. Vi ser det i allt från förortsorganisering  till byalag, från arbetet för lokala skolor och sjukhus till försvaret  av naturen omkring oss. I det arbetet märks det att det handlar om  gemensamma frågor, som vi kan driva bättre när vi väver ihop det  som händer över hela landet. Det finns mycket som förenar  Sveriges förorter, landsbygd, glesbygd och bruksorter.   

Klimatfrågan öppnar nya politiska möjligheter, eftersom en verklig  omställning bara kommer att kunna ske i samhällen som tar ett fast,  demokratiskt grepp om sin ekonomiska utveckling. Arbetet med  den sortens långsiktiga framtidsfrågor har sina egna utmaningar,  bland annat för att det ofta är långt till synliga resultat. Med en  stark motståndare som fossilkapitalet blir det desto viktigare att  samla människor brett och ge alla möjlighet att känna sig hemma i  rörelsen.  

Vänsterpartiet arbetar för att förändra samhället genom breda  folkrörelser och parlamentariska beslut. Demokrati är  samtidigt mer än lagar och regler. Civil olydnad är en yttersta  möjlighet för människor att försvara folkligt förankrade  värden som mänskliga rättigheter och naturresurser. Den  grundar sig på principer om icke-våld och öppenhet och är en  viktig del av demokratin.  

#### Arbeta strategiskt

Politiskt arbete behöver vara tålmodigt, långsiktigt och strategiskt.  Varje samhälle är alltid i förändring och fullt av motsättningar. För  att göra skillnad behöver vi förstå den situation vi är i, svara på de  öppningar som uppstår och samla människor så brett som möjligt  för ett steg framåt. Förmågan att bygga makt och hopp är  avgörande för att det ska hända. Det är genom att skapa en politisk  situation där verklig förändring är inom räckhåll vi motiverar  människor att agera.  

Som ett politiskt parti spelar Vänsterpartiet en särskild roll i  genomförandet av konkreta reformer. Vi arbetar för att förtjäna  människors förtroende och röster, vinna val och förändra den  politiska riktningen i parlamenten. Det är viktigt för oss att  förvalta den makt och det ansvar vi får från väljarna väl, så att varje  röst på oss har så stor betydelse som möjligt. Vi är ett parti som  finns till för att ändra spelplanen och driva igenom nya sätt att  organisera samhället.   

När vi tar fram förslag drar vi konfliktlinjerna medvetet för att  kunna samla människor brett. Där det går försöker vi isolera  aktörer som spelar en destruktiv roll, som fossilkapitalet eller  tobaksbolagen. Framstegen behöver komma alla till del och  samtidigt leda till jämlikhet och sammanhållning. Gemensamma  erfarenheter av segrar, av att framgångsrikt driva sina  intressen tillsammans, formar starka rörelser.   

Att utveckla kloka politiska lösningar kräver alltid tid och  eftertanke. En del av det är att kritiskt syna det som först kan verka  självklart, i allt från problemformulering till hur en ny lag skulle  fungera i verkligheten. Ser vi tillbaka i historien har vitt spridda  föreställningar visat sig konservativa och ogrundade, i allt från  dömande människosyn till godtyckliga föreställningar om vad som  är naturligt och inte. En framgångsrik rörelse för ett bättre samhälle  behöver bygga på stabil forskning och förslag som visar sig  effektiva och rättvisa den dag de genomförs.   

Vi vill genomföra strategiska reformer som både gör livet rikare och  friare, här och nu, och bygger upp människors möjligheter att driva  sina intressen framöver. Det skapar förutsättningar för att ta nästa  steg och uppnå större och mer grundläggande förändringar.  Det kan till exempel handla om lagar som försvarar människors  arbetsvillkor genom att stärka skyddsombuden, eller om institutioner  som både utför ett arbete och samlar kunskap om varför det arbetet  behövs. Det är ett politiskt hantverk som kräver eftertanke och  prioriteringar, men det leder till långsiktig förändring.   

### Strategiska reformer

#### Arbete ger utveckling

Det är med arbete vi bygger framtiden. Genom att medvetet skapa  goda förutsättningar för allt arbete, både lönearbetet och det  obetalda arbetet, vill vi lägga grunden för en bättre ekonomisk  utveckling. De flesta av Vänsterpartiets politiska förslag handlar  om att ta bättre hand om människors vilja och förmåga att arbeta,  och rikta det arbetet mot att lösa de stora samhällsproblemen.  

Vi vill se en samlad strategi för full sysselsättning som bygger på  stora, långsiktiga samhällsekonomiska investeringar. Det handlar  om att bygga upp ny infrastruktur och nya industrier, som tar oss ur  fossilberoendet och kärnkraftsberoendet. Det handlar om att  skapa ett starkt utbildningssystem, som ser till att alla människor  kan det de behöver på sina arbetsplatser och i livet i stort. Det  handlar om att bygga bostäder, som ökar människors frihet att bo  och leva som de vill.   

Det handlar om att ta ett starkare samhälleligt ansvar för  omsorgsarbetet. Vi vill höja ambitionerna för hur mycket tid det  finns för de som växer upp, både i vardagen på förskolan och när  någon behöver det särskilt. Genom hela livet ska det finnas bra stöd  att få, så att både brukare och anhöriga kan leva så fria liv som  möjligt. Det leder till att vi bättre använder människors  arbetsförmåga till det de är utbildade för, samtidigt som vi skapar  tid för allt det som görs bäst i nära relationer.  

Vi vill påbörja en stegvis arbetstidsförkortning med bibehållen lön,  som frigör tid för relationer, egna intressen och vila. Det är en  reform som skulle ta många människor ur stress och skapa en friare  vardag. Det handlar samtidigt om att gå över till ett mer hållbart  sätt att arbeta. Bättre hälsa, större engagemang i yrkeslivet och mer  tid till fritt arbete i till exempel föreningar betyder mycket för den  långsiktiga utvecklingen av samhällsekonomin.  

Varje steg mot full sysselsättning gör att människor står  starkare på arbetsplatserna. Gedigna trygghetssystem har samma  effekt, eftersom det gör att fler kan lita på sin ekonomiska stabilitet  vid arbetslöshet eller sjukdom. Vi vill också stärka arbetsrätten och  ta itu med oseriösa företag, för att gå mot ett Sverige där alla som  arbetar har riktiga löner och villkor. Många skulle kunna räta på  ryggarna på jobbet, trygga i att det inte går att behandla människor  hur som helst.   

Den förhandlingsstyrkan får många positiva samhällsekonomiska  konsekvenser. Den leder till att en större del av det vi skapar går till  löner och en mindre del till kapitalägarna. Företag kommer att  behöva se mer långsiktigt på sin personalförsörjning och investera  mer i utbildning och hälsa. Det sätter ett större tryck på att  organisera arbetet så att alla, oavsett hur vi fungerar fysiskt  eller intellektuellt, kan bidra efter förmåga. Utrymmet för  diskriminering och andra godtyckliga orättvisor minskar. Det är  skillnader som syns för alla som arbetar och betyder särskilt  mycket för dem som idag har de hårdaste villkoren.   

#### Trygghet och självständighet

Starkare ekonomisk självständighet gör att var och en kan  organisera sitt liv friare. Den jämlikhet som kommer ur det betyder  mycket för den riktning samhället utvecklas i långsiktigt, i allt från  hur bra demokratin fungerar till fördelningen av det obetalda  omsorgsarbetet.  

Det lägger också en bra grund för att fortsätta den historiska  utvecklingen mot relationer som bygger på ömsesidig kärlek  snarare än nödvändighet. Vi vill arbeta för att utveckla en starkare  samtyckeskultur och sänka acceptansen för social press och  kontroll. Arbetet mot mäns våld mot kvinnor behöver börja  prioriteras på ett sätt som motsvarar brottens omfattning och allvar.  

Vi vill bygga ett samhälle som alltid finns där för den som behöver  det. Från uppväxten till pensionen ska det vara tydligt att vi  tillsammans löser de problem människor möter i livet. En generell  välfärd med en hög lägsta nivå är en helt avgörande strategi för  att samla människor i det. Ett tryggare samhälle där vi är bra på  att ta hand om varandra kommer att vara långt mer redo att ta sig an  de uppgifter vi har framför oss. Det innebär också att vi bygger  ett samhälle där alla människor, oavsett hur vi fungerar  fysiskt, psykiskt eller intellektuellt, kan leva goda liv. Alla  behöver kunna vara delaktiga i samhället, bli bemötta för den  person de är, vara jämställda föräldrar, utbilda sig, ha arbete  och försörjning, använda kollektivtrafiken och ha en aktiv  fritid.   

Många av de investeringar vi vill genomföra handlar om att ta tag i  problem innan de vuxit sig stora. Genom att bygga upp välfärd och  infrastruktur i både förorterna och landsbygden kan vi se till att  hela Sverige utvecklas. Full sysselsättning, bostäder och välfärd är  kraftfulla verktyg för att bryta upp segregation och rasistiska  strukturer. I ett samhälle med gott om jobb och med stark  ekonomisk självständighet kommer människor inte att låta sig  hållas isär.   

#### Demokratiska ägandeformer

Vi vill att fler av de beslut som har stora, komplexa konsekvenser  för samhället ska fattas med ett genomtänkt helhetsperspektiv.  I nyckelsektorer som finans, infrastruktur, bostäder och energiförsörjning vill vi kraftigt stärka det gemensamma ägandet,  eftersom det är verksamheter där besluten i alla led behöver väga in  långsiktiga samhällsekonomiska mål. I vård, skola och omsorg  vill vi överhuvudtaget inte se några aktörer som är där för att  göra vinster i välfärden.   

Överhuvudtaget vill vi gå mot arbetsplatser där de som står för  arbetet spelar en större roll i de beslut som fattas. Ju mer  utvecklingen går i riktning mot komplex produktion, desto  viktigare blir det med delaktighet och förtroende för människors  yrkeskunnande. Vi vill stödja mer demokratiska ägandeformer,  som företag som drivs av dem som arbetar där, och utveckla arbetsrätten i riktning mot ett större inflytande för anställda.   

Världen behöver gå i riktning mot att upplösa den illegitima  politiska makt som till exempel fossilkapitalet sitter på. Det är inte  långsiktigt hållbart att ett litet fåtal samlar på sig absurda  förmögenheter, byggda med andra människors arbete. Varje genuint  demokratisk kraft behöver ta på sig uppgiften att förändra det.  

Vi vill utveckla mer övergripande demokratisk samordning av den  riktning ekonomin utvecklas i. De stora besluten om vår  gemensamma framtid behöver fattas demokratiskt, med en genuin  förståelse för hur människor, samhällen och ekosystem fungerar.  

#### Ut ur fossilberoendet

Vi vill sätta fasta ramar för att ta Sverige ur fossilberoendet, med  en utsläppsbudget och ett omställningsprogram som kan ta oss hela  vägen till utsläpp nära noll. Det kommer att behövas stora  investeringar, ett aktivt gemensamt ägande och en beredskap att ta  konflikt med fossila ekonomiska intressen för att genomföra det.  Vänsterpartiet avvisar planerna på att bygga ut kärnkraften  som en väg ut ur fossilberoendet.   

En politisk strategi för omställning behöver lägga fokus på åtgärder  som både har stora utsläppseffekter och kan vinna brett politiskt  stöd. Den behöver bygga på förtroende för att de flesta människor  gör sitt bästa i vardagen och se till att det blir så enkelt som  möjligt. Den behöver en trygg och stabil takt i omställningen, där  vi tillsammans ser till att de tekniska skiften vi genomför fungerar  för alla. Den behöver bygga på rättvisa och tydlighet i att de  som står för de största utsläppen är först med att förändra sina  vanor.   

Vi vill visa att det går att förena en radikal omställning med att  bygga ett samhälle som är enklare att leva i, med full  sysselsättning, välfärd och trygghet. Den politiska verktygslåda  som behövs för att fasa ut det fossila kommer också att kunna  användas för att lösa andra stora samhällsproblem.  

#### Sverige i världen

Stora samhällsekonomiska investeringar, gemensamt ägande och  jämlikhet skulle betyda att Sverige kan stå starkt också i tider av  konflikt i omvärlden. Vi vill utveckla ett bättre internationellt  samarbete mellan länder, som skapar ett större oberoende från  stormakternas geopolitiska spel. Tillsammans kan vi stärka alla  länders möjlighet att besluta om sin produktion, sina finansiella  flöden och sina naturresurser.  

Vi vill att Sverige utmanar EU:s överstatliga fördrag och lagar  när de sätter kapitalets intressen före jämlikhet och nationellt  självbestämmande. Vi lägger fram förslag i konflikt med den  sortens EU-lagstiftning för att i förlängningen vidga utrymmet  för progressiv politik.   

Vi vill att Sverige ska vara ett land som deltar helhjärtat i arbetet  med de historiska utmaningar världen står inför, som att ta  människor ur fattigdom och genomföra klimatomställningen.  Sverige behöver vara en stadig röst i världen som talar för mänskliga  rättigheter, fred och nationellt självbestämmande.   

#### Frigörelse

Det vi vill göra är saker Sverige har gjort förut, när människor  organiserat sig och använt demokratin för att skapa ett bättre  samhälle. Vi byggde ut allt från utbildning till sjukvård och  bibliotek genom att bestämma oss för att investera, äga tillsammans  och gå mot jämlikhet. Det fungerade. Mycket av det vi tänker på  som bra med det här landet kommer ur socialistiska och  feministiska reformer.  

Vi vill fortsätta i den riktningen och frigöra fler delar av  produktionen från kapitalets bojor. Genom att ta och vinna  konflikter om ägandemakten öppnar vi nya möjligheter. Vi vill  se kloka, samhällsekonomiska mål istället för kortsiktig  vinstutdelning. Vi vill demokratisera beslut som idag fattas i  stängda styrelserum. Vi vill komma ur kapitalismens ständiga  ekonomiska och politiska kriser, till ett samhälle som söker riktiga  lösningar. För att göra det behövs en rad skiften till  gemensamma, demokratiska ägandeformer som kan hantera  de stora samhällsutmaningarna.   

Det är det vi tänker på som socialism. Att slå in på en socialistisk  väg framåt handlar om att organisera arbetet på bättre sätt, för att  bygga ett samhälle där vi kan leva friare liv. Där det finns bra  skolor, bostäder och möjligheter i livet för alla. Där det finns gott  om tid för kärlek och omsorg om varandra. Där ekosystemen har  utrymme att återhämta sig. Där vi går med sträckta ryggar på  arbetsplatsen. Där vi fattar de avgörande besluten tillsammans.  Vi vill lämna maktstrukturerna bakom oss och utveckla ett  jämställt och socialistiskt samhälle, byggt av var och en efter  förmåga, åt var och en efter behov.   

För att göra det behöver vi odla sammanhållning mellan människor.  Kan vi arbeta tillsammans för våra gemensamma intressen står vi  mycket starkare i konflikterna med de gamla strukturer som är i  vägen. Att organisera den rörelsen är Vänsterpartiets huvuduppgift.  

## Vänsterpartiets grundsyn i sakfrågor

### Stärk den politiska demokratin

Vi vill bygga vidare på Sveriges demokratisering. Vi vill förstärka  och fördjupa de politiska fri- och rättigheter folket drivit igenom i  konflikt med en konservativ överhet. Den demokratiska principen  om en person en röst står i skarp kontrast till den makt som grundas  i ägandet av banker och kapital. FN:s allmänna förklaring om de  mänskliga rättigheterna var ett historiskt framsteg vi vill  förverkliga fullt ut.  

Folkets politiska makt över staten ska utövas av riksdagen, tillsatt  genom allmänna, fria och rättvisa val, där mandaten fördelas  proportionellt mellan partier. Regeringen ska vara ansvarig inför  riksdagen. Vi vill se en republikansk författning där statschefen  tillsätts i demokratisk ordning.  

Den offentliga maktutövningen ska vägledas av alla människors  lika värde. Grundlagarna ska värna demokratin och människors frioch rättigheter. Några av den politiska demokratins viktigaste  byggstenar är föreningsfriheten, yttrande- och tryckfriheten,  demonstrationsfriheten, meddelarfriheten och  offentlighetsprincipen. Alla människor har rätt till skydd av sin  personliga integritet. Religionsfriheten ska garanteras och staten  ska vara sekulär, det vill säga inte ha några band till någon religion.  

Samhället behöver ett väl fungerande rättsväsende, med  självständiga och opolitiska domstolar där det finns ett tydligt  lekmannainflytande. Utvecklingen av ett rättsväsende byggt på  rättssäkerhet är viktig att försvara och fortsätta förverkliga.  Principen att människor ska anses oskyldiga tills motsatsen är  bevisad är grundläggande i det. Rättssäkerheten behöver  upprätthållas genom rättshjälp, medborgerlig insyn och oberoende  granskning.   

Rättsväsendet har historiska problem som kommer ur klassamhället,  patriarkatet och rasistiskt tankegods och behöver ett aktivt och  medvetet arbete för att lägga det bakom sig. Det gör det särskilt  viktigt att säkra en folklig förankring i rättsprocesserna. Polisen och  Försvarsmakten spelar roller i samhället som innebär att de behöver  vara tydligt inordnade i demokratiska strukturer. Det ställer också  höga krav på fungerande ansvarsutkrävande vid lagbrott.  

Det kommunala självstyret är en betydelsefull del av den svenska  demokratin. Rollfördelningen mellan staten, regionerna och  kommunerna behöver balansera värdet i att fatta beslut nära  medborgarna med möjligheterna att bygga ett sammanhållet land.  Staten har det övergripande ansvaret för välfärden. Det gör det  möjligt att omfördela resurser på ett rättvist sätt genom det  progressiva skattesystemet. Inkomster och kostnader behöver  fördelas solidariskt över landet. Skola, vård, omsorg och annan  service behöver fungera överallt.   

### Ägande med helhetssyn

Vi vill att de verksamheter vi äger tillsammans drivs med fokus på  den långsiktiga samhällsnyttan. Det handlar både om att ta fram  kloka ägarstrategier och att odla en kultur av långsiktighet och  helhetsperspektiv i styrningen av välfärd, myndigheter och statliga  bolag. Det betyder till exempel att besluten i alla led ska utgå från  en strategi för klimatomställning och präglas av genuin omsorg om  människor och natur. Vi vill utveckla dem till föregångare som  arbetsplatser, som sätter en ny och högre standard för till exempel  arbetsmiljö och jämställdhet.  

Det skiftet mot att fatta beslut med helhetssyn behövs i fler  verksamheter än de som ägs gemensamt idag. Vi vill steg för steg  stärka det gemensamma ägandet i ekonomins nyckelsektorer  och därmed öka de demokratiska beslutens räckvidd. Det  handlar bland annat om strategiska naturtillgångar,  energiförsörjning, infrastruktur, bostadsproduktion och den  finansiella sektorn. Företag vars beslut har stora konsekvenser för  alla som bor här behöver ägandeformer som motiverar dem att lyfta  blicken och se det. Det skulle innebära nya möjligheter till en klok  samhällsekonomisk utveckling. Med ett bredare gemensamt ägande  kommer bättre möjligheter att samordna den ekonomiska  utvecklingen, till exempel kring de skiften i teknik och arbetssätt vi  behöver i klimatomställningen. Det ger också fler verktyg för att  skapa en regionalt balanserad ekonomisk utveckling.   

Gemensamt ägda verksamheter kan användas för att driva  förändring också i privata företag. Deras inköp av varor och  tjänster är ett viktigt strategiskt verktyg för att genomföra  samhällsekonomiskt betydelsefulla skiften och etablera standarder.  På marknader som präglas av övervinster eller dåliga avtalsvillkor  kan gemensamt ägda företag sätta tryck genom att erbjuda bättre  villkor.  

Redan idag är det gemensamma ägandet större än vad många  inser. Genom pensionsfonderna äger vi tillsammans en  betydande del av börsbolagen. Vi vill använda den  ägandemakten till att aktivt styra Sveriges ekonomiska  struktur mot samhälleliga mål som långsiktig hållbarhet och  ordning och reda på arbetsmarknaden.   

Förutom det gemensamma ägandet vill vi också uppmuntra andra  ägandeformer som stärker demokratiska strukturer och  samhällsnytta. Det kan till exempel handla om arbetarägda företag,  konsumentkooperationer eller brukarförvaltning. Makten över  ägandet är strategiskt avgörande eftersom den betyder så  mycket för makten i samhället i stort.   

Staten behöver agera strategiskt för att kapitalet inte ska sitta på  orimliga maktpositioner. Ett exempel på det är hur bankerna kunnat  använda samhällets beroende av deras ordinarie in- och utlåning till  att pressa fram garantier för sina riskfyllda affärer. För att komma  ur det vill vi lagstifta om att skilja de två verksamheterna från  varandra. Med ett starkare ägande i finanssektorn kan staten säkra  samhällsviktiga investeringar, skärpa konkurrensen och förbättra  villkoren för mindre företag. Staten kan också spela viktiga  roller för att hålla företagens prissättning på en rimlig nivå.   På sikt bör bankväsendet i sin helhet  demokratiseras. Tills dess behövs en hårdare reglering av  bankernas samhällspåverkande verksamhet.   

Skuldsättning försvagar många människors självständighet och  förhandlingsstyrka. Regleringen av lånevillkor behöver utgå från  våra gemensamma behov snarare än finanskapitalets. Staten spelar  en nyckelroll i att se till att det går att både bo och studera med  ekonomiskt trygga villkor.  

### Välfungerande arbetsplatser

Arbetsvillkoren gör ofta stor skillnad för vilken frihet vi har i våra  liv. Att bli mött som människa, med förtroende och respekt, tar  fram våra bästa sidor. Styrsystem som bygger på ekonomistiska  kontrollsystem och byråkratisk övervakning förstör människors  kreativitet. Tillit till kunskapen och engagemanget hos dem som  gör arbetet är helt avgörande för att långsiktigt utveckla väl  fungerande verksamheter. Den sorts arbetsvillkor som driver hög  produktivitet och kreativitet stärker samtidigt de anställdas  positioner.  

För många skulle det betyda mycket att ha kortare arbetsdagar,  bättre arbetsmiljö och högre löner. Vi vill stärka människors  förhandlingsstyrka på arbetsplatserna, så att mer av den rikedom  arbetet skapar går tillbaka till de som faktiskt gör jobbet. Vi vill  göra det till en del av en medveten utvecklingsstrategi, där ett högre  löneläge gör det rationellt för företag att investera mer i människor  hälsa och kunskaper.  

Fackföreningsrörelsen har en djup betydelse för människors makt i  arbetslivet. Vi vill förändra konflikträtten och annan  lagstiftning för att stärka löntagarnas positioner. De fackliga  organisationerna behöver till exempel bättre möjligheter att  säkerställa att lagar och avtal efterlevs och att driva lika lön för  lika arbete.   

Vi vill skapa tryggare jobb genom att utveckla arbetsrätten. Alla  anställda i Sverige ska skyddas av starka kollektivavtal. Vi vill  täppa till det utrymme som finns idag att exploatera människors  utsatta ställning. Arbetskraftsinvandringen ska bygga på trygga  villkor och samhällsekonomiska behov.   

Att utveckla en bättre arbetsmiljö gör stor skillnad både för  människors liv här och nu och för ett gott arbetsliv på lång sikt.  Ingen ska behöva dö på jobbet för att lagstiftningen inte ger ett  tillräckligt skydd. Det behövs regelverk och strukturer som säkrar  ett seriöst arbetsmiljöarbete på alla arbetsplatser, också de som idag  prioriterar kortsiktig vinst.   

Alla människor behöver goda möjligheter att balansera arbetsliv,  vila och andra delar av livet. Vi vill se en generell arbetstidsförkortning. Vi vill att alla anställda ska ha lagstadgad rätt till  heltid och större makt över sina arbetstider. Att barn- och  äldreomsorgen fungerar bra är avgörande för människors  möjligheter till förvärvsarbete och ekonomisk självständighet.  

Vi vill gå i riktning mot ett samhälle där demokratiska processer  genomsyrar hela samhället, även arbetsplatserna. Vi vill att  gemensamt ägda verksamheter går före i det skiftet och utvecklar  praktiska former för att ge anställda en större roll i hur arbetet  organiseras.  

### Stora investeringar

Det är människors arbete som driver den ekonomiska utvecklingen.  Vi vill bygga ett samhälle där det finns bra och meningsfulla jobb  till alla. Omfattande investeringar för att ta itu med de stora  samhällsutmaningarna bildar kärnan i den samlade strategi för full  sysselsättning vi vill se. Riksbanken behöver demokratiseras och  driva en penningpolitik som prioriterar sysselsättningen högt.  

Vi vill att regeringen arbetar med en utsläppsbudget som knyter  ihop klimatmålen med konkreta, siffersatta förslag. Det gör det  möjligt att bygga upp strategier med fokus på det som är mest  avgörande.  

Sverige behöver också ett medvetet och heltäckande arbete med att  skapa goda villkor för rika ekosystem, bland annat genom att  återställa natur och kompensera för förlorade naturvärden.  Våtmarker spelar en särskild roll för att sänka klimatutsläppen.  Vi vill se ambitiösa miljömål som följs upp med skarp lagstiftning  och praktiskt arbete.   

För att ta tag i de stora samhällsutmaningarna kommer  skattenivåerna att behöva öka igen, framför allt på stora  förmögenheter, fastigheter, arv och höga inkomster. Vi vill se ett  enkelt, stabilt och solidariskt skattesystem där de rikaste åter börjar  betala sin del. Skatterna bidrar på många sätt till ett tryggt, jämlikt  och jämställt samhälle, särskilt om de utformas progressivt och  används till saker alla har användning av.  

Skatter kan spela en viktig kompletterande roll för att driva på  klimatomställningen. De behöver utformas klokt för att få verklig  effekt och vinna politiskt stöd. Kostnaderna för utsläpp behöver  öka långsiktigt på ett sätt som är tryggt och förutsägbart, i takt med  att samhället bygger upp nya, bättre alternativ.  

### Starka försäkringar

Vi vill se starka försäkringar som skapar trygghet för var och en  under hela livet. Det handlingsutrymme som kommer ur det ger  människor bättre förhandlingsstyrka på arbetsplatserna. Därför har  det alltid funnits starka ägarintressen av att punktera den  tryggheten.  

Att socialförsäkringarna är allmänna och heltäckande gör dem  kostnadseffektiva och bygger upp förtroendet för dem. Att de är  rättighetsbaserade och grundade i en inkomstbortfallsprincip är  viktigt för att skapa en bred solidaritet kring dem. Samtidigt  behöver de vara utformade så att alla människor ska kunna  leva tryggt, också den som på grund av funktionshinder på  arbetsplatserna inte har kunnat lönearbeta.   

Vänsterpartiet vill se en individualiserad föräldraförsäkring, med  utrymme för att familjer ser olika ut. De steg som tagits åt det hållet  har betytt mycket för att män ska ta en större del av ansvaret för  föräldraskapet. Det gör stor skillnad för kvinnors ekonomiska  självständighet på både kort och lång sikt.  

Alla ska kunna känna sig trygga med att pensionen kommer att  räcka till ett gott liv. Vi vill se ett rättvist och jämställt pensionssystem som fungerar också för den som slitit hårt med sin kropp  genom ett långt arbetsliv.   

### Välfärdens verksamheter

Det arbete som utförs i välfärden är helt avgörande för att bygga  Sveriges framtid. Ju mer komplex produktion vi utvecklar desto  större betydelse har god utbildning, vård och omsorg, både för  människors frihet och för samhällsekonomin. Att bygga upp en  stark gemensam välfärd är samtidigt en avgörande del av att  öka den demokratiska makten i samhället.  

Vi vill bygga ett jämlikt samhälle där vi tar ett gemensamt  ansvar för varandra. Tillsammans kan vi hjälpas åt och se till  att alla medborgare och invånare har tillgång till vård, skola  och omsorg och annat vi behöver i livet. Genom en generell  välfärd där lägsta nivån är hög knyter vi ihop alla människors  intresse av att kunna leva goda och trygga liv.  

I välfärdens verksamheter behöver vi mötas som människor. Därför  har det gemensamma ägandet en särskild betydelse där. I skola,  vård och omsorg ska vi alltid vara trygga med att alla som är där  arbetar med våra faktiska behov som riktmärke.  

Vi vill att samhället tar ett större ansvar för att arbetsplatserna i  välfärden har goda arbetsvillkor och löner. Det betyder särskilt  mycket i de kvinnodominerade välfärdsyrkena, som länge har  pressats hårt. Det är en särskild utmaning att bygga upp  förhandlingsstyrka i yrken där en strejk snabbt får djupa  konsekvenser för vissa människors liv.   

### Utbildning

En väl fungerande utbildning, från förskolan till yrkesutbildningar  och universitet, är helt avgörande för att alla människor ska ha en  stark grund att stå på genom livet. Vi vill se en skola med ett brett  uppdrag och kapacitet att stödja varje barn under uppväxten. Den  har en viktig roll att spela i att kompensera för klasskillnader och  andra skillnader i förutsättningar. Möjligheterna att vara på  fritidshem betyder mycket för att alla barn ska ha en  meningsfull fritid.   

Skolan behöver bygga på en människosyn och pedagogik som är  grundad i vetenskap och inriktad på att stärka människors  möjligheter att leva goda liv. Den är en viktig del av den  demokratiska infrastrukturen, som kan bygga sammanhållning och  stärka förmågan att delta som aktiv medborgare i samhället.  Därför ska elever och studenter ha rätt till inflytande över sin  utbildning genom hela utbildningssystemet.  

Marknadsskolan är ett historiskt misslyckande, som visar vilka  djupgående konsekvenser fel ägandeformer och minskad  gemensam styrning kan leda till. Gemensamt ägande är det  tryggaste alternativet för att säkra att skolor drivs med helhetssyn  och genuin omsorg om elevernas bästa. För att bygga upp en jämlik  skola vill vi att staten tar över huvudansvaret från kommunerna och  att friskolesystemet avskaffas. Istället för dagens skolval vill vi  se en skolplacering av elever som motverkar segregation.  

Vi vill göra stora investeringar i att höja kvaliteten i utbildningssystemet. Det lägger en bättre grund för hela samhällets  ekonomiska utveckling. Utbildningsystemet behöver svara mot  människors långsiktiga behov i ett föränderligt arbetsliv, med breda  kunskaper under uppväxten och goda möjligheter till lärande  genom hela livet. Folkbildningen i form av bland annat  bibliotek och studieförbund betyder mycket för det.  Folkhögskolorna har en särskild roll i det svenska  utbildningsystemet, som vi vill stärka och utveckla.     Forskning behövs för att förstå den värld vi lever i. Den öppnar för  social, politisk och ekonomisk utveckling. Vi betonar vikten av  självständiga akademier utan politisk styrning och vill se en  utbyggnad av den fria grundforskningen.   

### Vård och omsorg

Hälso- och sjukvården genomgår en fantastisk utveckling.  Tillstånd som tidigare skulle ha inneburit döden kan idag  behandlas och ge många levnadsår till. Samtidigt har  sjukvården länge utsatts för förödande privatiseringar och  besparingar, som drabbat såväl personal som patienter.  Ojämlikheten i tillgång till hälso- och sjukvård har blivit större,  bland annat på grund av en politik som låtit vissa köpa sig  förtur. Vi vill sätta punkt för marknadsvården.  

Resurserna till hälso- och sjukvården måste öka för att täcka  behoven och stärka beredskapen inför kriser, katastrofer och  krig. Det gäller särskilt primärvården. Sjukvården ska vara  heltäckande, bygga på evidens och prioriteras utifrån behov.  Vi vill att ansvaret och styrningen över hälso- och sjukvården  ska ligga hos regioner och kommuner, inte förstatligas. Däremot  behöver staten ta ett större ekonomiskt ansvar för att ge dem  goda och jämlika förutsättningar i det. Staten har också ett  övergripande ansvar för att det finns utbildad profession i hela  landet.  

Vi vill att tandvården inkluderas i hälso- och sjukvården, med  samma taxor. Kostnaderna för vård och läkemedel ska vara  överkomliga för alla. Vi vill att staten äger apoteken och bygger  upp en egen kapacitet för läkemedelsproduktion.  

Vården behöver ses som en del i det bredare arbetet med  folkhälsa. Hur samhället fungerar i allt från arbetsmiljö till  föreningsliv och grönområden gör sammantaget stor skillnad  för hur långa, goda och fria liv vi människor lever.   

Vi vill se en restriktiv reglering av alkohol och tobak.  Systembolaget är ett bra exempel på hur vi genom gemensamt  ägande kan ta ansvar för komplexa problem och sätta en  genomtänkt ram för privata företag. Människor som fastnat i  missbruk ska ha tillgång till den vård de behöver och  behandlas med respekt. Det behövs ett effektivt arbete mot  narkotika i alla led, från det förebyggande till polisarbetet.  

Alla ska känna sig trygga i att det finns stöd att få när vi behöver  det. Vi vill investera i högre kvalitet i omsorgen för äldre och  personer med funktionsnedsättning för att få en bättre  rollfördelning mellan professionell omsorg och anhöriga.  

### Seriöst företagande

Vi vill se goda villkor för sunda företag, som tar tillvara på  människors drivkrafter att bygga upp egna verksamheter och riktar  dem mot samhällsekonomiska behov. Lagar och sammanhang kan  göra stor skillnad för att företagandet ska ta sig positiva uttryck. De  investeringar vi vill se i utbildning, infrastruktur och trygghet  skulle skapa goda förutsättningar att starta nya, seriösa  arbetsplatser.  

Sverige är ett land med goda förutsättningar för industriproduktion.  Vi har välutbildad arbetskraft, god tillgång till förnybar energi och  värdefulla råvaror. Sveriges export kan fylla viktiga funktioner i  den globala klimatomställningen.   

Vi vill att Sverige beslutsamt genomför de strategiska teknikskiften  som behövs i industrin för att komma ur fossilberoendet. Staten har  viktiga roller att spela i det, i allt från forskning till elproduktion,  infrastruktur, utbildning och ett ekonomiskt ramverk som driver på  utvecklingen. Det är en nyckeldel i ett bredare skifte till  cirkulär ekonomi och långsiktig hållbarhet i industrin.   

Vi vill gå mot ett större gemensamt ägande av gruvbrytningen i  Sverige för att säkra ett seriöst arbete och fördela vinsterna till hela  befolkningen. Den behöver drivas med ett långsiktigt  helhetsperspektiv som ser till såväl de globala behoven som lokala  naturvärden och den ekonomiska utvecklingen i närområdet.  

Jordbrukets långsiktiga hållbarhet är viktig för både ekosystemen  och Sveriges ekonomiska självständighet. Staten har ett särskilt  ansvar att se till att landet är tillräckligt självförsörjande med  livsmedel. Vi kommer att behöva ett långsiktigt skifte mot mer  hållbara matvanor för att klara omställningen. All djurhållning ska  utgå från att djur är kännande varelser som ska leva goda liv.  

Hur vi använder skogen har stora och komplexa konsekvenser, som  bland annat handlar om hur vi binder kol i naturen och i produkter.  Det behövs ett starkt skydd av skog med rika ekosystem och en  genomtänkt reglering av skogsbruket, som utgår från en  helhetssyn och inte ensidiga vinstintressen. Biomassan är en  begränsad resurs, särskilt i relation till världens klimatomställning.  Det kommer att vara viktigt att prioritera användningen av den  klokt.   

## Infrastruktur och bostäder

En radikal utbyggnad av den förnybara elproduktionen är  avgörande för Sveriges klimatomställning, krisberedskap och  ekonomiska utveckling. Det möjliggör en elektrifiering som kan ta  landet ur beroendet av importerad fossil energi. Vi vill se en  energipolitisk strategi som bygger på förnybar el och låga  klimatutsläpp. Nya projekt behöver jämföras i miljökonsekvenser  och säkerhet över hela sin livscykel. Vi ser el och värme som en del  av Sveriges infrastruktur, där investeringar och priser behöver utgå  från samhällsekonomiska mål.   

Vi vill bygga ett samhälle där varje människas rätt att ha  någonstans att bo är verklighet. Mot det står de ägarintressen som  vinner på bostadsbristen genom höga hyror och bostadspriser.  Hyresgäster behöver ett starkt stöd i lagstiftningen för att  tillsammans stå starka och försvara sina intressen.   

Vi vill se en generell bostadspolitik med goda och jämlika villkor  för alla boendeformer. Staten behöver ta huvudansvaret för att  stärka investeringarna i byggande, upprustning och  energieffektivisering av hyresbostäder. Vi vill prioritera att  bygga hyresrätter med rimliga hyror. Den gemensamt ägda  Allmännyttan behöver stärkas. Allmännyttan ska i första hand  ha ett samhällsansvar och inte styras av affärsmässiga  principer.   

Bostäderna behöver vara en del av ett större samhällsbygge,  där vi skapar goda boendemiljöer med skyddande grönska,  mötesplatser, service och arbetsplatser. Planeringen behöver en  genuin demokratisk förankring och utgå från människors kärlek till  platsen man bor på. Vi vill bryta upp segregationen och skapa en  blandning av hustyper och upplåtelseformer, för att bygga städer  där människor möts i vardagen.   

Det gemensamma ägandet av bostäder och mark ger starka verktyg  för stadsplaneringen. Byggande, förnyelse och förvaltning fungerar  bättre utan ensidigt vinstsyfte. Allemansrätten, som ger var och  en fritt tillträde till naturen utan att behöva äga den, och  kulturmiljölagen, som skyddar vårt kulturarv, är viktiga  för att alla människor ska kunna leva goda liv. Samma sak  gäller strandskyddet, som ger allmänheten tillträde till kuster  och sjöar, bevarar känsliga djur- och växtliv och stärker  motståndskraften mot klimatförändringarnas konsekvenser.  

Vi vill se en samhällsplanering som så långt som möjligt undviker  att låsa människor i bilberoende. Det behöver bli enklare att röra  sig i städerna och närmare till välfärd och service på landsbygden.  Vi vill se en sammanhållen strategi för elektrifiering av  transporterna, där stadsplanering, kollektivtrafik och elfordon alla  har viktiga roller att spela. Det behövs en omfattande och  långsiktig investeringsplan för det, som bland annat innehåller  en utbyggnad av järnvägen. Gemensamma ägandeformer och  samordning är avgörande för att trafiken och underhållet ska  fungera bra.   

Flygtrafiken är en av de stora utmaningarna i omställningen.  Eftersom möjligheterna till större tekniska skiften i flyget ligger så  långt i framtiden är det viktigt att bygga upp ekonomiska ramverk  där flyget börjar stå för sina samhällsekonomiska kostnader. Det  skulle sänka utsläppen och samtidigt stärka jämlikheten, eftersom  de ohållbara resvanorna framför allt finns i samhällets toppskikt.  

### Utveckla tekniken

Vi vill se stora gemensamma satsningar på den teknikutveckling  Sverige och världen behöver för att hantera de utmaningar vi har  framför oss. Företagens investeringar i det räcker inte, vilket i  grunden beror på att större delen av nyttan med ny kunskap inte  kommer att synas i deras avkastning.   

Vänsterpartiet vill se en teknikutveckling som står friare från de  begränsningar som kommer ur kapitalets strävan att äga  information. Vi vill gå mot en värld där den kunskap vi utvecklar  tillsammans kan användas fritt, oavsett om det handlar om  läkemedel, programvara eller miljöteknik. Vi vill bygga upp  digitala allmänningar med fri tillgång till mänsklighetens samlade  kunskap och kultur.  

Eftersom tekniska skiften spelar stor roll för maktrelationerna i  samhället i stort behövs det genomtänkta diskussioner om  vägvalen. Vi vill se skiften till arbetsplatser som bygger upp  människors kunskaper, medan kapitalet tvärtom föredrar att öka  utbytbarheten. Vi vill se en utveckling av nätets verktyg,  plattformar och ägandeformer som bygger på användarnas frihet,  integritet och självbestämmande, medan techjättarna tvärtom  försöker göra sig oundgängliga.   

Sverige behöver ett medvetet och strategiskt förhållningssätt till de  storföretag och stormakter som försöker skapa tekniska  maktpositioner. Gemensamt ägda verksamheter behöver agera  samordnat för att undvika inlåsningar och bygga upp långsiktigt  pålitliga strukturer. Det handlar bland annat om att använda öppna  standarder, fri programvara och egna lagringstjänster.  

### Kultur, media och föreningsliv

Människors möjligheter att faktiskt styra den politiska utvecklingen  vilar på en kombination av juridiska rättigheter och en demokratisk  infrastruktur. Det behövs gott om utrymmen för folkliga politiska  diskussioner som kan mynna ut i genomtänkta beslut. De  processerna bygger förtroende mellan människor, förståelse för  samhällets komplexitet och en känsla för de möjligheter vi  tillsammans sitter på.  

Den demokratiska infrastruktur som behövs för det består av allt  från starka och självständiga folkrörelser till ett levande föreningsoch kulturliv och ett rikt medielandskap. Det arbetet behöver goda  resurser för att fungera bra. Vi driver på för villkor som säkrar  en stark självständighet för medier, fackföreningar,  folkbildning, konst, kultur- och föreningsliv. Det skapar ett  kulturellt landskap som gör det svårare för auktoritära krafter att  urholka demokratin.   

Vi människor formar vilka vi är genom kultur. Tillsammans  skapar vi ett allt rikare gemensamt arv av berättelser,  reflektioner och uttryck, som ger oss nya sätt att möta den  värld vi lever i. Genom varandras erfarenheter utvecklar vi  våra egna drivkrafter och perspektiv. Kulturen kan vara en  stark frigörande kraft som öppnar nya möjligheter. Det är just  därför konservativa politiska krafter försöker kontrollera och  begränsa den.   

Vi vill se gott om utrymmen för kulturellt skapande, som når alla i  samhället och står fria från både politisk och affärsmässig styrning.  Kulturpolitiken behöver skapa goda ekonomiska villkor för såväl  institutioner som den fria kulturen, för såväl traditionella som nya  kulturformer.   

Idrott och friluftsliv spelar stor roll för många människors glädje,  välmående och sociala sammanhang. För att deltagandet ska vara  öppet för alla behöver dess organisationer vara förankrade i  föreningsdemokrati och stå starka mot marknadsintressen. En god  samhällsplanering som ser till att människor har nära till  idrottsanläggningar och naturupplevelser gör också stor skillnad.  

Det behövs en mångfald av kanaler för information, nyheter,  åsiktsbrytning och opinionsbildning som inte drivs av vare sig  stater eller kapitalintressen. Vi vill se en stark, bred och självständig  roll för public service, med ett uppdrag som utgår från grundlagen  men står oberoende från den politiska makten, och heltäckande  journalistisk bevakning på nationell, regional och lokal nivå.  

### Alla människors lika värde

De allra flesta människor bryter på ett eller annat sätt mot någon av  alla gamla och nya normer för hur vi förväntas vara. Vi har allt  att vinna på att tillsammans försvara varandras möjligheter att leva  goda liv varje gång konservativa krafter försöker stänga någon ute,  oavsett om det just den här gången handlar om sjukskrivna,  muslimer eller transpersoner.   

Sverige ska vara ett land med en god förståelse för hur olika vi är  och en stor öppenhet för att människor vill leva på olika sätt. Med  social och ekonomisk trygghet kan vi se till att alla har goda  möjligheter att leva fria liv. Det kräver ett aktivt arbete med att  förebygga och hindra olika sorters diskriminering i samhället.  

Arbetet för alla människors rätt att leva goda liv oberoende av  hur ens kropp och sinne fungerar behöver fortsätta. Bristande  tillgänglighet och anpassning leder fortfarande till fattigdom och  utestängning. Människor med normbrytande funktionalitet har  idag som grupp sämre hälsa och högre arbetslöshet.  

Den som har en funktionsnedsättning ska kunna vara delaktig i  samhället på samma villkor som alla andra och leva ett gott,  självständigt liv. Det handlar om allt ifrån att ha arbete och  försörjning, vara med i ett fotbollslag och använda sin rösträtt  till att ta en fika på stan. Alla barn ska växa upp med det stöd  som behövs för att klara skolan och hitta fram i livet.  

Vi vill skapa ett mer inkluderande samhälle genom att stärka  välfärden och göra alla delar av samhället tillgängliga. LSSinsatser och personlig assistans behöver utvecklas så att de på  riktigt täcker behoven. Vi vill att samhället särskilt  uppmärksammar övergrepp mot kvinnor med normbrytande  funktionalitet. Funktionshinder är ett perspektiv som ska finnas  med i alla politikområden.  

Utvecklingen mot starkare rättigheter för barn är en av de  progressiva rörelsernas största framgångar. Vi vill fortsätta arbetet  med att förverkliga barnkonventionen och principen att i varje läge  se till barnets bästa. Vi har ett starkt förtroende för unga  människors förmåga att fatta självständiga beslut om sina liv.  

Sexualupplysningen spelar en viktig roll i att bygga upp den  kunskap som behövs för att fritt kunna utveckla sin identitet, sin  sexualitet och sina relationer. Vi vill fortsätta att gå mot en  lagstiftning som utgår från hur människor faktiskt lever, med en  modern familjerätt. Vi vill investera brett i vården så att den håller  hög kvalitet i alla delar. Starka sexuella och reproduktiva  rättigheter är avgörande för människors kroppsliga autonomi och  makt över sina liv. Aborträtten är en viktig historisk seger för den  feministiska rörelsen som behöver försvaras. Vi vill därför skriva  in den i grundlagen.   

Vi ser det som avgörande att stärka hbtqia+-personers  rättigheter i en tid där auktoritära högerkrafterna försöker  angripa dem. Det handlar bland annat om att ge  regnbågsfamiljer ett gott rättsligt skydd. Vi vill också se en  könstillhörighetslag som baseras på självbestämmande och  införa ett tredje juridiskt kön. För den som tvingats fly hit på  grund av förföljelser för sin sexuella läggning eller  könsidentitet ska Sverige vara ett tryggt land.   

Kvinnors kroppar har både historiskt och i nutid använts som  handelsvaror, genom sexköp, porrindustrin och surrogatmödraskap.  Det är en bärande del i kvinnors frigörelse och rätt till sina kroppar  att den handeln upphör.  

Den svenska staten har genom historien utsatt urfolk och  nationella minoriteter för allvarliga oförrätter. Antisemitiskt  tankegods och andra rasistiska föreställningar var länge uttryckliga  delar av myndigheters syn på människor. Exploateringen av  norra Sverige är en del av en lång kolonial historia. Sverige  har en särskild uppgift att stärka urfolks och de nationella  minoriteternas rättigheter till identitet, språk, kulturarv och religion  som behöver ses i ljuset av den historien. Det kräver också en  särskild medvetenhet om markanvändningens betydelse.  

Sverige behöver överhuvudtaget bygga upp och sprida kunskap  om hur dagens rasistiska strukturer ser ut. Det behövs en mer  utbredd förståelse för hur olika sorters rasism kan se ut för den som  till exempel är afrosvensk eller muslim, och den historiska  bakgrunden till det. Myndigheter har ett särskilt ansvar att möta  alla människor med samma respekt.   

Sverige är ett land med goda förutsättningar att låta fler  människor bygga goda liv här. Vi vill se en asylpolitik grundad i  respekt för internationella konventioner och deras andemening,  som ger människor en fristad undan krig och förtryck. Här ska  den som söker asyl garanteras sin rätt till en individuell och  rättssäker prövning under värdiga omständigheter.  Kriterierna för asyl i Sverige ska vara solidariska och leda till  att färre människor lever i den juridiska utsatthet som oseriösa  företag kan exploatera. Permanenta uppehållstillstånd som  huvudregel och möjligheter till familjeåterförening är  grundläggande för att människor ska kunna börja nya liv i ett  nytt land.   

För att det ska fungera praktiskt behövs också ett starkt välfärdssamhälle som ger alla språkkunskaper och praktiska möjligheter att  arbeta och leva på samma trygga villkor som andra. Det står i skarp  konflikt med kapitalets intresse av att det bor människor i Sverige  som saknar grundläggande trygghet och därför kan användas för att  sätta press på löner och arbetsvillkor.   

### Brottsbekämpning

Ingen människa ska behöva utsättas för brott. Samhället behöver  kunna agera samlat, effektivt och rättssäkert för att förhindra brott  och ge den som utsatts stöd och upprättelse. Goda resurser till  polis, socialtjänst och andra aktörer är viktiga i det arbetet.  

Vägen till ett samhälle med låg brottslighet går genom en  genomtänkt människosyn, där vi bygger upp stabila sammanhang  som faktiskt förmår bryta destruktiva levnadsbanor. Starka  lokalsamhällen, välfungerande skolor, meningsfull fritid och full  sysselsättning är avgörande för att varje människa ska hitta  konstruktiva sammanhang att vara i. Straff och andra påföljder för  brott behöver vara noggrant utformade för att faktiskt leda till lägre  brottslighet och inte förvärra problem.  

Vi vill se ett genomgripande arbete mot våldsbrottslighet, som  utgår från forskningen. Genom att sänka acceptansen för social  press och våld kan vi bygga ett samhälle där alla människor växer  upp med en stark känsla för sin egen och andras integritet. Vi vill  fortsätta det historiska skiftet mot en samtyckeskultur, bort från  förväntningar om att kunna kontrollera sin partner eller sina  närstående. Materiell trygghet i form av till exempel ekonomisk  självständighet och bostäder betyder också mycket för människors  möjligheter att ta sig ur destruktiva sammanhang.   

Arbetet mot det patriarkala våldet behöver bygga på en  förståelse för de maktmönster som riskerar att gå över i  psykiskt, fysiskt, sexuellt och ekonomiskt våld. Det går att  förebygga med ett riktat arbete mot de olika uttryck det tar sig, som  sexköp och hedersförtryck. Kvinno- och tjejjourerna gör ett viktigt,  självständigt arbete som staten och kommunerna behöver lära av  och stödja.   

Ekonomisk brottslighet i företag leder till allvarliga konsekvenser,  bland annat i form av farlig arbetsmiljö och miljöförstöring. Det  fifflet leder till att seriösa företag slås ut och skatteintäkter  uteblir. Det handlar i hög utsträckning om överlagda beslut, där  risken för upptäckt kan spela stor roll. Vi vill gå mot en större  samhällelig insyn i företag där det behövs för att stävja fusk med  till exempel skatter och anställningar. Det behövs en effektiv  uppföljning av den organiserade ekonomiska brottsligheten för att  förhindra att den också börjar ta sig uttryck i till exempel  korruption eller våld.   

### Internationella relationer

Vi vill se ett robust samhälle som är byggt för att klara olika sorters  kriser. Investeringar i samhällets kärnverksamheter, gemensamt  ägande i nyckelsektorer och medvetna strategier för landets  ekonomiska utveckling är viktiga delar av det. Sverige har goda  möjligheter att bli mer självförsörjande på energi och livsmedel.  

Vi vill se ett modernt totalförsvar byggt på allmän värnplikt, med  en bred förmåga att möta olika former av kriser Sverige kan ställas  inför. Sveriges försvarsmakt behöver kapacitet att delta i  fredsbevarande insatser med FN-mandat.  

Världen behöver gå mot fred, nedrustning och färre vapen i  cirkulation. Det behövs en politisk medvetenhet om hur både  vinstintressen och stormaktsintressen drar åt helt andra håll. Vi vill  se ett starkt statligt ägande i vapenproduktionen för att  säkerhetspolitiska nyckelbeslut ska fattas med ett  helhetsperspektiv. Vi vill att Sverige ska vara ett land med höga  ambitioner i det internationella arbetet för en hållbar, fredlig  och demokratisk utveckling.  Vi arbetar också för  att förbjuda vapenexport till diktaturer, stater som för  angreppskrig och regimer som begår brott mot de mänskliga  rättigheterna.   

Det är viktigt att Sverige kan agera självständigt från stormakter  som USA, Kina och Ryssland för att inte dras in i geopolitiska  konflikter utan tvärtom kunna verka för att förebygga dem. Vi ser  grundläggande problem med Natos uppbyggnad. Vi arbetar  för att Sverige ska vara en motvikt till det, som istället bygger  upp bättre, defensiva samarbeten, med sikte på att lämna Nato.  Sverige behöver vara fritt från främmande militärbaser på eget  territorium. Vänsterpartiet står för fred och deeskalering. Vi  vänder oss emot militarisering och kapprustning. Vi vill se ett  förbud mot kärnvapen på svensk mark och arbetar för en  värld helt fri från kärnvapen.   

Vår utgångspunkt är att politiska konflikter i andra länder är  komplexa historiska processer, särskilt när de tar sig väpnade  former. Sverige kan ofta göra störst skillnad genom att konsekvent  försvara internationell rätt, mänskliga rättigheter och gemensamma  ekonomiska insatser.  

Människor världen över har gemensamma intressen av ekonomisk  och demokratisk frigörelse. Det syns till exempel i arbetet för att  säkra generiska läkemedel eller mot handelsavtal som inskränker  utrymmet för demokratiska beslut. Vi vill att Sverige verkar för  skuldavskrivning, solidariskt bistånd och rättvisa handelsvillkor.  Sverige behöver utveckla ett konstruktivt samarbete med andra  länder i klimatomställningen. Vi vill att Sverige ska spela en  pådrivande roll för långtgående internationella klimatavtal, som  både underlättar omställningen och ställer krav på takten.  Tekniköverföring och klimattullar är viktiga verktyg för det. De  länder som historiskt stått för de största utsläppen har ett särskilt  ansvar att ta. Omställningen behöver gå hand i hand med att ta  världen ur fattigdom.  

Väl fungerande internationella samarbeten behöver byggas med  den nationella självbestämmanderätten som grund. Alla länder och  regioner ska ges möjlighet att kontrollera sin produktion, sina  finansiella flöden och sina naturresurser. Folkrätten behöver  försvaras och fördjupas. På områden där det finns specifika behov  av bindande samarbeten är mellanstatligt överenskomna  minimiregler vanligtvis en bättre väg att gå än överstatliga beslut.  

Vi vill se ett tätt samarbete mellan de nordiska länderna och ett gott  samarbete mellan de europeiska länderna. Det är särskilt viktigt på  områden som klimat, miljö, energi, transporter och krishantering.  Genom att agera tillsammans ökar också ländernas möjligheter att  reglera finansiella flöden och internationella storföretag. Den  europeiska konventionen om skydd för de mänskliga rättigheterna  och de grundläggande friheterna är en bra grund för samarbete.  

Vi är däremot kritiska till EU:s odemokratiska struktur, som får  allvarliga konsekvenser för många länder både i och utanför EU.  Kommissionens och centralbankens grundläggande politiska  inriktning ligger utanför medborgarnas kontroll och det saknas  tydliga sätt att hålla någon ansvarig för fattade beslut.  Beslutsprocesserna sker till stor del utan demokratisk insyn, men i  tät samverkan med lobbyister. Det är en struktur som byggts upp  för att svara mot kapitalets intressen snarare än människors.  Bland annat därför måste Sverige fortsatt stå fritt från  valutaunionen EMU.   

EU-rätten innehåller orimliga begränsningar av medlemsländernas  rätt att genomföra den politik de vill, även när besluten har fattats  demokratiskt. Så länge de villkoren består utgör EU ett hinder för  genuint progressiv förändring. Det gör att Vänsterpartiet behöver  hålla fast vid alternativet att Sverige kan lämna EU. Ett utträde är  inget självändamål, men kan visa sig nödvändigt för att bygga ett  bättre samhälle i en situation där EU ställer sig i vägen för det. Det  kan också vara ett nödvändigt självförsvar mot angrepp på  grundläggande delar av det samhälle vi byggt upp, till exempel i  frågor om välfärd, arbetsrätt eller demokrati. Vi ser att det skulle  väcka komplexa frågor om bland annat handelsvillkor och  långsiktiga samarbetsformer.  

Samtidigt kan den sortens konflikter också sätta tryck på EUstrukturerna att hålla sig till en mer demokratiskt legitim roll. Trots  EU:s långtgående försök att likrikta medlemsländerna ser vi att  medlemskapet i själva verket ser väldigt olika ut, med en lång rad  formellt och informellt accepterade undantag. Vi ser en lösare  anslutning till EU, med folkligt förankrade undantag, som en  möjlig väg framåt.  

De framsteg som trots förutsättningarna gjorts inom ramen för EU  är viktiga att försvara och bygga vidare på. Det handlar bland annat  om utbyte mellan länder och vissa klimat- och miljöregleringar.  Ibland uppstår komplexa situationer där en kortsiktig framgång  riskerar att leda till långsiktiga problem, för att frågor börjar  avgöras i EU istället för i ländernas långt mer demokratiska  processer. Vi agerar pragmatiskt för att både förändra inom ramen  för EU:s strukturer och utmana dem, beroende på situationen.  

FN:s ställning i världen är av central betydelse för små länder  som Sverige. Genom FN får även små länder en röst och starka  militärmakters brott mot internationell lag kan  uppmärksammas och motverkas. Vi vill därför att Sverige  verkar för att FN ska spela en större roll i världspolitiken. Vi  vill se en demokratisering av FN:s strukturer som bryter  stormakternas dominerande ställning och istället stärker  folkrätten.   

### Vänsterpartiet

Vänsterpartiet är en organisation där vi tillsammans agerar och gör  skillnad i samhället. Vi arbetar för sammanhållning och frigörelse i  rörelser och i parlamenten, i bostadsområden och på arbetsplatser, i  hela Sverige från norr till söder.  

Vi inspireras av de marxistiska och feministiska idétraditionerna  när vi utvecklar vårt praktiska arbete och våra politiska förslag.  Vi är beredda att lyssna på forskningen och förena de insikterna  med politisk handling. Vi vill se ett brett, demokratiskt och aktivt  ledarskap på alla nivåer i partiet, som utgår från de politiska  situationer vi är i och tar fram strategier för att svara på dem.  

Vi bygger en organisation som är medveten om utmaningarna för  en genuin demokratisering. Den politiska makten behöver en stadig  grund i människors livsvillkor. Vi vill bryta upp de gamla  strukturer som fortfarande håller tillbaka många från att delta på  jämlika villkor i det politiska livet.  

En av våra styrkor är att människor med olika erfarenheter,  perspektiv, förslag, idéer och viljor möts hos oss. Det ger insikter  som betyder mycket för att skapa ett politiskt projekt som kan  samla ett bredare stöd. Därför är det viktigt för oss med en stark  förtroendekultur, där alla känner sig trygga att dela med sig av hur  de tänker.  

Vi vill skapa en varm och välkomnande organisation med ett brett  förtroende både i städerna och på landsbygden, i förorterna och i  bruksorterna. Vi behöver människor från alla håll i samhället som  agerar för politisk förändring. Vi strävar efter en politisk roll som  öppnar för fler att känna sig hemma i Vänsterpartiet.  

Att vara med i Vänsterpartiet handlar om att arbeta tillsammans för  ett bättre samhälle, som bygger på solidaritet, trygghet, frihet och  jämlikhet. Du är varmt välkommen!   

