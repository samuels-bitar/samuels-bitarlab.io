---
title: Lokal klimat- och miljöpolitik av Vänsterpartiet
image: malmo.jpg
description: Några korta exempel på klimat- och miljöpolitik av Vänsterpartiet
date: 2022-07-24 18:00
---
Vänsterpartiet har en ordentligt utarbetad klimat- och miljöpolitik. På riksplan så har Vänsterpartiet drivit socialistisk omställningspolitik länge. Några korta exempel:

* Vänsterpartiets [eko-eko-program](https://www.vansterpartiet.se/wp-content/uploads/2022/07/ekoeko-punktprogram-161013.pdf) 
* Vänsterpartiets [partiprogram](https://www.vansterpartiet.se/resursbank/partiprogram/)
* Vänsterpartiets [valplattform](https://www.vansterpartiet.se/wp-content/uploads/2022/07/valplattform-2022.pdf)
* Riksdagsmotionen [Klimatkrisen](https://riksdagen.se/sv/dokument-lagar/dokument/motion/klimatkrisen_H9023277/html)
* Riksdagsmotionen [Biologisk mångfald](https://riksdagen.se/sv/dokument-lagar/dokument/motion/biologisk-mangfald_H9023279/html)
* Riksdagsmotionen [Transporteffektivt och hållbart samhälle för framtiden](https://www.riksdagen.se/sv/dokument-lagar/dokument/motion/transporteffektivt-och-hallbart-samhalle-for_H9023203/html)
* Förslag på [klimatinvesteringar på 700 miljarder kronor](https://www.vansterpartiet.se/nyheter/700-miljarder-i-investeringar-for-klimatet/) genom ett 10-årigt moratorium för dagens överskottsmål

Men Vänsterpartiet driver också klimat- och miljöpolitik stenhårt lokalt. Partiets klimat- och miljöpolitiska nätverk har sammanställt en [lista på lokala program](https://klimatochmiljo.vansterpartiet.se/lista-pa-program/) och en [lista på lokala motioner](https://klimatochmiljo.vansterpartiet.se/lista-pa-motioner/).

Jag lyfter här fram lite olika exempel på lokal klimat- och miljöpolitik.

## Vänsterpartiet Sundbyberg

Vänsterpartiet Sundbyberg har under året tagit fram ett [miljömanifest](https://sundbyberg.vansterpartiet.se/wp/files/2022/03/Miljomanifest-31-mars-2022.pdf) på 18 sidor.

![Miljömanifest av Vänsterpartiet Sundbyberg](/images/sundbyberg-miljomanifest.jpg)

Så här skriver de på sin hemsida om de viktigaste sakerna:

**Utlys klimatnödläge!**
Inför en koldioxidbudget för kommunen, som alla beslut får förhålla sig till.

**Starta ett kommunalt elbolag**
Ett eget elbolag ska sälja förnybar el och låta bygga solceller och utveckla energilagring.

**Hållbar värme**
Norrenergi ska utveckla kollagrings-tekniker och inte kunna sälja utsläppsrätter

**Ställ krav vid byggnation**
Bygg trähus framför betong. Byggmaterial ska vara giftfria och så cirkulära som möjligt. Renovera när det behövs för energieffektivitet, reparera hellre än bygg nytt.
Underlätta hållbar livsstil

**Cirkulär ekonomi**
Vi vill se fler returpunkter för återbruk och reparation, verktygsbibliotek i kvarteren och en fritidsbank för gratis utlåning av sport- och friluftsutrustning.

**Klimatsmart resande**
Det måste byggas fler laddstolpar och stadsplanering ska förenkla för cyklister och fotgängare.

**Krav vid upphandling**
Miljöpåverkan och sociala villkor ska beaktas för varors/ tjänsters hela livscykel.

**Lätt att odla och vara i naturen**
Vi vill skydda Kymlinge, Rissne ängar, Golfängarna och utöka Igelbäckens naturreservat.
Satsa på ett nytt växthus, mer blomster, odlingslotter och en
park med ätbara växter för alla. Så gynnar vi biologisk mångfald.

**Hållbar mat**
Servera mer vegetabilisk, ekologisk, närodlad mat i alla verksamheter. Handlingsplan för minskat matsvinn.

[Vänsterpartiet Sundbybergs facebook-sida](https://www.facebook.com/vansterpartietsundbyberg)

## Vänsterpartiet Malmö

Vänsterpartiet Malmö har länge drivit klimat och miljö aktivt. I deras [motionsbank](https://malmo.vansterpartiet.se/malmovansterns-kommunalpolitik/motioner-i-kommunfullmaktige/) så kan vi läsa följande kimat- och miljömotioner bara under år 2021:

* [Utöka antalet cykelservicestationer](http://www.vmalmo.se/wp-content/uploads/2021/12/2112-Uto%CC%88ka-antalet-cykelservicestationer.pdf)
* [Fler cykelparkeringar vid Folkets park](http://www.vmalmo.se/wp-content/uploads/2021/10/2110-Fler-cykelparkeringar-vid-Folkets-park.pdf)
* [Cykelbanor och träd istället för parkerade bilar](http://www.vmalmo.se/wp-content/uploads/2021/10/2110-Cykelbanor-och-tr%C3%A4d-ist%C3%A4llet-f%C3%B6r-parkerade-bilar.pdf)
* [Fossilnedrustning](http://www.vmalmo.se/wp-content/uploads/2021/09/2110-Fossilnedrustning.pdf)
* [Inför en cirkulationsplan för Malmö](http://www.vmalmo.se/wp-content/uploads/2021/08/2108-Inf%C3%B6r-en-Cirkulationsplan-f%C3%B6r-Malm%C3%B6.pdf)
* [Koldioxidbudget](http://www.vmalmo.se/wp-content/uploads/2021/05/2105-Koldioxidbudget.pdf)
* [Gör Malmös innerstad bilfri](http://www.vmalmo.se/wp-content/uploads/2021/02/21-02-G%C3%B6r-Malm%C3%B6s-innerstad-bilfri.pdf)

![Facebook-bild: Malmö stad måste minska utsläppen](/images/v-malmo-klimat.jpg)

Inför valet har Vänsterpartiet Malmö samlat sin klimatpolitik under sidan [Omställning till ett grönt hållbart Malmö](https://malmo.vansterpartiet.se/malmovansterns-kommunalpolitik/kommunalpolitiskt-program/omstallning-till-ett-gront-hallbart-malmo/) där de delar in politiken i följande delar:

* Färre bilar och flyg, fler cyklar
* Plats för människor, bättre transporter
* Godstrafik
* Effektivisering av trafiken
* Avgiftsfri kollektivtrafik
* Malmö är en cykelstad
* Ett Malmö som leder energiomställningen 
* Konsumtion och återvinning 
* Återbruk och kollaborativ ekonomi 
* Hållbar mat
* Hållbar stadsutveckling
* En grönare stad
* Forskningsstöd

Under varje del så redogör de kort för politiken och med en länk för att kunna läsa ännu mer fördjupat.

[Vänsterpartiet Malmös facebook-sida](https://www.facebook.com/Vansterpartietmalmo)

## Vänsterpartiet Solna

Vänsterpartiet Solna är ännu ett exempel på en förening som är aktiva i klimat- och miljöpolitiken.

De driver på för [50 miljoner kronor årligen till klimatomställningen](https://solna.vansterpartiet.se/vi-lovar-50-miljoner-till-klimatomstallning/):

![Facebook-bild: (V)i lovar: 50 miljoner kronor till klimatomställningen](/images/v-solna-klimat.jpg)

Andra exempel på motioner de lagt:

* [Klimatkonsekvensbeskrivning](https://solna.vansterpartiet.se/files/2021/05/05.-Motion-om-klimatkonsekvensbeskrivning.pdf)
* [Halvera matsvinn inom offentlig verksamhet till 2025](https://solna.vansterpartiet.se/files/2019/04/Motion-om-att-halvera-matsvinn-inom-offentlig-verksamhet-till-2025.pdf)
* [Grönytefaktor](https://solna.vansterpartiet.se/files/2019/04/Motion-om-gr%C3%B6nytefaktor-GYF.pdf)
* [Halverad köttkonsumtion](https://solna.vansterpartiet.se/files/2019/04/Motion-om-halverad-k%C3%B6ttkonsumtion.pdf)

[Vänsterpartiet Solnas facebook-sida](https://www.facebook.com/VSolna)

## Vänsterpartiet i Halmstad

Vänsterpartiet i Halmstad har [flera klimat- och miljöförslag](https://halmstad.vansterpartiet.se/var-lokala-politik/politiska-omraden/miljo-och-klimat/):

* Andelen vegetarisk mat i kommunala verksamheter ska öka
* Solpaneler på kommunala byggnader och HFAB:s fastigheter
* Energibesparing som faktor vid markanvisningar och bygglov
* Inrätta en klimatanpassningsfond
* Nya riktlinjer för skötsel av kommunens skog
* Gynna anropsstyrd kollektivtrafik och samåkningspooler på landsbygden
* Satsa på bilparkeringar i stadens ytterområden där man lätt kan byta till buss eller cykel.
* Inrätta en kommunal fordonspool
* Avgiftsfri kollektivtrafik för unga upp till 19 år och alla pensionärer
* Avveckla det kommersiella flyget och bygg en ny grön stadsdel i stället

![Facebook-bild: Halmstad behöver högre klimatabitioner om vår del av Parisavtalet ska nås](/images/v-halmstad-klimat.jpg)

[Vänsterpartiet i Halmstads facebook-sida](https://www.facebook.com/VansterpartietHalmstad)

## Vänsterpartiet i Mariestad

Vänsterpartiet i Mariestad har också flera motioner. Här är några:

* [Utbildning om kommunalt miljö- och klimatarbete](https://mariestad.vansterpartiet.se/2022/04/26/motion-om-utbildning-om-kommunalt-miljo-och-klimatarbete/)
* [Ta fram en grönstrukturplan för Mariestads kommun](https://mariestad.vansterpartiet.se/2022/03/31/motion-om-att-ta-fram-en-gronstrukturplan-for-mariestads-kommun/)
* [Införa ett miljöledningssystem](https://mariestad.vansterpartiet.se/2021/12/12/motion-om-att-infora-ett-miljoledningssystem/)
* [Stadsodling](https://mariestad.vansterpartiet.se/2021/12/03/motion-om-stadsodling-26-11-21/)
* [Göra Kungsgatan bilfri](https://mariestad.vansterpartiet.se/2021/03/15/350/)
* [Handlingsplan för fler bilfria områden](https://mariestad.vansterpartiet.se/2021/11/14/motion-om-handlingsplan-for-fler-bilfria-omraden/)

![Facebook-bild: Att skydda och återskapa våra ekosystem är det snabbastes sättet att få bort koldioxid ur atmosfären](/images/v-mariestad-klimat.jpg)

Föreningen har också drivit på mycket för skydd av skog och ekosystem. [Den här insändaren](https://www.mariestadstidningen.se/2021/07/12/maste-vidga-vara-vyer-for-ekosystemen/) är ett bra exempel.

De har också lagt en motion om att [kartlägga och restaurera torrlagda våtmarker](https://mariestad.vansterpartiet.se/2022/06/21/motion-om-utbildning-om-kommunalt-miljo-och-klimatarbete-2/).

[Vänsterpartiet i Mariestads facebook-sida](https://www.facebook.com/VansterpartietiMariestad)

## Vänsterpartiet Västerås

Några exempel på motioner som Vänsterpartiet Västerås har lagt:

* [Klimatsmart mat som norm](https://vasteras.vansterpartiet.se/2020/02/11/klimatsmart-mat-som-norm/)
* [Ge Västerås en ätbar park](https://vasteras.vansterpartiet.se/2019/04/08/ge-vasteras-en-atbarpark/)
* [Utlys klimatnödläge i Västerås](https://vasteras.vansterpartiet.se/2019/10/02/utlys-klimatnodlage-i-vasteras/)
* [Vatten till västeråsarna](https://vasteras.vansterpartiet.se/vatten-till-vasterasarna/)
* [Cykelservicestation](https://vasteras.vansterpartiet.se/2018/06/07/cykelservicestation/)

Föreningen har också agerat för att årrskortet på kollekivtrafiken i Västerås ska bli billigare. De kallar satsningen "busstian":

![Facebook-bild: Busstian - Att ta bussen i Västerås ska inte kosta mer än tio kronor per dag](/images/v-vasteras-busstian.jpg)

[Vänsterpartiet Västerås facebook-sida](https://www.facebook.com/vvasteras)

## Vänsterpartiet i Stockholms stad

Vänsterpartiet i Stockholm stad har länge arbetat för att Stockholm ska bli en cykelstad. Här är [några förslag](https://stockholmsstad.vansterpartiet.se/2021/04/20/storstadernas-cykelboom-maste-bli-permanent-trend/) kring det:

* Stockholms stad ska jobba för att minska biltrafiken med 30 procent från 2017 års nivå.
* Fler gator ska göras om till permanenta gångfartsgator, både i innerstan och i förorterna.
* Bilfria zoner ska införas.
* Gång- och cykelinvesteringar bör göras på snabba och säkra cykelvägar med utrymme för omkörning såväl som säkra och framkomliga gångbanor samt en tydlig separation dem emellan.

![Facebook-bild: Stockholm ska vara en cykelstad](/images/v-stockholm-cykel.jpg)

Några exempel på motioner som man lagt:

* [Anläggning för storskalig biokolsproduktion](https://stockholmsstad.vansterpartiet.se/files/2019/09/Motion-biokol.pdf)
* [Utredning av investering i vindkraftverk](https://stockholmsstad.vansterpartiet.se/files/2019/09/Motion-Vindkraftverk.pdf)
* [Ökad klimatnytta genom en gemensam driftcentral för stadens fastighete](https://stockholmsstad.vansterpartiet.se/files/2020/03/Motion-driftcentral.pdf)
* [Anslutning till Allmännyttans klimatinitiativ](https://stockholmsstad.vansterpartiet.se/files/2019/09/Motion-om-allm%C3%A4nnyttans-klimatinitiativ.pdf)

Man har också lagt fram flera bra förslag på att [investera i naturreservat](https://stockholmsstad.vansterpartiet.se/2021/05/19/att-investera-i-naturreservat-ar-att-investera-i-folkhalsa-och-grona-jobb/) vilket gynnar hälsa, klimat och sysselsättning.

[Vänsterpartiet Stockholms stad facebook-sida](https://www.facebook.com/stockholmsvanstern)

## Och många fler...

Det här var bara ett litet axplock från några föreningar i Vänsterpartiet. Om du själv har ansvar för hemsidan för din partiförening så lägg gärna till en klimatdel på hemsidan där man kan se vad ni driver för politik.
