---
title: Rösta för socialistisk klimatpolitik och ockupera sen industrin
image: ende-gelande-road.jpg
image_description: Aktivister på väg att ockupera en kolgruva i massaktionen Ende Gelände av 350.org/Tim Wagner från <a href="https://flickr.com/photos/133937251@N05/26960987736/in/photolist-GHdLEm-GeCm2h-xmnHHW-wrF9c7-wrPeMP-x75ry3-GHdMdq-x74WWo-GZ58bf-H2mQFe-xpdgXy-xnUjeQ-xmnjFJ-H5s5rs-Gd34yk-GHdPhf-H5s7m9-H8p2L4-H8p2iv-xmo2Cy-H2mPTH-xnUcZf-GHdNWA-x75Tn5-Gd34hD-GZ55HQ-GZ553b-Ha8xUR-H219v4-GcFJpD-x7aJKg-H1Pj9f-H7bitE-wrDSHo-H7biQm-H46uii-xoF4Pa-xoE5eF-H46u26-xnGohY-wrQtRD-x75SQA-GZ55sQ-xgczNm-GHdNyS-xgeQsN-xggdB3-yo282r-xgj1px-xwZH5W/">Flickr</a>
description: Använd din röst i EU-valet och fortsätt sedan med att delta på Take Concrete Action 15-19:e juni
date: 2024-05-25 20:00
---

Klimatkrisen är akut. Vi behöver göra vad vi kan för att ställa om samhället. Varje tiondels grad upphettning som förhindras kommer göra stor skillnad.

Det är glädjande att se att klimatrörelsen blir tydligt mer systemkritisk och antikapitalistisk. Solidariteten mellan människor i olika länder är stark och solidariten med arbetare som jobbar i klimatskadliga industrier är stark. Arbetare ska inte behöva välja mellan arbetslöshet eller en hel planet att lämna över till sina barn.

## Socialistisk klimatpolitik

Man behöver inte välja mellan parlamentariskt arbete och utomparlamentariskt arbete. Man kan göra bägge. Och att använda sin röst för i EU-valet är för mig en självklarhet och jag hoppas det är det för andra klimataktivister. Kapitalismen är den enskilt största faktorn till klimatkrisen och kapitalismen fortsätter att sätta kortsiktig vinst framför våra gemensamma intressen för ett hållbart klimat. Jonas Sjöstedt (V) förklarar det bra i en [debattartikel](https://www.etc.se/debatt/klimatfraagan-aer-en-konflikt-mellan-hoeger-och-vaenster):

> Kapitalismen kräver ständig expansion. Men tillväxten är blind, den består också av sådant som skadar människor och natur. Sociala strukturer och naturens komplicerade väv av samband slits sönder av exploateringen. Naturen med dess rikedomar ses som gratis, den har bara ett värde när den omsätts som vara. 
>
> Klimatkrisen kräver att kapitalismens ”fria marknad” sätts ur spel. Användandet av fossila bränslen måste fasas ut. Det krävs statsstöd för att gynna förnybara alternativ. Ekonomi och konsumtion behöver mer långsiktig planering. Människors konsumtionsmönster måste förändras. 
>
> Högern klarar inte av att ifrågasätta kapitalismen som produktionssätt med dess inbyggda maktordning, de vill inte begränsa privilegierna för den globala överklassen och de vill inte föreslå politiska lösningar som innebär ökad jämlikhet eller demokratiskt inflytande över produktionen. Men det kommer att krävas för att rädda klimatet. Det är områden av politiken där vänstern sedan länge formulerat politiska alternativ och genomfört konkreta förändringar.

Klimatkrisen är global och det behövs internationella överenskommelser för att lösa den. EU har gjort en del grejer som är bra, som minimiregler och målsättningar för att få ner utsläppen. Men EU försvårar en rättvis omställning eftersom dess grund är marknadsliberalt och vill inte göra upp med kapitalismen. 

Vänsterpartiet vill genomföra stora klimatinvesteringar i Sverige. Det handlar om att ställa om industrin, bygga ut järnvägen och energieffektivisera bostäder. Det handlar om att bygga ut elproduktionen och sänka priserna på ren el så att fossila bränslen fasas ut. Men EU har flera krav på avreglering och konkurrensutsättning av exempelvis kollektivtrafiken och bostadsmarknaden som står i vägen för många av de här viktiga klimatinvesteringarna. EU sätter alltså käppar i hjulet för en offensiv rättvis omställning, t.ex. genom att de säger nej till statsstöd som behövs. 

Vänsterpartiet [vill förändra detta](https://www.europaportalen.se/2024/02/eu-kandidaten-jonas-sjostedt-ny-eu-politik-kan-underlatta-sverige-att-bli-varldsledande-i). Hur mycket kan man egentligen lyckas göra i EU? Hur mycket ska vi förlita oss på EU för att vara en positiv kraft i klimatomställningen? Jag vet inte. Jag vet bara att tiden är knapp och det är värt att i alla fall gå och rösta i valet för det gör skillnad om gröna socialister kommer in eller om det kommer in liberaler och fascister som inte kommer utmana EU alls.

Vill du läsa mer om vad ett gäng vänsterpartister tycker om klimat- och miljöpolitik? Läs då "Vänsterns svar på klimatkrisen" som jag [bloggat om här](/vansterns-svar-pa-klimatkrisen/).

## Det är de rika som är problemet

Vi kommer alla påverkas i omställningen. Men det är de rika som står för mest utsläpp och måste ställa om mest. Oxfam har i rapporten [Extreme carbon inequality](https://www.oxfam.se/blog/extrem-ojamlikhet-vaxthusgasutslapp) visat att de rikaste 10 procenten i världen står för nästan hälften av de konsumtionsbaserade utsläppen som är kopplat till livsstil:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam.jpg)

Sverige följer samma mönster inom landet i stort. I rapporten [Svensk klimatojämlikhet](https://www.oxfam.se/svenskklimatojamlikhet) så går Oxfam igenom hur det ser ut i Sverige. Där ser man att de allra rikaste har mycket högre utsläpp:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam-sverige-capita.jpg)

Och det är tydligt att om vi ska nå klimatmålen så är det de allra rikaste som måste ställa om mest. Även vanligt folk i medelklass och arbetarklass kommer att behöva ställa om men inte alls i samma utsträckning:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam-sverige-minskningar.jpg)

Men även om alla kommer att ställa om så behöver det inte vara något jobbigt, tvärtom! Det handlar om att det ska vara billigare och smidigare att ta tåget än flyget och att ta kollektivtrafiken eller cykeln istället för bilen. Med en rättvis omställning så gör vi omställningen tillsammans och samhället blir bättre.

## Naturskyddsföreningens granskningar

Naturskyddsföreningen har [granskat hur partierna agerat i EU](https://www.naturskyddsforeningen.se/artiklar/fran-uselt-till-utmarkt-miljobetyg-pa-sveriges-eu-parlamentariker/) gällande klimat och miljö. Så här blev resultatet:

![Prispall](/images/prispall.jpg)

Naturskyddsföreningen har också frågat partierna om vad de [tänkt att göra i EU](https://www.naturskyddsforeningen.se/artiklar/eu-val-2024-sa-mycket-lovar-partierna-for-miljon/). De lyfter 20 viktiga miljöpolitiska förslag och så får partierna svara om de har tänkt att jobba för det: ja, nej, delvis eller vet inte. Förslagen var:

1. En ambitiös politik för klimat och natur i EU 2024–2029
2. Ett utsläppsfritt EU senast 2040
3. Öka EU:s mål för kolsänkan 
4. Halvera energianvändningen till 2040  
5. Påskynda omställningen av finansiella flöden  
6. Minska flygets klimatpåverkan
7. Kraftfulla miljöåtgärder för jordbruket inom CAP 
8. Ett hållbart livsmedelssystem i EU
9. Ett gemensamt ramverk för skogsövervakning
10. Strikt skydd av EU:s ur- och naturskogar
11. Bindande krav för skydd av natur
12. Bibehåll skyddet för rovdjur, fåglar och andra arter i EU
13. Driv på för en havsgiv
14. Inför bindande mål för att minska EU:s materialfotavtryck
15. Minska användningen av skadliga bekämpningsmedel
16. Inför krav på innehållsförteckning av kemikalier
17. En starkare kemikalielagstiftning
18. Förbjud PFAS
19. Stoppa exporten av kemikalier som är förbjudna inom EU
20. Strategi för civilsamhället

Då blev svaret så här:

![Rankning kring löften](/images/rankning.jpg)

Det är bara på en punkt där Vänsterpartiet hade svarat "delvis" istället för bara "ja" och det var gällande "4. Halvera energianvändningen till 2040". Vänsterpartiet hade skrivit kommentaren:

> Vänsterpartiet ställer sig fullt ut bakom ambitionerna i EU:s
energieffektiviseringsdirektiv, inom vilken ram det föreslagna målet ligger, och ser det som
nödvändigt att kraftfullt öka ansträngningarna för ökad energieffektivitet. Vänsterpartiet
verkar också för en kraftig ökning av förnybar elproduktion för att stärka elektrifiering inom
transportsektorn och klara industrins klimatomställning inom EU.

Miljöpartiet hade skrivit kommentaren:

> Miljöpartiet ser energieffektivisering som avgörande för en rättvis
klimatomställning, där vi kan och måste göra mycket mer. Genom bland annat elektrifiering
och effektivare värmesystem kan vi snabbt få ner våra utsläpp och byta från ett ineffektivt och
fossilt energisystem till ett stabilt, 100% förnybart energisystem 2040.

Det är tydligt att det är Vänsterpartiet och Miljöpartiet som levererar när det kommer till klimat och miljö. Ur klimat- och miljöperspektiv så kvittar det nog lika kring många frågor vilket parti man väljer. Men om man är socialist så finns det nog andra frågor där man tycker det är en viktigt med en EU-kritisk socialist än en grön liberal.

## Take Concrete Action 15-19:e juni

Som vänsterpartist är det självklart att jag står bakom civil olydnadsaktioner som har gjorts och görs för klimatet. Civil olydnad är en självklar del i varje social rörelse. Detta är något som ännu en gång slagits fast i [Vänsterpartiets partiprogram](/vansterpartiet-partiprogram-klimat-miljo/):

> Vänsterpartiet arbetar för att förändra samhället genom breda folkrörelser och parlamentariska beslut. Demokrati är samtidigt mer än lagar och regler. Civil olydnad är en yttersta möjlighet för människor att försvara folkligt förankrade värden som mänskliga rättigheter och naturresurser. Den grundar sig på principer om icke-våld och öppenhet och är en viktig del av demokratin.

Jag har själv deltagit på flera stora civil olydnadsaktioner. Med [Ende Gelände](https://en.wikipedia.org/wiki/Ende_Gel%C3%A4nde) i Tyskland och [Folk mot fossilgas](https://www.youtube.com/watch?v=0FqG99vu3Ek&ab_channel=Fossilgasf%C3%A4llan) i Göteborg.

Jag blir glad av att läsa att gruppen Take Concrete Action (som tidigare utfört en [aktion mot Cementa](https://www.arbetaren.se/2022/12/13/take-concrete-action-vi-fordomer-cementa-domen/)) nu planerar en aktion mot delar av skogsindustrin 15-19:e juni. Take Concrete Action är också tydligt antikapitalistiska, målar ut [klimatkampen som en del av klasskampen](https://www.takeconcreteaction.info/) och riktar sin aktion mot industrin:

> Räddningen på klimatnödläget framställs ofta som en fråga om vår konsumtion. I någon mån är det naturligtvis sant, men det är en förflyttning av skuld från dem som gjort sig rika på försäljningen av kortlivat skräp till oss som tvingats köpa det. Engångsförpackningar, reklamfolders och papperskvitton är bara några exempel på saker som skulle göra större nytta i sin ursprungliga form: skogen. Medvetna konsumtionsval räcker inte, vi måste ställa om hela skiten.
>
> Vi anser att klimatpolitik i sin kärna är klasspolitik och att kampen om omställd produktion i slutändan är en fråga om ägandet av den. Alltså kommer solidariteten arbetare emellan att vara avgörande. Men vår makt går bortom makten på jobbet. Precis som att kollegor upptäcker sin makt när de ställer gemensamma krav eller tar en kollektiv konflikt, på samma sätt upptäcker vi vår makt i massaktionen.
>
> När hundratals, tusentals eller tiotusentals människor går ihop och bestämmer sig för att stänga ett cementbrott på Gotland, blockera väg för fascister eller tar natten tillbaka på internationella kvinnodagen, så upptäcker de sin kollektiva makt som massa. Att stoppa klimatkatastrofen kommer att kräva en massrörelse på gator, i kalkbrott och på mossklädda stigar.

Många organisationer är med på lägret i anslutning och har punkter i programmet:

![Program](/images/tca-program.jpg)

Kamraterna i Take Concrete Action har också en podd: TCA-podden. Man kan lyssna på avsnittet [direkt på soundcloud](https://soundcloud.com/take-concrete-action) eller ladda in deras [RSS-flöde](https://feeds.soundcloud.com/users/soundcloud:users:1388602662/sounds.rss) direkt in i sin poddspelare och lyssna där.

## Organisering!

Så slutligen, vad ska man göra för klimatet? Det finns mycket att göra. Mina snabbaste tips:

* Rösta för socialistisk klimatpolitik i EU-valet
* Delta på aktionen Take Concrete Action (eller delta bara på programpunkterna i lägret)
* Bli medlem i Jordens Vänner, Extinction Rebellion, Allt åt alla eller annan trevlig organisation
* Bli medlem i ett rödgrönt parti (helst Vänsterpartiet) och skriv motioner för tydlig socialistisk klimat- och miljöpolitik. Man behöver inte ta parlamentariska uppdrag bara för att man är aktiv i ett parti. Man kan engagera sig i klimat- och miljöfrågor i Vänsterpartiet t.ex. i det rikstäckande klimat- och miljönätverket [eller engagera sig lokalt](/lokal-klimatpolitik/)
* Lägg extra tid och energi på de aktviteterna du också tycker är roligt och meningsfullt

