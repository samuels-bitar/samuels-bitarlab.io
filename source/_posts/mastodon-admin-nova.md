---
title: Interview with Kris Nóva
image: servers_again.jpg
description: What is it like to be an admin of an instance with ~30 000 users
date: 2022-11-29 09:00
---

The federated social media network Mastodon has gained traction. With [more than 7 million users and more than 2400 instances](/mastodon-admins), people want to know more about this federated alternative to traditional centralized social media (such as Twitter).

I have previously blogged about Mastodon and given a [short introduction here](/lite-kort-om-mastodon/) and [interviewed Swedish admins here](/mastodon-admins/). When I searched for admins for Mastodon instances several persons directed me to Kris Nóva which runs the Hachyderm Mastodon instance. So I sent her some interview questions to get to know more about her work with the instance.

## What is your name? (Or handle if you would prefer)

Most folks will know me by my full name “Kris Nóva” or “Kris Nova” but everyone who knows me personally just refers to me as “nova”. I am the @nova account on Hachyderm.

![Kris Nóva](/images/kris.png)

## What instance are you running? What is the “profile” for that instance? For how long have you been running it? How many users does it have?

I am the primary adminstrator, owner, and decision maker behind [hachyderm.io](https://hachyderm.io) and our labratory and staging domain [hachyderm.wtf](https://hachyderm.wtf).

I work with a team of volunteers, friends, colleagues, family members, and companions to operate the server and moderate the content. Our servers have been serving traffic since April of 2022.

Today we are approaching 30,000 users on [hachyderm.io](http://hachyderm.io/) and have documented our growth extensively.


## For how long have you been on Mastodon

My relationship with Mastodon and the fediverse mirrors my relationship with hachyderm.io. I set the server up to give myself, my friends, and my family a safe place to prototype decentralized social media as we moved away from Twitter. I created my first account the day I turned on Hachyderm, and that has been my account since inception.

## What do you work with or do during your days?

I try very hard to create a hygenic and healthy boundary between my day job and my work with Hachyderm. I spend my days working as a Principal Engineer at GitHub, and I spend my nights and weekends streaming on Twitch and operating a number of free and open source projects including Hachyderm and my latest project: aurae. Aurae is on a mission to be the most loved and effective way of managing workloads on a single piece of hardware. Our hope is that by bringing a better set of controls to a node, we can unlock brilliant higher order distributed systems in the future.

As our numbers have grown the service has “bursted” more into my day time. I have done my best to balance the ongrowing needs of the server as we moved from 300 users to 30,000 users in less than a month. Having companion operators around the world has really helped make this possible.

## How come you started engaging with Mastodon on the level that made you host/administer an instance? Is it for ideological reasons? Social reasons? Or for the technical fun of it?

I started hosting Mastodon during one of my Twitch streams. Behind me on Twitch I have a dedicated server rack and testing lab that I use for my other open source projects. It was easy for me and my friends to turn the server on and serve traffic out of the data center in my basement.

The part we were not prepared for was the day that our homelab turned into production infrastructure. It has been interesting having to “undo” and “audit” a lot of our lab space just to keep the service online and up to speed with the demands of growth. At this point we will be building an entirely new instance of the infrastructure just to keep up! The homelab will return another day, the servers are full-time production hardware now.

## What does it mean to be an admin? How much time do you need to put on maintenance/support? How much money does it cost? Are you the only admin or are there others on the instance?

We are still figuring a lot of this out. Right now I am the owner of the service and we are receiving enough donations to keep up with growth. I am surprised at how willingly people began donating to Hachyderm.

We will eventually end up forming a legal entity to support Hachyderm and manage the money. I haven’t made any decisions on the exact implementation detail of how this will happen yet other than knowing that I will try to wait until 2023 for tax reasons in the US.

My top priority will always be keeping the service online, secure, and safe for our users and our community. We have a lot of decisions to make with regard to corporate accounts and our future as a legal entity. I have a lot of ideas though! Stay tuned.

As far as other admins go we have a group of 12 of us. Specifically Quintessence, Tani, Hazel, dma, Esk and Malte are the main ones that are keeping Hachyderm online today. They are the real miracle workers of the service. I just get on Twitch and tell people to join.


## This thing with different instances and rules, how does it work would you say?

Our instance, our rules. I assume total accountability and governance over our data which means I am doing everything in my power to give myself and the other moderators as much autonomy over our data as possible.

This is the thing that I would want to call out. I am currently leading 2 completely independent teams, each with about 12 volunteers on each. A single team specifically for infrastructure and keeping the services online, and another team specifically just for moderation. There is a LOT that goes into keeping Hachyderm functioning.

I stay out of moderation decisions, however I wrote the vast majority of our [community governance](https://github.com/hachyderm/community) myself which means I established our core set of rules myself. The rules came from my Twitch stream which also has the same rules. These have been working well for us there so far, however I know they aren’t perfect.

## How does it work with moderation? Are you doing that yourself? Or is there a whole team? Do you need additional help on your instance?

My loving partner Quintessence manages the mods, and I manage the infrastructure volunteers. It really is a “mom and pop” shop (or whatever the queer/lesbian version of the expression is) experience here. We have family discussions about the service at the dinner table. Having safe social media is important to our family.

We have other volunteer mods, specifically benwis has been very helpful in helping us deal with our reports. We currently see roughly 20-30 reports a day.

## Do you have accounts on other mastodon instances? Are there other mastodon instances you want to recommend to people? Do you have any favorite instances in Sweden/Scandinavia?

Yes! One of the lessons I learned about operating a brand account for myself as an influencer on Twitter is that I need to separate “Kris Nóva the brand” away from “Kris Nóva the person”. I have a private personal account on Hachyderm that is for my close friends only where I share personal updates and pictures.

I also intend to pivot on/off `nova@hachyderm.wtf` as well as `nova@hachyderm.io` so I have a better understanding of user experience with migrating accounts and federating content. We leverage hachyderm.wtf as our “lab” or “staging” environment where we can test things before we role them out to the main instance.

I really like fosstodon and infosec.exchange. The other admins and I have a unique way of staying in touch and I think there is a fair amount of mutual empathy that we have with each other when we talk to each other. We are all doing some fairly irrational things, and it makes us smile.

## What would you say make Mastodon different compared to other social media?

My server – my rules.

I spent years being manipulated, influenced, and exploited by ruthless product marketing gimmicks that called itself “Developer Advocacy”. I was so traumatized by Capitalism and the tech industry I ended up writing an entire book on the topic called “Hacking Capitalism”. Mastodon is a means to an ends for giving me total autonomy over my opinions and my brand without having to worry about corporate implications. I finally can sleep easy at night without worrying some tweet was going to jeapordize my family’s healthcare the next morning.

## Do you see similarities with other techologies and communities? (Like BBS, IRC, etc).

Yes. And I love it. I operated an IRC server for years, and this feels very similar. Hackers need a place to innovate and I suspect we see all the exciting and lucrative technology come out of spaces like Mastodon where innovation is free to happen without the constant looming threat of your corporation peering over your account.


## Can one help in some ways on your instance? How would one do if one would like to help? Whom should once contact?

There are 2 things we need help with today. Using the issue tracker, and donations. If you can do both of those you will be helping more than anything else.

I have to manage so many posts and notifications with admin style requests that I can’t even enjoy social media anymore. It makes me sad. I get a lot of requests for perfectly reasonable things like asking me to upload emojis or asking me some legal question or something. If I could ask people to put these style requests in the issue tracker on [GitHub](https://github.com/hachyderm/community) that would be best. That is why it’s there. It would be great if I was able to enjoy my social media again! I am more than just an administrator, after all. I also like cats and memes.

In 2023 we will change this to our new legal entity, however for right now its easy to donate to Hachyderm directly through my [Twitch donation page](https://ko-fi.com/krisnovalive).

Otherwise the hardest part of the service right now is that we have hit “tier 2” scale. Our limiting factor is getting knowledge out of the heads of our crews and into documentation so that new volunteers can be self sufficent with our infrastructure. Which means we need to create a “self service service for our service” which will take time to get right. Donations help us throw money at the problem whenever we can. Which helps a LOT for our peace of mind and our ability to sleep at night!

## Is there anything else you would like to say to the readers?

If you are a big tech name, a project, a company, or a big influencer I think you would enjoy operating your own instance. However there are perks in operating on instances like Hachyderm beause of our audience. I would caution everyone to check themselves and ask themselves what can we do different this time to make our new chance at social media successful and sustainable as possible? How do we build features that are inclusive to black communities? Marginalized communities? Disabled and traumatized communities? How do we fix some of our previous mistakes? We need to start treating this like an experiment and documenting our science. We need to start writing hypotheses and tracking them over time. We must share. We must collaborate. We must be transparent about our intentions and our information.