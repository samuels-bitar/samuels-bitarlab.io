---
title: Vad blir framtidens brott som ChatControl kommer leta efter?
image: surveillance-internet.jpg
description: Kommunikationsappar ska tvingas öppna upp sina användares kommunikation för att skanna efter brottsligt material. Var går gränsen?
date: 2023-03-31 10:45
---

Ett nytt EU-lagförslag debatteras nu i hela EU och även i Sverige. Det är ett massövervakningsförslag som kallas Chat Control 2.0. Det är ett förslag som tagits fram av EU-kommissionären Ylva Johansson. Resultatet blir att i princip all digital kommunikation ska öpppnas upp för att kunna skannas efter brottsligt material. Syftet är att hindra spridning av CSAM-material (barnporr) men det man gör är att öppna upp en Pandoras ask så att man kan skanna innehåll för allt annat.

Jag vill förklara här varför Chat Control 2.0 är en väldigt dålig idé. Lite kort om hur kryptering fungerar, om ändamålsglidning och vilka andra grupper som riskerar att drabbas nu eller i framtiden om det här massövervakningsförslaget går igenom. Och om varför kriminella inte kommer att drabbas.

## Bakgrund

Lagförslaget som kallas Chat Control 2.0 av kritiker har flugit under radarn. Det har mest varit integritetsaktivister och IT-säkerhetsexperter som lyft problematiken. Exempelvis har [Föreningen för Digitala Fri- och Rättigheter (DFRI)](https://www.dfri.se/), [Femte Juli](https://femtejuli.se/) och [Karl Emil Nikka](https://nikkasystems.com/2023/02/03/podd-183-chat-control-2-0/) lyft frågan.

Det finns en koalition av organisationer som gått ihop för att stoppa förslaget under parollen [Stop Scanning Me](https://stopscanningme.eu/sv/). I det ingår bland annat [EDRi](https://edri.org), [Women's Link Worldwide](https://www.womenslinkworldwide.org/en), [Center for Democracy & Technology (CDT)](https://cdt.org/) och [Electronic Frontier Foundation (EFF)](https://www.eff.org/).

I ett [öppet brev](https://stopscanningme.eu/sv/paper.html) så uppmanar 124 föreningar från civilsamhället EU-parlamentariker att dra tillbaka lagen.

# Enkelt om kryptering

Först av allt, lite kort om kryptering. Nuförtiden krypteras i princip all vår internettrafik eftersom vi använder https. När man surfar till en https-adress på nätet så krypteras innehållet mellan användarens webbläsare och webbservern. Det innebär att ingen annan på internet kan avlyssna trafiken. När man bara använder http-adresser så är inte trafiken krypterad och den kan avlyssnas. Som tur är normen nuförtiden att webbplatser har https. (Det är det hänglåset i adressfältet indikerar).

När vi kommunicerar med varandra via t.ex. Facebook så kommunicerar våra klienter (t.ex. facebook-appen på telefonen eller webbläsaren) med Facebooks servrar. Den kommunikationen sker krypterat så ingen kan avlyssna. Men Facebook har tillgång till vårt innehåll och kan läsa det.

Men det finns något som heter [end-to-end-kryptering](https://en.wikipedia.org/wiki/End-to-end_encryption). Det innebär att om två (eller fler) personer pratar med varandra så är det bara de personerna som kan se det. Inte ens de servrar som skickar meddelanden mellan klienterna kan läsa innehållet för att det är krypterat. Det är så appar som [Signal](https://signal.org/en/), [WhatsApp](https://faq.whatsapp.com/820124435853543), [Matrix/Element](https://element.io/features/end-to-end-encryption) och (delvis) [Apples iMessage](https://www.howtogeek.com/710509/apples-imessage-is-secure...-unless-you-have-icloud-enabled/) fungerar.

På grund av den säkerhet som end-to-end-kryptering innebär så föreslår exempelvis [SvD sina källor att kontakta dem via Signal](https://www.svd.se/a/ye96x/tipsa-svd). Reportrar utan gränser [uppmanar journalister att använda Signal](https://training.rsf.org/journalists-should-use-signal-to-protect-themselves-and-their-sources/) för att skydda sina källor. Signal är öppen källkod vilket innebär att man kan granska (och ändra) källkoden om man vill se hur programmet fungerar.

Men sådana här appar skulle bli meningslösa om de lägger in en bakdörr som kan skanna innehållet (och därmed förtar själva poängen med kryptering) eller förbjudna om de inte gör det om Chat Control 2.0 går igenom.

## Omöjliggör privat kommunikation och poängen med kryptering

I ett väldigt avslöjande [SvD-poddavsnitt](https://www.svd.se/a/wAVRkM/eu-s-chat-control-massovervakning-eller-trygghetsatgard) där Ylva Johansson och IT-säkerhetsexperten Karl Emil Nikka intervjuas så framgår tydligt att Ylva Johansson antingen inte förstår vad effekten blir av hennes lagförslag eller att hon inte bryr sig.

VPN-företaget Mullvad (som jobbar med kryptering dagligen) har [skrivit ett blogginlägg](https://mullvad.net/en/blog/2023/3/28/the-european-commission-does-not-understand-what-is-written-in-its-own-chat-control-bill/) där de granskar Johanssons uttalanden och hittade flera direkta felaktigheter.

Några saker som tas upp:

* Johansson säger att det är domstolar som ska bestämma vilket sorts material ska få skannas av. Men det är inget krav i lagförslaget att det är domstolar som ska besluta om det.
* Johansson menar att man kan "sniffa" efter brottsligt material utan att bryta upp krypterad kommunikation. Det går inte.
* Johansson försöker argumentera för att det inte rent tekniskt ska gå att skanna journalisters kommunikation med sina källor men att det ska gå att skanna pedofilers kommunikation. Det går inte heller.

Jag är själv civilingenjör i datateknik, lönearbetar som devops och webbutvecklare och jobbar ofta med kryptering. Det går inte att på ett rent tekniskt sätt "sniffa" efter bara ett sorts material om själva meddelandet inte är avkrypterat.

Det enda som man kan göra är att man öppnar upp för skanning (och/eller manuellt läsande) av kommunikation _innan_ meddelandet krpyteras och att man sätter upp lagar om vilket material man får söka efter. Men det finns flera problem med det. Det ena är att det finns mycket felträffar på automatisk skanning (t.ex. av AI). Enligt erfarenheter frå Schweiz är felaktigheterna på sådant 80-90%. Och det andra problemet är att det som inte får skannas idag kan beslutas om att det får skannas om i morgon.

## Öppen källkods-appar med end-to-end-kryptering blir förbjudna

I praktiken innebär det här att öppen källkods-appar som använder end-to-end-kryptering blir förbjudna. För det enda sättet att kunna skanna material med end-to-end-kryptering är att göra det på klientsidan (alltså i appen eller i webbläsaren) och med öppen källkod så kan man alltid ändra själv i programvaran. Så självklart kommer det då komma varianter som tar bort den här klientskanningen eftersom väldigt få personer som har möjligheten att undvika att ens innehåll blir skannas kommer att välja en app som gör det om de enkelt kan välja en annan.

Så därmed kommer i praktiken appar som Signal eller Matrix bli förbjudna. Eller så kommer de "bara" att förbjudas att finnas i app stores. Men det skulle innebära att väldigt få kan använda apparna. (För det är få personer som vet hur man installerar appar på sin telefon utan att använda app stores).

## Vad är olagligt imorgon?

Lagförslaget som kallas Chat Control 2.0 kommer tillåta och tvinga leverantörer av kommunikationstjänster att skanna efter CSAM-material (barnporr). Man får inte skanna efter något annat. Men det är ju inte svårt att se att det här kommer utökas. Man kommer självklart skanna efter fler saker när det här väl är på plats.

Anders Lindberg på Aftonbladet [skriver i en ledare](https://www.aftonbladet.se/ledare/a/O8aXkl/s-borde-saga-nej-till-chat-control) om just detta:

> Man kan ju jaga fler än pedofiler med chat control om man vill. Flyktingsmugglare kanske? Människor som bryter mot smittskyddsregler?
> 
> Kanske klimataktivister som stör trafiken? Det skulle nog vara populärt. Eller journalister som granskar ”fel” saker? Möjligheterna är oändliga.
> 
> Socialdemokraterna borde försöka stoppa förslaget om chat control. Sveriges röst är viktig om förslaget ska gå igenom på EU-nivå. Det vore att stå på rätt sida historien.
> 
> Dagens Europa är på väg i allt mer auktoritär riktning. Att då öka statens förmåga till övervakning, kontroll och repression är fel väg att gå.
> 

Jag skrev tillsammans med några andra i styrelsen från [Kamratdataföreningen Konstellationen](https://konstellationen.org/) en [debattartikel den 8 mars](https://www.etc.se/debatt/att-bekaempa-massoevervakning-aer-en-feministisk-oedesfraaga) om att massövervakning slår extra hårt mot kvinnor. Vi tar upp det sorgliga faktum att abort är förbjudet i många delstater i USA och att polisen redan där använder sig av inhämtande av privat kommunikation (från t.ex. Google och Facebook) för att jaga kvinnor som utför abort. Detta har upphov till att pro-choice-rörelsen i USA har bildat [Digital Defense Fund](https://digitaldefensefund.org/) som hjälper till med digital säkerhet för att säkerställa rätten till abort.

I EU ser vi redan att länder som Ungern och Polen har väldigt konservativa, kvinnofientliga och [hbtqi-fientliga lagar](https://www.svd.se/a/JEEEgP/ungerns-hbtq-lag-trader-nu-i-kraft-en-skam). I Sverige så [ska det bli straffbart att delta i en terroristorganisation](https://www.regeringen.se/pressmeddelanden/2023/02/det-ska-vara-straffbart-att-delta-i-en-terroristorganisation/) tycker regeringen. Och Tobias Billström (M) har sagt till turkisk media att Sverige kommer [kriminalisera terrorflaggor](https://www.etc.se/utrikes/billstroem-i-turkisk-media-sverige-kommer-kriminalisera-terrorflaggor). Det som syftas här är så klart PKK-flaggor. I och med NATO-ansökan så har Sverige anpassat sig till Turkiets önskemål. När Chat Control väl utökas till att skanna efter fler saker så kommer exempelvis kurdiska rörelser och individer aldrig att kunna lita på att deras kommunikation inte hamnar i ett filter. Det är tydligt när man ser hur kurdiska rörelser misstänkliggörs i Sverige på väldigt lösa grunder.

Runt om i världen [kriminaliseras klimatprotester](https://www.theguardian.com/environment/2021/apr/19/environment-protest-being-criminalised-around-world-say-experts). Den moderata regionpolitikern Kristoffer Tamsons skriver i [en debattartikel](https://www.expressen.se/debatt/naturskyddsforeningen-agerar-som-vagterrorister/) "Naturskyddsföreningen lämnar nu in en överklagan mot Tvärförbindelse Södertörn och agerar därmed som den byråkratiska grenen till vägterroristerna". Om en moderatpolitiker uttalar sig så om ett helt legitimt överklagande av en vägplan så är det inte svårt att se att klimataktivister snabbt kan klassas som terrorister.

## Kriminella drabbas inte

För pedofiler som vill dela vidrigt material så kommer de att undgå denna lag. De kommer inte nyttja de tjänster som förbjuds. Tekniken att kommunicera privat och krypterat finns tillgänglig idag och vem som helst med viss teknisk kompetens kan sätta upp egna kanaler. Så de som kommer att påverkas är vanligt folk som tänker att de inte har något att dölja. Det är de som kommer att massövervakas.

## Alternativet då?

Det enkla svaret är att alternativet till massövervakning är riktad övervakning. Självklart ska kriminella lagföras. Pedofiler som delar CSAM-material och drogkarteller som smugglar knark och mördar människor ska självklart lagföras. Men det är helt orimligt att införa en massövervakningsapparat där risken är otroligt stor att den kommer användas till mycket mer än att bara skanna efter CSAM-material. Speciellt med framtida mer konservativa och reaktionära regeringar. Dessutom så kommer de kriminella sätta upp egna kommunikationskanaler om de vanliga kanalerna är övervakade.

## Mer läsning

Det finns massvis med bra debattartiklar och länkar i den här frågan. Jag tar ett axplock här:

* [Stop scanning me!](https://stopscanningme.eu/sv/)
* [DFRI - Chat Control 2.0 – EUs förslag om massövervakning](https://www.dfri.se/projekt/chatt-kontroll-chatcontrol-slutaskannamig-stopscanningme-eu/)
* [Karl Emil Nikka - Podd #183: Chat Control 2.0](https://nikkasystems.com/2023/02/03/podd-183-chat-control-2-0/)
* [Femte Juli - #ChatControl – Ylva Johansson säger orimliga saker, del 1](https://femtejuli.se/2023/03/31/chatcontrol-orimliga-saker-ylva-johansson-sagt-del-1/)
* [Debattartikel i SvD av Mullvad VPN:s VD - "Stoppa förslaget om massövervakning i EU"](https://www.svd.se/a/76mlxW/jan-jonsson-stoppa-forslaget-om-massovervakning-ieu)
* [Nätaktivister varnar för nya EU-förslaget: "Oskyldiga blir övervakade"](https://www.etc.se/utrikes/naetaktivister-varnar-foer-nya-eu-foerslaget-oskyldiga-blir-oevervakade)
* [EFF - Users Worldwide Said "Stop Scanning Us": 2022 in Review](https://www.eff.org/tl/deeplinks/2022/12/year-users-worldwide-said-stop-scanning-us)
* [Debatt: Att bekämpa massövervakning är en feministisk ödesfråga](https://www.etc.se/debatt/att-bekaempa-massoevervakning-aer-en-feministisk-oedesfraaga)
* [Henrik Alexandersson:Var står svenska partier i frågan om Chat Control?](https://www.svd.se/a/eJ618l/henrik-alexandersson-chat-control-inskranker-manskliga-rattigheter)
* [S borde säga nej till "chat control"](https://www.aftonbladet.se/ledare/a/O8aXkl/s-borde-saga-nej-till-chat-control)
* [Europeiska folket säger nej till EU:s "Chat Control"](https://bahnhof.se/2022/10/26/europeiska-folket-sager-nej-till-eus-chat-control/)
* [Linus Larsson: EU-lagen om att skanna din chatt vållar strid](https://www.dn.se/ekonomi/linus-larsson-eu-lagen-om-att-skanna-din-chatt-vallar-strid/)
* [EU vill övervaka ALLT vi gör: "Vill göra det svårt för pedofiler"](https://www.tv4play.se/program/nyhetsmorgon/eu-vill-%C3%B6vervaka-allt-vi-g%C3%B6r-vill-g%C3%B6ra-det-sv%C3%A5rt-f%C3%B6r-pedofiler/20479462?offset=3)