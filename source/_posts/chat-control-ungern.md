---
title: Chat Control och kontaktuppgifter till ledamöterna i riksdagen
image: monitor-eyes.jpg
image_description: Bild av <a href="https://pixabay.com/users/geralt-9301/">Gerd Altmann</a> från <a href="https://pixabay.com/">Pixabay</a>
description: Chat Control lever igen. Ungern lägger fram nytt förslag på Chat Control
date: 2024-09-13 10:00
---

Chat Control lever igen. Senaste nyheter:

* [Nya förslaget om ”Chat control” läckt – Sverige måste ge besked inom 12 dagar](https://emanuelkarlsten.se/nya-forslaget-om-chat-control-lackt-sverige-maste-ge-besked-inom-12-dagar/)
* [Om chat control: ”Som att montera upp en kamera i alla våra rum”](https://www.tv4.se/artikel/6ZoKEBqxUaANdZl8BJE5rw/om-chat-control-som-att-montera-upp-en-kamera-i-alla-vara-rum)
* [Nytt försök med "chat control" i oktober](https://www.svd.se/a/wgRnn1/nytt-forsok-med-chat-control-i-oktober)
* [Controversial 'chat control' back on EU agenda this week](https://www.brusselstimes.com/1209673/controversial-chat-control-back-on-eu-agenda-this-week)
* [New EU push for chat control: Will messenger services be blocked in Europe?](https://www.patrick-breyer.de/en/new-eu-push-for-chat-control-will-messenger-services-be-blocked-in-europe/)

Kamratdataföreningen Konstellationen har [skickat en enkät](https://konstellationen.org/2024/09/12/chat-control-enkat/) till alla riksdagspartier med sju frågor om Chat Control, massövervakning och personlig integritet.

Frågorna är konkreta:

* Är ni motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
* Står ni bakom EU-parlamentets kompromissförslag?
* Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
* Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
* Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle ni säga nej till förslaget?
* Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?
* Anser ni att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Resultatet presenteras när deadline har nåtts.

Om resultatet blir som partierna förklarade efter Justitieutskottets möte 18 juni så är följande partier mot: V, C, MP och SD.

Men flera partier har svajat i frågan och sagt och agerat olika i Sverige och i EU-parlamentet. Fler partier kan mycket väl ansluta sig till motståndet.

## Övriga partiers agerande

Hur har då de andra partierna ställt sig i frågan? Följande är hämtat från [chatcontrol.se](https://chatcontrol.se/status/) där man kan läsa i detalj hur olika partier agerat.

**Socialdemokraterna** är de som är tydligast för Chat Control och ingen ledande företrädare har uttryckt stark kritik vad jag kan se. De enda som jag hittar som uttrycker kritik är Tekniksossarna i [en debattartikel](https://aip.nu/rosta-nej-till-forslaget-om-chat-control/).

Regeringspartierna stöttade inledningsvis förslaget. Detta meddelade justitieminister Gunnar Strömmer (M) i ett [inslag i Sveriges Radio](https://sverigesradio.se/artikel/regeringen-vill-kunna-kontrollera-alla-digitala-meddelanden) (2023-03-31). Tre veckor senare de drog de tillbaka sin ståndpunkt och valde att hålla sig neutrala med anledning av EU-ordförande­skapet. Denna ändring framfördes av justitieminister Gunnar Strömmer i en intervju i [Svenska dagbladet](https://www.svd.se/a/rlek2R/gunnar-strommer-om-chat-control-bestammer-oss-efter-ordforandeskapet) (2023-04-19).

Fem månader senare återgick regerings­partierna till att stötta förslaget med några [mindre justeringar](https://www.riksdagen.se/sv/dokument-och-lagar/dokument/bilaga-till-dokument-fran-eu-namnden/_HB0N4F1902/) (2023-09-18). Dessa justeringar påverkade ej ursprungs­förslagets krav på att kunna skanna meddelanden som skickas via totalsträcks­krypterade tjänster.

I SVT:s EU-valkompass fick alla partier tydliggöra sin ståndpunkt kring det politiska förslaget "[All privat digital kommunikation ska kunna övervakas i brottsförebyggande syfte](https://valkompass.svt.se/eu-2024/fraga/all-privat-digital-kommunikation-ska-kunna-overvakas-i-brottsforebyggande-syfte/)".

**Liberalerna** kryssade att det var ett "Mycket dåligt förslag" med motiveringen:

> Rätten till integritet ska vara stark - gentemot både staten och privata företag. Samtidigt måste integriteten ibland vägas mot andra viktiga intressen, som arbetet mot terrorism eller att bekämpa grova brott mot barn. Integritetsfrågorna måste alltid analyseras noggrant när lagförslag tas fram som kan påverka exempelvis kommunikation i krypterade chattar.

Liberalernas EU-parlamentariker Karin Karlsbro meddelade via sin [officiella Facebook-sida](https://www.facebook.com/karlsbroliberal/posts/pfbid02CKHivHcrBkwnWJCNTyJLWvGTS1GSLnjTU1rNetUoGqnH4BWxSYTcmHj6fNdS5exel) att hon sade nej till Chat Control 2.0 (2023-05-20).

När det gäller **Moderaterna** har de nog tydligast haft olika syn på Chat Control och är mest splittrade. De har fått företräda regeringens syn eftersom justitieminister Gunnar Strömmer är moderat.

EU-parlamentarikern Tomas Tobé (M) har samtidigt uttryckt kritik mot Chat Control [här](https://twitter.com/tomastobe/status/1648254012213403650), [här](https://www.aftonbladet.se/nyheter/a/oneXE0/m-i-eu-sager-nej-till-chat-control-gar-emot-regeringen) och [här](https://twitter.com/tomastobe/status/1791039640633254381). Även EU-parlamentarikern Arba Kokalari (M) har [uttryckt kritik](https://x.com/ArbaKokalari/status/1791109015864328474).

I ett partiuttalande till [Tidningsutgivarna](https://tu.se/standpunkt/pressfrihet/bred-enighet-om-pressfrihet-bland-svenska-eu-kandidater/) (2024-05-03) meddelade Moderaterna att "hederliga människor ska ha möjlighet att ha privata konversationer online och kommunicera via helt krypterade tjänster".

**Kristdemokraterna** har historiskt svajat fram och tillbaka men får anses stöda Chat Control 2.0 utifrån vad de sagt hittills och hur de agerat. Men intressant nog har deras nyvalda EU-parlamentariker [uttryckt kritik](https://x.com/alicemedce/status/1831342333863891427) (2024-09-04) efter EU-valet.

Med andra ord kan inte något av regeringspartierna sägas vara konsekventa i sin hållning till Chat Control. Genomgående är att det är partiernas EU-parlamentariker som uttrycker kritik mot Chat Control medan företrädarna i Sverige röstar för Chat Control i riksdagen.

## Kontaktuppgifter till partierna

Det är EU-nämnden och Justitieutskottet som har hanterat frågan om Chat Control i Sverige.

Kontaktuppgifter till alla ledamöter (även suppleanter) finns för [EU-nämnden här](https://www.riksdagen.se/sv/sa-fungerar-riksdagen/utskotten-och-eu-namnden/eu-namnden/) och för [Justitieutskottet här](https://www.riksdagen.se/sv/sa-fungerar-riksdagen/utskotten-och-eu-namnden/justitieutskottet/).

Info-adresserna för alla partierna [enligt riksdagens hemsida](https://www.riksdagen.se/sv/ledamoter-och-partier/partierna-i-riksdagen/):

* Socialdemokraterna - info@socialdemokraterna.se
* Moderaterna - info@moderaterna.se
* Kristdemokraterna - info@kristdemokraterna.se
* Liberalerna - info@liberalerna.se
* Vänsterpartiet - info@vansterpartiet.se
* Centerpartiet - info@centerpartiet.se
* Miljöpartiet - info@mp.se
* Sverigedemokraterna - info@sd.se

Det vore bra om fler kunde kontakta partierna om Chat Control så att de tydliggör sin position.