---
title: Krönika - Smartare än krypto – godare än Swish
image: money-transfer.jpg
image_description: Bild av <a href="https://pixabay.com/users/mohamed_hassan-5229782/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3588301">Mohamed Hassan</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3588301">Pixabay</a>
description: En krönika om det anonyma betalningssystemet GNU Taler
date: 2025-01-27 13:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/smartare-aen-krypto-godare-aen-swish).

**Det finns betalningssystem som är anonyma på riktigt, inte slukar el, och inte gör de rika rikare.**

Det blir allt svårare att betala med kontanter. I affärer förväntas man betala med kort (eller virtuella kort genom mobilen) och småköp görs via Swish, en infrastruktur som ägs av ett gäng banker, inte av staten. På nätet betalar man allt med kontokort eller tjänster som Klarna.

Men så kom kryptovalutan Bitcoin som sades möjliggöra enkla och anonyma betalningar. Med hjälp av [blockkedjeteknik](https://sv.wikipedia.org/wiki/Blockkedja) och ett snillrikt decentraliserat system kunde bankerna kringgås. Men man behövde kunna köpa kryptovalutor, vilket gjordes via en så kallad exchange. Och då behövde man ett kontokort knutet till ett bankkonto. Samma blockkedjeteknik som användes för att göra systemet fristående och decentraliserat säkerställde dessutom att man kunde följa alla betalningar. Så Bitcoin var inte speciellt anonymt ändå. (Det har senare kommit kryptovalutor som är svårare att följa och därmed är mer anonyma.)

Blockkedjetekniken som Bitcoin baserades på var visserligen spännande men drog ofantliga mängder el för att systemet skulle fungera. Andra kryptovalutor som Ethereum använder en mycket [mindre krävande teknik](https://en.wikipedia.org/wiki/Proof_of_stake) men med sidoeffekten att systemet tenderar att belöna de som redan har mest.

Därför är det glädjande att det finns andra betalningssystem som inte behöver styras av och berika bankerna, inte slukar energi, inte gör "de rika rikare" per automatik och som är anonymt på riktigt. Det är [GNU Taler](https://en.wikipedia.org/wiki/GNU_Taler), ett system byggt på öppen källkod. Istället för att förlita sig på blockkedjeteknik använder den sig av något som kallas blinda signaturer. Det smarta i systemet är att det möjliggör kunden eller betalaren att vara anonym medan säljaren alltid identifieras. GNU Taler-projektet har fått anslag från EU och även samarbetat med Schweiz nationalbank. Än så länge är systemet på teststadiet men [flera affärsverksamheter](https://taler.net/en/faq.html) utforskar systemet och har fungerande modeller.

Låt oss satsa på enkla, öppna, säkra och integritetsvänliga betalningsmetoder som GNU Taler i den digitala världen.