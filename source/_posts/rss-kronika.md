---
title: Krönika - Så stoppar du Elon Musk från att förstöra dina nyheter
image: rss.png
image_description: <a href="https://openclipart.org/detail/179848/rss-by-anarres-179848">RSS-teckning, OpenClipart</a>. 
description: En krönika om varför RSS är fantastiskt
date: 2024-04-15 21:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/saa-stoppar-du-elon-musk-fraan-att-foerstoera-dina-nyheter).

**Trött på att Twitter, numera X, inte är detsamma som förr? Då är det dags att introduceras för RSS – en lika enkel som kraftfull teknik som i dagarna fyllde 25 år.**

Jag kommer ihåg när Twitter blev stort och jag började använda det runt 2007/2008. Jag hade inget stort intresse av att skriva en massa grejer där men jag märkte att alla tidningar lade upp sina nyheter på Twitter. Perfekt! Ett enda ställe där jag kunde få alla mina nyheter! Det var så jag, och många andra, använde Twitter.

Jag hade av någon anledning inte riktigt förstått att jag kunde få samma sak fast ännu bättre genom RSS. Jag behövde bara en RSS-läsare och webbadresserna till de RSS-flöden jag ville följa. RSS är en lika enkel som kraftfull teknik, som i dagarna fyllde 25 år. Och det är hög tid för fler av oss att börja använda det.

Upplevelsen av X är minst sagt en helt annan idag än av Twitter 2007. 

Men om man fortfarande vill prenumerera på innehåll och bli notifierad genom RSS är processen likadan idag som för 25 år sedan. Fördelen är också att ingen äger RSS. Ingen Elon Musk kan komma och förstöra. RSS är en väl beprövad teknik som ger tillbaka makten till användaren.

Du kan prenumerera på flöden från ETC, SVT, DN, Arbetaren, The Guardian, BBC, The New York Times, Al Jazeera, Reuters och många fler. I princip varje nyhetssajt på internet har ett RSS-flöde.

Det enda du behöver för att komma igång är en RSS-läsare.  

För Android kan du testa: Feeder, Inoreader, Feedly eller Newsblur.

För Iphone kan du testa: Inoreader, Feedly, eller Netnewswire.

Jag har själv varit med och utvecklat en slags webbaserad RSS-läsare – nyhetsaggregatorn [Mediakollen.org](https://mediakollen.org/) som samlar innehåll från vänsterperspektiv, fackliga nyheter och gröna nyheter. Vi har mer än 150 RSS-flöden över tidningar, bloggar, poddar och aktiviteter som vi väljer ut nyheter från och visar på hemsidan. Om du bara vill följa några av källorna med din egen RSS-läsare så listar vi adresserna till varje flöde på vår [sida](https://mediakollen.org/feed).

Så börja med att öppna din RSS-läsare och skriv in adressen till dina två första flöden: https://www.etc.se/rss.xml och https://www.svt.se/rss.xml. Ett litet men viktigt steg för att ta tillbaka makten över internet.