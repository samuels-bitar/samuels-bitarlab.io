---
title: Progressiva bör oroa sig för massövervakningen
image: surveillance-screens.jpg
description: Massövervakningen kommer att påverka möjligheterna för progressiva rörelser att organisera sig
date: 2023-06-18 16:00
---

Bankers övervakning av sina kunder, misstänkliggörande av digitala verktyg och EU:s massövervakningsförslag Chat Control 2.0 är något som alla progressiva bör oroa sig för om man har ett intresse av att kunna fortsätta opinionsbilda, organisera sig och förändra världen.

Att människorättsaktivister, socialister, anarkister, fackliga organisatörer, feminister och andra progressiva rörelser under historien har blivit övervakade har vi vetat länge. Listan kan göras lång. Men ändå så verkar många progressiva rörelser (i alla fall i Sverige) inte göra så mycket för att ändra på det här.

Hur kan det ha blivit så här?

Är det så att vi tror att de mänskliga fri- och rättigheterna är uppnådda? Är det att vi tänker att [IB-affären](https://sv.wikipedia.org/wiki/IB) inte skulle kunna hända igen? Eller är det för att vi under så lång tid frivilligt har accepterat övervakningen genom att på nätet acceptera alla villkor hos tjänster där vi trycker på "OK" så att företag bakom Facebook, Instagram, Twitter, Google, Gmail, Hotmail, TikTok, Youtube och många fler ska kunna läsa vår information och sälja den till högstbjudande? Och att vi därför tänker att vi ändå gett upp så mycket att det inte är någon idé att göra motstånd?

Hur det än kommer sig att vi är i den situation vi är i nu så behöver alla progressiva vakna upp och göra motstånd.

Jag vill ta upp några saker som förhoppningsvis kan göra det tydligt att vi måste göra något åt massövervakningen och hur den drabbar progressiva rörelser.

## Betalade för att läsa svenska vänstertidningar – då stängde banken kontot

Så lyder rubriken på en [artikel på blankspot.se](https://blankspot.se/betalade-for-att-lasa-svenska-vanstertidningar-da-stangde-banken-kontot/) som handlade om bankkunden Ola Jönsson fick sitt bankkonto avstängt efter att han gjort banköverföringar till vänstermedier och vänsterprojekt. Några av de sakerna han skickat pengar till och som banken krävt förklaringar för är: ETC SOL, ETC BYGGLÅN, ETC SOLPARK, tidningen Arbetaren, brittiska socialistinriktade bokförlaget Pluto Books, svensk-kurdiska biståndsorganisationen Röda Solen, Alla kvinnors hus, som arbetar mot våld i hemmet, och Nätverket ingen människa är illegal.

När Blankspot frågade banken i fråga, Länsförsäkringar, så svarade de att det var på grund av Penningtvättslagen. Men de skyller på banksekretessen för att slippa svara på frågor. Läs mer [på Arbetaren](https://www.arbetaren.se/2023/06/14/prenumerade-pa-arbetaren-banken-stangde-kontot/). 

Vänsterpartiets riksdagsledamot [Ali Esbati reagerade på avslöjandet](https://www.arbetaren.se/2023/06/15/ali-esbati-det-har-ar-en-fraga-for-riksdag-och-regering/) och skickade en skriftlig fråga till det moderata statsrådet Niklas Wyman om han avser vidta åtgärder "för att se över risken för kränkningar av åsikts-, press- och föreningsfriheten för vanliga medborgare i samband med bankernas tillämpning av penningtvättslagstiftningen?".

– Det känns chockerande och illavarslande. Om det händer fler gånger är det illa, men även om det är ett enskilt misstag på en enskild bank så betyder det att det skett efter att en person fått i arbetsuppgift att gå igenom transaktioner med någon typ av idé om vad man ska reagera på. Då är det också ett stort problem som måste klargöras, säger Ali Esbati. 

Arbetarens tillförordnade chefredaktör Annie Hellquist ser också allvarligt på händelsen och kommenterade att "om en tidningsprenumeration på Arbetaren har fått varningsklockorna att ringa på banken, då är det något väldigt fel på varningsklockorna".

## Kryptering kriminaliseras i Frankrike

I Frankrike så riskerar aktivister framöver att drabbas hårt av att man använder kryptering och andra privatiseringsverktyg. [La Quadrature du Net](https://en.wikipedia.org/wiki/La_Quadrature_du_Net) ("Squaring the Net") är en fransk organisation som förespråkar digitala fri- och rättigheter för invånarna. (Ungefär som [Electronic Frontier Foundation](https://en.wikipedia.org/wiki/Electronic_Frontier_Foundation) i USA eller som den svenska [Föreningen för Digitala Fri- och Rättigheter (DFRI)](https://sv.wikipedia.org/wiki/F%C3%B6reningen_f%C3%B6r_digitala_fri-_och_r%C3%A4ttigheter)). La Quadrature du Net har skrivit utförligt om hur [kryptering kriminaliseras i Frankrike i och med en rättegång](https://www.laquadrature.net/en/2023/06/05/criminalization-of-encryption-the-8-december-case/).

Bakgrunden är det så kallade [8 december-fallet](https://en.wikipedia.org/wiki/8_December_2020_incident) i Frankrike som handlar om hur nio franska medborgare åker ner till Rojava för att ansluta sig till YPG i kampen mot IS. I och med att de frivilligt åkt ner har de stämplats som vänsterextremister och arresterats i en rad räder den 8 december 2020. Sju av nio åtalades för samröre med terrorism. Rättegången är planerad till oktober 2023.

[I inlägget](https://www.laquadrature.net/en/2023/06/05/criminalization-of-encryption-the-8-december-case/) så skriver La Quadrature du Net om fallet. Inlägget är väldigt informativt och bör läsas i sin helhet. Men för att plocka ut några saker:

* Aktivisterna anklagades för "hemligt beteende" i och med att de använde krypterade applikationer som Signal, det säkra operativsystemet Tails och använt anonymiseringsnätverket Tor.
* Innehav av teknisk dokumentation och träning i digital hygien anses vara "hemligt beteende" som bara kan förklaras av den "gruppens terroristiska karaktär".

De saker som tas upp som exempel för "hemligt beteende" är:

* Användningen av applikationer som Signal, WhatsApp, Wire, Silence eller ProtonMail för att kryptera kommunikation
* Användningen av privatiseringsverktyg som VPN, Tor eller operativsystemet Tails
* Skyddande av exploatering av persondata av Big Tech genom att använda tjänster som /e/OS, LineageOS och F-Droid
* Kryptera digital media
* Organisera och delta i digital hygien-träning
* Innehav av teknisk dokumentation

Om sådana saker ska anses vara "av terroristkaraktär" så riskerar jag själv personligen att drabbas. Jag själv använder:

* Den krypterade appen [Signal](https://en.wikipedia.org/wiki/Signal_(software)) i kommunikation med vänner, familj, kamrater och föreningsmänniskor
* Den krypterade tekniken [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol))/[Element](https://en.wikipedia.org/wiki/Element_(software)) för att hålla kontakt med andra i [Kamratdataföreningen Konstellationen](https://konstellationen.org)
* [Mullvad VPN](https://en.wikipedia.org/wiki/Mullvad) på både min mobil och dator för att kunna vara anonym på internet
* Operativsystemet [/e/OS](https://en.wikipedia.org/wiki//e/_(operating_system)) på min telefon för att slippa få min persondata skördad av Google
* [ProtonMail](https://en.wikipedia.org/wiki/ProtonMail) för att slippa få mina mail skannade

Att värna sin personliga integritet är alltså något som anses vara misstänksamt. Det säger en hel del om dagens samhälle.

## Nutida exempel på övervakning av progressiva och aktivister

Det finns läskigt många exempel på hur människorättsaktivister, hbtq-personer och hbtq-aktivister, klimataktivister, journalister, oppositionella men även hela befolkningar övervakas internationellt. Här är några exempel:

* Kina massövervakar sin befolkning. Se t.ex. [den här videon](https://www.nytimes.com/video/world/asia/100000008314175/china-government-surveillance-data.html) om analysen av 100 000 dokument om hur övervakningen fungerar. Finns [mycket information på Wikipedia](https://en.wikipedia.org/wiki/Mass_surveillance_in_China). Kina har också [digital Yuan](https://www.wired.com/story/chinas-digital-yuan-ecny-works-just-like-cash-surveillance/) som fungerar som kontanter men som samtidigt övervakar befolkningn.
* Iran installerar [kameror på offentliga platser](https://www.bbc.com/news/world-65220595) för att identifiera kvinnor som inte bär slöja
* Ryssland övervakar Moskvas tunnelbana med övervakningskameror och ansiktsigenkänning för att [fånga demonstranter](https://www.washingtonpost.com/world/europe/russia-facial-recognition-surveillance-navalny/2021/04/16/4b97dc80-8c0a-11eb-a33e-da28941cb9ac_story.html). [Wired](https://www.wired.com/story/moscow-safe-city-ntechlab/) har skrivit bra om hur Moskva är ett typiskt exempel på ändamålsglidning med övervakning
* [Ungern och Polen](https://www.theguardian.com/world/2023/may/09/eu-parliament-report-calls-for-tighter-regulation-of-spyware) använder [spionprogramvaran Pegasus](https://en.wikipedia.org/wiki/Pegasus_(spyware)) för att spåra journalister och oppositionella.

Så även i Europa har vi länder som Ungern och Polen som övervakar journalister och oppositionella.

För att läsa mer om hur det ser ut i olika länder, se [den här artikeln på Wikipedia](https://en.wikipedia.org/wiki/Mass_surveillance).


## Massövervakning används för att lagföra kvinnor som genomgått abort i USA

USA är ett exempel på hur massövervakningen inte bara slår mot politiskt aktiva utan även slår mot kvinnor som grupp. I och med att högsta domstolen upphävde Roe mot Wade, det nästan 50-åriga domstolsbeslutet som legaliserade abort över hela nationen, kan delstaterna besluta om abortlagstiftning själva. Väldigt många delstater har förbjudit abort eller begränsat aborträtten väldigt mycket.

Facebook och Google lämnar över användardata till polisen i USA för att hjälpa dem att lagföra de som vill göra abort. Det är inte längre säkert att ­diskutera frågor om reproduktiva rättigheter med nära och kära i flera sociala medier. Inte heller är det säkert att googla på vart närmaste abortklinik ligger. Många appar loggar platsdata och kan begäras ut för att bygga fall mot någon som misstänkts ha besökt en abortklinik. 

För de som bor i en delstat där abort är förbjudet och inte har möjlighet att åka till en delstat där det är lagligt finns möjligheten att beställa mediciner online och få hemskickat. Men även hemsidor som säljer abortpiller delar med sig av data till Google genom användade av Google analytics. Även att [ha en menscykelapp](https://www.wired.com/story/period-tracking-apps-flo-clue-stardust-ranked-data-privacy/) kan vara osäkert.

Det är tydligt att aborträttsrörelsen i USA har förstått kopplingen mellan mass­övervakning och kontroll av kvinnors ­sexualitet och därför skapat initiativ som [Digital Defense Fund](https://digitaldefensefund.org/about). De utbildar kring digital säkerhet som kommunikationsappar med kryptering.

## Massövervakningsförslaget Chat Control

Då kommer vi till massövervakningsförslaget Chat Control 2.0. 

EU-kommissionären Ylva ­Johansson föreslår ett massövervakningsförslag som av oss kritiker kallas "Chat Control 2.0". I förslaget vill man tvinga publika moln- och meddelandetjänster att skanna till exempel chattar, mejl, konversationer och filer efter material med sexuella övergrepp mot barn. Förslaget skulle göra det omöjligt att föra en privat och säker kommunikation även för politiker, journalister, sjukvårdspersonal och för de som söker efter närmaste abortklinik.

Även om syftet med "Chat Control 2.0" är behjärtansvärt så kommer det inte lyckas med sitt uttalade syfte då det alltid kommer att gå att komma runt för de som sprida material. ­Istället introduceras en massövervakningsapparat. Det stora problemet är ändamålsglidningen, att man med största sannolikhet kommer använda tekniken för andra syften i framtiden, då oftast med mindre behjärtansvärda motiv.

Professorn i digital forensik och cybersäkerhet vid Stockholms universitet, Stefan Axelsson, menar att "Inte ens Östtysklands säkerhetspolis Stasi hade övervakning på den här nivån."

För att lära dig mer om Chat Control, besök kampanjsidan [chatcontrol.se](https://chatcontrol.se) där du får lära dig om vanliga frågor och svar. 

Passa också på att använda deras [smidiga mailgenerator](https://chatcontrol.se/mejla/) så att du på 2-3 minuter enkelt kan genererar ett till politiker och uppmanar dem säga nej till Chat Control.

Fler bra artiklar om Chat Control 2.0:

* [Chat control skapar en rättsstat i fritt fall](https://www.arbetaren.se/2023/06/08/chat-control-skapar-en-rattsstat-i-fritt-fall/)
* [Rasmus Fleischer: Chat Control ger ett osäkrare internet för alla](https://www.etc.se/kronika/chat-control-ger-ett-osaekrare-internet-foer-alla)
* [Anna Ardin: Ylva Johansson behöver inte IB för att övervaka meningsmotståndare](https://www.etc.se/kronika/ylva-johansson-behoever-inte-ib-foer-att-oevervaka-meningsmotstaandare)
* [S borde säga nej till "chat control"](https://www.aftonbladet.se/ledare/a/O8aXkl/s-borde-saga-nej-till-chat-control)
* [Nej, vi behöver inget övervakningssamhälle](https://www.dagensarena.se/opinion/nej-vi-behover-inget-overvakningssamhalle/)
* [Håkan Boström: Ylva Johansson vill läsa din e-post – det är ingen bra idé](https://www.gp.se/ledare/ylva-johansson-vill-l%C3%A4sa-din-e-post-det-%C3%A4r-ingen-bra-id%C3%A9-1.93790797)
* [Kyrkor ­riskerar bli övervakade](https://www.sandaren.se/ledare/kyrkor-riskerar-bli-overvakade)

## Dags för progressiva att vakna

Alltså, övervakning av aktivister och massövervakning av befolkning har använts genom historien många gånger och oftast med väldigt dåliga konsekvenser. Då staten har infört övervakning och andra kontrollmetoder så har det i princip alltid skett en [utökning av tillämpningen](https://www.arbetaren.se/2023/06/08/chat-control-skapar-en-rattsstat-i-fritt-fall/), dvs en ändamålsglidning.

Idag så anger regeringen och EU skäl som terrorism, pengatvätt och barnövergreppsmaterial för att införa kontrollmetoder. Men med den nya terroristlagen så är det uppenbarligen "misstänksamt beteende" att vara för frihet för kurder. [SÄPO stämplar den största svenska kurdiska organisationen NCDK som terrorister](https://www.etc.se/inrikes/saepo-sveriges-stoersta-kurdorganisation-aer-terrorister). Pro-kurdiska [Heval förlag fick sitt bankkonto stängt](https://www.etc.se/inrikes/seb-staenger-pro-kurdiskt-foerlags-konto). Bankkunden Ola Jönsson fick sitt bankkonto nedstängt för att ha gett pengar till vänsterrörelser. Utrikesminister Tobias Billström (M) säger till turkiska medier att det ska bli [olagligt att "vifta med" terrorflaggor i Sverige](https://www.etc.se/utrikes/billstroem-i-turkisk-media-sverige-kommer-kriminalisera-terrorflaggor).

Med fascistiska SD vid makten (vare sig direkt eller indirekt) så hårdnar klimatet. Är det olagligt i morgon att gömma flyktingar? Är det olagligt i morgon att vifta med en pro-kurdisk flagga? Är det olagligt i morgon att skänka pengar för att betala böter för dömda klimataktivister? Är det olagligt i morgon att demonstrera? Och även om det inte är olagligt, kommer man att drabbas ändå? Antingen genom att få sitt bankkonto nedstängt eller bli sparkad från sitt jobb?

Våra demokratiska fri- och rättigheter är inte något som vi kan ta för givna. Inte heller är personlig integritet en hobby för integritetsaktivister utan något som gäller oss alla, speciellt vi som vill förändra samhället.

**Call to action**:

* Sprid information om övervakning och häxjakten på kurder och vänstern
* Börja använda totalsträcktkrypterade appar (som [Signal](https://en.wikipedia.org/wiki/Signal_(software)) och [Matrix/Element](https://en.wikipedia.org/wiki/Element_(software))) så att vi är många som förstår vikten av att skydda rätten till privat kommunikation och är mindre utsatta om det blir mer högerauktoritära vindar med mer repressiva åtgärder
* [Skriv ett mail](https://chatcontrol.se/mejla/) till en svensk EU-parlamentariker eller riksdagsledamot
* Gå med i en förening som gör motstånd, t.ex. [Kamratdataföreningen Konstellationen](https://konstellationen.org/bli-medlem/) (där jag själv är ordförande) eller [Föreningen för Digitala Fri- och Rättigheter (DFRI)](https://www.dfri.se/bli-medlem/)
