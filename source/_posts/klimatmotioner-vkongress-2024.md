---
title: Här är mina favoritmotioner om klimat och miljö
image: environmental-protection.jpg
image_description: <a href="https://pixabay.com/photos/environmental-protection-326923/">Bild på jordklot, Pixabay</a>. 
description: På kongressen i maj 2024 så tar Vänsterpartiet bland annat ett nytt partiprogram. Här är mina favoritmotioner
date: 2024-04-30 19:00
---

Äntligen så är det dags för [kongress](https://www.vansterpartiet.se/kongress2024/)! Det blir en riktigt lång kongress på fem dagar så man kommer vara ordentligt mör i huvudet efter. Det som är roligt i Vänsterpartiet är att även enskilda medlemmar kan skriva motioner. Det får också effekten av att det blir väldigt många motioner. Det är [987 motioner på partiprogrammet](https://www.vansterpartiet.se/wp-content/uploads/2024/04/b-motioner.pdf). 

Programkommissionen har gjort ett väldigt stort arbete med att skriva ett första förslag och sen genom att läsa igenom motioner och [svara på dem](https://www.vansterpartiet.se/wp-content/uploads/2024/04/b-motionssvar.pdf). De har tagit intryck av motionerna och gjort ett [reviderat förslag](https://www.vansterpartiet.se/wp-content/uploads/2024/04/programforslag-pkmotionssvar-kongress2024.pdf). Jag är glad att vi har kamrater som är villiga att ta ansvaret och tiden att skriva ett förslag på partiprogram med allt det innebär.

Det är en maffig uppgift att läsa igenom alla motioner för oss som åker på kongressen. Jag kan inte säga att jag har läst alla motioner till partiprogrammet jättenoga och jag är nog inte ensam om det.

Därför är det bra att få tips av andra kamrater vilka motioner de tycker är bra. Så att man kan se till att läsa några få motioner lite närmare.

Jag har fokuserat på att läsa motioner som har att göra med klimat och miljö på något sätt lite närmare. Så jag tänkte presentera vilka mina favoritmotioner till partiprogrammet är.

## B 37 Ludvika - Förtydliga Vänsterpartiets hållning i miljö- och klimatfrågor

Motionärerna tycker att vissa formuleringar kan vässas till kring klimat och miljö.

Föreslår kongressen besluta

* att meningen ”Det moderna samhället har ett överdrivet fokus på produktivitet och tillväxt vilket leder till att vi överutnyttjar de ekosystem och de ändliga resurser som vi har tillgång till.” läggs till på rad 4 efter ordet ”jämlikhet” och innan den mening som börjar med ordet ”Vi”

Radreferens: 4

Förslag från programkommissionen: avslag

## B 82 Hammarby-Skarpnäck - Mer fokus på biologisk mångfald och skydd av värdefull natur – del 3

Motionärerna reagerar på att biologisk mångfald inte nämnts men att det är en viktig fråga, inte minst för att det är en förutsättning för vår matproduktion.

Föreslår kongressen besluta

* att meningen på rad 32 utökas med “inom de planetära gränserna” och därmed lyder: "De allra flesta vinner på en politik för trygghet, frihet och jämlikhet inom de planetära gränserna."

Radreferens: 32

Förslag från programkommissionen: avslag

## B 238 Vänsterpartiet Göteborg - Ändringar av kapitlet Ekologiska kriser med start på rad 384

Motionärerna tycker att det måste framgå tydligare att vi nu står inför både en klimatkris och ekologisk kris.

Föreslår kongressen besluta:

* att nedanstående skrivning ersätter den nuvarande från rad 384-403: ”Planetär ekologisk kris Vi befinner oss i en ekologisk global kris orsakad av ekonomiska system som ensidigt strävar efter ökad tillväxt och maximala vinstuttag. Fossilanvändningen är en av de största anledningarna till klimatförändringarna, men absolut inte den enda. Den livsviktiga ekologiska mångfalden rubbas av hänsynslös exploatering av naturtillgångar. Skövling av skog, rovfiske i haven kombinerat med monokulturer, konstgödning och kemikalieanvändning både inom jordbruk och industrin är ytterligare orsaker till den klimat- och miljökris vi är mitt uppe i med massdöd av arter i såväl växt- som djurlivet. Vi står inför vägval som är oåterkalleliga. Vi behöver utveckla nya hållbara sätt att arbeta på en lång rad områden som bygger på en genomtänkt omsorg om livets villkor. Hela samhället måste ställa om i allt från industrier och transportsystem till ändrad livsstil. Omställningen ska ske rättvist sett både ur ett nationellt och ett globalt perspektiv. De rikas avtryck medför större ansvar och den rika världens och slit- och slängkultur med överkonsumtion av varor och naturtillgångar kan inte fortsätta som nu.” (Kapitlet fortsätter med nuvarande skrivning från raderna 404-409.)

Radreferens: 384

Förslag från programkommissionen: avslag

(Även B 239 och B 243 är bra och ger förslag på ändringar på samma text)

## B 253 Enskild - Ett vänsterparti på ekologisk grund del 3

Motionären vill lägga till kritik kring slit och släng.

Föreslår kongressen besluta:

* att meningen på rad 393-396 ”Mänsklighetens begränsade tekniska förmåga och sätt att organisera arbetet har lett till att kretslopp överbelastats och naturresurser förbrukats i snabbare takt än de förmått återhämta sig.” ersätts med: ”Kapitalismens rovdrift på naturen för kortsiktig vinst har lett till att naturliga kretslopp förstörts och ersatts av en linjär slit-och-släng-produktion med enorma mängder skadligt avfall som följd. Resurser har förbrukats i snabbare takt än naturen hinner återskapa dem och det skulle krävas många fler jordklot om alla i världen konsumerade såsom den rikare delen av världen gör idag. Samtidigt vet vi att alla kan få en god modern levnadsstandard med långt lägre resurs- och energianvändning. Det som krävs är att resurserna används smartare och att de fördelas mycket mer jämlikt. De rikas överkonsumtion hotar idag allas vår tillgång till det grundläggande."

Radreferens: 393

Förslag från programkommissionen: avslag

## B 262 Karlstad - Lägg till att ständig tillväxt är en av grundorsakerna till klimatförändringarna

Motionärerna vill få fram tydligare kritik mot tillväxt.

Föreslår kongressen besluta:

* att meningen på rad 404 som börjar ”Kapitalets brist på hänsyn” ändras till ”Utöver kapitalets krav på ständig tillväxt är dess brist på helhetssyn på samhället en av grundorsakerna till att situationen gått så långt som den gjort.”

Radreferens: 404

Förslag från programkommissionen: avslag

## B 267 Vänsterpartiet Göteborg - Tillägg till kapitlet Ekologiska kriser från rad 409

Motionärerna vill få in en skrivning om hur det internationella kapitalet och den globala marknaden hotar urbefolkningars habitat och livsstilar.

Föreslår kongressen besluta:

* att det efter rad 409 tillkommer ett nytt stycke med följande innehåll: ”Därtill kommer att kapitalet är en internationell aktör. Globala marknader och internationella handelsavtal suddar ut nationers gränser och demokratiskt framtagna beslut och spelregler. Internationella bolag exploaterar naturtillgångar i hela världen och åsidosätter ursprungsbefolkningars rätt till land och levebröd. Det händer överallt, även i vårt land, där samernas möjligheter att bedriva renskötsel förbises framför gruvbolags intressen. Befolkningen i den fattiga världen får betala med omänskliga arbetsvillkor och barnarbete. Vi måste gå i bräschen för att utveckla den internationella solidariteten så att arbetares villkor och rättigheter stärks världen över.” (Sen fortsätter nuvarande texten från rad 410.)

Radreferens: 409

Förslag från programkommissionen: avslag

## B 277 Vänsterpartiet Göteborg - Individens ansvar för privatkonsumtionen

Motionärerna vill få in skrivningar om att den privata överkonsumtionen är ett problem och att den drivs på av kapitalet.

Föreslår kongressen besluta:

* att på rad 419 efter meningen som slutar ”...än att låta dem komma till nytta.” lägga till: ”Vi har alla ett kollektivt ansvar att minska den privata konsumtionen, men det är kapitalet som driver oss till överkonsumtion.”

Radreferens: 419

Förslag från programkommissionen: avslag

## B 295 Vänsterpartiet Göteborg, Hammarby-Skarpnäck, Örnsköldsvik, m.fl. - Rad 456 Tillägg om miljö- och klimatrörelser

Motionärerna vill nämna miljörörelse och klimatrörelser som drivit på för att skapa opinion och möjliggöra för progressiva partier att lägga förslag som öppnar vägarna framåt.

Föreslår kongressen besluta:

* att på rad 456 före ”organisera” lägga till ”med hjälp av miljö - och klimatrörelser”

Meningen blir då:

”Det har gått att med hjälp av miljö- och klimatrörelser organisera politiska krafter som varit starka nog att utmana ägarintressen och öppna nya vägar framåt”

Radreferens: 456

(B 296 är en liknande motion)

Förslag från programkommissionen: avslag

## B 297 Vänsterpartiet Göteborg, Centrum - De planetära gränserna är utgångspunkten för partiets miljö- och klimatpolitik

Motionärerna vill ha en tydligare skrivning om den forskningsbaserade kunskapen som miljö- och klimatpolitiken måste bygga på. Samt nämna vilka partier är i vägen.

Föreslår kongressen besluta:

* att ersätta rad 461-471 med följande skrivning: ”Klimat - och miljöpolitiken måste styras av de planetära gränserna för att jorden skall vara kvar i ett stabilt tillstånd och inte överskrida brytpunkter – irreversibla förändringar i klimatet och miljön. Borgerliga och auktoritära partier i Sverige motverkar genomförandet av en ansvarsfull miljö- och klimatpolitik och skjuter problemen till framtida generationer. I tider av ekonomisk kris blir det än svårare att driva igenom en effektiv miljö- och klimatpolitik, men inte omöjligt. Miljöforskningen visar att acceptans för klimat- och miljöpolitiska åtgärder kommer genom att åtgärderna är effektiva och tar hänsyn till en rättvis fördelning av negativa effekter.”

Radreferens: 461

Förslag från programkommissionen: avslag

## B 304 Vänsterpartiet Göteborg, Hammarby-Skarpnäck, Sundsvall, Örnsköldsvik, m.fl. - Rad 472-479 Omformulering med bredare perspektiv på klimatomställningen

Motionärerna vill ha tydligare skrivningar kring hur vi kan nå utläpssminskningar.

Föreslår kongressen besluta:

* att rad 472-479 omformuleras till ”Klimatkrisen ö ppnar för nya, bättre sätt att organisera samhället. Vänsterpartiet ser att effektiva utsläppsminskningar kan nås genom nya tekniska innovationer som innebär att utsläppen minskar, beteendeförändringar, minskad konsumtion och förändrad efterfrågan. Samt att återställandet av våtmarker och bevarande av skog ytterligare kan minska utsläppen. Investeringar, innovation och storskaliga samhälleliga projekt kan ge de största effekterna i klimatomställningen, som i produktionen och genom satsningar på förnybar energi, järnväg och kollektivtrafik eller på renovering av miljonprogrammet med bra miljöval och energieffektivitet. Vi utgår från forskning och behöver hitta strategier med ett tydligt rättviseperspektiv och förena det gröna perspektivet med ett rött för att vinna folkligt stöd.”

Radreferens: 472

Förslag från programkommissionen: avslag

## B 308 Majorna - Angående tillväxt

Motionärerna vill få in ett stycke om tillväxtkritik. Det är samma stycke som föregående motion jag tipsade om (B 304) också vill ändra.

Föreslår kongressen besluta:

* att följande stycke ersätter raderna 472-479 i programförslaget: “Grunden i den kapitalistiska ekonomin är tillväxt, inte av nyttigheter utan av vinst. Därför har det överordnade målet i politiken blivit en ständigt ökande BNP, en villkorslös tillväxt, utan koppling till mänskliga behov eller naturens gränser som inte skiljer mellan fler stridsflygplan och bättre sjukvård. För den stora majoriteten, vi som lever av vårt arbete, är tillväxt varken bra eller dåligt i sig. Det avgörande är vad som växer och vilka konsekvenser det får för oss och vår livsmiljö. För kapitalet däremot är innehållet ointressant, bara det ger ökad vinst. Att denna tomma tillväxt är alltings måttstock beror inte på girighet eller bristande kunskap. Det är inte ens ett systemfel. Den är systemet. Samma drift som en gång fick kapitalismen att ställa dittills otänkbara resurser till mänsklighetens förfogande hotar idag vår överlevnad. En obegränsad tillväxt på en begränsad planet är omöjlig. Därför blir alla visioner om ”nolltillväxt” och ”frikoppling” bara luftslott så länge vi respekterar ramarna för det system som för sin överlevnad kräver evig expansion. Den frikoppling som krävs är alltså mellan samhällets ekonomi och kapitalets vinst. Först när ett sådant skifte sker kan en behovsstyrd, långsiktigt hållbar ekonomi bli verklighet. Vi kallar ett sådant samhälle där den blinda vinstjakten ersätts med demokratiska styrmedel för socialism.”

Radreferens: 472

Förslag från programkommissionen: avslag

## B 417 Jönköping - Effektivt och rättvist viktiga ledord

Motionärerna vill ändra på meningen: ”En framgångsrik rörelse för ett bättre samhälle behöver bygga på stabil forskning och förslag som visar sig effektiva och populära den dag de genomförs.”

Motionärerna tycker att det ska stå att förslag som läggs ska vara rättvisa, inte populära. 

Föreslår kongressen besluta:

* att på rad 751 ska ordet ”populära” bytas mot ”rättvisa”

Radreferens: 751

Förslag från programkommissionen: avslag

## B 419 Vänsterpartiet Göteborg, Hammarby-Skarpnäck, Sundsvall, Trosa, Örnsköldsvik - Rad 754 Tillägg om klimatomställning för att säkra människors grundläggande behov och trygghet

Motionärerna vill ändra följande mening: ”Vi vill genomföra strategiska reformer som både gör livet rikare och friare, här och nu, och bygger upp människors möjligheter att driva sina intressen framöver”.

Föreslår kongressen besluta:

* att efter ”framöver” på rad 754 lägga till meningen ”Vi vill genomföra klimatomställning och skydd av natur för att säkra människors grundläggande behov och trygghet.”

Radreferens: 754

Förslag från programkommissionen: avslag

## B 477 Vita Bergen, Västra Södermalm, m.fl. - Vision för en levande framtid

Motionärerna vill få in en mer hoppfull skrivning om hur vi kan lösa klimat- och artkrisen.

Föreslår kongressen besluta

* att avsnittet på rad 857 får rubriken ”En levande och rättvis framtid” och därefter inleds med ett nytt stycke som lyder: ” Allt oftare beskrivs klimatkatastrofen som oundviklig, men vi vet att framtiden ligger i våra händer. Vår kunskap ger oss medel att möta det nya så att utsläppskurvorna vänder och så att omställningen blir en positiv väg mot ett rättvist samhälle. Jordbruket ska ställas om till hållbar odling som ger föda åt alla, subventionerna till storskalig animalieproduktion och skadlig diesel fasas ut. Skogarna ska brukas så att de är en tillgång för planeten och oss alla. Vinsterna från råvaror och energi ska fördelas så att de gynnar de platser där utvinningen sker. Kärnkraftsutbyggnad ska inte subventioneras, det vi behöver är en snabb omställning till säker och förnybar energi. Nya storskaliga energianläggningar för till exempel havsbaserad vindkraft ska inte ägas av utländska spekulanter utan vara gemensamt ägda så att elkraft och ekonomiskt överskott kan fördelas för samhällets bästa. Konsumtionen av ändliga resurser måste minska: i industrin, hos de rika men också hos oss alla. I stället ska våra liv ha plats för det som ger verkligt välbefinnande – tid för varandra, för vila och personlig utveckling och för ett rikt kulturliv.”

Radreferens: 857

Förslag från programkommissionen: avslag

## B 506 Vänsterpartiet Göteborg - Förtydligande om allas roll i vägen ut ur fossilberoendet

Motionärerna vill göra det mer tydligt att alla kommer att påverkas och behöva bidra i omställningen, att det inte kommer räcka med teknikutveckling.

Föreslår kongressen besluta:

* att texten på raderna 865-868 ersätts med texten: “Den behöver ge människor verktyg för att kunna minska sin klimat- och miljöpåverkan och bygga på förtroende att de flesta människor gör sitt bästa i vardagen. Omställningen kommer att beröra oss allihop, men den ska ske rättvist. De som står för den största förstörelsen kommer behöva bidra mer.”

Radreferens: 865

Förslag från programkommussionen:

* att följande mening läggs till på rad 868: "Den behöver bygga på rättvisa och tydlighet i att de som står för de största utsläppen är först med att förändra sina vanor."
* att motion B 506 anses besvarad med det ovan anförda

## B 709 Hammarby-Skarpnäck - Mer fokus på biologisk mångfald och skydd av värdefull natur – del 8

Motionärerna vill få in skrivningar om skogen.

Föreslår kongressen besluta:

* att stycket på rad 1177-1183 ändras på följande sätt: "Hur vi använder skogen har stora och komplexa konsekvenser, som bland annat handlar om hur vi binder kol i naturen och i produkter och hur vi skyddar den hotade artrikedom som finns där. Det behövs ett stärkt skydd av kvarvarande gammelskog och skog med rika ekosystem och en hårdare reglering av skogsbruket, som utgår från en helhetssyn med långsiktigt perspektiv för kommande generationer och inte ensidiga vinstintressen. Det ska däremot vara mer lönsamt att driva ett natur- och klimatvänligt skogsbruk, även i småskalig form. Biomassan är en begränsad resurs, särskilt i relation till världens klimatomställning. Det kommer att vara viktigt att prioritera användningen av den klokt."

Radreferens: 1177

Förslag från programkommissionen: avslag

## B 716 Vänsterpartiet Göteborg, Hammarby-Skarpnäck, Norrtälje, Nyköping, Sundsvall, Trosa, Örnsköldsvik, m.fl. - Omformulering om energi med tillägg nej till ny kärnkraft, rad 1188-1190

Motionärerna vill få in tydligt om nej till kärnkraft.

Föreslår kongressen besluta

* att texten på rad 1188 efter ”fossil energi” till och med rad 1190 fram till ”Nya projekt” ersätts med: ”Vänsterpartiet arbetar för ett samhälle med hållbar energiförsörjning ur ett ekonomiskt, miljömässigt och socialt perspektiv. För att minska beroendet av fossil energi krävs stora satsningar på förnybar energi, energieffektivisering och energihushållning. Vänsterpartiet säger nej till ny kärnkraft. Det är inte bara en osäker och riskfylld energikälla, den är också dyr och försvårar omställningen till en förnybar energiproduktion. Det finns inte heller någon säker metod för slutförvaring av det radioaktiva avfallet. Vi säger också nej till prospektering och brytning av uran i Sverige”.

Radreferens: 1188

Förslag från programkommissionen: avslag

## B 746 Årsta, Örnsköldsvik, m.fl. - Tydligare om klimatomställning inom transportområdet, rad 1212-1217

Motionärerna vill få in skrivningar om minskat bilberoende.

Föreslår kongressen besluta:

* att rad 1212-1217 ersätts med texten: “Vi vill se en samhällsplanering som underlättar en utveckling mot minskat bilberoende. En sammanhållen strategi som bygger på kollektivtrafik, delade fordon, offentligt stöd för elektrifiering, och planering för ökad närhet till offentlig och privat service och välfärd både i städer och på landsbygden.”

Radreferens: 1212

Förslag från programkommissionen: avslag

## B 913 Vänsterpartiet Göteborg, Sundsvall, Örnsköldsvik - Tillägg om planetära gränser i avslutningen

Motionärerna vill få in om att vi ska hålla oss inom de planetära gränserna.

Föreslår kongressen besluta:
* att på rad 1489 efter den mening som avslutas med ”… politiska förslag” lägga till: ”Vi förenar våra politiska förslag med de planetära gränser som vetenskapen pekar ut som villkor för en långsiktigt hållbar utveckling.”

Radreferens: 1489

Förslag från programkommissionen:

* att på rad 1489 lägga in följande mening: ”Vi är beredda att lyssna på forskningen och förena de insikterna med politisk handling.”
* att motion B 911, B 912, B 913, B 914, B 915, B 916 anses besvarad med det ovan anförda

## B 922 Vänsterpartiet Skaraborg, Trosa, m.fl. - Fredlig civil olydnad är en viktig del av demokratin

Motionärerna vill få in en skrivning om civil olydnad, en fråga som drabbar klimat- och miljörörelsen om man slår ner hårdare på den.

De föreslår att stoppa in nuvarande partiprograms skrivning i det nya:

”Vänsterpartiet kämpar för att förändra det vi uppfattar som felaktiga beslut den parlamentariska vägen. Men demokrati är inte bara lagar och regler. Civil olydnad är en yttersta möjlighet för människor som saknar makt att på andra sätt försvara folkligt förankrade värden som mänskliga rättigheter och naturresurser. Civil olydnad grundar sig på principen om icke-våld och öppenhet.”

Föreslår kongressen besluta

* att stycket om civil olydnad som finns i vårt partiprogram idag (citerat i texten ovan) läggs in i det nya partiprogrammet, efter rad 1508

Radreferens: 1508

Se även B 405 och B 571 som föreslår lägga in samma text fast på annan plats.

Förslag från programkommissionen: avslag