
---
title: Nyårslöfte 2024 - Tankar om skrivande och fasta
image: blog-more.jpg
description: Några tankar om skrivande, veganism, fasta, fokus och ambitioner
date: 2023-12-30 16:00
---
Då sitter jag på ett tåg för att fira nyår i Hudiksvall. (Blev visst ett blogginlägg förra gången jag satt på tåg dit). Jag funderar på det här med nyårslöfte eller nyårsambitioner för jag tänker inte lova något, varken till mig själv eller någon annan.

Men att sätta upp mål för en begränsad tid tror jag verkligen på. Inom många religioner så är längre period av fasta en återkommande grej. Inom ortodox kristen tradition så fastar man inför påsk från animalisk mat, alltså man käkar vegansk mat en månad. Inom kristenheten i Sverige (iaf de flesta frikyrkor och Svenska kyrkan) så är fastan inte lika strikt och inte lika tydlig. Man gör liksom lite vad man själv vill som man tror kan vara bra för ens andliga liv. En klassiker nu för tiden är att fasta från sociala medier för att det tar för mycket tid. Andra fastar från godis och det har blivit vanligare att fasta från kött eller animalier.

## Fasta och veganism

Det var genom fastan som jag började mitt veganska liv, tror det var fastan inför påsken 2012. Frugan och jag var både vegetarianer sedan lång tid tillbaka. Men vi hade fått upp ögonen för det djurförtryck som fanns inom mjölk- och äggindustrin och även fått upp ögonen för industrins klimatpåverkan. Så vi hade börjat skära ner på alla animalier. Men vi bestämde oss att för att helt avstå från animalier under fastan som är 40 dagar, söndagar undantagna. Vi hade tidigare fastat från alkohol under tidigare fasteperioder så vi tänkte det var ett fungerande koncept. Att käka veganskt under fastan brukar kallas paradisfasta och föreningen Vildåsnan har uppmuntrat kristna att köra en sådan fasta.

Fastan blev för oss ett startskott att käka veganskt och därefter märkte vi att det gick bra så vi fortsatte. Här vill jag ändå understryka att poängen med fastan är att tillfälligt göra avkall på saker eller tillföra andra delar till sitt liv. Man måste inte fortsätta med det efter fastan är slut. Då skulle själva fasteperioden förlora lite sin poäng. Men om man har tankar på att göra förändringar i sitt liv så fungerar fastan bra som en testperiod. Då blir det dessutom bara en bonus om man fortsätter.

Nu måste man inte vara religiös för att under perioder göra förändringar i sitt liv. [Veganuary](https://veganuary.com) är exakt samma sak fast ur ett sekulärt djurrättsperspektiv. Det är en årlig utmaning som drivs av den icke-vinstdrivande organisationen Veganuary med syfte att få folk att äta veganskt under januari (och förhoppningsvis fortsätta med det sen). I Sverige har Djurens Rätt drivit utmaningen Veganuari med samma syfte. Organisationerna hjälper till med menyer, pepp, kunskap och tips. Under januari 2024 driver vegosajten [Jävligt Gott veganuari](https://javligtgott.se/bloggen/veganuari-med-javligt-gott).

## NaNoWriMo - Skriva under november

Konceptet att göra en specifik sak under t.ex. en månad finns kring det mesta. När jag skrev ett blogginlägg om mina tankar kring skrivande så fick jag ett tips av en vän på Mastodon om något som kallas [NaNoWriMo](https://nanowrimo.org) (National Novel Writing Month). Det är en icke-vinstdrivande organisation i USA som driver projektet. Det är en utmaning om att skriva 50 000 ord under november månad. Det hela startades 1999 av frilansskribenten Chris Baty då med 21 deltagare. Två år senare deltog 5 000. År 2022 deltog mer än 400 000 personer. Det kostar ingenting att vara med och det delas inte heller ut några priser. De som vinner är de som helt enkelt håller ut hela november och vid slutet av november har skrivit minst 50 000 ord. Det kan vara en komplett novell, roman eller annat färdigt verk eller så är det ett verk som man jobbar vidare med.

Jag tycker det låter sjukt skoj att vara med på det! Så kanske att jag deltar 2024. Meeeen jag vill nog inte riktigt ha det som ett nyårslöfte. Men jag har en annan ambition: blogga!

## Blogga mera under 2024

Jag har bloggat mycket när jag var aktiv i Vänsterpartiet i Haninge typ 2014-2020. Bloggat privat började jag med för tre år sedan. Det blev en handfull inlägg under 2021 som jag publicerade på https://tofu.bitar.se med fokus på teknik, öppen källkod och övervakning. 2022 så drog jag igång den här bloggen. Tanken var att blogga lite bredare här, inte bara om teknik. Under 2023 så har det blivit 15 inlägg.

Mitt nyårslöfte (nja, ambition snarare) är att blogga minst en gång per vecka. Men då undantar jag storhelger och sommar. Så med andra ord, de flesta vanliga jobbveckor så tänkte jag skriva minst ett blogginlägg i snitt. Så ungefär 35-40 inlägg under året borde det bli om jag skippar juni, juli, augusti och någon storhelg.

Egentligen så är nog ett år lite för långt för att sätta upp ett mål kring. Men min förhoppning är att komma in i lite vana att blogga. Inte nödvändigtvis att alla blogginlägg är toppkvalitet men att minska trösklarna för att komma igång och skriva.

## Några hinder?

Jag har funderat en del på det här med vad som är hinder för mig att komma igång med att blogga och hur jag kan komma över de hindrena. Det jag tänker på just nu är:

* Hitta tid för att skriva
* Ha något att skriva om
* Hitta bilder till inläggen
* Lägga upp grejerna på bloggen

### Tiden

Det vanligaste och svåraste för de flesta är väl tid. Jag har inga större problem med att komma igång med att skriva. (Sen hur bra det är får andra bedömma). Men ofta tar det längre tid än vad jag tror att skriva ett blogginlägg. Så därför kan jag gå länge och fundera på att skriva ett blogginlägg men att det aldrig blir av för jag har för höga (?) ambitioner kring det. Jag var t.ex. på BornHack i somras vilket var en fantastisk upplevelse! Jag hade så många saker att skriva om! Jag tänkte skriva om mitt webbläsarspel som jag skrev i ett game jam, om kamrater jag lärt känna, om de politiska diskussionerna som väckts (t.ex. om Chat Control), om CTF-challenges som jag gärna skulle varit med på, om de små kodarstunderna jag hade med andra hackare och en massa annat. Men det blev inte av. Det som däremot blev av var ett kort blogginlägg om att [skriva blogginlägg på Android](/blogga-telefonen/) som jag faktiskt skrev under själva BornHack. (Där kanske ledtråden är att hellre kort än inget alls).

### Ha något att skriva om

Gällande det här med att ha något att skriva om så kan man lösa det på lite olika sätt. Ett sätt är att skriva ett blogginlägg om något intressant om jag har någon idé och se till att skriva ner mina bloggidéer i ett dokument. Så när jag hittar tid för att blogga så tittar jag i dokumentet. Och sen kan jag ha som fallback att skriva om intressanta artiklar jag läst. Så min rutin kan se ut så här: jag skriver ner idéer på intressanta blogginlägg i ett dokument. I ett annat (eller via annat verktyg) så sparar jag ner länkar till intressanta artiklar, poddar eller annat som jag tagit del av under veckan. Cory Doctorow använder sin egen blogg i sin skrivarprocess, som någon slags databas. Han skriver blogginlägg dagligen. Hans hemsida heter just [Pluralistic: Daily links from Cory Doctorow](https://pluralistic.net). Han börjar alla (?) inlägg med rubriken "Today's links" och listar de länkar han sedan skriver om. Så min tanke är att kopiera det upplägget lite men snarare ha "Veckans länkar".

### Bilder

Jag gillar ju att ha bra bilder till mina blogginlägg. Det är ofta också det (tror jag) som kan locka många till att läsa ett blogginlägg. Så generellt sätt så kan det vara en bra idé att välja bilder med omsorg. Men det är nog bäst att välja bilder allra sist, när själva texten är färdig. Jag använder mest Pixabay och Unsplash. En variant är att helt skippa bilder men det hade tagit emot lite. Kanske att jag skulle fixa en uppsättning av några standardbilder som man kan växla mellan och som jag redan har uppladdade på bloggen så jag bara behöver länka till dem.

### Lägga upp på bloggen

Jag använder mig av static site generatorn [Hexo för att skriva blogginlägg](/en-liten-blogg/). Det innebär att jag skriver textfiler i så kallat markdown-format och att jag sedan "committar" de textfilerna till mitt repo på Gitlab. Då kickas en process igång som genererar filer och sedan görs åtkomliga på https://samuels.bitar.se. Det funkar väldigt bra! Men det finns några nackdelar.

Det funkar toppenbra om jag sitter med min personliga laptop. Men krångligare om jag sitter med min jobblaptop. Det här är så klart något jag kan lösa genom att sätta upp git, ssh och allt sånt som behövs men jag har än så länge inte pallat fixa. Det jag då får göra är att surfa in på https://gitlab.com, logga in med mitt gitlab-konto, gå till mit repo, öppna Web IDE och ändra direkt där. Nackdelen är att jag då inte kan förhandsgranska resultatet så eventuella brutna länkar eller pajade bilder märker jag först när inlägget är publicerat. Just ja, det är också rätt meckigt att publicera blogginlägg från min mobil. Vilket för övrigt var det jag löste och beskrev i [blogginlägget på Android](/blogga-telefonen/). Det funkar på mobilen men är inte smidigt, speciellt inte om jag har bilder jag vill ha med.

För den som är nyfiken på static site generator så kan jag rekommendera följande guider jag skrivit på Konstellationens wiki:

* [Starta blogg med Gitlab Pages](https://wiki.konstellationen.org/sv/guider/blogg-med-gitlab)
* [Skriva blogginlägg med Gitlab Pages och Hugo](https://wiki.konstellationen.org/sv/guider/skriva-blogginlagg-med-gitlab-pages)

## Skriva om och med fler

Jag skulle verkligen vilja få igång fler inom vänstern att blogga. Jag har skrivit mycket om varför vi behöver göra oss mindre beroende av Big Tech sociala medier. Men det viktigaste är att vi skapar plattformar som vi har kontroll över. Jag tror att personliga bloggar är det bästa sättet att komma igång. Men det är också viktigt att vi har plattformar där vi bestämmer. Självklart spelar vänstertidningar (som Dagens ETC, Flamman, Arbetaren, Internationalen, m.fl.) en väldigt viktig roll och det är viktigt att stödja dem. Men det är också viktigt att vi har bloggar och debattportaler.

Ett initiativ som är värt att inspireras av och titta mer på är Rörelsen, ett slags debattforum och portal för socialdemokratisk diskussion. Där har jag för övrigt skrivit en text om Mediakollen så det är inte bara sossar som får skriva där.

Ett gäng vänsterpartister har dragit igång ett liknande projekt: [Vänsterdebatt](https://vansterdebatt.se). Jag hoppas det kan få lite luft under vingar. Kanske jag borde skicka in lite texter dit?

Och så självklart så har vi Mediakollen som vi i Kamratdataföreningen Konstellationen har utvecklat. Där har vi dessutom ett block på förstasidan för utvalda blogginlägg.

Här är några inlägg på temat bloggande och alternativ till Big Tech:

* [Samuel Skånberg: Mediakollen utmanar det borgerliga nyhetsurvalet](https://www.tidningenrorelsen.se/p/samuel-skanberg-mediakollen-utmanar), [Mediakollen tar emot tips på tidningar, bloggar, poddar och vill tipsa om medlemsskap ](https://konstellationen.org/2023/10/26/mediakollen-tips/), [Intresset för alternativ till stora plattformar är stort](https://konstellationen.org/2023/08/20/intresset-for-alternativ-stort/) och [Ett annat internet är möjligt](https://konstellationen.org/2023/06/12/ett-annat-internet-ar-mojligt/)
* [Vänstern behöver en egen infrastruktur på nätet](https://blog.zaramis.se/2023/12/26/vanstern-behover-en-egen-infrastruktur-pa-natet/), [En gång hade vänstern en egen infrastruktur på nätet](https://blog.zaramis.se/2023/12/28/en-gang-hade-vanstern-en-egen-infrastruktur-pa-natet/) och [Är det över för internet?](https://blog.zaramis.se/2023/12/29/ar-det-over-for-internet/) av vänsterbloggaren Anders Svensson
* [12 år av Internet, anarki & jävlar anamma!](https://www.turist.blog/2023-08-16-12-ar-av-anarki-och-javlar-anamma/), [Internet är en skitstorm](https://www.turist.blog/2023-06-08-internet-ar-en-skitstorm/) och [Ökenvandring](https://www.turist.blog/2023-11-24-okenvandring/) av Jesper Nilsson
* [Vad ska vi göra för att överleva sociala medier-döden? Jag har ett förslag (spoiler: det handlar om RSS)](https://detendaalternativet.home.blog/2023/05/10/vad-ska-vi-gora-for-att-overleva-sociala-medier-doden-jag-har-ett-forslag-spoiler-det-handlar-om-rss/)

Så en målsättning jag också har är att kommentera om vad andra bloggare skrivit om för att få igång lite mer diskussion. Och länkar till vänstertidningar också så klart. Mediakollen har ett bra [flöde med bloggar](https://mediakollen.org/feed). Några bloggar som jag kommer försöka hålla extra koll på är:

* [Arbetarbloggen](https://mediakollen.org/feed/29/curated)
* [Clarté](https://mediakollen.org/feed/121/curated)
* [Det enda alternativet](https://mediakollen.org/feed/105/curated)
* [Det glada tjugotalet](https://mediakollen.org/feed/106/curated)
* [Gnomvid](https://mediakollen.org/feed/170/curated)
* [Guldfiske](https://mediakollen.org/feed/11/curated)
* [Jens Holm](https://mediakollen.org/feed/103/curated)
* [Laserjesus](https://mediakollen.org/feed/150/curated)
* [Rörelsen](https://mediakollen.org/feed/18/curated)
* [SubRosa](https://mediakollen.org/feed/27/curated)
* [Svenssons Nyheter](https://mediakollen.org/feed/94/curated)
* [Vänsterdebatt](https://mediakollen.org/feed/168/curated)
* [Turist](https://mediakollen.org/feed/52/curated)

Vem vet kanske fler blir inspirerade av det här. Och kanske att vi tillsammans drar igång någon kortare utmaning om att blogga och länka till varandra?

Hade varit superkul om du kunde börja blogga också!