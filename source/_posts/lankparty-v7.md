---
title: Länkparty - Chat Control i limbo, Right To Repair, Google lyssnar, deadline för IPv4-död i Tjeckien, AI-skit, open source sökmotor
image: network-chain2.jpg
description: Ett gäng intressanta länkar om teknik jag samlat på mig senaste tiden
date: 2024-02-13 09:00
---

Länkparty:

* [Nu förlängs Chat Control 1 – i väntan på Chat Control 3](https://femtejuli.se/2024/01/31/nu-forlangs-chat-control-1-i-vantan-pa-chat-control-3/)
* [This Guy Has Built an Open Source Search Engine as an Alternative to Google in His Spare Time](https://www.404media.co/this-guy-is-building-an-open-source-search-engine-in-real-time/)
* [New EU law sets to make repair more affordable for selected products, campaigners push for widespread right to repair](https://repair.eu/news/new-eu-law-sets-to-make-repair-more-affordable-for-selected-products-campaigners-push-for-widespread-right-to-repair/)
* [ChatGPT is violating Europe’s privacy laws, Italian DPA tells OpenAI](https://techcrunch.com/2024/01/29/chatgpt-italy-gdpr-notification/)
* [Google Update Shows How Bard AI May Work With Your Messages App](https://www.forbes.com/sites/zakdoffman/2024/01/28/new-details-free-ai-upgrade-for-google-and-samsung-android-users-leaks/)
* [Czech Republic sets IPv4 end date](https://konecipv4.cz/en/)

Och så lite avslutande tips på andra svenska bloggar.

## Chat Control 1 förlängs

https://femtejuli.se/2024/01/31/nu-forlangs-chat-control-1-i-vantan-pa-chat-control-3/

Vi lyckades stoppa Chat Control 2, tillfälligt i alla fall. Men Chat Control 1.0 gäller fortfarande. Det stora som skiljer Chat Control 1.0 är att det är **frivilligt** för plattformarna att implementera. Och om man inte vill riskera att bli övervakad så kan man skippa t.ex. Facebook och andra plattformar som frivilligt övervakar sina användare.

[EU-parlamentets LIBE-utskott](https://www.europarl.europa.eu/news/en/press-room/20240129IPR17212/child-sexual-abuse-online-meps-endorse-one-off-extension-of-current-rules) har godkänt en förlängning av Chat Control 1.0 i ett år.

Ylva Johansson som lade fram förslaget på Chat Control 2.0 [petas som EU-kommissionär](https://www.msn.com/sv-se/nyheter/inrikes/uppgifter-ylva-johansson-petas-som-eu-kommission%EF%BF%BDr/ar-AA1m59ql) så kanske att sossarna inte behöver vara hålla fast vid Chat Control 2.0 så hårt som man gjort.


## Stract - Open Source sökmotor

https://www.404media.co/this-guy-is-building-an-open-source-search-engine-in-real-time/

Om ni inte redan läst härliga 404 media så kan jag varmt rekommendera dem! Det är journalistgänget bakom Vice Motherboards podcast CYBER som slutade på Vice och startade eget mediaföretag. Hur som helst, de har skrivit om sökmotorn [Stract](https://stract.com) som är öppen och integritetsvänlig. Användaren har möjlighet att se vad som händer och kan konfa mycket själv. Koden är öppen på [github](https://github.com/StractOrg/stract).

Projektet [har fått stöd](https://nlnet.nl/project/Stract/) från [the NGI0 Entrust Fund](https://nlnet.nl/entrust/).

## Right to Repair Europe positiva till nya EU-lagen

Rätten att reparera sina prylar har varit en del av hackerkulturen länge. Men det har också varit en självklar del av konsumentrörelsen och av bönder som behöver [laga sina John Deere-traktorer](https://www.theverge.com/2023/1/9/23546323/john-deere-right-to-repair-tractors-agreement). 

Koalitionen Right to Repair Europe representerar mer än 130 organisationer och är positiva till nya EU-lagen som reglerar reperationer. Det gäller regler för rimliga priser på reservdelar, förbud mot mjukvara som hindrar oberoende reperationer och användning av kompatibla och återanvända reservdelar.

Reglerna gäller dock bara produkterna som EU ställer reperationskrav på. Det gäller bl.a. smarta telefoner och plattor, diskmaskiner, tvättmaskiner, frysar, servrar och svetsutrustning. Koalitionen kräver att fler produkter ska omfattas av reperationsreglerna.

EU-kommissionen kommer etablera en onlineplattform för att hjälpa konsumenter att hitta reperationsmöjligheter.

## ChatGPT bryter mot GDPR enligt Italien

https://techcrunch.com/2024/01/29/chatgpt-italy-gdpr-notification/

Italiens dataskyddsmyndighet menar att OpenAI:s chatbot ChatGPT bryter mot EU:s regler om integritet. Detta efter en månads lång utredning.

Böter mot GDPR kan uppgå till 20 miljoner euro eller upp till 4% av globala omsättningen.


## Googles AI Bard skannar dina meddelande

https://www.forbes.com/sites/zakdoffman/2024/01/28/new-details-free-ai-upgrade-for-google-and-samsung-android-users-leaks/

Forskare har avslöjat att i en pre-release av Google Messages har AI-funktionalitet lagts till som skannar ens meddelanden.

Googles AI-chatbot Bard kommer ha tillgång till dina meddelanden. Tanken är att Bard ska vara en slags personlig assistent. Den ska t.ex. kunna analysera dina meddelanden för förstå kontexten av konversationen, din ton och dina intressen.

Ett annat problem är att meddelanden kommer skickas till molnet för att bearbetas, användas för träning av AI:n och kanske även ses av människor (dock i anonymiserad form).

Så även om Google Messages kommer ha totalsträckskryptering så informeras Google själv. Det förlorar lite sin poäng. Det är liksom ingen poäng att ha totalsträckskryptering i sina konversationer om det visar sig att det är fler med i konversationen som man inte visste om.


## Tjeckien sätter dödsdatum för IPv4

https://konecipv4.cz/en/

IPv4 är den formen av IP-adresser som nog de flesta känner till. Aftonbladet.se är domännamnet till Aftonbladets hemsida. Men det översätts till en ip-adress: 35.71.185.87.

Bara en liten kul grej att den tjeckiska regeringen beslutade den 17 januari 2024 att godkänna materialet "Restarting the implementation of DNSSEC and IPv6 technologies in the state administration".

Det innebär att den 6 juni 2032 så kommer den tjeckiska statsapparaten inte tillhandahålla sina tjänster över IPv4.

Läs mer om [IPv6 på Wikipedia](https://sv.wikipedia.org/wiki/IPv6).

## Tips på svenska bloggar

Jag är så himla glad att Jesper är så aktiv på sin blogg! Några av hans senast alster:

* [Vilse i AI-djungeln med moralisk kompass men utan karta](https://www.turist.blog/2024-02-12-vilse-i-ai-djungeln/)
* [Veckans länkar: Ett konstigt internet med ett mänskligt ansikte](https://www.turist.blog/2024-02-10-veckans-lankar-ett-konstigt-internet/)
* [Veckans länkar: Hemarbete, övervakning och motstånd](https://www.turist.blog/2024-02-03-veckans-lankar-hemarbete-overvakning-motstand/)

Joel är också riktigt aktiv på sin blogg. Här är några av de senaste:

* [Knutpunkt, 2024-02-11: Pengar & politik.](https://gnomvid.se/2024/02/11/knutpunkt-2024-02-11-pengar-politik/)
* [Om »Norrsken«, av Arne Müller (2023)](https://gnomvid.se/2024/02/04/om-norrsken-av-arne-muller-2023/)
* [Knutpunkt, 2024-01-26: Gaza, ekologisk hushållning, samt »AI«:s politiska ekonomi och faktiska hot](https://gnomvid.se/2024/01/26/knutpunkt-2024-01-26-gaza-ekologisk-hushallning-samt-ais-politiska-ekonomi-och-faktiska-hot/)

Anna bloggar också, mycket om sci-fi. Jag läste detta fantastiska blogginlägg för ett tag sen: [Astronautdamer och damastronauter](https://annien.wordpress.com/2024/01/03/astronautdamer-och-damastronauter/) om hur kvinnor inte synliggörs i rymdfarten och vikten av sci-fi-böcker (och rymdböcker) även gestaltar kvinnor. Fick många bra tips på böcker som The Lady Astronaut of Mars.