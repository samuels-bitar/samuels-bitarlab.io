---
title: Organisera och må bra
image: ende-gelande-buddies.jpg
image_description: Massaktionen Ende Gelände mot kolgruvor i Tyskland. Bild av Ruben Neugebauerfrån <a href="https://www.flickr.com/photos/350org/20566765166/">Flickr</a>.
description: Träffa andra, snacka, anordna aktiviteter och gör skillnad i ditt och andras liv 
date: 2025-03-05 11:00
---

Två texter har gjort ett stort avtryck i mig senaste tiden:

* Boken Rörelsesocialism som jag [bloggat om här](/rorelsesocialismen/)
* Artikeln [You Can’t Post Your Way Out of Fascism](https://www.404media.co/you-cant-post-your-way-out-of-fascism/) av Janus Rose på 404media

Det var egentligen inte så mycket i det jag läste som var nytt för mig men det var paketeringen som gav upphov till aha-upplevelser.

I dessa tider när högerauktoritarism och fascism är på frammarsch i USA, Europa och Sverige så kan det kännas rätt tungt. Vi som vill se en annan inriktning måste lägga en större andel av vår tid att bygga nytt och bygga eget istället för att bara reagera.

Det är så mycket skit som händer och det känns som att man nästan lägger all tid på att bemöta det. Och det enklaste sättet att göra det är att posta på sociala medier. Det är det här "You Can’t Post Your Way Out of Fascism" handlar om:

> If there’s one thing I’d hoped people had learned going into the next four years of Donald Trump as president, it’s that spending lots of time online posting about what people in power are saying and doing is not going to accomplish anything. If anything, it’s exactly what they want.
> ...
> But perhaps the greatest of these sins is convincing ourselves that posting is a form of political activism, when it is at best a coping mechanism—an individualist solution to problems that can only be solved by collective action. 
> ...
> So what is the alternative? If we log off, what exactly are we supposed to do instead? How are we supposed to get information without constantly raising our antennae into the noxious cumulonimbus cloud of social media?
>
> It isn’t quite as simple as “touch grass,” but it also sort of is.
>
> Trusted information networks have existed since long before the internet and mass media. These networks are in every town and city, and at their core are real relationships between neighbors—not their online, parasocial simulacra.
>
> Here in New York City, in the week since the inauguration, I’ve seen large groups mobilize to defend migrants from anticipated ICE raids and provide warm food and winter clothes for the unhoused after the city closed shelters and abandoned people in sub-freezing temperatures. Similar efforts are underway in Chicago, where ICE reportedly arrested more than 100 people,  and in other cities where ICE has planned or attempted raids, with volunteers assigned to keep watch over key locations where migrants are most vulnerable.
> ...
> Some of these efforts were coordinated online over Discord and secure messaging apps, but all of them arose from existing networks of neighbors and community organizers, some of whom have been organizing for decades.

Artikel kändes för mig lite som en örfil och en smekning på kinden. Det här var saker som jag ändå visste om men också en påminnelse om att jag behöver göra det. Det är inte en fråga om aktivism i fysiska världen eller den digitala världen. Som författaren konstaterade använde man digitala plattformar för att mobilisera. Men syftet vara för att mobilisera i den fysiska världen.

## Saker jag gör i den fysiska världen

Det känns nästan löjligt med rubriken men det kan vara värt ändå att fundera över vad för saker jag gör i den fysiska världen där jag träffar andra och som känns meningsfulla.

* **Anordna matlag i kollektivhuset**. Jag är med i två matlag i [kollektivhuset Fristad](https://fristadkollektivhus.se/). Vi lagar mat tillsammans och erbjuder den till boende i huset. Det är ett trevligt sätt att lära känna grannar mer och roligt att erbjuda en mötesplats för andra boende i huset.
* **Anordna filmkvällar och pysseltillfällen i kollektivhuset**. Jag är också med i cafégruppen som anordnar enkla, trevliga och kostnadsfria aktiviteter.
* **Vara mötesordförande på kollektivhusmöten**. Verksamheten i kollektivhuset drivs av en förening som har husmöten fyra gånger per år. Det är roligt för det kommer ändå 30-40 personer på sådana möten.
* **Anordna hackathons med Konstellationen**. Det har varit något av det mest givande och roliga jag gjort. Jag får träffa massa goa kamrater och skapa ett forum för att kunna koda och hänga tillsammans. Det blir mest fokus på det logistiska, laga mat, osv, men ibland får jag tid över till kodande själv :)
* **Hålla föreläsningar om alternativen till Big Tech**. Jag har hållit några föreläsningar om Big Tech. Jag har besökt Vänsterpartiet i Haninge, Uppsala Linux User Group och har några fler inbokade.
* **Rollspelsgruppen**. Jag har nyligen gått med i en rollspelsgrupp. Vi är fem personer som träffas en gång i månaden. Det är opolitiskt men helt enkelt väldigt kul! Det är fint att träffa andra regelbundet och spela och umgås.

Sen gör jag otroligt mycket roligt med min fru. Men det är på en helt annan nivå som skulle kräva många blogginlägg. Jag nöjer mig med att säga att jag är otroligt glad jag får dela livet med henne och gör att mörka tider är lättare att hantera.

## Saker jag skulle vilja göra

Om jag inte hade behövt bry mig alls om att dygnet bara har 24 timmar så skulle jag vilja:

* **Gå med i en bokklubb**. Jag skulle vilja läsa socialistisk, kommunistisk, anarkistisk, feministiskt och antirasistisk politik. Det hade också varit najs att läsa sci-fi tillsammans med andra! Jag skulle vilja blanda facklitteratur med skönlitteratur.
* **Laga mat på Kafé 44**. Jag var på folkkök på Kafé 44 i helgen tillsammans med frugan och en granne. Det var [Solidaria socialt center](https://solidaria.space/) som anordnade folkkök. Det kom också folk från [Arbetarrörelsens arkiv och bibliotek](https://arbark.se/) och hade en intressant föreläsning om arkivering :)
* **Engagera mig i Solidaria**. Solidaria håller hus i Sundbyberg nära Duvbo T-bana. Det har börjat använda Organisera.org för att publicera sina events vilket jag tycker är kul! De har bland annat på gång: [Release av ”Instruktionsbok för kämpande hyresgäster: framgångsrika exempel och metoder”](https://organisera.org/events/4871fc26-8e41-4c22-8a24-b4e39bc530cb) och filmvisningen [Sara - My Whole Life was a Struggle](https://organisera.org/events/80193b6f-5a2d-4f33-86f0-96e3e7c4ac08)
* **Engagera mig i Allt åt alla**. Förra året deltog jag på några grejer med Allt åt alla. Vi var ett gäng som stack till KTH och deltog i tältlägret mot kriget i Gaza. Allt åt alla känns som ett kul vänstersammanhang. De driftar också lite egen IT-infrastruktur vilket jag gillar. De har [hemsida](https://alltatalla.se/), poddar i [Radio åt alla](http://radio.alltatalla.se/) och lite annat.
* **Gå på fler demonstrationer och föreläsningar**. Jag har varit engagerad länge i Vänsterpartiet och har också skaffat mig många nya kamrater och vänner i den utomparlamentariska vänstern. Därför råkar jag ju alltid på folk jag känner när jag går i 1 maj-tåget eller Palestinademonstrationer. Samma sak gäller föreläsningar av olika slag. Jag kan tipsa om sajten https://www.gnistor.se om man vill få koll på föreläsningar, demonstrationer och andra vänsterorienterade events som händer.
* **Delta på fler civil olydnadsaktioner**. Jag har deltagit i klimataktioner i Tyskland med Ende Gelände och i Göteborg med Folk mot fossilgas. Erfarenheterna från Ende Gelände gjorde stora avtryck i mig. Det var där jag kände i kroppen hur anarkistisk eller utomparlamentarisk icke-hierarkisk organisering kunde se ut. Det var väldigt lärorikt och jag vill gärna göra det igen.
* **Starta en vänsterteknikpodd**. Jag hade gärna startat en podd med några kamrater. Det är ett tillfälle att träffas och umgås men även göra något. Jag har pratat med en kamrat om det. Men vi har bägge rätt mycket grejer att göra. Men det hade varit himla kul!
* **Engagera mig i Hyresgästföreningen**. Vi har nyss startat upp en lokal hyresgästförening i huset! Några andra grannar har varit väldigt drivande och peppiga! Jag hade väldigt gärna varit med men känner att den kan bli för mycket. Men jag har bidragit med hjälp som mötesordförande på möten :)

Sen finns det massvis med fler saker :)

## Tips på vad du kan göra

Om du själv känner dig frustrerad över hur världen ser ut så vill du kanske få ännu fler tips på saker att göra.

Jag tänker egentligen att nästan allt man gör tillsammans, vare sig det är politiskt eller inte, är ett motstånd mot utvecklingen vi ser idag. Jag tänker kanske speciellt saker som är med arbetskamrater, studiekamrater eller grannar.

[Gnomvid skrev ett inlägg](https://gnomvid.se/2025/03/04/knutpunkt-2025-03-04-stormaktspolitik-och-manniskovarde-trump-2-tyskland-ater-till-vittangi-kulturens-och-indierollspelens-infrastruktur-barn/#7-arsmotestider) som var ett slags kort brandtal för föreningslivet. Det var så peppigt att jag kände att jag ville skriva det här blogginlägget. Här kommer lite blandade tips från mig där jag har inspirerats av Gnomvid:

* **Gå en studiecirkel**. En studiecirkel är najs eftersom det är tidsbegränsat. Man signar inte upp för hela livet. Du kan anordna själv eller gå på något som t.ex. [ABF](https://www.abf.se/) anordnar. [Centrum för marxistiska samhällsstudier](https://www.cmsmarx.org/) anordnar också bokcirklar. Solidaria anordnar [Anarchist Book Circle: The Nation on No Map: Black Anarchism and Abolition](https://organisera.org/events/c0dad937-6ee0-47c6-8cc4-16c5734ca50f)
* **Gå med i facket**. Om du inte redan är med i facket så gör det! Du kan engagera dig i din lokala fackklubb eller starta en. Själv är jag med i Stockholms LS av SAC.
* **Gå med i en förening eller nätverk**. Det finns massvis med föreningar och nätverk! Några föreningar jag gillar är Kamratdataföreningen Konstellationen (så klart), DFRI, Jordens Vänner, Naturskyddsföreningen (finns ofta en lokalförening man kan engagera sig i), Friluftsfrämjandet, Klimataktion, Fridays for future, XR
* **Börja skapa**. Det kan kännas väldigt kul att skapa på olika sätt. Ja, allt man gör i en förening är ju skapande av olika slag. Men kan vara najs med något mer konkret. Det kan handla om att gå med i en teatergrupp, en konstnärsgrupp eller en skrivargrupp.

Innan jag skrev det här blogginlägget skrev jag ett [inlägg på Mastodon](https://social.spejset.org/@samuel/114108423772522723) och fick lite andra tips på saker att göra:

* **Bidra till Wikipedia**. Allt från lite korrekturläsning till skrivandet av långa artiklar är meningsfullt.
* **Studera din närmiljö genom studieresor** (kan vara väldigt lokala platser eller längre bort). Enkelt sätt är att låna en biblioteksbok om områdets historia, natur, kultur etc för att läsa på och sedan utforska och dokumentera på egen hand 
* **Egenorganiserade bokklubbar/bokcirklar** 
* **Starta föreningar** inom ditt intresseområde 
* **Bjuda hem grannar, arbetskamrater eller bekanta** på fika eller mat för att lära känna varandra och utvecklas socialt
* **Starta en informell lunchklubb**. Malmös hacklunchare träffas varje onsdag sedan 20 år.

## Kamratdataföreningen Konstellationen

Som ordförande i Kamratdataföreningen Konstellationen så ägnar jag mycket tid just åt just skapa digital infrastruktur för vänstern och andra sociala rörelser och att folkbilda så att andra kan ta makten över sin data och kommunikation. Väldigt mycket av det vi gör sker i den digitala världen. På så sätt kanske jag motsäger mig själv nu? Nej, jag tycker det är rimligt och bra att vissa föreningar fokuserar på nätet. Det behövs även experter för de digitala frågor. Och det behövs också agitatorer på nätet. Men alla behöver inte göra det hela tiden. En stor del av vänstern lägger för mycket tid på att bara posta på sociala medier. (Absolut inte alla så klart.)

Trots att vi i Kamratadatföreningen Konstellationen är en huvudsaklig digital gemenskap så har vi anordnat flera aktiviteter i den fysiska världen, i [demonstrationer](https://konstellationen.org/2023/09/20/chatcontrol-pressmeddelande-efter-demo/) och [manifestationer](https://konstellationen.org/2024/06/08/picknick-pressmeddelande/) mot Chat Control, anordnat flera [hackathons](https://konstellationen.org/2024/11/14/hackathon-2024-rapport/) och även en temakväll om [Bortom Big Tech sociala medier](https://konstellationen.org/2023/08/20/intresset-for-alternativ-stort/).

Men att träffas i mindre grupper i videomöten kan också skapa gemenskap. Det har vi gjort i [Rollspelsworkshop med digital omröstning](https://konstellationen.org/2024/03/10/strejk-pa-karnevalskullen/), i [skrivarstuga](https://konstellationen.org/2025/01/25/skrivarstuga-2025/) och i [studiecirkel om digitala verktyg för social förändring](https://konstellationen.org/2025/01/08/studiecirkel-rapport/).

## Organisera och må bra

Är all organisering kul? Mår man bra av allt? Nej, så är det inte. Det kan kännas tufft ibland att organisera men det är lättare att hantera en mörk tid tillsammans. Organisering är nyckeln till att skapa hopp och känna mening. Jag tror de allra flesta skulle må bra av att träffa andra i det fysiska rummet oftare. Det kan vara skönt att komma bort från skärmarna och få umgås med folk.

Hoppas du hittar bra sammanhang där du kan må bra och bidra till en bättre värld!