---
title: Krönika - Vänsterpreppa - så är du förberedd när krisen kommer
image: digital-prepping.jpg
image_description: Bild av <a href="https://unsplash.com/@marvelous?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Marvin Meyer</a> från <a href="https://unsplash.com/photos/people-sitting-down-near-table-with-assorted-laptop-computers-SYTO3xs06fU?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
description: En krönika om vikten av att vänsterkrafter bygger upp en beredskap inför demokratiska kriser, som när fascister successivt tar över samhällsinstitutioner
date: 2024-11-06 18:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/prepping-foer-vaenstern). Den skrevs innan Trump utropats till segrare i presidentvalet i USA.

**Vänsterkrafter behöver bygga upp en beredskap inför demokratiska kriser, som när fascister successivt tar över samhällsinstitutioner. Det finns exempel på bloggar och sociala rörelser som gör just det.**

Fascismen i dess olika gestalter är på frammarsch. Inte blir det bättre när makten över vår kommunikation och data innehas av vinstdrivande företag som bryr sig mer om pengar och makt än att motverka radikalisering av unga, något som [högerextrema inte är sena att utnyttja](https://www.etc.se/kronika/twitter-bevaepnar-sveriges-hoegerextrema).

Vänstern missgynnas av plattformarna. Innehåll på Facebook och Instagram blir ofta smygblockerat (shadow banned), deras inlägg döljs helt eller delvis för deras följare. Bland annat Human Rights Watch har visat hur detta [drabbar pro-palestinska röster](https://www.hrw.org/news/2023/12/20/meta-systemic-censorship-palestine-content). Även nu inför valet i USA blir innehåll smygblockerat, något som [instagramprofilen Arielle Fodor](https://www.washingtonpost.com/technology/2024/10/16/instagram-limits-political-content-shadowban-election-posts/) fick erfara när hon skrev om bland annat aborträtt och Trump.  

The Guardian har visat att [Trumps presidentvalskampanj har jobbat tillsammans med X](https://www.theguardian.com/us-news/2024/oct/12/x-twitter-jd-vance-leaked-file) för att förhindra att information om vicepresidentkandidat JD Vance ska spridas. Elon Musk som äger X har offentligt tagit ställning för Trump. Finns det någon som tror att vänstern har en chans på X?

Det finns en förståelse om behovet av krisberedskap för naturkatastrofer, cyberattacker och krig. Även sociala rörelser har börjat [”Preppa tillsammans](https://preppatillsammans.se/)”. Vänsterkrafter behöver också bygga upp en krisberedskap inför demokratiska kriser, som när fascister successivt tar över samhällsinstitutioner. Men det kan även vara för att bemöta temporära kriser som sommarens [rasistiska upplopp](https://www.arbetaren.se/2024/08/06/rasism-inte-legitim-ilska-nar-valdet-sprider-sig-i-storbritannien/) i Storbritannien. Bloggen [prepp.fritext.org](https://prepp.fritext.org/blog/flykt/beredd-att-mota-lynchmobben-motstandskraft-under-en-pogrom/) tar avstamp där och diskuterar hur man kan förbereda sig praktiskt inför en kris som en pogrom eller lynchmobb.

När det kommer till det digitala är ett första steg för vänstern och andra progressiva rörelser att ha direktkommunikation med sina medlemmar och sympatisörer på plattformar där de själva har makten, exempelvis genom nyhetsbrev, uppdaterade hemsidor och att vara (extra) närvarande på alternativa och decentraliserade plattformar som Mastodon.

Hellre utveckla en krisberedskap som inte behövs än att vi står där oförberedda när krisen är ett faktum.