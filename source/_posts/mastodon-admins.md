---
title: Intervjuer med Mastodon-admin
image: servers.jpg
description: Hur funkar det att administrera en Mastodon-instans
date: 2022-11-27 18:30
---

Det federerade sociala medier-nätverket Mastodon fortsätter att växa i takt med Elon Musks hanterande av Twitter och dess anställa. När Elon Musk [tog över](https://www.theguardian.com/media/2022/nov/01/mastodon-twitter-elon-musk-takeover) så lämnade 70 000 användare på en dag. När Musk skulle bygga "Twitter 2.0" och krävde av sina anställda att de skulle ställa upp på att vara "hardcore" så [växte Mastodon med 200 000 användare](https://www.dexerto.com/entertainment/mastodon-grows-by-over-200000-overnight-as-riptwitter-trends-1989657/) på en dag i samband med att hashtaggen #RIPTwitter trendade.

![Mastodon-statistik](/images/mastodon-stats-2022-11-18.png)

Antalet användare och instanser fortsätter att växa på Mastodon. Enligt boten [Mastodon Users](https://bitcoinhackers.org/@mastodonusercount) så har Mastodon 2022-11-27 drygt 7,6 miljoner användare på drygt 2400 instanser.

Intresset för "alternativet till Twitter" är fortsatt stort. Vi är så vana vid att sociala medier är stora centraliserade och stängda plattformar som drivs av vinstdrivande big tech-företag med algoritmer som ska hålla oss kvar på plattformen för att exponera oss för så mycket annonser som möjligt. Att det då vinst icke-vinstdrivande federerade lösningar som är byggda på öppen källkod går stick i stäv med det vi är vana vid. Därför har jag skrivit en [kort introduktion till Mastodon här](/lite-kort-om-mastodon) för alla nya.

Den grundläggande tanken med Mastodon är att istället för att ett företag (eller organisation) tillhandahåller en jättestor central server så består nätverket av flera tusentals instanser som kan variera i storlek. Jag tog chansen för att intervjua några av administratörena för några av de svenska instanserna. Jag ställde ett antal frågor som de besvarade.


## Vad heter du? (Eller vilket alias vill du bli presenterad med?)

Micke Nordin på https://camp.smolnet.org

![Micke](/images/micke.png)

Robert / ordfRobert / coeur66 på https://suedi.club

![Robert](/images/robert.jpg)

Jonathan på https://social.linux.pizza

![Jonathan](/images/jonathan.jpg)

Mårten Björklund på https://mastodon.nu

![Marten](/images/marten.jpg)


## Vad driver du för instans? Vad har den för profil? Hur länge har du driftat den? Hur många användare har den?

**Micke**: Jag har en instans tillsammans med mina kompisar som heter camp.smolnet.org. Under smolnet.org-domänen har vi samlat lite molntjänster som vi själva behöver och som vi kan drifta själva istället för att lägga våra grejer hos ett stort techföretag. Vi har ca 70 användare i dagsläget, vi stängde registreringen när den stora floden från Twitter kom, men nu i dagarna har vi försiktigt öppnat igen. Vi vill inte få problem med kapaciteten och har inget behov av massor av användare, utan vi vill mest ha det lite lugnt och skönt :).

**Robert**: Jag driver suedi.club, den grundades för att erbjuda en säkrare plats i fediverse för användarna av myra åbeck öhrmans discord, så vår instans har främst benhård moderering och är väldigt hbtqi+ orienterad (se våra regler som exempel).

Den här instansen är mindre än två veckor gammal, men det är inte min första, vilket vårat antal användare visar (mindre än 40)

**Jonathan**: Jag driver en teknikfokuserad instans, mest Linux, Free Software/Open Source och nörderi.

**Mårten**: Jag driver Mastodon.nu och vi profilerar oss till Norden (Island, Danmark, Norge, Sverige, Åland & Finland) och den stora målsättningen är att föra alla nordbor närmare varandra. Jag startade instansen/servern sommaren 2021 när jag hade lite tid över och det var dåligt väder så motorcykeln fick vila. I skrivande stund närmar vi oss 12 000 aktiva användare. Mastodon.nu drivs på 100% förnybar energi, till och med backup-generatorerna går på fossilfritt bränsle. 

## Hur länge har du varit på Mastodon?

**Micke**: Jag har funnits specifikt på någon Mastodonserver sedan några år tillbaka. Jag har varit på lite olika servrar under åren, jag tror mitt första Mastodon konto skapades 2017. Jag har dock funnits på Fediverse av och till sedan 2009, när jag skaffade konto på identi.ca som fanns då. Jag har även haft en egen diaspora pod under en period.

**Robert**: Jag har använt mastodon sedan sent 2016, det har varit en skitrolig resa hittills.

**Jonathan**: Juni 2018

**Mårten**: Jag har haft olika Mastodon-konton sedan 2019-2020.

## Vad jobbar du som till vardags? Eller vad gör du om dagarna?

**Micke**: Jag jobbar statigt med IT inom högre utbildning, bland annat håller jag just nu på med att sätta upp en Mastodonserver till jobbet :).

**Robert**: Jag jobbar med ungdomar och unga vuxna. innan dess analytiker, innan dess grundade jag en numera framgångsrik firma inom infosäk-fältet.

**Jonathan**: Linux Systemadministratör till vardags, kvällar och helger - nörd och idiot.

**Mårten**: Jag jobbar som IT-arkitekt inom offentlig sektor och med vårdinriktning, försöker varje dag göra livet lättare genom teknik för vår fantastiska vårdpersonal och alla patienter.

## Hur kommer det sig att du började engagera dig för Mastodon så pass att du började drifta en instans? Är det ideologiskt drivet? Socialt drivet? Eller för att det är skoj med det tekniska?

![Tux](/images/tux.png)

**Micke**: Av alla dessa anledningar, jag har länge varit engagerad i rörelsen för fri kultur och fri programvara och eftersom jag kan, och tycker att det är kul, så kan jag gärna sköta om lite tjänster för mig själv och mina vänner.

**Robert**: Det är en rad olika imperativ som ledde mig till att starta en egen mastodon-instans om jag ska vara helt transparent. För det första så ökade hatspråket på twitter lavinartat när musk tog över. Men även att plattformen började uppvisa tekniska problem väldigt tidigit i övertagandet.

Det är även drivet av två av mina guidande principer, do good shit och bygg upp andra, mastodon och fediverse handlar väldigt mycket om just detta, att visa omtanke, hänsyn och respekt. Dessa tre är trotsallt något som många efterfrågar, så varför inte bara muta in en ö för de jag bryr mig om?

**Jonathan**: Lite utav en slump. Hade en domän över när jag upptäckte GNUSocial som ett färdigpaketat paket i min labbmiljö. Vilket fick mig att undersöka saken. Och senare den dagen så föddes social.linux.pizza.

**Mårten**: Jag har länge sökt efter ett alternativ till de stora centraliserade och överkommersialiserade sociala medierna, jag tycker att det med tiden blev för mycket fokus på reklam och att lyfta fram kontrovers istället för att erbjuda en trygg och vänlig plats där människor kan dela upplevelser, tankar och diskutera. Det finns också en stor osäkerhet i hur dina uppgifter och data behandlas hos de stora amerikanska och kinesiska bolagen och om jag som någorlunda tekniskt bevandrad inte kunde hitta de svar jag sökte så tror jag tyvärr att gemene man inte har en chans att få reda på vad som potentiellt kan hända när dina uppgifter säljs och köps konstant. Jag tittade på Mastodon första gången 2017, det var då ganska jobbigt att använda och jag provade genom åren flera alternativ som Pleroma och Diaspora. Jag började använda Mastodon på allvar 2021 och insåg rätt snabbt att det kommer behövas många fler stabila trygga servrar och att det var lika bra att komma igång med det direkt. Jag gillar teknik och Mastodon är en superb mix av roliga tekniker samtidigt som det finns potential att skänka glädje till många och samtidigt minska avstånden mellan människor. Skaparen av Mastodon Eugen (@gargron) Rochko har verkligen lagt en mycket fin grund för “riktig” social media.

## Vad innebär det att vara admin? Hur mycket tid får du lägga på underhåll? Ungefär hur mycket kostar det? Är du själv admin eller finns det flera?

**Micke**: Vi är tre admins på vårt Camp och det tar inte så mycket tid. Lite arbete med att klappa om servern och lite arbete med att klappa om människorna. Kostnaden för Mastodon vet jag inte riktigt, eftersom en del av infrastrukturen delas med våra andra smolnet.org-tjänster, men det rör sig om nånstans mellan 500 och 1000 kronor i månaden. Man kan komma undan billigare än så förstås om man vill ha en server för sig själv, men vi har lite extra finesser i infrastrukturen :)

**Robert**: Ouff, det är tungt, skämt åsido. Det är ljuvligt, visst är det en del underhåll, men samtidigt så handlar det om att aktiv administration skapar tydlighet för användarna, så därför får det vara som det är.

Vi lägger c:a en timme per kväll till att hålla ett öga på blocklistor, rapporter och andra mer tekniskt orienterat underhåll, mest återkommande är att hålla ett öga på vad andra instanser blockerar och varför, för att snabbt kunna gå till beslut.

Sen är jag inte dummare än att jag kör ett lokalt git-repo som jag synkar blocklistor till, så får jag ut information så fort det är förändringar i någon av dem och vad dessa förändringar är.

Men sen sker såklart även arbete utanför den tidsramen.

**Jonathan**: Jag driftade instansen själv ett tag, men sen när intresset ökade så ökade även stressen för mig, samtidigt som min tid upptogs mer och mer utav familj. Jag tog beslutet att flytta den till en managerad lösning.

**Mårten**: Till en början var det inte särskilt mycket jobb att vara administratör, jag checkade in då och då för att se så servern mådde bra och att serverns regler efterlevdes, samtidigt som jag själv sporadiskt var en vanlig användare, så flöt det på i ungefär 7-8 månader. Sedan ett par veckor tillbaka har det där vänt och nu är det ganska mycket jobb att vara administratör för Mastodon.nu, som jag ser på det just nu så har jag i princip två jobb, mitt vanliga dagjobb och “kvällspasset” när jag kommer hem.

Det krävs en del förebyggande arbete och ibland har vi lite tekniska problem och det händer att användare låser ute sig och behöver hjälp men den stora tidsåtgången just nu är att designa och snart bygga den nya IT-miljön för Mastodon.nu samt sköta modereringen. Vi vill inte behöva stänga nyregistreringen och vi vill kunna ge alla som vill den bästa möjliga upplevelsen av Mastodon. Vårt närmsta mål är att migrera till en ny och mer skalbar miljö så fort det bara går, dels för att få ner priset för driften och dels för att kunna ta emot många fler användare. När jag startade Mastodon.nu 2021 så gjorde jag det med förväntningen av 100-500 användare och kostnaden var ungefär 500kr/månad, vilket betalades helt ur egen ficka. I Mars 2022 så kom den första vågen med “twitter-flyktingar” och vi hamnade någonstans runt 800-1000 användare men prestandan var fortfarande ok efter lite justeringar. För våg nummer två som skedde vecka 44 var jag inte alls förberedd och vi hade en del problem då, vilket vi lyckades parera genom att jobba hela helgen med optimeringar. Jag säger här “vi” för det var i det här skedet som jag på allvar insåg att det kommer vara omöjligt att ensam fortsätta driva servern ideellt och jag bad om aktiv hjälp från personer i min närhet (Det har funnits personer i bakgrunden som backup om nåt skulle hända mig enligt “Mastodons överenskommelse”, mer om den senare) och dom ställde genast upp med sin egen mer begränsade fritid.

I dagsläget är vi ett litet team på 3-4 personer och du kan finna oss på “Om”-sidan på Mastodon.nu och det finns ytterligare personer involverade som hjälper oss hålla koll på innehållet på servern. Måndagen den 7e November (dagen efter vi arbetat hela helgen med att få ordning på allt) såg vi att Greta Thunberg skapat ett konto hos oss på Mastodon.nu och drog till sig massor med följare vilket återigen fick vår infrastruktur att gå på knäna, framförallt lagringen av media-filer (bilduppladdningar, profilbilder etc etc) såg ut att bli ett akut problem. Vecka 45 arbetade jag i princip dygnet runt utan sömn och på nåt sätt hann jag också med att svara på frågor i radion för P1s Kulturnytt. Alternativet till att jobba på nätterna hade varit att helt stänga servern för underhållsarbete i en dag eller två vilket jag och teamet ville undvika. Den här arbetsinsatsen lönade sig och för stunden har vi en stabil och bra miljö men den kommer med ett pris, den senaste fakturan vi betalade till vår hosting leverantör var på ca 5200kr och det finns ytterligare kostnader på andra ställen, t.ex översättningsfunktionen som gör det möjligt att direkt i flödet översätta inlägg på andra språk till ditt eget. Så idag kostar servern 10-15 gånger mer än när jag startade den, det följer ganska exakt antalet användare. Vi hade inte haft en chans att stå för denna kostnadsökning själva. Utan våra fantastiska supporters på Patreon, Paypal och snart Swish och Revolut hade vi aldrig haft råd att skala upp. Jag ser alla våra supporters och deras bidrag som den direkta anledningen till att Mastodon.nu fungerar. Jag vill också poängtera att vi kan få ner kostnaderna men det kommer kräva en initial investering under en tid och vi kommer att genomföra detta.

## Det här med olika instanser och regler, hur funkar det?

![Network](/images/network.jpg)

**Micke**: Det är inte så konstigt egentligen, man kan tänka på det som epost. Alla kan som regel skicka epost till varandra oberoende av vilken epostleverantör man har, men börjar man missköta sig eller skicka spam, så kan man bli avstängd. Reglerna är lite olika mellan leverantörerna och blir det för stor missmatch mellan dessa så kan man stänga av en annan leveratör som t.ex. tillåter att spam skickas.

Vi har stängt av kontakten med ett fåtal servrar som släpper in extremhögern eller håller antivaxxare t.ex.

**Robert**: Det är väldigt enkelt egentligen, du går inte fram till en vilt främmande människa och vrålar dem i ansiktet om nån åsikt eller övertygelse du har, så bör det ej heller fungera i det digitala.

Regler är således vad som definierar vad vi gör för att skapa trevnad för alla som deltar, se det lite som en civilisation, en del menar att utan lag och ordning så är vi inte en.

Så därför är det även viktigt att hålla ett öga på vad andra instanser har för regler när man snubblar över ett problem eller mottar en rapport.

**Jonathan**: Man kanske kan jämföra den med någon trädgård, lägenhet eller hem. Det finns regler som man får förhålla sig till och sköter man sig inte så får man gå. Att andra instanser har andra regler och vilda besökare kan jag inte rå för - mer än att kanske bryta kontakten om det är störande.

**Mårten**: Vem som helst kan starta sin egen server (eller instans som det hette tidigare) och sätta sina egna regler MEN för att få vara med på den officiella listan över servrar och även få vara med inom federationen så måste administratören och reglerna stämma överens med "Mastodon överenskommelsen" (The Mastodon server covenant). Denna överenskommelse innebär att servern måste ha aktiv moderation mot rasism, homofobi och sexism t.ex, det måste tas regelbundna backuper på servern (och vara möjligt att återställa från dessa), det måste finnas minst en ytterligare person med åtkomst till infrastrukturen om något händer “ägaren” samt 3 månaders förvarning OM servern av någon anledning kommer stängas ner. Du kan läsa hela innehållet här: https://joinmastodon.org/covenant

## Hur funkar det med moderation? Gör du det själv? Eller finns det ett team? Behöver du hjälp på din instans?

![Comments](/images/comments.png)

**Micke**: Vi är som sagt tre admins och alla som har konto på vår instans sköter sig jättebra, så det handlar mest om att vi ibland får in anmälningar mot nån otäcking på en annan server eller så.

**Robert**: Att moderera är väldigt enkelt, ännu enklare med mastodon v4 men det finns förbättringar att göra. Ett vanligt steg är att hålla ett öga på inkomna rapporter, läsa #fediblock -hashtaggen, och därigenom ha ett humm om vilka instanser som kräver åtgärder.

Men innan vi går till beslut om någon form av instans-bred moderering som t.ex blockering av en instans där vi är osäkra, så kör vi en samling av så många av användarna som möjligt, och går till beslut tillsammans med dem. Återigen, det är viktigt med tydlighet och information.
   
Vi har idag mig som admin, en som kan ta över om det blir så att jag inte har tid just då och två moderatörer med lika låg tröskel för att acceptera eller ens tolerera svammel, som mig själv.

Men det är bara jag som har nycklarna till kungadömet (den tekniska infrastrukturen)

**Jonathan**: Oftast får jag en rapport om en användare som är klassifierad (oftast) som spam, men även fall om ren ohyfs, trakasserier och rasicm. För mig är det en så kallas "suspension" - användaren eller hela instanser kan då ej kommunicera med någon användare på vår instans.

**Mårten**: Mastodon programvaran har många smarta verktyg inbyggda för innehållsmoderering, dessa tillsammans med de rapporter vi får från användare via rapportfunktionen gör att vi hittills lyckas hålla en vänlig, öppen och rolig miljö på Mastodon.nu. Vi stänger av konton som bryter mot våra regler, generellt försöker vi varna först men i vissa fall blir det ingen varning, t.ex vid vidriga påhopp och hot om det ena och det andra. Vi är ett team som delar på ansvaret för detta MEN vi behöver och kommer behöva hjälp med modereringen, ibland händer det också att vi går för hårt ut och man kan då som avstängd skicka in en överklagan. Alla som använder Mastodon.nu kan känna sig trygga i att vi tittar på alla rapporter som kommer in och gör en objektiv bedömning utifrån de regler vi satt upp, det kan hända att vi behöver justera våra regler framöver men de verkar fungera bra än så länge.

## Har du konto på andra mastodon-instanser? Finns det andra mastodon-instanser du vill tipsa om? Vilka är dina favoritinstanser i Sverige/Skandinavien?

**Micke**: Den instans jag hade konto på innan jag satte upp min egen server stängde ned under en period (men är tillbaka nu), så nu har jag bara konto på min egen server. Snart kommer dock jobbinstansen upp också, så det blir väl ett konto där också. Jag har ingen särskild instans att tipsa om, men jag vill tipsa alla om att kolla runt lite. Det finns många servrar med helt olika profil. De stora servrarna är bra som en första landningsplats,men kolla gärna omkring lite efter en mindre server som har en profil och folk som du gillar. Du kan ta med dina följare till en ny server om du väljer att flytta. Ta också chansen att snacka med människor från andra delar av världen, det finns några riktigt coola människor där ute.

**Robert**: Nej jag gjorde mig av med dem kort efter att jag gick all in för vår instans, jag skulle rekommendera mastodon.nu för att vara rak. men det finns en mindre uppsjö av instanser idag.

**Jonathan**: Jag har konto på några instanser samt en privat labbinstans. Såklart är social.linux.pizza min favoritinstans ;) Men mina andra favoritinstanser i norden är nog suedi.club och mastodon.se.

**Mårten**: Jag har inga ytterligare konton på Mastodon, det finns ett antal ytterligare bra servrar för Mastodon därute och jag ser att det tillkommer fler Svenska/Nordiska servrar vilket är väldigt roligt. Jag vill inte rekommendera någon specifik server eftersom jag inte har någon insyn i hur dem drivs eller styrs MEN jag har haft en del kontakt med administratören på mastodon.se och det är en person som verkar ha en mycket sund och trevlig bild på teknik och open source som jag också brinner för. Även @Admin på suedi.club verkar vara en hyvens person. Mitt tips när du söker ett hem för ditt Mastodon-konto är att ha i åtanke att i princip alla servrar drivs ideellt av en eller flera individer som lägger ner sin obetalda fritid samt egna pengar för att få allt att fungera, det här är inte Twitter eller Facebook som har miljardintäkter via reklam varje månad. Jag hänger i princip enbart på min egen server men jag följer givetvis personer och hash-tags från hela Mastodon-världen (Federationen) och behöver därför inte se allt som händer lokalt på alla andra servrar.

## Vad skulle du säga skiljer Mastodon mest jämfört med andra sociala medier?

![Social media](/images/social-media.png)

**Micke**: Mest tempot och stämningen, folk är för det mesta nyfikna, respektfulla och omtänksamma. Det gillar jag.

**Robert**: Vänlighet, respekt, hänsyn, omtanke. Det är den stora skillnaden, det finns även verktyg för att efterleva detta inbyggt i plattformen.

**Jonathan**: Den stora skillnaderna är att man själva skapar sin upplevelse, ingen dator, algoritm eller något annat omänskligt som bestämmer åt en. Dessutom slipper man annonser. Ett produkt byggt utav människor - för människor.

**Mårten**: Den största skillnaden är att Mastodon är ett ideellt projekt, reklamfritt och där oönskat innehåll sorteras bort mer eller mindre direkt. Det finns heller inga algoritmer som är till för att lyfta fram kontroverser och fånga ögon och antal “clicks”. Det är genuina interaktioner mellan människor som står i fokus.

## Ser du likheter med andra tekniker och sammanhang? (Typ BBS, IRC, osv)

**Micke**: Ja, det finns rent tekniska likheter med t.ex. epost som jag nämnde tidigare, men jag påminns socialt om hur det kändes att hänga på IRC som tonåring och snacka med coola människor på andra sidan jorden :)

**Robert**: Vore det ändå upp till mig skulle jag ännu hänga på IRC ;)

Men närmare skulle jag säga att det påminner väldigt mycket om hur tumblr en gång var.
Men samtidigt tänker jag att mastodon är en exceptionell plattform för att intuitivt skapa samhörighet.

**Jonathan**: Jag ser nog mest likheter mellan mail, och gamla hederliga mailinglistor ;)

**Mårten**: Många brukar likna tekniken vid hur e-post fungerar, själv ser jag att Mastodon är en blandning av tekniker som skapar en väldigt trevlig upplevelse. Jag hängde inte så mycket på BBSer men kan erkänna att jag ser med nostalgiska ögon på IRC och man skulle kanske kunna dra paralleller till att en Mastodon server är en IRC-kanal som kan prata med andra IRC-kanaler.

## Kan man hjälpa till på något sätt på din instans? Hur gör man då? Vem kontaktar man?

**Micke**: Om man vill (och har råd) kan man bidra med en slant till serverkostnaderna, det kan man göra på liberapay: https://liberapay.com/micke/

**Robert**: Känner en att vår instans är något för en, så är det bara att maila mig, min epostadress ligger uppe på vår om-sida, men vi har krav grundade i vårt regelverk, kortfattat, kan man ställa upp bakom följande är det bara att skicka ett mail:

Transrättigheter är mänskliga rättigheter.
No Pasaran!

**Jonathan**: Absolut kan man det, men jag anser att det är viktigt att man ska tycka det är roligt först och främst. Det sista jag vill är att någon ska känna sig tvungen. Men skulle man vilja vara med, så kan man alltid kontakta mig direkt på Mastodon.

**Mårten**: Vi kommer behöva hjälp och det är redan flera personer som hört av sig och erbjudit hjälp (vi har inte glömt er). Arbetsbördan ökar i takt med antal användare och det är inte bara innehållet på Mastodon.nu som behöver gås igenom utan allt innehåll som flödar in till vår server från andra servrar. Det finns inte särskilt många personer som har erfarenhet av Mastodons modererings-verktyg så vi kommer att behöva genomföra en aktiv onboarding process där vi går igenom hur det funkar. Vi skulle bli väldigt glada om personer som känner att de är bra på att bedöma situationer och innehåll objektivt samt förstår att det i alla fall till en början handlar om helt ideellt arbete hör av sig med en kort resumé samt motivering till admin@mastodon.nu om varför just du skulle passa för moderator rollen. Vi kommer så fort vi får tid över att återkoppla till er, tyvärr kanske vi inte kan svara på en gång. Det finns även andra sätt att hjälpa till som inte kräver så stor arbetsinsats och det är att svara på nykomlingars frågor, rapportera innehåll som förmodas bryta mot våra regler, boosta inlägg som du tycker är bra eller bara delta allmänt i de många publika diskussionerna som hela tiden dyker upp. Man kan även stötta oss ekonomiskt om det finns möjlighet via antingen, Patreon, PayPal, Revolut och snart Swish.

## Är det något annat du vill skicka med till läsarna?

**Micke**: Om du nyligen kom till Fediverse från Twitter, så kan det vara värt att komma ihåg att det inte är en kopia av Twitter.  Även om det ser ut på ett liknande sätt, så är kulturen annorlunda. Ta dig tid att detoxa från Twitter och lär känna Fediverse i all sin vackra, men enkla charm.

**Robert**: Mastodon är nytt. Mastodon är annorlunda, gå in i det med öppna ögon, nyfikenhet och ha ett äventyr. Det är dags för att vi gör sociala medier sociala, inte aktivt uppmuntra till asocialt beteende som de tidigare plattformarna.

Och snart kommer även instagram-workalikes, så håll i kepsen!

**Jonathan**: Det är väl att man ska fokusera på att göra saker som får en att må bra :)

**Mårten**: Jag och resten av teamet hoppas att ni alla kommer att trivas på Mastodon och Mastodon.nu och vi tycker det är viktigt att finnas som ett seriöst alternativ till de stora amerikanska och kinesiska bolagen och deras tjänster. Vi har den tekniska kompetensen för att vidareutveckla detta till något stort. Var inte heller blyg eller rädd för att komma med feedback till oss, vi vill hellre veta om vi kan göra något annorlunda än att inte ha en aning. Slutligen så vill jag också från hela teamet på Mastodon.nu tacka alla användare för att vi har ert förtroende och givetvis alla våra supporters på Patreon & PayPal som gör hela det här projektet möjligt!

## Nästa intervju

Nästa intervju kommer att vara med Kris Nóva som är admin för instansen https://hachyderm.io som har över 28 000 användare.

Om du tycker det är intressant med alternativ som Mastodon så har jag skrivit ett blogginlägg om att just [bryta sig loss](/bryta-sig-loss/) från big tech.