---
title: Klimatkrisen och förlusten av den biologiska mångfalden hänger ihop
image: forest-river.jpg
description: Vänsterpartiet har en politik för klimatkrisen och för den biologiska mångfalden
date: 2022-07-27 23:50
---
Idag läste jag en debattartikel av Miljöpartiet med titeln [Vi måste ställa om – med hjälp av naturen](https://www.svd.se/a/34rm5L/mp-vi-maste-stalla-om-med-hjalp-av-naturen). Jag tycker att den var bra, att de tog upp många bra poänger. Och jag tänkte lite snabbt berätta om Vänsterpartiets politik i de frågorna.

"Vi befinner oss i ett klimatnödläge, men det är i praktiken två existentiella hot som samhället står inför: klimatkrisen och förlusten av den biologiska mångfalden. Dessa globala utmaningar behöver hanteras tillsammans eftersom de är kopplade till varandra.

Samma kapitalistiska logik som sliter ut arbetares kroppar för kortsiktig vinst utarmar även naturresurserna och förändrar klimatet. Det är genom en socialistisk systemförändrande politik som omställningen kan bli lyckad."

Så skriver Vänsterpartiet i den [regionalpolitiska valplattformen för Storstockholm](https://storstockholm.vansterpartiet.se/wp/files/2022/04/Regionalpolitisk-valplattform-2022-1.pdf).

Miljöpartiet tar i sin debattartikel upp några konkreta åtgärder:

* Ställ om skogsbruket
* Återställ våtmarker
* Skydda och stärk haven

Det är mycket bra förslag och som jag själv stödjer fullt ut. Det är också förslag som Vänsterpartiet driver. I Vänsterpartiets [centrala valplattform](https://vansterpartiet.se/wp-content/uploads/2022/07/valplattform-2022.pdf) skriver vi följande:

"Ett ständigt överutnyttjande av naturens resurser leder till kraftig utarmning av den biologiska mångfalden och växthusgasutsläpp. Skogens roll att naturligt minska och lagra utsläpp behöver stärkas och torrlagda våtmarker som läcker utsläpp ska återställas. Skyddet av skogs- och strandområden är en viktig del för att värna den svenska allemansrätten och den biologiska mångfalden. För att uppnå klimatmålen behöver skogens funktion som kolsänka bevaras de kommande decennierna, därför behöver kalhyggesbruket fasas ut. Den skog som avverkas måste användas klokt där endast restprodukter ska användas till biobränsle."

I Vänsterpartiets riksdagsmotion [Biologisk mångfald](https://riksdagen.se/sv/dokument-lagar/dokument/motion/biologisk-mangfald_H9023279/html) skriver vi:

"**Sverige bör anta mål om att skydda 30 procent**

Enligt SCB är knappt 9 procent av Sveriges skogsmark formellt skyddad (reglerat av lagar och förordningar, exempelvis naturreservat). Då är även den fjällnära skogen och improduktiv mark inräknad. Enligt samma myndighet uppgår de frivilliga avsättningarna (skog där markägaren frivilligt har beslutat att inte utföra åtgärder som skadar naturvärden) till knappt 6 procent. När det gäller de frivilliga avsättningarna finns dock osäkerheter (se text ovan). Sverige har inte klarat att skydda tillräckligt med skog i enlighet med vare sig de internationella eller nationella målen. Vänsterpartiet anser att Sverige, för att värna den biologiska mångfalden, bör anta en nationell målsättning om att totalt 30 procent av skogsmarken långsiktigt ska skyddas. Detta mål bör uppnås genom att minst 20 procent av den produktiva skogsmarken skyddas och att den skyddade skogsmarken ska bestå av representativa och ekologiskt sammanhängande nätverk när det gäller naturtyper och geografisk utbredning. En sådan målsättning skulle även ligga i linje med EU-kommissionens förslag om att minst 30 procent av EU:s landyta ska ha rättsligt skydd för att leva upp till intentionerna i Konventionen om biologisk mångfald (CBD).

Sverige bör anta en målsättning om att 30 procent av skogsmarken långsiktigt ska skyddas i ekologiskt sammanhängande nätverk med avseende på naturtyp och geografisk utbredning. Detta bör riksdagen ställa sig bakom och ge regeringen till känna."

I Vänsterpartiets riksdagsmotion [Hållbara hav](https://www.riksdagen.se/sv/dokument-lagar/dokument/motion/hallbara-hav_H7021618/html) så föreslås bland annat:

* Stärk lokala stödet för våtmarker och andra vattenvårdande åtgärder
* Inför ytterligare reglering av fisket för att stärka torskbeståndet
* Begränsa bottentrålningen
* Öka skyddet av havsområden

Jag är väldigt glad över att skydd av skog, återställande av våtmarker och skyddande och stärkande av hav lyfts upp i debatten. **Jag är glad att Vänsterpartiet och Miljöpartiet ser dessa frågor som oerhört viktiga och hoppas att vi tillsammans kan verka för att rädda både klimatet, skogen, havet och den biologiska mångfalden.**

Klimatdelen i Vänsterpartiets centrala valplattform avslutas med orden:

"Att som individ försöka minska sin klimat- och miljöpåverkan är bra, men det kommer aldrig att räcka. Vi måste ta oss an klimatkrisen som samhälle. Mycket av det vi konsumerar behöver vi för att leva bra liv – mat, kläder, boende och resor – men sättet sakerna och energin framställs på behöver ändras. För de flesta av oss sker en stor del av konsumtionen och resandet för att hållbara alternativ saknas. Det gäller inte minst på landsbygden. Det är samhället och politikens ansvar att se till att alternativen finns. Där inte teknikskiften räcker för att minska utsläppen måste konsumtionen styras bort från det smutsiga och alla ges förutsättningar att leva mer hållbart. Vi ser ett hållbart liv som ett bättre liv. Kortare arbetsdagar med mer tid för  relationer och egna intressen, produkter som håller och lätt kan återanvändas och delas istället för slit- och släng. Renare storstadsluft, billig och fungerande tåg- och kollektivtrafik. Hälsosammare mat från vår egen svenska landsbygd. Men för ett fåtal är konsumtion en livsstil av lyx och överflöd. Den rikaste procenten släpper ut tio gånger mer utsläpp än majoriteten av befolkningen. Det är denna elit som behöver dra ner sin konsumtion mest. Hos samma personer samlas rikedomar på hög istället för att gå till investeringar i framtiden. Det är hög tid att de få som har stora kapitaltillgångar bidrar mer till omställningen. För alla oss andra kommer omställningen att skapa större möjligheter, nya jobb, friskare liv och mer framtidstro."
