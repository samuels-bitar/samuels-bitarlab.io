---
title: Var kommer utsläppen från?
image: klimat-sektorer.jpg
description: En genomgång av några stora utsläppskällor och olika sätt att dela in utsläpp på
date: 2022-07-27 18:00
---
Hur ser en effektiv klimat- och miljöpolitik ut? Det är inte helt självklart. Det är flera saker som man måste tänka på:

* Var kommer de stora utsläppen från?
* Vilka delar har man direkt inflytande över?
* Vilka åtgärder har man folkligt stöd över?

Men allt är inte hugget i sten. Det går att ändra. Exempelvis kan man skaffa sig större inflytande över delar av samhället och utvecklingen. Och åtgärder går att skapa folkligt stöd för. Jag tänkta ta upp några delar.

# Olika sätt att räkna utsläpp

Innan man börjar fundera över vilka utsläpp man ska minska måste man veta *vilka* utsläpp man ska minska. Man brukar säga att det finns tre sätt att räkna utsläpp. Så här delar [Naturvårdsverket](https://www.naturvardsverket.se/amnesomraden/klimatomstallningen/sveriges-klimatarbete/tre-satt-att-berakna-klimatpaverkande-utslapp/) upp det:

1. **Territoriella utsläpp – huvudsakligt mått**. Utsläpp inom Sveriges gränser. Beräknas bottom up (baserat på detaljerade data om aktiviteter som utförs inom Sveriges gränser) och används för att följa upp klimatmålen som satts upp för Sverige inom FN, EU och nationellt
2. **Produktionsbaserade utsläpp – kompletterande mått**. Utsläpp från svenska aktörer. Beräknas bottom up (baserat på detaljerad statistik om bränsleanvändning i kombination med de territoriella utsläppen). Statistiken omfattar utsläpp från svenska företag och personer som skett både utanför och innanför Sveriges gränser, och följer samma avgränsning som gäller för nationell ekonomisk statistik – nationalräkenskaperna.
3. **Konsumtionsbaserade utsläpp – kompletterande mått**. Utsläpp som tar hänsyn till klimatpåverkan som svensk konsumtion orsakar i Sverige och andra länder. Beräknas modellbaserat, vilket ger viss osäkerhet för utsläpp som bryts ned till en finare upplösning. Utsläppen i Sverige till följd av svensk konsumtion baseras på utsläppen från svenska aktörer, det vill säga de produktionsbaserade utsläppen. Utsläppen i andra länder är beräknade med hjälp av en modell och baseras på statistik om produktion i andra länder samt import och export.

Så här ser en graf ut från Naturvårdsverket över de olika utsläppen:

![Tre olika utsläpp](/images/naturvardsverket-tre-olika.png)

Här kan man se att konsumtionsbaserade utsläpp ligger mycket högre än de andra utsläppen. För enkelhetens skull så skippar jag produktionsbaserade utsläpp och fokuserar på konsumtionsbaserade utsläpp och territoriella utsläpp.

## Territoriella utsläpp

Det här är som sagt det officiella sättet att räkna på. Sveriges klimatmål mäter på detta. Därför så blir det här fokus när man ser vad vi som land ska göra för att dra vårt strå till stacken när det gäller Parisavtalet. Den här grafen från Naturvårdsverket visar hur utvecklingen sett ut i Sverige:

![Tre olika utsläpp](/images/naturvardsverket-territoriella.png)

Några saker man kan se att de största utsläppen är (för år 2020):

* Industri (14,44 milj co2ekv)
* Inrikes transporter (14,89 milj co2ekv)
* Utrikes transporter (9,26 milj co2ekv)

Av de mindre utsläppen är följande störst:

* Jordbruk (6,93 milj co2ekv)
* El och fjärrvärme (3,52 milj co2ekv)

I runda slängar så står industrin för en tredjel och inrikes transporter för en tredjedel. Därför känns det rätt naturligt att först och främst se till att minska utsläppen från industri och transporter.

Men sen har vi också förändrad markanvändning och skogsbruk som står för negativa utsläpp på 39,77 milj co2ekv!

### Vilka företag släpper ut mest i Sverige?

Naturskyddsföreningen har redovisat vilka företag som [släppte ut mest i Sverige 2021](https://www.sverigesnatur.org/aktuellt/de-slappte-ut-mest-koldioxid-2021/):

1. SSAB, stålproducent, släppte förra året ut 4,78 miljoner ton koldioxid från kol och koks i masugnarna i Luleå och Oxelösund. Det är ca 11 procent av Sveriges totala koldioxidutsläpp, exklusive flygtrafik. På sikt planerar SSAB att ersätta kol med vätgas i Sverige och Finland. 
2. Preem, oljeraffineringsföretag, ägs av den etiopisk-saudiarabiska företagaren Mohammed Al-Amoudi. Företaget ökade under perioden sina utsläppen rejält, till mer än 2,1 miljoner ton.
3. Cementa, del av koncernen Heidelberg Cement, dominerar den svenska cementmarknaden. Bolagets utsläpp minskade något 2020-2021, från 1,9 till 1,8 miljoner ton.
4. LKAB, gruvbolag, utvinner järnmalm och förädlar den till pellets. De planerar liksom SSAB att ersätta kol med vätgas som produceras med förnybar el.
5. Borealis,  österrikiskt olje- och gasbolag (OMV) med stort arabemiratiskt ägande, producerar kemikalier för plast med mera, från fossilgas.
6. Kalkindustrin, med bland annat företaget Nordkalk har stora klimatutsläpp från kalkbränning på samma sätt som cementindustrin. Kalk används bland annat inom stål- och pappersindustrin.
7. ST1 är ett finskägt oljeraffinaderi i Göteborg som även producerar biodrivmedel. De bygger tillsammans med skogsbolaget SCA ett bioraffinaderi som ska vara igång under 2023.
8. Stockholm Exergi i Stockholm producerar värme och el från avfall och biobränslen. Ägare är till hälften Stockholm Stad och till hälften av bland annat en rad pensionsfonder.

...och en rad fler.


### Det där med biobränslen...

Enligt officiell statistik från [Naturvårdsverket](https://www.naturvardsverket.se/data-och-statistik/klimat/vaxthusgaser-territoriella-utslapp-och-upptag) har Sveriges utsläpp minskat med drygt 33 procent sedan 1990. Flera saker anges som orsak:

* En historisk utbyggnad av koldioxidfri elproduktion (vattenkraft och kärnkraft samt på senare år biokraft och vindkraft)
* En utbyggnad av fjärrvärmenäten och den följande övergången från oljeeldade värmepannor till både el och fjärrvärme
* En hög användning av biobränslen och avfallsbränslen inom el- och fjärrvärmeproduktionen
* Bränsleskiften inom industrin, samt minskad deponering av avfall.

Biobränslen anges som en del av lösningen. Men det finns en del kritik mot biobränsle som lösning på klimatkrisen. Det hela hänger på att vi inte har så mycket tid på oss att lösa klimatkrisen. Om vi skulle hugga ner skog för att elda upp eller ha i bränsletanken så kommer den koldioxiden kunna återupptas av nyplanterad skog först efter många år i framtiden ([50-100 år](https://supermiljobloggen.se/miljofakta/biobranslen-klimatlosning-eller-skogsskovling)) och eftersom vi bara har några år på oss att få ner koldioxidhalten i atmosfären så är därför inte sådan biobränsle en bra lösning. Men att ta vara på restprodukter från annat skogsbruk (till exempelvis timmer) skulle vara ett sätt att göra hållbart biobränsle. Men frågan är vad som klassas som restprodukter och om det räcker eller inte.

![Bränslepump](/images/pump.jpg)

Eftersom en rätt stor del av den föreslagna räddningen för flygbranschen är just biobränsle och en stor del av utsläppsminskningen från vägtrafiken kommer från reduktionsplikten så är det värt att veta att det finns en pågående diskussion bland forskare och miljöorganisationer. [Supermiljöbloggen](https://supermiljobloggen.se/miljofakta/biobranslen-klimatlosning-eller-skogsskovling/) har gjort en bra genomgång.

Miljörörelsen är inte eniga i det här. Greenpeace och Skydda skogen är [mer kritiska till biobränslen](https://www.natursidan.se/nyheter/naturskyddsforeningen-vi-behover-gora-skillnad-pa-bra-och-daliga-biobranslen/) än vad Naturskyddsföreningen är.

## Konsumtionsbaserade utsläpp

Det här är vad den svenska befolkningen ger upphov till för utsläpp. Det vanliga här är att man ser vad en person i genomsnitt ger upphov till och därför anger man oftas utsläpp per capita. För det är ju uppenbart att ett land med många fler invånare kommer att ha högre utsläpp än ett mindre land om man har samma slags levnadsvanor.

I Sverige så är de konsumtionsbaserade utsläppen ca 9 ton koldioxidekvivalenter per person och år. Dessa utsläppen omfattar alltså utsläpp från varor och tjänster som används i Sverige oavsett var utsläppen sker. Utsläppen kan alltså ske både inom Sveriges gränser och i andra länder.

Cirka 60 procent av av de konsumtionsbaserade utsläppen kommer från hushållens konsumtion.

![Konsumtiomsbaserade utsläpp](/images/konsumtion-klimat.png)

De största utsläppen från hushållens utsläpp är alltså:

* Transporter
* Livsmedel
* Boende

Därav uttrycket Bilen, Biffen, Bostaden. 

För att nå 1,5 gradersmålet behöver genomsnittet för en världsmedborgare vara på 1 ton.

![Från 9 ton till 1 ton](/images/naturvardsverket-9-till-1.png)

Men det betyder också att om man som enskild svensk skulle leva superklimatsmart i sitt hushåll så kommer man fortfarande genom den offentliga gemensamma konsumtionen bidra till utsläpp.

De resterande 40 procent kommer från offentlig konsumtion och investeringar. Offentlig konsumtion är de varor och tjänster som exempelvis skolor, sjukhus och myndigheter köper in för sin verksamhet. Investeringar är utsläpp kopplade till inköp av byggnader, maskiner, datorer, värdeföremål och lagerinvesteringar.

Miljömålsberedningen, där alla riksdagspartier ingår, har kommit överens om att införa klimatmål som även innefattar konsumtionsbaserade utsläpp. De mål som [Miljömålsberedningen föreslår till regeringen är](https://www.naturvardsverket.se/amnesomraden/klimatomstallningen/omraden/klimatet-och-konsumtionen/):

* Mål för konsumtionens klimatpåverkan, inklusive ett långsiktigt mål om att nå nettonollutsläpp till 2045. 
* Mål för exportens klimatpåverkan. 
* Mål om att inkludera det internationella flygets klimatpåverkan i Sveriges långsiktiga territoriella klimatmål.
* Mål om att inkludera klimatpåverkan från halva sträckan för internationell sjöfart som avgått eller anlöpt svensk hamn, i Sveriges långsiktiga territoriella klimatmål.
* Mål om att inkludera koldioxidutsläpp från inrikes flyg i etappmålet för inrikes transporter till 2030. 
* Mål om att utsläpp från offentlig upphandlade varor och tjänster ska minska snabbare än utsläppen från samhället i övrigt. Miljömålsberedningen föreslår även ett paket med styrmedel och åtgärder inom detta område, inklusive ett krav att beakta de nationella klimatmålen i offentlig upphandling.

### Vilka delar av befolkningen släpper ut mest?

Även om man skulle se till konsumtionsbaserade utsläpp, utsläpp från befolkningen, och se att de måste minska radikalt så är det tydligt att utsläppen fördelas väldigt ojämlikt. De rikaste släpper ut många gånger mer än fattiga. Oxfam har i rapporten [Extreme carbon inequality](https://www.oxfam.se/blog/extrem-ojamlikhet-vaxthusgasutslapp) visat att de rikaste 10 procenten i världen står för nästan hälften av de konsumtionsbaserade utsläppen som är kopplat till livsstil:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam.jpg)

Sverige följer samma mönster inom landet i stort. I rapporten [Svensk klimatojämlikhet](https://www.oxfam.se/svenskklimatojamlikhet) så går Oxfam igenom hur det ser ut i Sverige. Där ser man att de allra rikaste har mycket högre utsläpp:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam-sverige-capita.jpg)

Och det är tydligt att om vi ska nå klimatmålen så är det de allra rikaste som måste ställa om mest. Även vanligt folk i medelklass och arbetarklass kommer att behöva ställa om men inte alls i samma utsträckning:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam-sverige-minskningar.jpg)

Men det skulle inte räcka med att bara de topp tio procenten ställer om. Vilket visas av den här bilden:

![Rika har mycket högre utsläpp än fattiga](/images/oxfam-sverige.jpg)

Att omställningen måste vara rättvis är ett måste för att lyckas få folkligt stöd. Ilskan över rika svenskars flygningar i privatjets som [Dagens ETC rapporterade om](https://www.etc.se/feature/stordalen-hyllar-greta-och-plastbantar-hotellen-flyger-mest-privatjet-i-norden) är ett tecken på det.

![Stordalen flyger mycket](/images/stordalen-flyg.jpg)

## Ska vi ändra vårt sätt att leva?

I diskussionen om minskade utsläpp så blir det ofta en fråga om individ eller struktur. Det finns det en tendens att antingen prata om att minska industrins och företagens utsläpp eller att prata om att individen måste välja mer klimatvänligt i sin vardag. Men sanningen är ju den att båda sakerna måste ske samtidigt. Och att det är politiska beslut som krävs för att minska befolkningens klimat- och miljöpåverkan, inte en informationskampanj om vad som är moraliskt mest korrekt med förhoppningen att "folk skärper sig".

Det smidigaste för politiker är så klart om man kan lägga förslag som minskar utsläppen men som inte stör väljarna. Därför är det smidigt för politiker om teknikutveckling kan göra att folk kan fortsätta köra bil men nästa bil är en elbil. Eller att man kan fortsätta flyga som vanligt men nästa flygresa är med ett elflygplan. Eller att man kan fortsätta äta mycket kött men nästa biff har lägre klimat- och miljöpåverkan.

Klimat- och miljösmart teknikutveckling är bra. Men det kommer inte räcka med teknikutveckling. [Klimatmålen kräver ny teknik OCH ändrat beteende](https://www.chalmers.se/sv/institutioner/see/nyheter/Sidor/Forandringar-i-teknik-och-beteende-kravs-for-att-na-klimatmalen.aspx) enligt bland annat forskare på Chalmers.

![Barn som cyklar på cykelväg](/images/cycling-kids.jpg)

Men beteendeförändringar kan bli något som gör oss mer hälsosamma och lyckligare. Det är en fråga för politiken att se till att teknikutveckling och politik för beteendeförändringar blir på ett sätt så att jämlikheten ökar och livet förbättras.

Det finns dessutom ett starkt stöd att agera bland befolkningarna i Europa, Kina och USA för att rädda klimatet, [visar en undersökning gjord av Europeiska investeringsbanken](https://www.etc.se/klimat-miljo/undersokning-stor-vilja-att-agera-klimatet). Villigheten att agera i Sverige är också stor när deltagarna tillfrågas om de kan tänkas ta kollektivtrafik istället för bil, flyga mindre på semestern eller äta mindre rött kött. Naturvårdsverket har också undersökt [svenskarnas attityder till klimatfrågor](https://www.naturvardsverket.se/amnesomraden/klimatomstallningen/sveriges-klimatarbete/allmanhetens-kunskap-och-attityder-till-klimatfragor/) och får liknande svar.

Jag tänkte att i kommande blogginlägg fördjupa mig i de områden där vi har störst chans att få ner utsläppen.






