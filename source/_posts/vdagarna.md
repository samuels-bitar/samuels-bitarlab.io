---
title: Peppad efter Vänsterdagarna
description: Några korta tankar från Vänsterdagarna
date: 2023-11-26 19:00
image: vdagarna-nooshi.jpg
---
Nu sitter jag på tåget från Göteborg hem till Stockholm efter att ha deltagit på en härlig helg med kamrater från Vänsterpartiet. Jag skriver ner några korta tankar när jag har dem färska.

## Gröna vänstern

Det blev en lyckad nätverksträff med Gröna vänstern, [Vänsterpartiets klimat- och miljönätverk](https://klimatochmiljo.vansterpartiet.se/), på fredagen. Vi var runt 75 deltagare. Vi pratade en del om vad vi gör i nätverket (på sistone mest anordnat webbinarium), valde en ny styrgrupp (jag blev omvald), pratade i smågrupper om tankar om det nya förslaget till partiprogram och fick en rapport från Andrea Andersson-Tay och Kajsa Fredholm (riksdagsledamöter och tillika talesperoner för miljö respektive klimat). Det blev också foto med gänget som Gertrud Ingelman fixade med.

![Andrea Andersson-Tay och Kajsa Fredholm](./images/andrea-kajsa.jpg)*Andrea Andersson-Tay och Kajsa Fredholm*

Styrgruppen valdes med acklamation (med rop) så det behövdes ingen votering. På ett sätt lite synd för jag hade förberett ett system bestående av ett cryptpad-formulär, två pythonskript och utskrivna unika koder. Men det kanske kommer till nytta en annan gång :)

Nätverket hade även en monter i utställningshallen som de andra kamraterna i styrgruppen bemannade väl. Jag hängde också en del där och snackade. Gertrud drog ett extra stort lass.

![Tove Pehrsson, sammankallande för nätverket](./images/tove.jpg)*Tove Pehrsson, sammankallande för nätverket*

Det fanns några programpunkter med fokus på klimat och miljö. Tove Pehrsson som är sammankallande för styrgruppen och nätverket hade en programpunkt om lokal klimat- och miljöpolitik. Jag lyssnade på en intressant punkt om tio budord för klimatomställning i Norge. En facklig person från oljeindustrin berättade om hur facket och miljörörelsen (Natur och Ungdom) hade kommit överens om tio viktiga principer för omställning i Norge. De var väl inte överens om allt när det gäller politik för omställning men de kunde enas om tio principer de höll med varandra om. Intressant om att höra från erfarenheter från andra länder.

## Järnväg, godset från Norviks hamn och en onödig motorväg

Jag lyssnade också på ett intressant panelsamtal om järnvägen som Linda Snecker, riksdagsledamot och trafikpolitisk talesperson, modererade. Jonas Sjöstedt gjorde ett inhopp i slutet och höll ett litet brandtal med visionär järnvägspolitik i slutet för att sammanfatta.

Efter panelsamtalet snackade jag med en av paneldeltagarna från Green Cargo som kör gods på järnväg. Jag är också aktiv i ett nätverk som heter [Tvärnit Södertörn](https://tvarnitsodertorn.se) där vi arbetar för att stoppa motorvägen Tvärförbindelse Södertörn som planeras söder om Stockholm. Ett bärande argument för att bygga vägen är att man menar att den behövs för att transportera gods som kommer från den nya hamnen Norvik som ligger utanför Nynäshamn. Men det har visat sig att det kommer väldigt lite gods till Norviks hamn. (Verkar ha varit en dålig idé över huvud taget att bygga hamnen). Det finns ett järnvägsspår från hamnen som kopplar samman med Nynäsbanan (järnvägsspår från Nynäshamn till Stockholm som pendeltåget går på). Nu är det så lite gods så att Green Cargo har bestämt sig för att sluta köra för att det är inte lönsamt. (Green Cargo har krav på sig att vara affärsmässiga).

Så frågan är om man har byggt en hamn helt i onödan och dessutom (för att rädda ansiktet) planerar att bygga en ny motorväg för 16 miljarder kronor. Det verkar så. Vi kollar vidare.

## Mediakollen

Kamratdataföreningen Konstellationens mediaprojekt [Mediakollen](https://mediakollen.org) har blivit en succé! Jag delade ut flygblad under helgen och frågade om man kände till Mediakollen. Det var väldigt många som redan kände till sidan och som uppskattade den. Andra kände inte till med blev glada över att höra om projektet. En annan medlen av Konstellationen hjälpte till att skriva ut flygblad och jag träffade flera medlemmar. Några andra partikamrater sa att de skulle bli medlemmar i Konstellationen.

![Flygblad om Mediakollen och Kamratadataföreningen Konstellationen](./images/mediakollen-flygblad.jpg)*Flygblad om Mediakollen och Kamratadataföreningen Konstellationen*

## Integritetsfrågor

Vänsterpartiet har bedrivit en väldigt bra politik när det gäller att värna den personliga integriteten. Vi sade nej till FRA-lagen, vi har motsatt oss övervakningsinitiativ både i Sverige och i EU. Nu senast så har vi sagt nej till Chat Control. Kamraterna i riksdagsgruppen och i EU har varit lyhörda och på tårna. Men det är svårt att få tid till allt och svårt att prioritera alla frågor. Speciellt i EU är det snårigt och det krävs mycket tid att sätta sig in i frågorna.

Så vi är nu ett gäng som planerar att dra igång ett nätverk för att jobba mer systematiskt med frågorna och sprida kunskap om när aktuella förslag kan komma upp i riksdagen och i EU.

## Mycket energi från helgen

Aron Etzler hade ett trevligt avslutningstal om att det är trevligt att lyssna på föreläsningar, vara med på workshop, träffa gamla kamrater och lära känna nya. Även om man idag är trött så tar man med sig energin och bär den med sig länge. Och det är så jag känner.

![Samuel trött och glad på ett tåg](./images/samuel-tag.jpg)*Samuel trött och glad på ett tåg*