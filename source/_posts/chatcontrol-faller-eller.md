---
title: Vann vi nyss? Kommer massövervakningsförslaget Chat Control att falla?
image: balloons.jpg
description: Chat Control är ett massövervakningsförslag från EU som Kamratdataföreningen Konstellationen kämpat emot
date: 2023-10-26 17:20
---

Jag fick nyss reda på att den tyska piratpartisten Patrick Breyer som också är EU-parlamentariker [skrev det här blogginlägget](https://www.patrick-breyer.de/en/historic-agreement-on-child-sexual-abuse-proposal-csar-european-parliament-wants-to-remove-chat-control-and-safeguard-secure-encryption/) med rubriken "Historic agreement on child sexual abuse proposal (CSAR): European Parliament wants to remove chat control and safeguard secure encryption":

![Skärmdump av Patrick Breyers hemsida](./images/chat-control-fallen-breyer.png)

Han skriver:

"This week, the European Parliament’s negotiators reached a broad majority agreement on a common position concerning the controversial EU chat control bill. The Commission’s bill proposes bulk scanning and reporting of private messages for allegedly suspicious content by using error-prone algorithms, including „artificial intelligence“. But the European Parliament’s position removes indiscriminate chat control and allows only for a targeted surveillance of specific individuals and groups reasonably suspicious of being linked to child sexual abuse material, with a judicial warrant. End-to-end encrypted messengers are exempted. Instead, internet services will have to design their services more securely and thus effectively prevent the sexual exploitation of children."

Alltså, hela massövervakningsdimensionen tas bort, det som vi kritiker till Chat Control varnat mest för. Det vill säga, att ALLA användare ska övervakas i förebyggande syfte även om man inte är misstänkt för något brott.

Vidare så är totalsträckskrypterad kommunikation undantaget. Detta är också något som vi kritiker understrykit vikten av att göra för om man inte hade gjort det undantaget hade man behövt installera spionbakdörrar i appar som Signal, WhatsApp och Element/Matrix, appar som använder sig av totalsträckskryptering.

Patrick Breyer avslutar med att säga att EU-parlamentets LIBE-utskott ska bekräfta överenskommelsen 13 november.

Här är lite fler länkar om Chat Control:

* [Progressiva bör oroa sig för massövervakningen](/progressiva-bor-oroa-sig/)
* [Vad blir framtidens brott som ChatControl kommer leta efter?](/vad-blir-framtidens-brott/)
* [Pressmeddelande - Bred demonstration mot Chat Control](https://konstellationen.org/2023/09/20/chatcontrol-pressmeddelande-efter-demo/)
* [Video för webbinarium om Chat Control och mailverktyg](https://konstellationen.org/2023/06/21/webbinarium-video-mejla/)
* [Att bekämpa massövervakning är en feministisk ödesfråga](https://www.etc.se/debatt/att-bekaempa-massoevervakning-aer-en-feministisk-oedesfraaga)

Och så självklart den fantastiska kampanjsajten mot massövervakningsförslaget: https://chatcontrol.se

Vi ska inte ropa hej förrän vi är över bäcken. Men det ser riktigt bra ut!

*UPPDATERING 2023-10-26 20:30*

Det ser alltså ut som att det är *EU-parlamentets* hållning i frågan som kommer vara mycket bättre och mot generell massövervakning. Men det är inte slut där. Max Andersson, f.d. EU-parlamentariker, [kommenterar på sin blogg](https://maxandersson.eu/spannande-uppgifter-om-chatcontrol/):

"Men vi som oroar oss för massövervakning har inte vunnit den här frågan än. Nästa steg är att det ansvariga utskottet i EU-parlamentet ska anta det förhandlingsmandat partigrupperna kommit överens om. Det är närmast en formalitet. Men därefter ska Ministerrådet ena sig om ett eget förslag på sitt håll. Det kan ta tid. Om de inte kommer överens om samma sak som EU-parlamentet – och det tror få att de gör – så blir det trialog-förhandlingar mellan Ministerrådet, EU-parlamentet och Kommissionen.

Trialogförhandlingar kan dra ut rejält på tiden och det politiska läget kan ändras. De olika parterna kommer att behöva kompromissa, och det finns alltid en risk att en trialog kommer fram till något som är betydligt sämre än förhandlingsmandatet. Om förhandlingarna håller på ända tills nästa mandatperiod så kan partigrupperna ändra sig, och en del ledamöter av kommer rimligen bytas ut efter valet."

Som sagt, vi ska inte ropa hej än. Men vi har definitivt flyttat fram våra positioner många steg!