---
title: Äntligen dags för kongress med Vänsterpartiet
image: 1maj-2024.jpg
image_description: Bild på mig med fana i 1 maj tåg 2024 . Bild Samuel Scherman
description: Jag skriver några reflektioner om mitt partiengagemang och vad jag hoppas på nu när vi drar igång en femdagarskongress
date: 2024-05-08 10:00
---

Nu är jag på väg till Vänsterpartiets femdagarskongress! Här kommer några reflektioner kring mitt engagemang inom partiet.

Det här blir nog den tredje eller fjärde kongressen jag är med på. Det har alltid varit en trevlig upplevelse. Utöver det har jag varit med på ett antal valkonferenser och distriktsårskongresser.

Det jag ser mest fram emot är att träffa andra kamrater i partiet. Ibland kan det kännas trögt att vara politiskt aktiv. Man sitter på sin kammare, deltar på några videomöten, skriver grejer och på nästa möte får man ursäkta sig för att man inte hunnit med allt man tagit på sig.

Men när man träffas i det fysiska rummet så ger det i alla fall för mig en rejält boost! Det kände jag senast nu på 1 maj. Jag gick runt på Medborgarplatsen i Stockholm, träffade kamrater jag inte träffat på länge och en gammal barndomsvän som också gått med i partiet och som jag inte träffat på länge. Det var många leenden och fantastiskt att få sjunga internationalen med ordentligt ackompanjemang av blåsorkestern.

Ett annat exempel där jag fick känna styrkan av gemensam organisering i det fysiska rummet var på plats på Gubbängen där vi demonstrerade mot det rasistiska våldet. Det var efter nazistattacken mot ett antifascistiskt möte som anordnats av Vänsterpartiet och Miljöpartiet där Expo och antifascisten Mathias Wåg skulle föreläsa och delta i ett panelsamtal. Som tur var blev ingen allvarligt skadad men det är läskigt med det fascistiska våldet som hejas på i extremhögers "alternativmedier" och som kommer till på grund av ett rasistiskt samhällsklimat som det fascistiska partiet Sverigedemokraterna och deras meningsfränder aktivt skapar med hets, hat och hot.

Att få skrika ut om att rasismen ska krossas både i Gubbängen och i första majtågen tillsamman med andra kamrater både inom parlamenten och utanför parlamenten stärker mig och mitt engagemang.

Mycket av mitt engagemang handlar om att jag känner att jag måste göra det för att det är så viktiga frågor. Men när jag träffar andra kamrater på demonstrationer, på första maj, i föreningarnas lokala aktiviteter och på civil olydnadsaktioner, då påminns jag om att det faktiskt är roligt också att vara engagerad! Det är roligt att engagera sig för en socialistiskt värld som är jämlik och där vi lever inom planetens gränser. En försmak av det kan vi få se i enagemanget. Att träffa andra likasinnade är viktigt och stärkande.

Nu är jag på väg till Vänsterpartiets kongress. Som sagt, det jag mest ser fram emot är att träffa alla fina kamrater. Men jag ser också fram emot de politiska diskussionen, om vilka vägval vi i partiet ska ta. Det finns nog lika många tankar om det som det finns medlemmar i vårt parti. Vänsterpartister är folk som vågar stå upp för det man tror på, som kan tänka fritt och vill uttrycka sig. Vi är alla så klart olika och alla kanske inte trivs i talarstolen men vi är med i Vänsterpartiet för att vi vill påverka samhället att gå i en socialistisk utveckling.

Jag själv blev aktiv i Vänsterpartiet 2014 när jag blev invald i kommufullmäktige i Haninge. Jag blev snabbt engagerad i det rikstäckande klimat- och miljönätverket och blev invald i styrgruppen. Sen blev det distriktets miljö- och klimatutskott, något år senare ledamot i distriktsstyrelsen. I valet 2018 blev jag listetta i Haninge och sen gruppledare. Kongressen 2022 blev jag invald i partistyrelsen som första ersättare. Under årets gång har jag skrivit många motioner, både i Haninge för Vänsterpartiet, men även intern i Vänsterpartiet till kongresser, till valplattformar och kunnat påverka Vänsterpartiets politik.

Självklart är jag medveten om att jag har det extra förspänt som vit man i 30-årsåldern som pluggat på universitet. Men jag vet också att kulturen i Vänsterpartiet är att vi gärna vill engagera fler! Vi vill att alla ska vara med, oavsett bakgrund. Och vi försöker hjälpa till för att det ska hända. Jag försökte som gruppledare i Haninge skapa ett sådant klimat och försöker göra det i andra sammanhang. Och i Vänsterpartiet går det att som ny medlem bli invald direkt in i en styrelse eller bli ombud på en kongress. Det är möjligt att sitta själv och skriva en motion och skicka in till kongressen. Och ofta kan man få sin partiförening att skriva under den och skicka in i föreningens namn.

Därför är det inte konstigt att resultatet från kongresser blir bra. "Kongressen har alltid rätt". Jag tycker Vänsterpartiets demokrati är en rätt häftig sak och något att vara stolt över och värna över. Självklart kan man bli missnöjd över saker och ting. Det är vi alla som är Vänsterpartiet och vi är ju bara männsikor. De flesta av oss gör det helt på vår fritid. Och de som är avlönade för att göra det på heltid har så sjukt många uppgifter (även ofta de ideella inom partiet) och har svårt att få tiden att räcka till. Många irritationsmoment kan vara på grund av enkel tidsbrist. Men sen kan vi så klart också tycka olika.

Jag tror att kamrater i partiet har goda avsikter, vare sig man är gräsrotsaktivist som delar flygblad, deltar i demonstrationer och aktivistar på nätet eller om man är riksdagsledamot, kommunalråd, gruppledare, partistyrelseledamot eller VU-ledamot. Men vi är olika människor, har olika erfarenheter och olika åsikter om hur man bör göra olika saker. Nu när vi träffas och tillsammans ska staka ut vägen för partiet i flera dagar så hoppas jag att även om vi har skilda åsikter i olika frågor så kan vi utgå ifrån att vi alla har goda avsikter och ha en kamratlig ton även om man har helt skild uppfattning i en fråga.

Vänsterpartiet är ett speciellt parti. Ett parti för socialister, feminister, antirasister, klimataktivister och många fler som vill kämpa tillsammans för ett bättre samhälle. Vi är många och vår politik är något som attraherar. Vår politik är inget som ska tonas ner eller vattnas ur för det är vår politik som gör att man söker sig till Vänsterpartiet. Men vi ska kommunicera våra politik på ett enkelt sätt som gör att fler vill vara med. Det tror jag vi gör på många olika sätt. Att vi skriver ett bra partiprogram är självklart en viktig byggsten. Men jag tror att det riktiga arbetet börjar efter kongressen. Belsuten vi tar på kongressen är viktiga men det är i det faktiska arbetet som det realiseras. Det är i aktivieterna vi anordnar med lokala antirasistiska föreningen, i frågestunden på kommunfullmäktige där vi kräver svar på varför högern stänger ännu en fritidsgård och i samtalet med vänner, arbetskamrater och familj.

Jag ser fram emot en lång och härlig kongress även om jag kommer att vara rätt mör efteråt. Och slutligen: kongressen har alltid rätt!



