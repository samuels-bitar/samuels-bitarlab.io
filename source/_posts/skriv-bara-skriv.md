---
title: Skriv bara skriv
image: skrivbok.jpg
description: Några tankar om skrivande som jag har
date: 2023-12-29 22:00
---

Jag har gått i tankar kring skrivande rätt länge. Det har varit något av ett tankeprojekt för mig och med det menar jag mest ett projekt som är kul att gå att tänka på (och där det är den största behållningen) men att det är ungefär lika sannolikt som att jag genomför projektet som att jag bara funderar på det.

Att skriva är något som jag funderat på länge och som jag varit sugen på länge. Men skriva vad? Blogginlägg, krönikör, faktaböcker, noveller eller romaner? 

Jag har snackat om de här tankarna med min fru och i julklapp fick jag den trevliga boken "Skriv bara skriv" av Louise Alvarsson med många bra tips på att komma igång och som har en massa roliga övningar. En av grejerna hon tar upp som tips är att starta en blogg. Och eftersom jag redan har en så tänkte jag att jag bara kan skriva lite mer här :)

## Blogga privat

Jag har bloggat nu privat i några år, både på den här bloggen och på [Tofubitar](https://tofu.bitar.se/). Fokus har varit på teknik och politik. Innan det så bloggade jag när jag var kommunpolitiker för [Vänsterpartiet i Haninge](https://haninge.vansterpartiet.se/) 2014-2021 ungefär. Och skrev motioner och interpellationer. Så att skriva har jag gjort länge. Men jag vill utöka det, både på bredd och på djup. Blogga mer men också skriva bredare.

## Blogga på jobbet

Jag fick möjligheten att skriva ett väldigt nischat tekniskt blogginlägg om Googles nya annonssatsning för att komma med ett alternativ till tredjepartscookies (som fler och fler verktyg blockar), så det blev blogginlägget [Så stoppar vi Googles försök att övervaka dig utan cookies](https://www.etc.se/etc/sa-stoppar-vi-googles-forsok-att-overvaka-dig-utan-cookies).

## Krönikor

Den som väntar på något gott kommer snart få läsa en techkrönika av mig i en trevlig tidning! Jag funderar på att utöka och ta kontakt med fler tidningar och se om de vill ha techkrönikör.

## Böcker

Jag tycker om att läsa böcker och läser periodvis mycket. Men skulle jag kunna skriva en bok själv? Jag har många vänner, kamrater och bekanta som har skrivit böcker. Innan jag kom i kontakt med dem så tänkte jag att en författare är typ Stephen King, Liza Marklund eller någon annan som "jobbar som författare" och kan leva av det. Men många av de jag känner tjänar väldigt lite pengar på det. De gör det antingen som ett hobbyprojekt på fritiden vid sidan av sitt lönearbete eller så är de heltidsaktivister som lever på otroligt små marginaler (finansierat oftast via gåvor eller deltidsjobb) och där skrivandet är en del av aktivismen.

Några vänner och kamrater som inspirerat mig är:

* **[Jonas Lundström](https://www.patreon.com/jonas_lundstrom)**. Anarkist och aktivist som bland annat skrivit om polisvåld och brottsbekämpning i [Batongerna slår nedåt](https://www.bokus.com/bok/9789176993392/batongerna-slar-nedat-en-berattelse-om-brottsbekampning/)
* **[Annika Spalde](https://sv.wikipedia.org/wiki/Annika_Spalde)**. Kristen ickevåldsaktivist och djurrättare. Hon har skrivit bl.a. [I vänliga rebellers sällskap](https://www.bokus.com/bok/9789188552501/i-vanliga-rebellers-sallskap-kristet-ickevald-som-konfrontation-och-omhet/), [Varje varelse ett Guds ord](https://www.bokus.com/bok/9789188552600/varje-varelse-ett-guds-ord-omsorg-om-djuren-som-kristen-andlighet/) och [Mystik och politik](https://www.bokus.com/bok/9789173155069/mystik-och-politik-befriande-kristendom/)
* **[Pelle Strindlund](https://sv.wikipedia.org/wiki/Pelle_Strindlund)**. Kristen ickevåkldsaktivist och djurrättare. Han har skrivit flera böcker tillsammans med Annika Spalde men även många egna, t.ex. [Jordens herrar](https://www.bokus.com/bok/9789185703678/jordens-herrar-slaveri-djurfortryck-och-valdets-forsvarare/), [Djurens förintelse](https://www.bokus.com/bok/9789187207716/djurens-forintelse-manniskans-nazism-mot-andra-arter/) och [Motståndets väg](https://www.bokus.com/bok/9789185703746/motstandets-vag-civil-olydnad-som-teori-och-praktik/)
* **[Lena Staaf](https://verbalforlag.se/forfattare/lena-staaf/)**. Vänsterpartist och socialarbetare som skriver dikter. Hon har skrivit bl.a. [Asociala dikter](https://verbalforlag.se/bocker/asociala-dikter/)
* **[Jens Holm](https://jensholm.se/om/)**. Vänsterpartist och f.d. riksdagsledamot, EU-parlamentariker och klimatpolitisk talesperson. Han har skrivit bl.a. [Om inte vi, vem?](https://www.bokus.com/bok/9789187193309/om-inte-vi-vem-politiken-som-raddar-klimatet-och-forandrar-vanstern/) som handlar om vänsterpolitik för att lösa klimatkrisen

Jonas Lundström har använt sig också av print on demand som gör att man enkelt kan skriva ut böcker utan eget förlag. Så om jag har ambitionen att skriva och få ut en bok som man kan sälja på eget förlag t.ex. så är ju ribban satt bra mycket lägre än om man siktar på att bli nästa Liza Marklund. Och att syftet är att skriva om saker som jag själv tycker är roliga att läsa och skriva om.

## Skriva om vad?

Jag är rätt sugen på att skriva om både det ena och det andra. Det som ligger närmast till hand att det faktiskt blir av är bloggtexter, krönikör och debattartiklar. Och ämnena blir sannolikt tekniktexter om öppen källkod, integritet, övervakning, AI, infrastruktur och digital organisering från ett vänsterperspektiv. Som webbutvecklare (eller devopsare), civilingenjör i datateknik och mångårig användare av öppna verktyg så är det väl de grejerna som jag kan bäst och har något "att säga om".

Men jag är också väldigt sugen på att skriva skönlitterärt. Vad ska man skriva om då? Jag har hört någonstans att det är bra att skriva om saker man känner till. Så det kan handla om teknik, programmering, övervakning, bo i kollektiv och i kollektivhus, vara uppväxt i frikyrkomiljö, Vänsterpartiet, kommunpolitik, internpolitiskt arbete, föreningsarbete, veganism, klimataktivism och lite sånt.

## Husguden Cory Doctorow

Min personliga husgud får man väl säga är [Cory Doctorow](https://en.wikipedia.org/wiki/Cory_Doctorow). Han är författare, bloggare, journalist och aktivist. Han skriver både faktatexter och skönlitterära texter. Dessutom har har skrivit många texter och släppt dem under en fri [Creative Commons-licens](https://en.wikipedia.org/wiki/Creative_Commons_license) vilket gör att de är gratis att ladda ner och sprida. (Tyvärr är det något han inte gör med sina nyare böcker). Doctorow skriver om teknik från ett skönlitterärt sätt och hans böcker får väl klassas som sci-fi även om de inte ligger långt fram i framtiden och ibland inte ens det.

Några riktiga pärlor av Doctorow är:

* [Little Brother](https://en.wikipedia.org/wiki/Little_Brother_(Doctorow_novel)) och de efterföljande böckerna i serien [Homeland](https://en.wikipedia.org/wiki/Homeland_(Doctorow_novel)), [Lawful interception](https://www.tor.com/2013/08/28/lawful-interception/) och [Attack Surface](https://www.goodreads.com/book/show/49247283-attack-surface). De handlar om motstånd mot massövervakning kan man väl kort säga
* [For the Win](https://en.wikipedia.org/wiki/For_the_Win)
* [Pirate Cinema](https://en.wikipedia.org/wiki/Pirate_Cinema_(novel))

Två böcker som är väldigt aktuella men som jag ännu inte hunnit med är:

* [How to Destroy Surveillance Capitalism](https://bookshop.org/p/books/how-to-destroy-surveillance-capitalism-cory-doctorow/16078317?ean=9781736205907)
* [The Internet Con: How to Seize the Means of Computation](https://www.goodreads.com/book/show/120806182-the-internet-con)

## Tips och trix?

Har du några tips på att komma igång med skrivande? Skriver du själv? Vad har du för tankar om det? Varför gör du det? Vad får du inspiration från? Skriver du skönlitterärt eller facklitteratur?

Låt tipsen komma! Maila mig gärna på samuel@bitar.se eller [peta på mig på Mastodon](https://social.spejset.org/@samuel).