---
title: Krönika -  Så hotar ljudboken jämlikheten
image: audiobook.jpg
image_description: Bild av <a href="https://pixabay.com/users/sik-life-2171488/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3106986">Felix Lichtenfeld</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3106986">Pixabay</a>
description: En krönika om hur jämlik tillgång till kultur och litteratur hotas
date: 2024-12-28 13:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/saa-hotar-ljudboken-jaemlikheten).

**Fler och fler böcker produceras bara för att bli ljudböcker. Storytel producerar ljudböcker som bara finns tillgängliga på deras plattform. Det innebär även att biblioteken inte har tillgång till dem.**

Läsandet av pappersböcker har gått ner. Samtidigt har lyssnandet på ljudböcker gått upp. På knappt tio år har andelen som angett att de lyssnat på en ljudbok senaste året gått från 26 procent till 42 procent enligt [SOM-undersökningen](https://www.gu.se/sites/default/files/2024-09/Kulturvanor%20i%20Sverige%201987-2023.pdf). På frågan "Bokläsning minst varje vecka de senaste 12 månaderna" har 20 procent svarat att de "lyssnat på ljudbok/talbok". [Långt fler](https://www.gu.se/sites/default/files/2021-06/153-164%20Wallin%20m.fl_..pdf) tog del av ljudböcker via digitala prenumerationstjänster (som Storytel, Nextory eller Bookbeats) än via bibliotek.

På Stockholms biblioteks hemsida kan man [filtrera böcker på e-ljudböcker](https://beta.biblioteket.stockholm.se/sok?text=&mediaType=E_AUDIO_BOOK). Då ser jag att 3 069 böcker är tillgängliga. Långt ifrån Storytels utbud som är runt en miljon titlar eller Nextory som har hundratusentals titlar.

Min fru och jag har testat ljudboksapparna Storytel och Nextory. Vi har också testat "bibliotekens app" Biblio som är gratis och där man kan ladda ner ljudböcker via sitt bibliotek (till exempel från Stockholm). Vi testade ett tag men det var för jobbigt att använda appen och svårt att hitta litteratur.

Fler och fler böcker produceras bara för att bli ljudböcker. Storytel producerar ljudböcker som bara finns tillgängliga på deras plattform. Det innebär även att biblioteken inte har tillgång till dem.

Jag vill att biblioteken fortsättningsvis ska kunna bidra till jämlik tillgång på kultur och fakta. Det är gratis att låna pappersböcker och även de med låga inkomster kan idag storkonsumera aktuell kultur. Men i takt med att mer litteratur produceras för att bara bli ljudböcker och att tillgången till de ljudböckerna blir svårare via biblioteken så står vi inför ett problem som bara kommer att öka om inget görs.

Det allra bästa vore om Biblio-appen förbättras och utbudet ökar så att även kommande generationer barn kan ta del av aktuell kultur på jämlika villkor. Annars blir det en kulturgärning att tipsa om bästa platserna att piratkopiera ljudböcker.