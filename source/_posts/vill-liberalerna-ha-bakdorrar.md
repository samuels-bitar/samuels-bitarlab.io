---
title: Vill Liberalerna ha bakdörrar i våra kommunikationsappar?
image: liberalerna-surveillance.jpg
image_description: Bild av <a href="https://pixabay.com/users/kirill_makes_pics-5203613/">kirill_makes_pics</a> från <a href="https://pixabay.com">Pixabay</a>
description: Jag gör ännu ett försök med att försöka få svar från Liberalerna på rätt enkla frågor.
date: 2025-02-28 16:00
---

Jag gör ännu ett försök med att försöka få svar från Liberalerna på rätt enkla frågor. Den här gången handlar det om att regeringen föreslår [bakdörrar](/backdorrar-osakert/) i kommunikationsappar. [Vad tycker L om Chat Control?](/duckade-chat-control-igen/) är en fråga jag försökte få svar på men ännu inte fått.

Så jag skickade ett mail ännu en gång.

## Ämne: Vill ni ha bakdörrar i våra kommunikationsappar?

Hej Liberalerna,

Jag mailar er för att få veta hur ni ställer er till att kräva bakdörrar i svenskar kommunikationsappar och hur ni ser på rätten till privat kommunikation (som är en mänsklig rättighet).

Regeringen har tagit fram lagförslaget "Datalagring och tillgång till elektronisk information" där man bland annat föreslår att bakdörrar måste byggas in i totalsträckskrypterade appar.

I utkastet till lagförslaget föreslår man:

> "Tillhandahållare av nummeroberoende interpersonella kommunikationstjänster ska omfattas av regler om lagringsskyldighet och ska anpassa sin verksamhet så att hemliga tvångsmedel kan verkställas."

Eller med andra ord: man föreslår ett krav på bakdörrar, även för totalsträckskrypterade appar som Signal.

https://www.svt.se/nyheter/inrikes/it-experten-man-vill-komma-at-kriminella-men-laglydiga-straffas

Remisserna är väldigt kritiska till att bygga in bakdörrar.

https://www.regeringen.se/remisser/2024/11/remiss-av-utkast-till-lagradsremiss-datalagring-och-tillgang-till-elektronisk-information/

Försvarsmakten skriver i sitt remissvar:

> "Utifrån sin sakkunskap genom uppdraget att leda och samordna signalskydds­tjänsten ser Försvarsmakten anledning att kommentera vad som anförs i utkastet om hur anpassnings­skyldigheten kan genomföras vid totalsträcks­krypterad kommunikation. Försvarsmakten bedömer att kravet på anpassnings­skyldighet av nummer­oberoende interpersonella kommunikations­tjänster inte kommer att kunna uppfyllas utan att införa sårbarheter och bakdörrar som kan komma att nyttjas av tredje part."

Truesec skriver i sitt remissvar:

> "Enligt Truesec saknas tillräcklig sakkunnig expertis inom cyber och cybersäkerhet i utredningen. Detta gör att konsekvenserna för totalsträcks­krypterade s.k. nummer­oberoende interpersonella kommunikations­tjänster (”NOIK”) inte är tillräckligt utredda, särskilt hur förslaget kan leda till sämre säkerhet för samtliga användare."

Journalistförbundet skriver i sitt remissvar:

> "Förslaget till reglering av nummer­oberoende interpersonella kommunikations­tjänster i utkastet till lagrådsremiss har mycket gemensamt med CSAM-förordningen, EU-förordningen som även kallas Chat control, eftersom båda förslagen skulle omöjliggöra fullsträcks­kryptering. Mot bakgrund av de negativa effekter förslaget skulle få för journalister och andra som har stort och legitimt behov att kommunicera anonymt avstyrker Journalist­förbundet förslaget."

Internetstiftelsen skriver i sitt remissvar:

> "Som vi framfört tidigare ifrågasätter Internetstiftelsen inte att tillgång till information och bevisning från elektroniska kommunikationer ofta är av stor betydelse i brottsutredningar. Men att bryta eller försvaga krypteringslösningar för att komma åt uppgifter kan få stora konsekvenser; det öppnar upp möjligheten för missbruk, både vid själva användningen och av tredje part, vilket äventyrar samtliga användares integritet och säkerhet.
> 
> Internetstiftelsen välkomnar förtydligande i utkastet till lagrådsremiss att det är av vikt att exempelvis totalsträckskrypterade tjänster alltjämt kan användas (s. 88) men noterar samtidigt att utredningen framhåller att med förslaget blir de aktuella tillhandahållarna skyldiga att lämna ut uppgifter till de brottsbekämpande myndigheterna ur den totalsträckskrypterade informationen. För Internetstiftelsens går resonemanget inte ihop eftersom hela syftet med totalsträckskrypterade tjänster är att ingen förutom mottagare och sändare ska kunna ta del av informationen"

Nu går Signal ut och säger att de kommer att lämna Sverige om lagförslaget går igenom:

https://www.svt.se/nyheter/inrikes/signal-lamnar-sverige-om-regeringens-forslag-pa-datalagring-klubbas

Försvarsmakten, EU-kommissionen och EU-parlamentet uppmanar Signal just för att den är totalsträckskrypterad och inte har bakdörrar.

https://www.forsvarsmakten.se/sv/aktuellt/2025/02/forsvarsmakten-anvander-appen-signal-for-oppen-kommunikation-med-mobiltelefoner/

https://www.politico.eu/article/eu-commission-to-staff-switch-to-signal-messaging-app/

https://www.politico.eu/article/european-parliament-urge-mep-message-signal-encryption/

När Salt Typhoon-hacket nyligen genomfördes av Kina mot amerikanska telefonoperatörer och internetleverantörer så användes just en bakdörr. Något som påminner oss att det finns inget som heter bakdörr som bara de goda har tillgång till:

https://techcrunch.com/2024/10/07/the-30-year-old-internet-backdoor-law-that-came-back-to-bite/

https://www.eff.org/deeplinks/2024/10/salt-typhoon-hack-shows-theres-no-security-backdoor-thats-only-good-guys

I somras/höstas svarade V, C, MP och SD ja på bland annat dessa frågor:

* Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
* Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
* Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?

https://konstellationen.org/2024/09/19/chatcontrol-rapport/

Centerpartiet motsätter sig lagförslaget och även ert ungdomsförbund LUF gör det:

> ”Dessutom skulle dessa datalagringssystem bli högt värderade mål för hackare från skurkstater som Kina och Ryssland,” säger Anton Holmlund i ett pressmeddelande från ungdomsförbundet.
>
> ”Vi riskerar att förlora förtroendet för digital kommunikation, oavsett om det handlar om privatpersoners integritet, företagshemligheter eller statens säkerhet”, tillägger han.
>
> Vill grundlagsskydda rätten till krypterad information
>
> LUF skriver att de istället vill se att rätten till att kryptera information ska grundlagsskyddas.

https://www.svt.se/nyheter/inrikes/centern-nej-till-tekniska-bakdorrar-i-krypterade-appar

Så därför skulle jag vilja ha svar på följande frågor från er:

1. Tycker ni att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
2. Tycker ni att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
3. Anser ni att privat kommunikation är (eller bör vara) en mänsklig rättighet?

Vänliga hälsningar,
Samuel Skånberg

## Kontaktuppgifter

Här är kontaktuppgifter till Liberalerna tagna från [riksdagens hemsida](https://www.riksdagen.se/sv/ledamoter-och-partier/partierna/liberalerna/):

* Officiell mail - info@liberalerna.se
* Partisekreterare Jakob Olofsgård - jakob.olofsgard@riksdagen.se
* Gruppledare Lina Nordquist - lina.nordquist@riksdagen.se
* Vice gruppledare Louise Eklund - louise.eklund@riksdagen.se
* Ledamot i Justitieutskottet, Martin Melin - martin.melin@riksdagen.se

Man kan passa på att cc: Liberalernas ungdomsförbund också. Det gör man genom att cc:a martin@luf.se som är pressekreterare för [förbundsordförande Anton Holmlund](https://www.luf.se/anton-holmlund/).

## Svar från Liberalerna 2025-03-04

Hej Samuel,

tack för ditt mail.

Liberalerna har varit tydliga med att i EU-parlamentet säga nej till de chat-control förslag som hittills presenterats.

Vi kommer att väga remissvaren mot de brottsbekämpande förslagen för att hitta en för oss acceptabel balans mellan brottsbekämpning och rätten till privat kommunikation. Vi tar med oss det du skriver in i det fortsatta arbetet, men ber att få återkomma med vårt ställningstagande när vi har ett slutgiltigt förslag att ta ställning till.

Mvh

Lars Granath
Utredare 