---
title: Chat Control handlar om massövervakning av vanligt folk
image: man-with-binoculars.jpg
image_description: Bild av man med kikare av <a href="https://pixabay.com/users/mohamed_hassan-5229782/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3539810">Mohamed Hassan</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3539810">Pixabay</a>
description: Chat Control handlar inte om att totaltförbjuda kryptering utan att massövervaka vanligt folk och göra det svårt att skydda sig genom kryptering
date: 2024-06-28 19:00
---

Turerna kring EU-direktivet Chat Control 2.0 har varit dramatiska minst sagt senaste året! Det har varit ett [massivt motstånd](https://konstellationen.org/2023/09/14/chatcontrol-pressmeddelande/) mot massövervakningsförslaget från flera olika håll. Minsterrådets legal service, EU-parlamentets konsekvensutredning, EU:s dataskyddsmyndighet, Advokatsamfundet, Centrum för Rättvisa, Integritetsskyddsmyndigheten, Internetstiftelsen och Journalistförbundet varnar för förslagets konsekvenser. Även 35 stycken ledarsidor har skrivit [kritiska texter](https://chatcontrol.se/). 

Senaste turerna i riksdagen [förra veckan](https://samuels.bitar.se/v-mp-chat-control/) var också dramatiska. Belgien hade EU-ordförandeskapet och ville samla medlemsländernas regeringar kring ett gemensamt kompromissförslag. Förslaget var inte lika illa som det ursprungliga förslaget från EU-kommissionen men i grunden var det samma massövervakningsförslag: all kommunikation skulle skannas mellan oskyldiga nätanvändare för att i "förebyggande syfte" säkerställa att man inte skickade något olagligt. Alltså, ungefär som Stasi gjorde i Östtyskland, fast den här gången i modern tid och granskning av digital kommunikation. Emanuel Karlsten gjorde ett bra arbete i att gräva i frågan. Regeringen med Gunnar Strömmer (M) i ledningen ville snabbt och smidigt att Sverige skulle ställa sig bakom det belgiska förslaget. Punkten slängdes in på dagordningen på Justitieutskottets möte och det blev en dragning på sittande möte. C och SD anmälde avvikande mening. [V och MP erkände senare att de missat](https://samuels.bitar.se/v-mp-chat-control/) och borde gjort det också. Gällande MP hade [Rasmus Ling gått mot partilinjen](https://www.svt.se/nyheter/inrikes/internbrak-i-mp-kring-chat-control-det-blev-krig) och fått rejäl kritik inifrån partiet. Centerpartiet gjorde en likande miss förra året när de inte anmälde avvikande mening, just för att handlingarna kommit sen och de inte var förberedda. Om handlingarna kommit i rimlig tid och alla partier hade haft möjlighet att förstå vad det handlade om hade C, V, MP och SD anmält avvikande mening.

Men det blev [inget beslut om Chat Control](https://www.aftonbladet.se/nyheter/a/vg5xPL/avgorande-dag-for-chat-control-i-eu) i ministerrådet. Punkten om Chat Control plockades bort för Belgien hade inte lyckats samla tillräckligt med stöd. Frågan kan eventuellt aktualiseras i höst igen. Vi har därmed förhoppningvis tid nu för att kunna folkbilda mer kring Chat Control och varför EU-kommisionens förslag är dåligt och varför även det belgiska förslaget är dåligt.

Därför skulle jag i det här inlägget vilja ta upp igenom följande punkter:

* Chat Control är ett massövervakningsförslag först och främst.
* Sannolikt kommer inte kryptering förbjudas, bara göra det otillgängligt för gemene man. I den mån kryptering försvårar Chat Control ska man gå runt det genom klientside-skanning.
* Det är de stora och populära plattformarna man inriktar sig mot.
* Det är inte de enskilda förslagen som är viktiga, det är principerna.
* Tekniskt kunniga och intresserade kommer alltid kunna fortsätta kommunicera privat.

## Chat Control är ett massövervakningsförslag

Anledningen till att vi nätaktivister pratar om att Chat Control är ett massövervakningsförslag är för att alla användares kommunikation på en plattform kommer att skannas. I vanliga fall när man misstänker att någon begår brott så görs en riktad övervakning mot en enskild person eller en grupp av personer. Det kan göras på olika sätt. Polis kan bugga telefoner och hem. De kan även hacka sig in i telefoner. Det finns så klart saker att vara vaksam för även där. Men den stora skillnaden är att det är riktad övervakning. Övervakningen sker mot någon som är misstänkt för brott.

Chat Control däremot går ut på att skanna allt innehåll. Även idag skannas din kommunikation på t.ex. Facebook eller Gmail utan att du är misstänkt för brott. Det är skäl nog att byta plattform till plattformar som respekterar din integritet. Men det nya med Chat Control är att det blir obligatoriskt för företagen eller organisationerna som tillhandahåller tjänsterna att massövervaka alla. Just nu är det tillåtet för de som driftar plattformarna att övervaka sina användare om de vill. Om Chat Control går igenom så kommer de tvingas att massövervaka.

## Kommer alla kommunikationsplattformar tvingas skanna sina användares kommunikation?

Nja, alla kommunikationsplattformar kommer inte behöva skanna sina användares kommunikation, bara de populära i praktiken. Bara de som får en **spårningsorder** utfärdad från en nationell myndighet kommer behöva massövervaka sina användare. Det kommer i praktiken bara bli de populära plattformarna som man känner till som spårningsorder kommer utfärdas för. Så det blir "bara" de plattformar som får en spårningsorder mot sig som behöver övervaka sina användare. Därför kommer teknikkunniga eller intresserade kunna sätta upp sin egen mailserver, XMPP-server, Matrix-server eller annan privat kommunikationsplattform.

[Integritetsmyndigheten](https://www.imy.se/blogg/csam-forordningen-chat-control-2.0-och-ett-tryggt-informationssamhalle/) sammanfattar hur det skulle fungera med spårningsorder:

> Enligt förslaget till den nya CSAM-förordningen (CSAM är en förkortning av ”child sexual abuse material”) kommer leverantörer som tillhandahåller kommunikations- och molnlagringstjänster kunna bli ålagda att spåra hela eller delar av sina tjänster för att söka efter material med sexuella övergrepp mot barn och grooming (vuxnas kontaktsökning med barn i sexuellt syfte). Det kommer att ske genom att en nationell myndighet utfärdar en så kallad spårningsorder. Sökningen avgränsas inte till enskilda användare av tjänsten utan det är tjänsten i sig som spårningsordern omfattar. Om en leverantör av en kommunikations- eller molnlagringstjänst får en spårningsorder innebär det att leverantören utifrån hur spårningsordern är utformad ska skanna innehållet för att söka efter material innehållande sexuella övergrepp mot barn eller grooming.

Det är alltså en spårningsorder som omfattar hela tjänsten, inte den misttänkte förövaren. 

## Vilka plattformar kommer omfattas?

Jag är rätt säker på att man bara kommer bry sig om de stora plattformarna. Jag tror att de här blir påverkade:

* Facebook
* Instagram
* Twitter
* Snapchat
* TikTok
* Youtube
* Roblox, Fortnite och andra onlinespel
* Gmail, Hotmail och andra mailleverantörer
* Discord

Kanske även:

* Signal
* Telegram
* Matrix/Element

(Och många fler populära jag inte kommer på nu).

Det är de här plattformarna och apparna som jag tror man kommer vilja komma åt. Och det är de som myndigheter kommer utfärda spårningsorder för.

Men vad skulle hända då om ett företag eller organisation skulle vägra? Ja, juridiskt så kommer de knappast kunna gömma sig. De kommer behöva lyda om de ska fortsätta få bedriva verksamhet i EU. Men säg att det ändå skulle vara ett företag eller organisation som tillhandahåller en plattform och av princip kommer vägra att installera klientside-skanning t.ex. och att de skulle lyckas klara sig undan rent juridiskt. Inte så sannolikt, men vi säger att det går för sakens skull.

Det skulle ändå räcka med om man skulle förbjuda Google och Apple att ha med appar till de plattformar som inte respekterar Chat Control. Även om det finns en webbversion av appen eller om man kan ladda ner appen och installera på annat sätt än via app-stores så kommer det räcka med att förbjuda appen i app-stores för att få tillräcklig effekt. För de kommersiella plattformarna så skulle det vara en stor risk att de skulle gå under om deras appar inte finns på app-stores. (Vilket i sig visar risken med den centraliserade makten som Google och Apple har).

Någon kanske tänker att man kan ju använda webbversionen av t.ex. Matrix/Element och det stämmer. Det skulle nog gå bra. Även om Matrix-appar (som Element) skulle förbjudas i apps-tores i EU så skulle man kunna använda webbversionen. Och det skulle sannolikt fortfarande gå att ladda ner XMPP-klienter och använda totalsträckskrypterad kommunikation på så sätt och därmed fortsätta ha sin kommunikation privat. Även PGP-krypterade mail med sin lokala mailklient (t.ex. Mozilla Thunderbird) kommer säkert att fortsätta.

Men poängen är att "vanligt folk" inte kommer att använda de teknikerna. De flesta har varken kunskap, intresse eller ork att skydda sin integritet. Jag har lyckats få min familjs gruppchatt att vara på Signal. Men om mina familjemedlemmar inte längre skulle kunnat ladda ner Signal från app-stores så tror jag inte det hade gått att byta. Det är mycket lättare att använda de plattformar som "alla andra" använder.

Så (totalsträcks)kryptering kommer knappast förbjudas. Men kryptering kommer sluta vara smidigt tillgängligt och därför inte användas av gemene man. Och det är det stora problemet när det kommer till massövervakningen. För även om jag själv skulle kunna isolera mig och bara snacka med andra nätaktivister krypterat så vill jag ju inte att mina vänner, familj, partikamrater, arbetskamrater och alla andra ska övervakas. Dessutom så vill jag ju fortfarande kommunicera med dem. Så då kommer jag ändå bli övervakad om kommunikationen med vänner och familj sker via plattformar som övervakar sina användare.

## Krypteringsförbud?

Själva krypteringstekniken som sådan kommer sannolikt inte att förbjudas. (Den var ju förbjuden i t.ex. USA under 90-talet vilket som tur är historia nu). Men kryptering kommer inte tillåtas på plattformar som får spårningsorder riktades mot sig om den krypteringen gör vad den ska, dvs att den håller kommunikationen privat och hemlig bara för de som kommunicerar. Det troligaste är att apparna kommer tvingas installera bakdörrar som kan skanna av meddelandet innan det krypteras och skickas till mottagaren. Och då försvinner ju själva poängen med krypteringen.

Och som sagt, det kommer sannolikt inte bli något förbud mot att skicka PGP-krypterade mail med sin egna mailklient.

Chat Control handlar inte om att göra det omöjligt för brottsliga att kommunicera privat för de kommer alltid kunna fortsätta göra det. Det här handlar om att övervaka vanligt folks kommunikation. Visst, nog är det så att man kan få tag på en del personer i början som inte tänker sig för och delar olagligt material på de populära plattformarna.

## AI och automatisk skanning

Men det är ju massvis med material att granska i så fall kanske du tänker. Och det stämmer. Det är mängder med material! En sådan mängd så att det behöver granskas automatiskt. Antagligen kommer allt material skickas genom ett AI-system för att se om något misstänkt dyker upp. För varje meddelande, bild eller video. Det är lite enklare om man bara ska upptäcka redan känt CSAM-material för då kan man jämföra den övervakade kommunikationen med en databas över redan känt olagligt material. (T.ex. genom hasher av filerna).

Men för okänt material så blir det väldigt svårt för att inte säga omöjligt. Den tekniken som finns idag klarar inte av det. Det blir väldigt mycket felrapporter. Och det säger sig själv att det är omöjligt för en AI (eller annan teknik) att kunna avgöra om en bild är på en 17-åring eller 18-åring även om innehållet är sexuellt explicit. Men det blir ännu svårare om innehållet inte är explicit sexuellt. Vad avgör då vad som är olagligt eller inte? Är en bild på badande barn något som automatiskt ska rapporteras? Det är svårt nog för människor att avgöra och man behöver veta kontexten. Det tydligaste exemplet är kanske när en man skickade en [bild till sin läkare](https://www.theverge.com/2022/8/21/23315513/google-photos-csam-scanning-account-deletion-investigation) i medicinskt syfte på sin sons skrev. Det fångades upp av automatiska verktyg och klassades som CSAM-material. För en människa kan man utifrån kontext förstå om det är CSAM-material eller inte. Det är omöjligt för en maskin.

## Viktiga principer att hålla fast vid

Oavsett vad det kommer för kompromissförslag från ministerrådet eller annat håll så är det principerna som är viktiga att hålla koll på. Inte om det är lite bättre än EU-kommissionens ursprunglia förslag.

* Det är orimligt att massövervaka alla användares kommunikation i förebyggande syfte om man inte är misstänkt för något. Avlyssning och övervakning ska vara riktad mot misstänkta, inte generell och riktad mot alla.
* Bakdörrar ska inte tillåtas. När de väl installerats så kan de utnyttjas till att utöka övervakningen. Och bakdörrarna för med sig fler säkerhetsrisker som illvilliga aktörer kan använda sig av.
* Begränsningen att skanna efter CSAM-material är huvudsakligen juridisk, inte teknisk. Det innebär att när man väl infört en massövervakningsapparat så kan högerauktoritära politiker i framtiden enkelt ändra så att man skannar efter mer. Det är ett sluttande plan. Först kommer man börja med terrorism, droghandel och internationell brottslighet. Sen blir det politiskt mycket enklare att fortsätta utöka det till klimataktivister, politiskt oppositionella eller hbtqia+-personer som "bryter mot traditionella värderingar".
* Rätten till privat korrespondens är fastslagen i deklaration för mänskliga rättigheter och i Europakonventionen.

Man ska inte låta sig luras av att det t.ex. införs flera olika juridiska kontrollmekanismer för spårningsorder. För när en sådan väl är på plats så kommer det införas massövervakning på en plattform.

## Vad är alternativet till massövervakning om man vill skydda barn?

Hur ska man då skydda barn på nätet? Detta är ett ämne som är ett lika stort som frågan om rätten till personlig integritet. Och det är inte mitt expertisområde. Men här är några tankar från mig:

* Införa krav på kommersiella kommunikationsplattformar som har fler än 1 miljon användare att enkelt kunna rapportera CSAM-material och groomingförsök och att företaget/organisationen bakom måste ha tydliga rutiner och resurser för att hantera anmälningarna. Det finns redan mycket bra principer att inspireras av EU-direktivet [Digital Markets Act (DMA)](https://en.wikipedia.org/wiki/Digital_Markets_Act) som syftar till att reglera Big Tech. Där regleras bland annat så kallade "gatekeepers" och man räknas som sådan om man har mer än 45 miljoner aktiva användare i EU och mer än 10 000 aktiva företagsanvändare. Även [Digital Services Act (DSA)](https://en.wikipedia.org/wiki/Digital_Services_Act) finns som reglerar moderering av plattformar. Först bör man se vilka regleringar och lagar som finns som man kan använda sig av redan i dag.
* Ge mer resursen till polisen att anmäla de anmälningar som kommer in. Så sent som förra året hade [13 000 tips](https://www.svt.se/nyheter/inrikes/tusentals-bilder-med-misstankta-overgrepp-har-samlats-pa-hog-polisen-startar-sarskild-insats) om övergreppsmaterial lagts på hög.
* Ta hjälp av de folkbildande insatser som finns så att föräldrar och skola kan lära sig om barnens digitala värld och kan hjälpa dem. Internetstiftelsen producerar en mängd bra guider. Jag skrev en [krönika](https://www.etc.se/kronika/saett-magdalena-andersson-i-skolbaenken) om det i ETC Nyhetsmagasin
