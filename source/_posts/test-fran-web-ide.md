---
title: Test från Web IDE
image: fox.jpg
description: Jag testar att använda Gitlabs Web IDE för att skapa blogginlägg
date: 2022-02-21 11:40
---
Gitlab har en Web IDE som man kan lägga upp grejer i.

I den så kan man lägga upp nya filer, ändra och committa till git. Men man kan också skärmdumpa och klistra in med ctrl+c vilket är trevligt.

Så här ser det ut när man skriver i Web IDE:

![Skärmdump av Gitlabs Web IDE](/images/screenshot-web-ide.png)

Bilden ovan är skärdumpad och klistrad in i Web IDE. Det som händer då är att det klistras in en text:

```
![images.png](./images.png)
```

(Texten i första klamern är beskrivningen av bilden och texten inom parentes är filvägen till bilden).

Det skapas också en bild i filträdet: `source/_posts/images.png`. Nu så vill vi ju inte att alla bilder ska heta `images.png` och vill inte heller att beskrivningen ska vara `images.png`. Så då tar vi och ändrar till:

```
![Skärmdump av Gitlabs Web IDE](/images/screenshot-web-ide.png)
```

Sen kopierar vi texten `/images/screenshot-web-ide.png`. För sen ska vi flytta bilden `images.png`. Då flyttar vi till `source/images/screenshot-web-ide.png`.

