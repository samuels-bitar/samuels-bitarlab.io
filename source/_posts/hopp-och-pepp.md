---
title: Träning, poddar och böcker från vänster hjälper mot ångestkänslor
image: kitten.jpg
description: Lite kort om min helg och hur man kan bota ångestkänslor
date: 2024-01-14 23:00
---

Jag skulle väl inte säga att jag har ångest för det är att ta i. Men ibland kan jag få obehagskänslor som jag nog bäst kan kalla ångestkänslor.

Det är för mig väldigt tydligt när jag får dem. Det är när jag är ensam. Jag har hängt med frugan hela veckan vilket är najs eftersom hon annars jobbar halva veckan i Hudiksvall. Men nu på söndagkvällen åkte hon hem. För det mesta så brukar jag inte alls få ångest när jag har varit social och sen blir själv ett tag. Och jag har varit social typ non-stop sedan torsdag så vanligtvis skulle jag välkomna en helt ensam kväll med Morris (vår katt).

## Träning bra mot ångest

Jag vet att många avskyr självhjälpsböcker. Egentligen håller jag ju med om det. När självhjälsböcker används för att klämma ur oss ännu lite mer energi innan vi går itu så är det något som kapitalet kan jubla över men knappast något som vi vill hurra över.

Men på ett personligt plan så vill man ju inte gå itu alls. Och det är ju fiffigt att förstå hur man själv fungerar och hur ens kropp fungerar. Att människor är sociala djur och mår bra när man har sociala kontakter är inga nyheter direkt. Att man då blir deppig eller får ångest när man är ensam är inte så konstigt.

En bok som verkligen gjort intryck på mig är Anders Hansens bok [Hjärnstark](https://bookwyrm.social/book/1215962/s/hjarnstark). Den handlar kort och gott om varför motion är så himla bra. Det verkar vara en mirakelmedicin mot precis allt om man får tro honom. Det är ju kanske mer än populärvetenskaplig bok snarare än en självhjälpsbok. Men det man tar med sig är just att träning är bra så man får ju lust att träna. Man kan läsa mer:

* [Ny studie visar att träning hjälper mot ångest](https://sverigesradio.se/artikel/ny-studie-visar-att-traning-hjalper-mot-angest)
* [Ångest lindrades med träning tre gånger i veckan](https://www.forskning.se/2021/11/09/angest-fysisk-traning/#)
* [Studie: Så effektivt är träning mot ångest](https://www.etc.se/halsa/studie-saa-effektivt-aer-traening-mot-aangest)

Jag har länge haft som en rutin att om jag känner obehagskänslor så brukar jag ta en promenad som minsta grej. Men det var ändå dags att träna så det gjorde jag. Jag är så lyckligt lottad så att det kollektivhuset jag bor i har ett litet gym. Så jag körde lite cross trainer, pull-ups och vikter. Så på med en bra podd och så kör man!

## All kärlek till vänsterpoddare

Jag är så himla glad över alla vänsterpoddar jag upptäckt! Flera har jag hittat via Kamratdataföreningen Konstellationens mediaprojekt [Mediakollen](https://mediakollen.org).

Nu lyssnade jag på [Vänsterpolitisk orienteringskarta av Krakelpodden](https://mediakollen.org/feed/34/curated). Krakelpodden är en podd som jag inte kände till innan Mediakollen. Men det är en av mina favoritvänsterpoddar. De hade en gäst från en annan favoritvänsterpodd, [Komintern](https://mediakollen.org/feed/42/curated). De pratar om reformism, revolution, vänsterpartiet, socialdemokrati, autonoma rörelser och synen på att bekämpa kapitalism och ingång. Mycket spännande!

Det blir så mycket roligare att träna när man lyssnar på en bra podd! Jag har dessutom börjat uppskatta att handdiska (vi har ingen diskmaskin i lägenheten) för då får man tid att lyssna på podd :)

Ännu lyxigare är att i kollektivhuset finns det också en bastu. I vanliga fall bastar jag med andra och då är det najs att bara sitta och snacka och slippa telefoner. Meeeen när jag bastar själv så är det ändå najs att kunna ta med sig en bluetooth-högtalare in i bastu och fortsätta lyssna på podden.

En annan podd jag verkligen uppskattar är [Tech Won't Save Us](https://www.techwontsave.us/). Det är en teknikpodd men från ett vänsterpolitik. Jag började lyssna på [What Social Media Meant for the Mass Protest Decade](https://www.techwontsave.us/episode/202_what_social_media_meant_for_the_mass_protest_decade_w_vincent_bevins) som går igenom vad för roll social media spelade under bl.a. arabiska våren. 

## Vänsterns svar på klimatkrisen

En riktigt göttig grej jag gjorde i helgen var att gå på släppet av boken Vänsterns svar på klimatkrisen. Det är en antologi som Jonas Sjöstedt och Jens Holm varit redaktörer för. Flera av författarna var på plats på ABF-huset i lördags och berättade kort om sina respektive kapitel.

Det finns skäl att vara politiskt deprimerad i dessa tider. Själv så drabbas jag inte så hårt av det. Men jag märker verkligen att jag får så mycket energi när en sådan här bok släpps! Det passar verkligen bra när vi har ett EU-val framför oss. Men det passar också bra nu när vi ska diskutera partiprogram i Vänsterpartiet som går av stapeln i maj.

Här är från [Verbal förlags sida](https://verbalforlag.se/bocker/vansterns-svar-pa-klimatkrisen/):

> När högerpolitiker världen över går till attack mot klimatpolitiken är det upp till vänstern att driva på för en rättvis och effektiv omställning. Att hejda klimatförändringen och bygga ett bättre samhälle för flertalet måste vara ett gemensamt projekt.
>
> Jonas Sjöstedt och Jens Holm samlar i denna antologi 14 röster som ur skiftande perspektiv visar vägen framåt.
>
> Medverkande: Andrea Andersson-Tay, Max Andersson, Malin Björk, Deniz Butros, Shora Esmailian, Kajsa Fredholm, Lars Henriksson, Gertrud Ingelman, Annika Lillemets, Elin Segerlind, Linda Snecker, Rickard Hjorth Warlenius, 

Man kan även läsa på [Jens Holms blogg](https://jensholm.se/2024/01/12/vansterns-svar-pa-klimatkrisen/).

## Bloggar också!

Okej, jag kanske inte tänkte just på bloggar som metod när jag skulle skriva det här blogginlägget. Men bara grejen att skriva är väldigt avslappnat och skönt att kunna få ner sina tankar och fokusera ett tag. Det är en härlig känsla.

Jag blev också glad när fler och fler kamrater börjar blogga! Jag blev glad när Gnomvid taggade mig efter han skrivit blogginlägget [Knutpunkt, 2024-01-13: Israel inför rätta, Vapenlobbyn, Vänstern på nätet, lärares arbetsmiljö & Historien i människans händer](https://gnomvid.se/2024/01/13/knutpunkt-2024-01-13-israel-infor-ratta-vapenlobbyn-vanstern-pa-natet-larares-arbetsmiljo-historien-i-manniskans-hander/). Han skrev att han hade fått inspiration från mig med mina senaste blogginlägg om [Länkparty - Decentraliserat nätverk, elaka bilar, dumma glasögon och EU](/lankparty-intro/) och [Länkparty - Open source AI, zrok istället för ngrok, PWA-revolution och Big Tech-böter](/lankparty-igen/). Sådant blir man glad över att höra!

Jag har också varit rejält pepp på det samtalet som Anders Svensson, Jesper Nilsson och jag har haft i bloggform om Mastodon och decentralisering. Här är några av de blogginlägg som berör frågan:

* [Sociala medier – Mastodon trevligare än de andra](https://blog.zaramis.se/2024/01/05/sociala-medier-mastodon-trevligare-an-de-andra/)
* [Mysigt med Mastodon](/mysigt-med-mastodon/)
* [Mysigt med Mastodon och interoperabilitet](https://www.turist.blog/2023-01-06-mysigt-med-mastodon-ochinteroperabilitet/)

Anders har gjort ett bra arbete om att skriva om vänstern och infrastruktur, bloggande och fediversum. Här är några intressanta inlägg:

* [Varför vänstern borde lämna de stora sociala medierna](https://blog.zaramis.se/2024/01/07/varfor-vanstern-borde-lamna-de-stora-sociala-medierna/)
* [Fediversum lämpar sig väl för vänstern](https://nyhetskartan.se/2023/09/11/fediversum-lampar-sig-val-for-vanstern/)
* [Vänstern och fediversum – bloggar](https://blog.zaramis.se/2024/01/08/vanstern-och-fediversum-bloggar/)
* [Vänstern och fediversum – mikrobloggar](https://blog.zaramis.se/2024/01/10/vanstern-och-fediversum-mikrobloggar/)
* [Vänstern och fediversum – diskussionsforum](https://blog.zaramis.se/2024/01/12/vanstern-och-fediversum-diskussionsforum/)
* [En gång hade vänstern en egen infrastruktur på nätet](https://blog.zaramis.se/2023/12/28/en-gang-hade-vanstern-en-egen-infrastruktur-pa-natet/)
* [Vänstern behöver en egen infrastruktur på nätet](https://blog.zaramis.se/2023/12/26/vanstern-behover-en-egen-infrastruktur-pa-natet/)

Jesper har skrivit en rad intressant inlägg också:

* [12 år av Internet, anarki & jävlar anamma!](https://www.turist.blog/2023-08-16-12-ar-av-anarki-och-javlar-anamma/)
* [Meta's Threats](https://www.turist.blog/2023-07-13-metas-threats/)
* [Internet är en skitstorm](https://www.turist.blog/2023-06-08-internet-ar-en-skitstorm/)
* [En nätpolitisk kartografi eller hur vi gör något helt annat](https://www.turist.blog/2023-09-28-en-natpolitisk-kartografi-eller-hur-vi-gor-nagot-helt-annat/)

Och ett blogginlägg av Judith Kahrer, styrelseledamot i Kamratdataföreningen Konstellationen, om föreningens event om eventet Bortom Big Tech sociala medier.

* [Intresset för alternativ till stora plattformar är stort](https://konstellationen.org/2023/08/20/intresset-for-alternativ-stort/)

## Bra med pepp helt enkelt

Så nu är jag nytränad, nybastad och nypeppad och mår bra :)

Kom ihåg att man sällan kan ge för mycket pepp. Det betyder ofta väldigt mycket att få ett uppmuntrande ord eller höra att något man gjort var bra. Så strössla med peppiga ord om er! Och om du bloggar, så länka till andra bloggare och poddare!

