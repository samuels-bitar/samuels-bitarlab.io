---
title: Skrivarkursen har startat
image: jakobsbergskullen.jpg
image_description: Bild på grusgången upp till kullen där Jakobsbergs folkhögskola ligger
description: Nu har jag startat skrivarkursen
date: 2024-08-24 15:00
---

Nu har jag gått på tjänstledighet i ett år! Så mina dagar kommer inte längre ägnas åt att knacka kod, grotta i konstiga dependencies för paket, skruva på kubernetesklustret för att säkerställa att sajten är stabil eller prestera för andra. Nu ligger ett år framför mig när det jag gör (eller inte gör) bara påverkar mig själv.

Nåja, jag kommer göra mycket mer än bara skrivarkursen. Utmaningen är väl att det där andra kommer ta för mycket tid för att jag helt plötsligt "har mer tid". Så jag skriver lite om det också.

## Jakobsbergs folkhögskola

I torsdags var första dagen i skolan igen. Vi skulle alla samlas i stora aulan (med det fräsiga namnet Höga salen) och blev välkomnade av rektorn. Det kändes kul! Innan vi alla gick in i aulan så hängde vi utanför. Det var en blandad skara av alla som skulle gå på Jakobsbergs folkhögskola. Vissa skulle göra som jag och gå skrivarkursen, andra skulle bli PT (personliga tränare), ytterligare några andra skulle fixa med konst och trädgårdsmästeri och sen var det rätt många som skulle komplettera sina gymnasiebetyg i allmänna kursen.

![Jakobsbergs folkhögskola](./images/jakobsbergs.jpg)*Jakobsbergs folkhögskola*

Det är alltid lite kul och spännande att komma ensam i ett nytt sammanhang. Det var en kvart innan vi skulle in i aulan. På grusvägen uppför kullen till skolan så snackade jag med en kille som skulle komplettera sina gymnasiebetyg. Han berättade att han var sugen på att koda sen varpå jag berättade att jag själv jobbade som webbutvecklare. Detta följdes av att han berättade för mig om AI och vilka programmeringsspråk som var viktiga. Det kändes gött och intressant att få bli mansplainad av en 20-åring, speciellt när jag sa "javascript" och han hörde "java". Jag hummade och tipsade om olika utbildningar man kunde gå och han berättade då för mig om hur arbetsmarknaden såg ut och att allt handlade om vilken portfolio man skulle ha. Inget illa mot den killen. Det finns ju en gammal talesätt om att ingen vet så mycket som en gymnasist. Han skulle prata med en lärare som han fick syn på så jag önskade honom lycka till och gick vidare.

Jag såg en kvinna stå lutad mot husväggen. Hon stod ensam men såg inte alltför försjunken ut i sin mobil så jag gick fram och hälsade. Det visade sig att vi skulle läsa samma skrivarkurs så vi snicksnackade om våra förväntningar och sånt.

Väl inne i aulan blev vi välkomnade av rektor Felicia som berättade om Jakobsbergs historia, lite kort om hur folkhögskolor skiljer sig från andra skolor och att vi som läser här är deltagare och inte studenter. Jag är själv van vid folkbildning och dess ideal sen tidigare så jag satt och myste. Efter presentationen av skolan och personalen var klar så fick vi alla följa våra respektive lärare till rätt sal.

## Skrivarkursen

Kursen jag läser heter [Skriv! – poesi, prosa, dramatik](https://www.jakobsbergsfolkhogskola.se/sv/vara-kurser/skriv-poesi-prosa-dramatik.aspx). Kursen är "en ettårig konstnärlig och praktisk utbildning i skönlitterärt skrivande. Kursen ger en bred översikt i litterär gestaltning. Som kursdeltagare får du prova på olika sätt att skriva och uttrycka dig. Noveller, dikter, pjäser och berättelser."

Men hur kommer det sig att jag ska läsa den här kursen?

Jag har tidigare skrivit lite om att jag ska vara [tjänstledig ett år för att skriva](/tjanstledig-skriva/), om mitt [nyårslöfte och tankar om skrivande](/blogga-mera/) och om boken [skriv bara skriv](/skriv-bara-skriv/) som jag fick av frugan som visste att jag var pepp på skrivande.

Jag har bloggat privat i perioder. Den första egna bloggen (tror jag) är [Samuels lilla matkasse](https://samuelslillamatkasse.wordpress.com/2017/12/23/antligen-en-hemsida-till-samuels-lilla-matkasse/) år 2017 som handlar om ett upplägg jag hade på min arbetsplats där jag sålde vegansk mat till självkostnadspris och bloggade lite grann kopplat till mat. Jag fick en nystart i början av 2021 då jag startade upp en egen privat blogg då jag skrev om teknik, [Tofubitar](https://tofu.bitar.se/2021/01/02/hemsida-och-annat/). Det blev en drös inlägg under januari 2021 sen dog det av. Ett år senare, februari 2022 startade jag upp denna blogg, Samuels bitar, och har bloggat här sen dess och bredare om allt möjligt: klimat, politik, teknik och skrivande.

Att skriva fritt på en blogg har varit väldigt givande! Jag kan och får skriva exakt vad jag vill. Det kan vara hur krångligt och oläsligt som jag vill, hur långt eller kort som jag vill och handla om vad jag vill. Det är samtidigt kul att sidan av det skriva krönikor med väldigt givna ramar och en redaktör som ger feedback. Jag gillar också att läsa och läser mycket i perioder. Den författaren som jag fastnat mest för är Cory Doctorow som skriver sci-fi i nära tid där han tar upp politiska frågeställningar kring tekniken och om man kan göra på ett annat sätt. Han lyckades med att gifta ihop mina nördintressen: politik kopplat till teknik och övervakning med sci-fi.

Har jag några ambitioner då med skrivarkursen? Jag stod och vägde mellan om jag skulle skriva skönlitteratur eller [skriva facklitteratur](https://www.jakobsbergsfolkhogskola.se/sv/vara-kurser/skrivprojekt-facklitteratur.aspx). Det hade legat närmare till hands för mig att skriva en bok om någon av de nörderigrejerna jag är intresserad av, kanske om Chat Control eller riskerna och möjligheterna med AI. Men jag ville ta tjänstledigt också för att ta ett slags sabbatsår och göra något helt annat. Få testa något nytt! Så därför valde jag kreativt skrivande. Det ska bli roligt att läsa en massa nya grejer och lära sig skriva i olika genrer. Mest intresserad tror jag att jag är av noveller och poesi. Men jag tror allt kommer bli kul!

Men klassen då tänker du kanske? Just ja, vi är typ 25 personer. Jag skulle tro att majoriteten är 20-30 år. Typ fyra är över 50. Resten är 30-40 år. En tredjedel av deltagarna är män. En överväldigande majoritet har svensk bakgrund. Några enstaka deltagare har utomnordisk bakgrund.

![Ett grupparbete i full gång med växter](./images/vaxter.jpg)*Ett grupparbete i full gång med växter*

Vi har bara gått två dagar och är alldeles i startgroparna så många är fortfarande nervösa och kanske lite blyga men vi är alla nyfikna på varandra. Jag försöker att inte pladdra på och ta alltför mycket plats vilket lätt händer om jag inte lägger band på mig själv. Men samtidigt vill jag bidra till att skapa en trygg och trevlig atmosfär. Det som är kul med att det är en folkhögskola är att hela poängen är att det inte ska vara en tävling. Man får inte betyg. Det är en fråga om personlig utveckling fast som görs kollektivt.

Lärarna heter Peter och Julia och är väldigt trevliga.

## Läsa läsa

Nu har jag ett år avsatt för att fokusera på att läsa och på att skriva. I början kommer vi inte ha kurslitteratur (men det kommer) utan vi kommer skapa vår egen kurslitteratur genom övningar och sen kommer vi i mindre grupper ge feedback på varandras texter. Det ska verkligen bli kul! Men jag tänkte att nu är det läge också för mig att bredda mitt läsande! Om jag läser det som ligger närmast till hands så blir det Stephen King eller Sci-Fi. Så jag promenerade till Rinkebys bibliotek med min feta ryggsäck som rymmer hur mycket som helst och lånade en massa olika böcker.

![Hög me böcker](./images/books.jpg)*Hög me böcker*

Det blev Ett öga rött av Jonas Hassen Khemiri, Molnfri bombnatt av Vibeke Olsson, Kvinnor och äppelträd av Moa Martinsson, Lisa och Lily av Mia Lodalen, Farväl till vapen av Ernest Hemingway och en drös andra.

Har du tips på några bra böcker som jag verkligen borde läsa får du gärna tipsa! Helst inte en superlång serie kanske för då kommer jag inte ha tid att läsa något annat.

## Skriva skriva

Sen ska jag ju skriva också är det tänkt. Jag har ju bloggen så klart. Men det blir ju inte direkt att jag övar mig i skönlitterärt skrivande. Så jag behöver hitta en bra form för att skriva och veta när jag ska skriva vad. Det finns något som kallas flödesskrivande där man bara skriver allt det som ploppar upp i hjärnan utan att bedömma det. Vi gjorde en sådan övning igår. Det anses väl vara någon slags uppvärmning för att komma igång eller komma förbi blockeringar. Tanken är inte att det ska bli någon färdig produkt, även om det så klart också kan ge bra idéer och material. Så jag har skapat en katalog på datorn, `Documents/folkis/` där jag samlar allt. Under `Documents/folkis/flödes` lägger jag flödesskrivande. Jag funderar på någon slags dagbok också. Men återigen, är det något som hjälper mig med det kreativa skrivandet eller inte? Jag landar i att jag ändå skapar katalogen `Documents/folkis/dagbok`. Om jag vill öva på det kreativa kan jag ju skriva dagbok åt någon annan.

## Teknikhjälp

Det här är ju ett kaninhål man kan komma djupt ner i. Jag har redan skrivit om mitt projekt med en [Cyberdeck för att skriva i solen](/cyberdeck/). Tanken är att skapa en slags laptop med en skärm som funkar i solen. Så jag har köpt en liten e-ink-skärm som funkar bra i solen. Just nu håller jag på att (försöka) installera [PaperTTY](https://github.com/joukos/PaperTTY) på den så att jag kan använda e-ink-skärmen som en terminal i Linux.

![Raspberry Pi Zero WH med e-ink-skärm](./images/eink-rpi.jpg)*Raspberry Pi Zero WH med e-ink-skärm*

Jag är van från jobbet och tidigare projekt att det är mycket fix och trix, olika versioner av bibliotek och programvaror som inte vill lira med varandra. Jag var beredd, men shit alltså vad meckigt det är! Jag har ännu inte löst det. Bilden ovan visar en skärm som uppdateras via ett testscript, alltså den funkar inte som en terminal än.

Oh well, jag har också köpt ett mini-tangentbord till min lilla cyberdeck och det är jag faktiskt sjukt nöjd med! Det är bluetooth-tangentbordet [Deltaco TB-631 mini](https://www.computersalg.se/i/322507/deltaco-tb-631-mini-tangentbord-bluetooth-nordisk-svart). Bortsett från keypaden så är det ett komplett tangentbord men sjukt kompakt, 28.5 cm x 12.2 cm x 2.3 cm. Och det väger bara 180 gram!

![Mini-tangentbord](./images/keyboard.jpg)*Mini-tangentbord*

Jag testade att koppla ihop det med min telefon och det funkar riktigt bra att skriva där! Men eftersom en telefon generellt navigeras med touch och inte med tangentbordet blir det inte toppen om man använder de vanliga apparna för att skriva, speciellt inte om man vill byta mellan dokument att skriva i. Så jag testade med terminalemulatorn termux och skrev istället med hjälp av textredigerare som vim, nano och micro. Det funkade fint!

Det funkade också helt okej att sitta utomhus i solen på balkongen att skriva. Jag tror dock det skulle bli mycket bättre med en e-ink-skärm eller en lcd-skärm med konstrast som [Beepy](https://hackaday.com/2023/08/07/review-beepy-a-palm-sized-linux-hacking-playground/) har. Man kan för övrigt köpa en sådan skärm separat på [Adafruit](https://www.adafruit.com/product/4694). Men jag måste ju få den att lira som skärm. Det enklaste är att köpa en Beepy direkt och koppla in mitt bluetooth-tangentbord till den (för tangentbordet på Beepyn är för litet) men de är slutsålda nu.

Det finns för övrigt ett annat tangentbord som också är trådlöst men inte via bluetooth (om man vill skippa det) som heter [Deltaco TB-632 mini](https://www.computersalg.se/i/1169892/deltaco-tb-632-mini-tangentbord-tr%c3%a5dl%c3%b6s-nordisk-svart).

Hur som helst, just nu tänker jag satsa på att fixa en bra setup med min telefon och bluetooth-tangentbordet. Jag behöver fixa och trixa med inställningar på telefonen, med termux, fontstorlek och lite sånt. Sen behöver jag en slags ställning för telefonen så att den är rätt vinklad och gärna kommer upp lite i höjd. Lösningen med e-ink-skärmen är jag egentligen mest pepp på men det kanske får ligga lite på is ett tag.

## Ideellt engagemang

Jag har successivt minskat ner mitt engagemang i Vänsterpartiet. Från att ha suttit i partistyrelsen, klimat- och miljönätverket på riks och i miljö- och klimatutskottet i Storstockholm så har jag bara kvar mitt uppdrag i klimat- och miljönätverket. Det känns skönt!

Den stora anledningen är att jag vill ge mer utrymme för Kamratdataföreningen Konstellationen som jag är ordförande för. Vi har nu dragit igång efter sommaren och jag är så pepp! Det kommer bli studiecirkel under hösten och jag planerar ett fysiskt hackathon en heldag och några kortare kvällshackathons. Jag är så glad för alla kamrater i styrelsen och för alla peppiga och engagerade medlemmar! Vi har nu senast haft en tävling om att göra design till klibbor/klistermärken. Tävlingen är avklarad och de [vinnande bidragen](https://wiki.konstellationen.org/sv/material/klibbor) är framröstade.

![Konstellationen-klibba](./images/konstellationen-klibba.png)*Konstellationen-klibba*

I kollektivhuset så är jag ansvarig för ett "vanligt" fredagsmatlag och mitt minimatlag Blixten som serverar enkel och vegansk mat på vardagar ibland.

Så min förhoppning är att det kommer bli ett år som präglas av lust och kreativitet och en himla massa böcker!



