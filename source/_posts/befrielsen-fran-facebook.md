---
title: Befrielsen från Facebook
image: hiking.jpg
image_description: Bild av <a href="https://unsplash.com/@glenjjackson?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Glen Jackson</a> från <a href="https://unsplash.com/photos/man-sitting-on-stone-beside-white-camping-tent-mzZVGFfMOkA?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
description: Då var det äntligen dags att lämnar Facebook
date: 2025-01-20 17:00
---

Först blev Twitter en högerextrem soppa i och med Elon Musks köp av plattformen. Han bidrog även till att stödja Trumps presidentkandidatur både med [pengar](https://edition.cnn.com/2024/12/05/politics/elon-musk-trump-campaign-finance-filings/index.html) och [plattform](https://www.theguardian.com/us-news/2024/oct/12/x-twitter-jd-vance-leaked-file). Trump blev vald. Och nu kommer Mark Zuckerberg som äger Meta (som driftar Facebook, Instagram och WhatsApp) och anpassar sina plattformar så att de ska behaga Trump.

Jag har länge velat lämna Facebook. Men när jag var kommunpolitiskt aktiv för Vänsterpartiet så var det väldigt svårt att göra. Så jag kände inte att jag kunde göra det. När jag var invald i partistyrelsen så kände jag också att jag behövde vara kvar på Facebook för att ha kontakt med andra partikamrater. Jag kandiderade inte till omval (något jag [skrivit om här](https://samuels.bitar.se/rorelsesocialismen/)) och sedan maj har jag inte behövt kolla in på Facebook vilket har varit väldigt skönt!

## Inte samma för alla

Jag tycker det är väldigt viktigt och bra om vänstern och andra progressiva rörelser tar makten över sin data, kommunikation och infrastruktur. Därför hoppas jag att så många som möjligt slår sig fria och bygger egna nätverk och rum så att de inte sitter i knäet på storföretagen. Det är viktigt att [göra en maktanalys](https://samuels.bitar.se/socmed-strategi/) och ha en strategi för sin nätvaro och kommunikation och kontakt med medlemmar, sympatisörer, kamrater och väljare. (Man kan även se det som en slags ["vänsterprepping"](https://samuels.bitar.se/vansterpreppa-digitalt/).)

**Men det är olika svårt för olika personer och organisationer att lämna.**

Därför menar jag inte att alla individer, tidningar, föreningar, organisationer, partier och så vidare måste agera på exakt samma sätt. Men för min del kan jag lämna nu. Och jag tror också att jag bör lämna. Det är viktigt att det byggs starka och självständiga vänstersammanhang utanför Big Tech. Och så länge jag tänker att en stor del av mina kamrater, vänner och intresserade är på Facebook och att jag postar där för att få spridning kommer det att påverka hur jag väljer att organisera mig och kommunicera.

Jag är absolut inte först med att lämna Facebook. Snarare rätt sen. Och det är egentligen inget speciellt med just mig och med att just jag lämnar Facebook. (Men jag tycker om att blogga och folk brukar gilla att läsa så varför skulle jag inte skriva om det?) Det är flera kamrater som har gått före och lämnat Facebook i nära framtid, som de också bloggat om:

* [Hejdå Facebook](https://autonomin.fritext.org/hejda-facebook/) av Omi
* [Om att lämna Facebook, Insta och Messenger](https://www.patreon.com/posts/om-att-lamna-och-119976705) av Jonas Lundström

Det finns många fler exempel men de personerna inspirerade mig att faktiskt ta steget.

## Innan jag lämnar

Men det är ju många kontakter som man förlorar om man lämnar hux-flux. Visserligen är det väldigt många vänner jag har på Facebook mest ytligt bekanta som jag träffat i något sammanhang. Där är det kanske inte en så stor förlust. Men det är också många som jag verkligen uppskattar och ogärna helt vill förlora kontakten med.

Det allra bästa vore så klart om jag automatiskt kunde flytta över alla mina Facebook-kontakter till Mastodon eller annan plattform. Men det är ju det som är själva inlåsningeffekten med Big Techs sociala medier. De har makten inte bara över din data utan över dina nätverk. Det är därför det är så svårt att lämna.

Men det jag kan göra i alla fall är att göra det enkelt för de som vill hålla kontakten med mig. Så jag har några saker jag vill ha på plats innan jag lämnar:

* En hemsida med kontaktuppgifter till var och hur man kan kontakta mig. Check! [bitar.se](https://bitar.se/)
* Ett nyhetsbrev som man kan prenumerera på om man vill få lite sporadiska uppdateringar. Check! [Samuels brevbitar](https://buttondown.com/samuelsbrevbitar)
* Konto på Mastodon. Check! [@samuel@social.spejset.org](https://social.spejset.org/@samuel)
* Guider om hur man kommer igång med Mastodon för de som vill joina. Check! Guiden [Kom igång med Mastodon](https://wiki.konstellationen.org/sv/guider/mastodon), guiden [Hitta folk att följa](https://wiki.konstellationen.org/sv/guider/mastodon-hitta-folk), guiden [Tips på hur man har det trevligt med andra på Mastodon](https://wiki.konstellationen.org/sv/guider/tips-mastodon)
* Tips på hur man man få del av vänsternyheter utanför Facebook. Check! [Mediakollen](https://mediakollen.org/)


## Organisationer som hjälper till

Det finns en rad organisationer som man kan få hjälp och inspiration från när man lämnar.

Jag själv är ordförande för den socialistiska, feministiska och antirasistiska [Kamratdataföreningen Konstellationen](https://konstellationen.org) och vår verksamhet går ut på att utveckla och drifta öppna system. Några saker att läsa vidare om:

* [En förening är född](https://konstellationen.org/2023/02/25/en-forening-ar-fodd/). Vi berättar om poängen med föreningen.
* Vi anordnar [demonstrationer](https://konstellationen.org/2023/05/20/demo-chatcontrol-tal/), [skriver debattartiklar](https://www.etc.se/debatt/att-bekaempa-massoevervakning-aer-en-feministisk-oedesfraaga) och [rapporter](https://konstellationen.org/2024/09/19/chatcontrol-rapport/) om varför massövervakningsförslaget Chat Control är dåligt.
* Vi anordnar [hackathons](https://konstellationen.org/2024/11/14/hackathon-2024-rapport/) där socialister och feminister träffas för att knacka på kod för samhällsförändring.
* Vi anordnar [studiecirklar](https://konstellationen.org/2025/01/08/studiecirkel-rapport/) och [workshops](https://konstellationen.org/2024/03/10/strejk-pa-karnevalskullen/) för att folkbilda om olika öppna system och om att slå sig fri från Big Tech.

En systerförening som ligger oss nära kring mycket är [Föreningen för Digitala Fri- och Rättigheter (DFRI)](https://www.dfri.se) som verkar för främjandet av digitala fri- och rättigheter och mot övervakning. DFRI placerar sig inte på någon vänster-höger-skala.

Jag har i en [krönika lyft andra techkollektiv](https://samuels.bitar.se/foreningar-tjanster-kronika/) och organisationer som man ta en titt på.

## Första stegen

Vill du också slå dig fri från Big Tech? Du behöver ju inte göra allt på samma gång och bara för att du tar några första steg på andra plattformar och använder dig av bättre och friare tekniker innebär det inte att du måste stänga ner alla andra konton direkt.

Men om du liksom mig vill ta några första steg är mitt tips:

* Om du har Facebook, X, Instagram eller andra sociala medier installerade som appar på mobilen så börja med att ta bort dem. Se till att du bara kommer åt dem via datorn.
* Skapa ett konto på Konstellationens Mastodon-server [Spejset](https://social.spejset.org) eller skapa ett konto Instagramalternativet Pixelfed på [Pixelfedsweden-instansen](https://pixelfedsweden.se)
* Läs nyheter via [Mediakollen](https://mediakollen.org/) eller via en RSS-läsare. Guide [här](https://konstellationen.org/2024/10/31/facebook-till-rss/) och [här](https://wiki.konstellationen.org/sv/guider/rss-intro)
* Tycker du det är skoj att skriva? Starta en blogg eller ett nyhetsbrev. Guide till hemsida [här](https://samuels.bitar.se/jakten-pa-bloggen/) och [här](https://wiki.konstellationen.org/sv/guider/blogg-med-gitlab) 
* [Bli medlem](https://konstellationen.org/bli-medlem/) i Kamratdataföreningen Konstellation

Lycka till!