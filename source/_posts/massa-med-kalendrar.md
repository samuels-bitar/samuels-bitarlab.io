---
title: Tips på adventskalendrar
image: kaffekalender.jpg
description: Det finns massvis med trevliga kalendrar. Här är några av mina tips.
date: 2023-12-02 15:00
---

Nu är det advents- och juletider så då är det dags för kalendrar. Här listar jag några trevliga kalendrar att ta del av.

## Kaffeadventskalender

Jag fick en kaffeadventskalender av frugan! (Se bilden högre upp). Så varje dag får jag smaka en ny sorts bryggkaffe. Det är en rad olika smaker. Mycket trevligt start på dagen! Man kan [köpa den här](https://nabostore.se/store/nabos-adventskalendrar/kaffekalender/kaffeadventskalendern-2023/). Om man hellre vill dricka te så har de en [tekalender](https://nabostore.se/store/te/ekologiskt-te/teadventskalendern-2023/) också.

![Lista på kaffesorter i kalendern](./images/kaffe-lista.jpg)*Lista på kaffesorter i kalendern*

De första två var Thomas båtblandning (sådär) och Brasilien Santos (rätt okej ändå). Ska bli kul att se om man hittar någon ny favorit! Finns även smaksatta :)

## GigWatch-poddens adventskalender

När jag lyssnade på GigWatch-poddens avsnitt [När chefen tar din saccosäck](https://podcasters.spotify.com/pod/show/gig-watch/episodes/Nr-chefen-tar-din-saccosck-e2c1a30) så berättade de att de skulle släppa en adventskalender 1a advent som heter "Julen smälter isen" som ska handla om tomtenissar som organiserar sig. Ser jag väldigt mycket fram emot!

![Organiserad nisse. Eget montage med bilder från Pixabay](./images/factoryelf.jpg)*Organiserad nisse. Eget montage med bilder från Pixabay*

Om man vill ha fler tips på bra poddar och poddavsnitt så kika in på [Mediakollens utvalda poddavsnitt](https://mediakollen.org/feed/category/5/curated). Det var där jag fick nys på GigWatch-poddens senaste avsnitt. (Jag kände ju redan till dem men via Mediakollen får jag bättre koll på när det släpps grejer).

## SVT:s julkalender Trolltider

Frugan och jag har en trevlig morgonritual under december (som jag tror många andra har), vi dricker kaffe och kollar på SVT:s julkalender. Det här året heter kalendern [Trolltider - legenden om bergatrollet](https://www.svtplay.se/julkalendern-trolltider-legenden-om-bergatrollet):

> När trollens magiska bärnsten plötsligt försvinner hotas hela deras värld. Om den inte hittas kommer det väldiga Bergatrollet att vakna till liv. Människoflickan Saga och trollpojken Love blir vänner och bestämmer sig för att hjälpa till att hitta stenen. Tillsammans ger de sig ut på ett magiskt äventyr där hela världens framtid står på spel. Lyckas de hitta stenen och rädda världen? I rollerna: Matilda Gross, Ossian Skarsgård, Kjell Bergqvist, Katarina Ewerlöf, Henrik Dorsin, Nour El-Refai m.fl.

![Trolltider](./images/trolltider.jpg)*Trolltider*

Än så länge är den mycket trevlig! 

## Advent of Code 2023

Advent of Code börjar bli en tradition för programmerare! Det har hållit på sedan 2015. Det är utvecklaren Eric Wastl som har fixat med det. Från [Wikipedia-artikeln](https://en.wikipedia.org/wiki/Advent_of_Code):

> Advent of Code was created by Wastl, who is still the sole maintainer of the project.
>
> The event was initially launched on December 1, 2015. By midnight EST (UTC−05:00), 81 people had signed up for the event, going slightly over Wastl's planned 70-participant capacity. Within 12 hours, about 4,000 people had joined, nearly causing a system crash. After 48 hours, there were about 15,000 people, and by the end of the 2015 event, the total had risen to 52,000.
>
> In 2020, perhaps due to the COVID-19 pandemic, the event saw a 50% growth in traffic, with over 180,000 participants worldwide.
> 
> On December 4, 2022, Wastl announced that the project had reached 1,000,000 registered users.

[Årets Advents of Code](https://adventofcode.com/2023/day/1) handlar om att det är något fel på världens snöproduktion och att du får hjälpa nissarna att lösa problemen:

> Something is wrong with global snow production, and you've been selected to take a look. The Elves have even given you a map; on it, they've used stars to mark the top fifty locations that are likely to be having problems.

![Advent of Code. Enkelt upplägg](./images/advent-of-code.jpg)*Advent of Code. Enkelt upplägg*

Det var några tips från mig på kalendrar. Hoppas det kan inspirera. Har du själv några bra tips?