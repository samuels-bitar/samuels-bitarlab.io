---
title: Krönika - Därför är Kristerssons Instagramkramar ett problem
image: social-media-phone.jpg
image_description: Foto av <a href="https://unsplash.com/@stereophototyp">Sara Kurfeß</a> från <a href="https://unsplash.com/photos/gold-iphone-6-YddMIRck34I">Unsplash</a>
description: Idag kan man inte delta i samhället på samma villkor om man inte installerar ett dussin appar och skapar konton på plattformar där datan man producerar och informationen om en själv säljs vidare.
date: 2024-10-07 11:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/daerfoer-aer-kristerssons-instagramkramar-ett-problem).

**Idag kan man inte delta i samhället på samma villkor om man inte installerar ett dussin appar och skapar konton på plattformar där datan man producerar och informationen om en själv säljs vidare.**

Jag är tjänstledig ett år och läser en kurs på en folkhögskola. Något av det bästa är den fantastiska maten med mycket grönsaker. Och att vi kursdeltagare kan påverka matsedeln genom att kontakta köket. Toppen! Då skickar jag ett mejl, tänker jag. Men nej, den enda kontaktvägen är via Instagram.

Vi kan se det här överallt i samhället. Den hastigt avgångna utrikesministern Tobias Billström (M) [kommunicerade flitigt på Twitter/X](https://www.aftonbladet.se/ledare/a/mQ2gav/utrikesminister-tobias-billstrom-anvander-x-till-hemliga-kontakter) istället för via Utrikesdepartementets hemsida. Om man vill veta vad Sverige tycker i utrikespolitiken, då ska man ha ett konto på Twitter/X. Men det verkar inte gälla för journalister som [blockats av ministern](https://www.etc.se/ledare/billstroems-blockfinger-pekar-mot-naagot-obehagligt). Extra komiskt blir det när Billström annonserar sin [avgång på Twitter/X](https://sverigesradio.se/artikel/tobias-billstrom-lamnar-regeringen) och statsminister Ulf Kristersson skickar sina [styrkekramar via Instagram](https://sverigesradio.se/artikel/tobias-billstrom-lamnar-regeringen). Det här med att skriva pressmeddelanden på regeringens hemsida verkar inte längre funka?

Visst, nya appar och plattformar kan ofta komma med nya finesser och vara ett smidigt sätt att kommunicera för en del. Men idag kan man alltså inte delta i samhället på samma villkor om man inte installerar ett dussin appar och skapar konton på plattformar där datan man producerar och informationen om en själv säljs vidare.

Ett slags minsta hygienkrav för alla organisationer borde vara att kommunicera viktig allmän information via hemsidan och där ange en e-post-adress som man kan mejla till. Om man vill göra ännu mer kan man se till att hemsidan har stöd för RSS (så att man kan prenumera på sidan) och ha ett nyhetsbrev man kan prenumera på via sin e-post-adress. Sen kan man för all del också finnas på en drös andra sociala medier och plattformar.

Det här vore ett smidigt – och jämlikt – sätt att få reda på att barnens förskola renoveras och att hämtningstiderna ändras eller vad som händer i det parti, eller förening där du är engagerad.

Så våga vara lite jobbig, be skolan, föreningen och, för all del, regeringen, att få information via e-post och hemsida.

**PS.** Efter krönikan publicerats så pratade jag med köket och fick en e-post-adress och har nu mailat dem med lovord om deras mat och önskemål från vår klass om några mindre grejer.