---
title: Två veckor med Mediakollen
image: mediakollen_party.png
description: Jag beskriver hur de senaste två veckorna med Mediakollen har varit
date: 2023-11-08 10:00
---
För två veckor sen så släppte vi i [Kamratdataföreningen Konstellationen](https://konstellationen.org/) mediaprojektet [Mediakollen](https://mediakollen.org). Så här beskriver vi projektet på [hemsidan](https://mediakollen.org/about):

"Mediakollen är ett minimalistiskt mediaprojekt som drivs av Kamratdataföreningen Konstellationen. Vi har startat Mediakollen för att fler ska få koll på vänstermedia och sociala rörelser utan att styras av nätjättarnas algoritmer."

Det är alltså ett slags "vänsteromni". Vi hämtar nyheter från drygt 100 källor med hjälp av RSS (tidningar, bloggar, poddar, osv) och samlar på ett ställe där en ideell redaktion väljer ut mest intressanta nyheter att visa på förstasidan.

Och det har blivit en succé! Jag tänkte här berätta om projektet och ge mina egna tankar.

## Utvecklingen innan relase

Idén på att ha ett slags "vänsteromni" har jag gått med under en rätt lång tid. Jag gillar [Omni](https://omni.se) som samlar nyheter från olika medier men eftersom medierna i Sverige är så otroligt borgerligt dominerade så är det svårt att läsa vänsterperspektiv på nyheter. Men även *valet* av nyheter är politiskt och det framgår väldigt tydligt om man jämför vänstermedia och högermedia.

Styrelsen i Konstellationen fick i mandat från årsmötet att ta fram en verksamhetsplan och budget (föreningen bildades i februari) och i verksamhetsplanen lade vi till "vänsteromni" som projekt.

Jag skapade en enkel POC (proof of concept) som jag såg till att ha färdigt innan [Konstellationens hackathon](https://konstellationen.org/2023/10/15/hackathon-rapport/) i oktober. På hackathonet så diskuterade vi namn, design och vilka features vi skulle ha med. Under själva hackathonet (som pågick en helg) så tog vi flera stora steg framåt. Vi fick en helt ny design (min egen var väldigt sparsam), ett nytt namn (Mediakollen) och nya features (olika flöden och sidor).

Men det var speciellt i veckorna efter hackathonet som en stor del av arbetet utfördes. Mycket programmering utfördes, väldigt många nya flöden (RSS-flöden) lades till och vi bjöd in Konstellationens medlemmar att betatesta sidan. Det roligaste var att så många var med i utvecklingen. Några deltog som utvecklare och knackade kod och andra fixade nya flöden och fick igång bra diskussioner.

## Releasen av Mediakollen

Vi lade ut ett [pressmeddelande](https://konstellationen.org/2023/10/25/mediakollen/) på hemsidan och mailade en rad olika medier (huvudsakligen de som vi har som flöden att hämt anyheter ifrån) och delade på sociala medier (huvudsakligen Mastodon, Facebook och Bluesky). Konstellationen har både ett [mastodonkonto](https://social.spejset.org/@konstellationen) och en [facebook-sida](https://www.facebook.com/KamratdataforeningenKonstellationen).

Intresset var stort! Vi fick många glada tillrop och delningar i sociala medier. Och jag blev [intervjuad i Flamman](https://www.flamman.se/vansterns-omni-vill-samla-rorelsens-medier/):


![Flammanartikeln](./images/flamman-mediakollen.png)*Flammanartikeln*

[Vänsterpartiets tidning Rött](https://www.rott.nu/nyheter/mediakollen-till-stod-for-vanstermedia/) skrev också om Mediakollen. Vi blev också omnämnda i [Dikko magasin](https://dikko.nu/kamratdataforeningen-konstellationen-lanserar-rattviserorelsernas-omni-mediakollen/) och en del bloggar ([här](https://blog.zaramis.se/2023/10/26/initiativ-for-att-starka-vanstern-pa-natet/) och [här](https://maxandersson.eu/mediakollen-org-ar-har/)).

De glada tillrop i sociala medier var allt för många för att återge men här är ett axplock:

* "Behöver verkligen detta. Har gjort till startsida i webbläsaren nu."
* "Tack för en superbra sida, fyller ett tomrum"
* "Mkt bra initiativ!"
* "Så bra grej!!!!"
* "Det här är det närmaste gamla fina Knuff.se man kan komma under 2020-talet!"
* "Utmärkt idé !!! Grymt jobbat"
* "Oj va bra! Fast det blir mycket att läsa 😃"
* "Den är riktigt bra!"
* "Mycket bra initiativ, tack för det!"
* "Oj! Vad kul!"
* "Fränt! 🙌"
* "👏👏👏"

Kul med bra gensvar!

## Trafiken på sidan

Första dagen när vi releasade Mediakollen så blev trafiken så klart störst. Och när Flamman-artikeln kom så gick trafiken upp ordentligt. Vi använder oss av det integritetsvänliga och fria verktyget [GoatCounter](https://www.goatcounter.com/) för att mäta trafiken. Genomsnittet antal besök per dag under den här perioden har varit drygt 400. Det var så klart mest i början när vi releasade och när Flamman-artikeln kom. Men även nu när det har gått två veckor så ser vi att det är många som använder sidan dagligen.

## Syftet med Mediakollen

Syftet är inte att hålla kvar besökare på Mediakollen. Vi vill att Mediakollen ska göra det enklare att på ett enkelt sätt ta del av nyheter som är vänster/gröna/progressiva på ett och samma ställe. Och vi hoppas vi kan få folk att upptäcka tidningar de gillar som de kan prenumerera på eller stödja på annat sätt för fri rödgrön journalistik behövs! Vi kommer fortsätta att utveckla sidan för att underlätta för läsare och få spridning för vänsternyheter.

## Egna tankar

Jag hoppas att sidan kan växa organiskt. Jag använder själv sidan dagligen (också för att jag är med i den ideella redaktionen som väljer ut nyheter att visa på förstasidan) och har stor glädje av den redan nu! Jag vill gärna att Mediakollen gör vad den ska och att den gör det bra. Det är ett minimalistiskt mediaprojekt med syfte just att göra en sak bra. Det är en sida som jag hoppas att många vänstermänniskor börjar använda för att lättare få upp ögonen för all bra vänstermedia som finns!

Jag har själv redan upptäckt nya tidningar, bloggar och poddar. Vissa hade jag hört om tidigare men aldrig kommit igång att kolla. Några nya saker jag har tagit del av som jag gillat är: 

[Tidningar/nyhetssidor](https://mediakollen.org/feed/category/3/curated):

* Arbetsvärlden
* Extrakt
* Natursidan

[Poddar](https://mediakollen.org/feed/category/5/curated):

* Studio Expo
* Krakel
* Scocconomics
* Komintern
* Källarpodden
* Kommentariatet

[Bloggar](https://mediakollen.org/feed/category/4/curated):

* SubRosa
* Rörelsen
* Tobias Hübinette
* Arbetarbloggen
* Svenssons Nyheter
* Arbetarmakt
* Clartébloggen
* Husby Arbetarblad

Sen finns det massvis med tidningar och bloggar jag redan kände till och läste men som jag nu läser *mycket* mer. Några av de är:

* ETC.se (Dagens ETC och ETC Nyhetsmagasin)
* Flamman
* Arbetaren
* Arbetet
* Syre
* Dagens Arena
* Aftonbladet Ledare

Vi har ju också ett block på förstasidan specifikt för blogginlägg. En förhoppning jag har är att fler kommer börja blogga! Jag tror verkligen på bloggformatet. Så om du börjar blogga om vänster/gröna/progressiva grejer så säg till så lägger vi gärna till din blogg bland våra flöden!

## Framtiden

Vår ideella redaktion fortsätter att välja ut nyheter och vi kommer att utöka den inom kort med fler personer. Vi fortsätter också att vidareutveckla sajten. Vi har fått flera tips på förbättringsförslag och önskemål om nya features. Och vi fortsätter så klart att lägga till fler flöden till sajten.

Har du några tankar, idéer eller förslag? [Kontakta oss](https://mediakollen.org/contact) och berätta!