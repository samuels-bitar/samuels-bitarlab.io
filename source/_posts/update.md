---
title: Skrivandet, partiet, föreningen och katten
image: samuel-jakobsberg.jpg
image_description: Jag framför Jakobsbergs folkhögskolas huvudbyggnad
description: En liten uppdatering om livet
date: 2024-10-21 21:00
---

Det var länge sen jag blogg-bloggade. Det har blivit rejält med annat skrivande på kursem så det har varit superkul! Och jag har fortsatt skriva krönikor. Men det hade varit kul att uppdatera er lite.

## Skrivakursen

Som jag skrivit innan så är jag [tjänstledig](/tjanstledig-skriva/) ett år för att gå en [skrivarkurs](/skrivarkursen-startad/) på Jakobsbergs folhögsskola. Eftersom jag ville hålla isär mitt skrivande en del så valde jag att skapa en ny blogg: https://text.bitar.se. Där har jag bara saker kopplat till skrivandet. Det känns kul att dela med mig av lite alster som jag producerar under det här året. Så här är några dikter:

* [Vanligt folk](https://text.bitar.se/vanligt-folk/) - sakpoesi
* [Tomaten](https://text.bitar.se/dagsvers-tomat/) - dagsvers
* [Framtidens skymning](https://text.bitar.se/framtidens-skymning/) - dikt jag läste upp på Göran Palm-pristudelningen då [Sara Parkman fick stipendiet](https://www.dagensarena.se/innehall/sara-parkman-ar-2024-ars-goran-palm-stipendiat/).

Och så har jag skrivit några noveller:

* [Svalorna flyger lågt](https://text.bitar.se/svalorna-flyger-lagt/) - Novellen jag skickade med min ansökan
* [Britta och tantfronten](https://text.bitar.se/personbeskrivning-britta/) - En superkort novell om vänstersossen Britta
* [Brinnande sängar](https://text.bitar.se/brinnande-sangar/) - En novell utspelad i studentmiljö. Mycket inspirerad av min egen tid där 2005-2011.
* [Färger](https://text.bitar.se/farger/) - En novell om kamp och gemenskap
* [Ensamheten](https://text.bitar.se/ensamheten/) - En novell om politisk depression och ensamhet

Som sagt, jag var med när Sara Parkman fick ta emot Göran Palm-stipendiet på Jakobsbergs Folkhögskola. Det var en väldigt trevlig tillställning! Vi i skrivarkursen deltog. Men det allra finaste var när Sara spelade [Ing​-​Maries vals](https://saraparkman.bandcamp.com/track/ing-maries-vals).

![Sara Parkman](/images/sara-parkman.jpg)*Sara Parkman med sin fiol*

Hon berättade om hur det gick till när sången kom till och hur de spelade upp den. Texten i sig är väldigt vacker:

"Lavar tronar, skogen nynnar, myren har sin gång
Gruvor vilar, minnen sjunger, ekar året om
O allting ändras men platsen står
Ropar sorger som fanns igår
Åkrar torka o ängens sus
Skatten sinar och tiden viskar sin egen väg

Ladaladaj-da

I glimmerljusa sjöar
Vit, storstadsglitter, öar
Mellan mörka höga bergen efter djupa långa skräcken
Lyckan kommer lyckan går, jobben kommer o jobben går

I glimmerljusa sjöar
Vit, storstadsglitter, öar
Mellan mörka höga bergen efter djupa långa skräcken
Lyckan kommer lyckan går, jobben kommer o jobben går
Men vi förblir på jorden vår"

Sara berättade att den nyanlända vännen Ahmed hjälpte till i samband med produktion och uppspelning. Han hade fått uppehållstillstånd, pluggat färdigt sin utbildning och börjat jobbat. Men i och med Sveriges inhumana migrationspolitiken utvisades han. Tillbaka till Gaza.

"Lyckan kommer lyckan går, jobben kommer o jobben går
Men vi förblir på jorden vår"

Hur vi kunde stå där 2015, hjälpa till, öppna våra hjärtan och statsministern deklarerade att hans Sverige inte byggde murar.

Sen svängde det.

Jag blev så hårt drabbad av den sången. Men Sara avslutade med ett hoppets ord, att om det kunde svänga 180 grader på så kort tid så kan det svänga tillbaka snabbt igen. Det tar jag med mig. Men mest tar jag med mig hur fint det var att dela den kollektiva sorgen över hur mycket av medmänsklighet som har har försvunnit i Sverige, som vatten som rinner genom händerna. Att inte vara själv i det är viktigt.

Rektorn Felicia Hedström höll också ett väldigt bra inledande tal om vikten av folkbildningen och hur regeringen med Sverigedemokraterna i spetsen går till attack mot den.

![Felicia Hedström](/images/felicia-hedstrom.jpg)*Felicia Hedström*

Det är speciellt att gå en kurs i kreativt skrivande i dessa dagar. Kommer den här kursen finnas om tio år? Kommer alla stöd från staten vara borta? Eller kommer bara de "nyttiga" kurserna vara kvar, de som tillfredsställer arbetsmarknaden? Jag inser nu när jag skriver det här att jag inte reflekterat tillräckligt över det här.

Jag är så tacksam över att jag har möjlighet att vara tjänstledig ett år och gå den här fantastiska kursen! Jag vet att det är långt ifrån alla som har möjlighet att göra det. Det funkar ju för att frugan och jag har gemensam ekonomi, billig hyra, en del sparat och så klart att den här kursen i kreativt skrivande finns.

Jag vill att fler ska få den här möjligheten.

## Konstellationen

Arbetet puttrar på med Kamratdataföreningen Konstellationen. Det är ett så himla kul sammanhang! Jag ser verkligen fram emot två roliga saker som kommer hända närmaste tiden:

* [Socialistiskt hackathon 2 november](https://konstellationen.org/2024/09/29/hackathon-2024/) - Det kommer vara i kollektivhuset :) Vill du koda på https://mediakollen.org eller hålla på med devops/admin-grejer så anmäl dig!
* [Studiecirkel - Digitala verktyg för social förändring](https://konstellationen.org/2024/10/08/studiecirkel/) - Tina och Jonathan i styrelsen håller i kursen. Det kommer bli så himla bra! Se till att anmäla dig om du är sugen!

Det är roligt att föreningen puttrar på. Och att kamraterna i föreningen är så peppiga! Och speciellt styrelsen som jag ju träffar regelbundet på videomöten (Jitsi). Fint gäng!

Jag ser fram emot när vi kommer igång med lite regelbundna hackathons. Jag har lite tankar kring det och hoppas andra är pepp att haka på.

Är du socialist, feminist och antirasist och tycker det är skit att Big Tech har så stor makt över våra liv? [Bli då medlem!](https://konstellationen.org/bli-medlem/)

[Följ oss gärna på Mastodon!](https://social.spejset.org/@konstellationen)

## Vänsterpartiet

Jag har trappat ner mitt engagemang i Vänsterpartiet nu. Jag sitter kvar i styrgruppen för det rikstäckande klimat- och miljönätverket. Det känns lagom just nu. Konstellationen tar tillräckligt med tid. Jag följer med en del om vad som snackas om i partiet men blir mest trött. Jag har varken ork eller lust att formulera någon kritik. Så då gör jag något som jag lärt mig på sistone: ingenting, bara vara tyst. Funkar bra ändå. När jag har mer energi kanske jag växlar upp engagemanget. (Det lär ju i alla fall bli inför valet). Det är supertydligt att Vänsterpartiet verkligen är partiet som står mig närmast och självklart det parti jag engagerar mig för. Men just nu är jag nöjd med att lägga ner min tid och energi på saker som faktiskt mest bara är kul.

## Livet i övrigt

Frugan och jag är så glada över vår söta Morris!

![Moris](/images/morris.jpg)*Morris*

Han kommer och väcker en på morgonen och vill ha mat. En ständig förhandling. Men vår kompromiss nu är att vi ger honom mat 06:00 och sen trillar man ner i sängen igen. Men Morris är en bra förhandlare och lyckas ofta förhandla till sig mat tidigare än så.

## Några länkar

Det här blev inte något "länkparty" men jag vill slänga ut lite goa länkar:

* [Beredd att möta lynchmobben? Motståndskraft under en pogrom](https://prepp.fritext.org/blog/flykt/beredd-att-mota-lynchmobben-motstandskraft-under-en-pogrom/). Jonathan har bloggat väldigt bra om hur man kan förbereda sig inför fascism på olika sätt, eller i alla fall upplopp.
* [S08E01 - Lära Karl Marx knulla (netto noll-kapitalackumulation)](https://soundcloud.com/kominternpodd/s08e01-lara-karl-marx-knulla-netto-noll-kapitalackumulation) - Ett bra avsnitt med [Komintern](https://mediakollen.org/feed/42/curated) där Jesper deltar. Han bloggar förresten om det på [www.turist.blog](https://www.turist.blog/2024-09-29-ojamnt-utbyte-i-siffror/)
* [Ett steg till ner mot effektivitetshelvetet](https://www.etc.se/kronika/ett-steg-till-ner-mot-effektivitetshelvetet) - Krönikörskamraten 
Myra Åhbeck Öhrman skriver om brottbekämpning och övervakning i ETC Nyhetsmagasin
* [V-varning efter uppgörelsen: Ny kärnkraft kan äta upp allt](https://www.etc.se/inrikes/v-varning-efter-uppgoerelsen-ny-kaernkraft-kan-aeta-upp-allt) - Ida Gabrielsson, Vänsterpartiets ekonomisk-politiska talesperson, varnar för att kärnkraften slukat allt utrymme som frigörs av att överskottsmålet ersätts med ett balansmål.

Det var en liten uppdatering från mig :)