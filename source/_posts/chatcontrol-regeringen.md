---
title: Vänsterpartiet undrar om regeringen kommer stödja EU-parlamentets kompromiss om Chat Control
image: svenneling.jpg
description: Den svenska regeringen har tidigare ställt sig huvudsakligen positiv till Chat Control men i och med att EU-parlamentet har nått en kompromiss, kommer man att stödja den?
date: 2023-12-07 17:00
---

Chat Control kallas det massövervakningsförslag som EU-kommissionär Ylva Johansson (S) tagit fram. Kritiken har varit massiv mot förslaget. 

Jag har bloggat om det tidigare [här](/vad-blir-framtidens-brott/), [här](/progressiva-bor-oroa-sig/) och [här](/chatcontrol-faller-eller/).

En kort sammanfattning av den huvudsakliga kritiken kan man läsa på [Kamratdataföreningen Konstellationens hemsida](https://konstellationen.org/2023/09/14/chatcontrol-pressmeddelande/) där jag är ordförande.

Det finns också massvis med kritiska artiklar som man kan hitta på [chatcontrol.se](https://chatcontrol.se). Här är ett litet urval:

* [Journalistförbundet: ”Chattkontroll hotar källskyddet” ](https://www.journalisten.se/nyheter/journalistforbundet-chattkontroll-hotar-kallskyddet)
* [Utgivarna uppmanar regeringen att protestera mot Chat control](https://www.journalisten.se/nyheter/utgivarna-uppmanar-regeringen-att-protestera-mot-chat-control)
* [Nej, vi behöver inget övervakningssamhälle](https://www.dagensarena.se/opinion/nej-vi-behover-inget-overvakningssamhalle/)
* [Johanssons förslag hotar privatlivet, inte pedofiler](https://www.expressen.se/ledare/johanssons-forslag-hotar-privatlivet-inte-pedofiler/)

Nu har EU-parlamentet tagit ställning mot de sämsta delarna av Chat Control (de som har att göra med massövervakning) och kommit överens om en [kompromiss](https://www.europaportalen.se/2023/10/genombrott-i-kontroversiellt-forslag-om-kampen-mot-barnporr-pa-natet) där alla partigrupper i EU-parlamentet står bakom texten.

Den tyske piratpartisten Patrick Breyer har varit en av de hårdaste kritikerna och har förhandlat för den gröna gruppen. Han verkar vara nöjd med kompromossisen när han säger:

– Vi [tog] bort de omtvistade och problematiska delarna, såsom masskanning av hela tjänster, även för totalsträckskrypterade tjänster, obligatorisk ålderskontroll för alla kommunikationstjänster eller att utesluta alla barn under 16 år från vanliga appar. Istället lade vi till mer effektiva, domstolssäkra och rättighetsrespekterande åtgärder till det ursprungliga förslaget för att hålla barn säkra på nätet, sade Patrick Breyer.

[51 av 54 ledamöter](https://www.etc.se/utrikes/eu-parlamentet-roestade-nej-till-kritiserade-chat-control) i LIBE-utskottet röstade för EU-parlamentets kompromissförslag.

Så EU-parlamentet har landat i en bra kompromiss där massövervakningsaspekterna tas bort.

Det räcker dessvärre inte att EU-parlamentet är överens. Max Andersson (V), tidigare EU-parlamentariker, [kommenterar på sin blogg](https://maxandersson.eu/spannande-uppgifter-om-chatcontrol/):

> Men vi som oroar oss för massövervakning har inte vunnit den här frågan än. Nästa steg är att det ansvariga utskottet i EU-parlamentet ska anta det förhandlingsmandat partigrupperna kommit överens om. Det är närmast en formalitet. Men därefter ska Ministerrådet ena sig om ett eget förslag på sitt håll. Det kan ta tid. Om de inte kommer överens om samma sak som EU-parlamentet – och det tror få att de gör – så blir det trialog-förhandlingar mellan Ministerrådet, EU-parlamentet och Kommissionen.

Om man kan få fler regeringar att stödja EU-parlamentets hållning så skulle mycket vara vunnet.

Nu senast så har riksdagsledamotet Håkan Svenneling (V) ställt frågan till Statsrådet Jessika Roswall (M) [2023-12-06](https://www.riksdagen.se/sv/webb-tv/video/debatt-om-forslag/kommissionens-arbetsprogram-2024_hb01uu5/?pos=3857&autoplay=true) huruvida regeringen kommer ställa sig bakom EU-parlamentets kompromiss (ca 6 minuter, 01:04:17-01:10:24):

<iframe src="https://www.riksdagen.se/sv/webb-tv/video/debatt-om-forslag/kommissionens-arbetsprogram-2024_hb01uu5/embed/?start=3860&end=4224" width="560" height="315" allowfullscreen="" frameborder="0" scrolling="no" title="Kommissionens arbetsprogram 2024 (Debatt om förslag 6 december 2023)"></iframe>

Dessvärre så är inte Jessika Roswall (M) inte tillräckligt insatt i frågan och kan inte ge svar. Väl värt att notera är att alla [svenska EU-parlamentariker röstade för](https://www.dn.se/varlden/brett-stod-i-eu-parlamentet-mot-overvakning-av-krypterade-chattar/) EU-parlamentets kompromiss. Men frågan bör fortsätta ställas till regeringen.

Jag tänkte nämna några andra saker som Vänsterpartiet gjort:

* Riksdagsledamoten och ledamoten i konstitutionsutskottet [Jessica Wetterling (V) tar ställning mot Chat Control](https://www.etc.se/utrikes/v-saeger-nej-till-chat-control-enormt-intraang)
* Ledande vänsterpartister har skrivit en [kritisk debattartikel mot Chat Control](https://www.sydsvenskan.se/2023-06-04/chat-control-oppnar-for-massovervakning-av-sallan-skadat-slag)
* Vänsterpartiet var det enda parti som yttrade sig negativt om Chat Control på [EU-nämndens möte 2023-09-24](https://handlingar.se/request/1978/response/3279/attach/html/2/Protokoll%2023%2024%20JuU1.docx.html)
* Riksdagsledamöterna [Andrea Andersson-Tay](https://chatcontrol.se/demo-sthlm/) och [Ilona Szatmári Waldau](https://konstellationen.org/2023/09/20/chatcontrol-pressmeddelande-efter-demo/) talade på demonstrationer mot Chat Control

Det finns ett stort motstånd mot Chat Control och flera svenska partier har landat rätt både i Sverige och i EU (V, C och MP) medan tidöpartierna svajar ordentligt. Man kan läsa mer om [hur de olika partierna agerat här](https://chatcontrol.se/status/).

Så fortsätt gärna att höra av er till riksdagsledamöter och ministrar. Det finns ett bra mejlverktyg på [https://chatcontrol.se/mejla/](https://chatcontrol.se/mejla/) som man kan använda sig av. Om vi är många som sätter tryck på att alla riksdagens partier måste stödja EU-parlamentets kompromiss så är mycket vunnet.


