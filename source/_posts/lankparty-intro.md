---
title: Länkparty - Decentraliserat nätverk, elaka bilar, dumma glasögon och EU
image: network-2.jpg
description: Ett gäng intressanta länkar om teknik jag samlat på mig senaste tiden
date: 2024-01-03 09:00
---

Länkparty:

* [Meshtastic](https://meshtastic.org/) - An open source, off-grid, decentralized, mesh network built to run on affordable, low-power devices
* [Jag lämnar Facebook](https://www.estwikphoto.se/jag-lamnar-facebook/)
* [Your Car Is Tracking You. Abusive Partners May Be, Too.](https://www.nytimes.com/2023/12/31/technology/car-trackers-gps-abuse.html)
* [EU CRA: What does it mean for open source?](https://berthub.eu/articles/posts/eu-cra-what-does-it-mean-for-open-source/)
* [How Meta’s New Face Camera Heralds a New Age of Surveillance](https://dnyuz.com/2023/12/13/how-metas-new-face-camera-heralds-a-new-age-of-surveillance/)

Ett litet axplock av senaste intressanta länkar jag snappat upp senaste tiden.

## Meshtastic

Som man kanske vet så är jag både datornörd och integritetsnörd. Jag har stört mig sjukt mycket på att man inte längre kan köpa anonyma kontantkort. Ens telefon är en egen personlig spårare för dig som har koll på var du är hela tiden oavsett om du använder telefonen eller inte genom så kallad [cell tracking](https://www.eff.org/issues/cell-tracking). Det är något jag skrivit om [här](https://tofu.bitar.se/2021/01/02/hemsida-och-annat/) och [här](https://tofu.bitar.se/2021/01/09/46elks-och-telefonnummer-utan-tracking/).

Att man inte längre kan köpa och använda kontantkort anonymt har kritiserats av bl.a. [DFRI](https://www.dfri.se/dfri-intervjuade-i-sveriges-radio-om-ratten-till-att-ha-kontantkort-anonymt-och-sakert/) och har [inneburit problem för kvinnojourer](https://www.svt.se/nyheter/lokalt/vast/anonyma-kontantkort-forbjuds-bra-for-polisen-men-problem-for-kvinnojourer).

Det finns fortfarande möjlighet att använda anonyma kontaktkort om man köper via andra EU-länder.

Hur som helst, det är min ingång. **Vad är då Meshtastic?**. Från [hemsidan](https://meshtastic.org):

> Meshtastic® is a project that enables you to use inexpensive LoRa radios as a long range off-grid communication platform in areas without existing or reliable communications infrastructure. This project is 100% community driven and open source!

Det hela är baserat på [LoRa-teknologin](https://en.wikipedia.org/wiki/LoRa) som är en sätt att kommunicera över radiovågor. Fördelen är att man kan nå väldigt långt. Nackdelen är att man inte har hög bandbredd, dvs man kan inte överföra så mycket data. Men det kan lämpa sig väl för text.

![Skiss över hur ett meshtatic-nätverk med LoRa kan se ut](./images/lora-topology.png)*Skiss över hur ett meshtatic-nätverk med LoRa kan se ut*

Man behöver en LoRa-enhet som är kompatibel. Men sen kan man ansluta den till nätverket. Den kommer att ansluta till andra i närheten och tillsammans kan man koppla samman många. Sedan kan man ansluta sin smarta telefon eller dator över bluetooth till sin LoRa-enhet och då nå andra på nätverket.

Rätt så coolt!

## "Jag lämnar Facebook"

I inlägget [Jag lämnar Facebook](https://www.estwikphoto.se/jag-lamnar-facebook/) så beskriver naturfotografen Michael Estwik om varför han lämnar Facebook.

> Facebook har förändrats genom åren, inte bara i utseende utan även i hur vi ser våra flöden. Något som jag upptäckte tidigt i år var att mina följare på Facebook helt plötsligt slutade gilla mina bilder och antalet gilla markeringar sjönk över tid, även på de bilder som jag visste att de tilltalade min följarskara.
> 
> Efter att ha vädrat mina bekymmer i en fotogrupp så kom vi fram till att Facebook hade infört samma groteska algoritm som på Instagram.
>
> Vad detta betyder är att om du som följare inte interagerar med mina bilder så kommer du inte längre se mina bilder. Vad jag som ”kreatör” måste göra är att skapa stories och reels får att få tillbaka följare som inte längre ser mitt innehåll. Båda är något jag absolut avskyr att skapa, av den enkla anledningen att det är ett tvång. Det hade varit en annan sak om det var frivilligt, för jag kan tänka mig att filma en videosnutt om var jag är och så, men detta är enbart till för att föda en algoritm just för att ni följare ska kunna se mig och inte ”glömma bort mig”.

Alltså, ännu en person som tröttnat på Big Techs sociala medier som genomgår [skitiferingsprocessen](https://www.etc.se/kronika/koer-skiten-i-botten).

## Övervakning i bilar

Sista dagen 2023 publicerade New York Times artikeln [Your Car Is Tracking You. Abusive Partners May Be, Too.](https://www.nytimes.com/2023/12/31/technology/car-trackers-gps-abuse.html). Det är en historia om hur en kvinna blir övervakad av sin man... via sin bil!

Det här är ingen jättenyhet egentligen, mest bara en tydlig personlig berättelse om hur bilars övervakning drabbar vanligt folk. Mozilla gjorde en maffig genomgång av bilmärken och hur [kassa de är på integritet](https://foundation.mozilla.org/en/privacynotincluded/articles/its-official-cars-are-the-worst-product-category-we-have-ever-reviewed-for-privacy/).

## EU Cyber Resilience Act - Kanske inte lika kasst som man trott?

Det finns så mycket som händer på EU-nivå gällande IT. Vi har så klart hela [Chat Control-grejen](https://chatcontrol.se/) där man vill övervaka alla EU-invånares kommunikation i praktiken utan brottsmisstanke, något som vi i Kamratdataföreningen Konstellationen anordnat flera [demonstrationer mot](https://konstellationen.org/2023/09/20/chatcontrol-pressmeddelande-efter-demo/). Sen har vi också [Artificial Intelligence Act](https://edri.org/our-work/eu-ai-act-deal-reached-but-too-soon-to-celebrate/), [Digital Services Act / Digital Markets Act](https://edri.org/our-work/digital-service-act-document-pool/) och [EU Digital Identity Framework (eIDAS)](https://blog.mozilla.org/netpolicy/2021/11/04/mozilla-publishes-position-paper-on-the-eu-digital-identity-framework/).

[Cyber Resilience Act](https://en.wikipedia.org/wiki/Cyber_Resilience_Act) är ännu en EU-reglering gällande cybersäkerhet och cybermotstånd. Förslaget har kritiserats och kommenterats av [Open Source Initiative](https://blog.opensource.org/what-is-the-cyber-resilience-act-and-why-its-important-for-open-source/), [Linux Foundation](https://www.linuxfoundation.org/blog/understanding-the-cyber-resilience-act) och [bloggare](https://hackaday.com/2023/04/21/the-cyber-resilience-act-threatens-open-source/).

Nu är en [kompromisstext offentliggjord](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CONSIL:ST_17000_2023_INIT). Texten har nu lästs igenom och kommenterats av [mjukvaruutvecklaren Bert Hubert](https://berthub.eu/articles/posts/eu-cra-what-does-it-mean-for-open-source/).

En snabb sammanfattning är att CRA omfattar kommersiell verksamhet:

> The CRA regulates commercial activity: “(10) This Regulation applies to economic operators only in relation to products with digital elements made available on the market, hence supplied for distribution or use on the Union market in the course of a commercial activity.”

Han går sedan igenom olika fall som kan uppstå om man driver ett open source-projekt, om man tar emot pengar/hårdvara, om man erbjuder support, osv. Kontentan är väl att om man "bara" driver ett open source-projekt utan att tjäna pengar behöver man inte oroa sig men om man driver ett företag som använder sig av open source-programvara är det inget skydd från att slippa uppfylla CRA.

## Metas integritetskassa glasögon

Man hade hoppats att det här med vandrande övervakningskameror på vanligt folk skulle tagit slut med [Google Glass](https://en.wikipedia.org/wiki/Google_Glass) som fick mycket kritik och [därför misslyckades](https://www.wired.com/story/google-glass-reasonable-expectation-of-privacy/):

> But where many people see Google Glass as a cautionary tale about tech adoption failure, I see a wild success. Not for Google of course, but for the rest of us. Google Glass is a story about human beings setting boundaries and pushing back against surveillance — a tale of how a giant company’s crappy product allowed us to envision a better future.

![En användare av Google Glass](./images/admiral-harris-google-glass.jpg)*En användare av Google Glass*

Så Google misslyckades med Google Glass vilket är najs. Mindre najs är att Meta nu gör samma sak (fast med mer dold kamera). Tillsammans med märket Ray Ban så har de tillverkat ett par [glasögon med inbyggd kamera](https://www.wired.com/review/review-ray-ban-meta-smart-glasses/). Meta är inte dummare än att de förstår att de kommer få mycket kritik för integritetskränkningar i och med glasögonen. Därför har de installerat en [diod som lyser eller blinkar](https://dnyuz.com/2023/12/13/how-metas-new-face-camera-heralds-a-new-age-of-surveillance/) när man tar foto eller spelar in. Det kommer knappast att lugna oss kritiker.

På samma sätt som att [klimataktivister punkterar däcken](https://www.theguardian.com/environment/2022/jul/27/tire-deflators-suv-new-york-climate-crisis) till klimatskadliga SUV-bilar så kanske vi kan se integritetsaktivister som helt enkelt trampar på övervakningsglasögon om bäraren inte omedelbart tar av dem. Det hade varit en ny form av aktivism. Så vitt jag vet har civil olydnad och direktaktion inte varit integritetsaktivister främsta gren. Det kanske ändras om sådana här glasögon blir poppis.