---
title: Hur får vi ner utsläppen från resor och transporter
image: traffic-jam.jpg
description: Inrikes transporter står runt en tredjedel av Sveriges totala utsläpp. Hur får vi ner det?
date: 2022-08-21 20:25
---

I ett [tidigare blogginlägg](/klimat-sektorer) så gick jag igenom vilka de största utsläppskällorna är för Sveriges totala utsläpp. Det här inlägget tänkte jag djupdyka just kring transporterna.

Enligt [Naturvårdsverket](https://www.naturvardsverket.se/data-och-statistik/klimat/vaxthusgaser-territoriella-utslapp-och-upptag) uppgick utsläppen av växthusgaser från inrikes transporter år 2021 till ungefär 15 miljoner ton koldioxidekvivalenter. Det motsvarar ungefär **en tredjedel av Sveriges totala utsläpp** (31 procent).

Huvuddelen av växthusgaserna från transportsektorn [kommer från vägtrafiken](https://www.naturvardsverket.se/data-och-statistik/klimat/vaxthusgaser-utslapp-fran-inrikes-transporter/), där utsläpp från personbilar och tunga fordon dominerar enligt Naturvårdsverket.

![Utsläpp av växthusgaser från inrikes transporter](/images/naturvardsverket-inrikes-transporter.png)*Bild från Naturvårdsverket, Utsläpp av växthusgaser från inrikes transporter*

Siffrorna för 2021 är preliminära så vi tittar på år 2020 istället. Totala utsläppen är 14,89 miljoner ton koldioxidekvivalenter.  Här är för alla:

* Personbilar - 9,36 milj ton co2ekv (62,9%)
* Tunga lastbilar - 2,97 milj ton co2ekv (20,0%)
* Lätta lastbilar - 1,36 milj ton co2ekv (9,1%)
* Sjöfart - 0,66 milj ton co2ekv (4,4%)
* Bussar - 0,21 milj ton co2ekv (1,4%)
* Inrikes flyg - 0,2 milj ton co2ekv (1,3%)
* Mopeder och motorcyklar - 0,1 milj ton co2ekv (0,7%)
* Järnväg - 0,04 milj ton co2ekv (0,3%)

Så personbilar och lastbilar står för 92% av utsläppen. Vi kan slå ihop de sista fyra minsta utsläpparna för att se det visuellt:

![Utsläpp av växthusgaser från inrikes transporter år 2020, eget diagram](/images/naturvardsverket-data-transporter-eget-diagram2.jpg)*Utsläpp av växthusgaser från inrikes transporter år 2020, eget diagram*

Enligt Sveriges klimatmål för transportsektorn ska utsläppen minska med minst 70 procent till 2030 jämfört med 2010. Hittills har utsläppen inom sektorn minskat med 27 procent jämfört med 2010.

Mycket av den minskningen i utsläppen är enligt Naturvårdsverket huvudsakligen till följd av att andelen förnybart bränsle ökat och för att fordonen har blivit mer energieffektiva. Men som vi nämnt tidigare så är biobränsle eller inblandning av biobränsle (via den så kallade reduktionsplikten) inte oproblematisk. Vissa är väldigt positiva till biobränslen (som biobränslebranschen, Fossilfritt Sverige), vissa är väldigt negativa (som Greenpeace och Skydda skogen) och andra hamnar lite mitt emellan (som Naturskyddsföreningen).

Följande artiklar är intressanta att läsa i frågan om biobränsle:

* Supermiljöbloggen - [Biobränslen – klimatlösning eller skogsskövling?](https://supermiljobloggen.se/miljofakta/biobranslen-klimatlosning-eller-skogsskovling/)
* Dagens ETC - [Biobränsle – från klimaträddare till grön synvilla](https://www.etc.se/klimat-miljo/fran-klimatraddare-till-gron-synvilla)
* Naturskyddsföreningen - [Vanliga frågor om biobränslen](https://www.naturskyddsforeningen.se/artiklar/vanliga-fragor-om-biobranslen/)

Eftersom det är så pass osäkert om biobränsle kan vara en lösning (mer än bara en mindre del) så är det bra att vara uppmärksam på vilka åtgärder som föreslås i debatten för att få ner utsläppen från transporterna. Debattörer som bara lyfter upp biobränslen har ingen trovärdig politik.

Personbilstrafiken står alltså för 62,9 % av utsläppen. Det är uppenbart vi behöver rikta in oss här mest.

Väl värt att poängtera är att vi ser på inrikes transporter i det här blogginlägget. Om vi ser till utrikestransporter så är det uppenbart att svenskarnas flygande är väldigt problematiskt. Svenska befolkningens totala [klimatpåverkan från flyget](https://www.naturvardsverket.se/amnesomraden/klimatomstallningen/omraden/klimatet-och-konsumtionen/flygets-klimatpaverkan) står för lika stora utsläpp som hela personbilstrafiken i Sverige enligt Naturvårdsverket. Men det får bli ett separat blogginlägg om det.

![Hastighetsmätare](/images/speedometer.jpg)*Hastighetsmätare*

De klimatpåverkande utsläppen från vägtransporter beror enligt Naturvårdsverket på

* trafikarbetet (hur många kilometer som fordon färdas)
* hur mycket drivmedel som fordonet förbrukar i drift (fordonets bränsleeffektivitet) per fordonskilometer
* hur stor andel av drivmedelsförbrukningen i fordonen som utgörs av förnybart eller fossilt bränsle.

Som vi tidigare påtalat så är ökad användning av biobränsle omstritt och osäkert. 

Regeringen gav Energimyndigheten i uppdrag att tillsammans med Boverket, Naturvårdsverket, Trafikanalys, Trafikverket och Transportstyrelsen ta fram en strategisk plan för omställningen till en fossilfri transportsektor. Resultatet blev rapporten [Strategisk plan för omställning av transportsektorn till fossilfrihet](https://energimyndigheten.a-w2m.se/Home.mvc?resourceId=109664).

![Strategisk plan för omställning av transportsektorn till fossilfrihet](/images/transportsektorn-till-fossilfrihet.jpg)*Strategisk plan för omställning av transportsektorn till fossilfrihet*

I det inledande kapitlet så sammanfattas vad som behövs under rubriken **Tre nödvändiga delar: Transporteffektivt samhälle, fordon, drivmedel**:

"Omställningen av transportsystemet behöver stå på tre ben: med de mest kostnadseffektiva åtgärderna för samhället ska vi uppnå ett mer transporteffektivt samhälle, energieffektiva och fossilfria fordon och farkoster samt högre andel förnybara drivmedel. Det kommer inte att vara tillräckligt att endast arbeta med ett eller två av dessa områden. Dels för att resurser för att framställa förnybara drivmedel, batterier, fordon och infrastruktur är begränsade, dels för att sprida risken om något område inte utvecklas som förväntat. Det finns också andra goda ekonomiska, miljömässiga och sociala skäl till att utveckla mer transporteffektiva samhällen Det är en stor omställning som ska ske på kort tid och potentialen inom alla tre områden kommer där för att behöva utnyttjas för att nå de ambitiösa mål som satts upp. De tre områdena kommer dock att ges olika stort fokus i staden respektive på landsbygden. Åtgärder för ett transporteffektivt samhälle har särskilt stor potential i urbana miljöer där förutsättningar är goda för överflyttning mellan trafikslag och där bebyggelseplanering kan bidra till minskad efterfrågan på bilresor. På landsbygden kommer större vikt att behöva läggas vid energieffektiva fordon och förnybara drivmedel (även om lösningar för till exempel resfria möten och distansarbete också kan ha stor potential).

Med ett transporteffektivt samhälle menar myndigheterna i samordningsuppdraget ett samhälle där trafikarbetet med energiintensiva trafikslag som personbil, lastbil och flyg minskar. Detta kan ske både genom överflyttning till mer energieffektiva färdmedel/trafikslag och genom att transporter effektiviseras, kortas eller ersätts helt. Effektivisering av transporter kan ske genom exempelvis ökad fyllnads/beläggningsgrad i gods- och personfordon. Transporter kan kortas genom exempelvis en mer tät och funktionsblandad bebyggelse. Ersättning av transporter kanske via bland annat resfria möten eller förändrade arbetssätt och konsumtionsval. I och mellan städer och tätorter är en överflyttning till andra alternativ än personbil och lastbil enklare än på landsbygden där bilen är fortsatt viktig. Även vad gäller minskat flygande ser förutsättningarna olika ut i olika delar av landet."

**Så det går alltså inte att bara byta ut fossilbilar till elbilar**. Det är också något som Klimaträttsutredningen säger. De menar att Trafikverket bör få nya instruktioner så att flyg- och bilresor minskar och att fler cyklar, går eller åker tåg istället.

– Elektrifieringen är lovande, men den räcker inte till för att klara klimatmålet till 2030. Sen kräver även elbilar resurser, metaller och energi så det finns fortfarande anledning att vara effektiv med de fordon vi använder och köper, [säger Anders Roth som är transportforskare på IVL Svenska miljöinstitutet och sekreterare i Klimaträttsutredningen](https://www.svt.se/nyheter/vetenskap/ny-utredning-stoppa-motorvagar-biltrafiken-maste-minska).

Om regeringen väljer att följa utredningen skulle det innebära att många planerade motorvägsprojekt aldrig kommer att byggas.

## Förslagen för minskade utsläpp från vägtrafiken

Så här kommer några korta exempel på vad som kan göras.

Stoppa motorvägsbyggen. I Wales har regeringen [stoppat motorvägsbyggen som strider mot klimatmålen](https://www.etc.se/klimat-miljo/wales-vaegbyggen-stoppas-efter-klimatgranskning). Det är också något som nätverket Tvärnit Södertörn (där jag är aktiv) har föreslagit. Steg ett är att [säga nej till den planerade motorvägen Tvärförbindelse Södertörn](https://www.etc.se/debatt/goer-som-wales-regering-stoppa-klimatskadliga-motorvaegsbyggen) söder om Stockholm. [Naturvårdsverket är kritiskt](https://www.naturvardsverket.se/lagar-och-regler/provningsarenden/vagar-jarnvagar/tvarforbindelse-sodertorn-ny-motortrafikled-mellan-e4e20-vid-skarholmen-soder-om-stockholm-till-vag-73-i-haninge/) till motorvägsbygget för att den gynnar bilen på bekostnad av hållbara alternativ som kollektivtrafik, cykel och gång och går på tvärs emot klimatmålen.

![Demonstration mot motorvägen Tvärförbindelse Södertörn](/images/tvarnit-demo.jpg)*Demonstration mot motorvägen Tvärförbindelse Södertörn april 2022. Bild av Skiftet*

Satsa på kollektivtrafiken och gör den billigare. I Tyskland genomförde regeringen en stor satsning så att man för bara [9 euro i månaden](https://www.svt.se/nyheter/utrikes/sommarexperiment-i-tyskland-nastan-gratis-kollektivtrafik) kunde resa vart som helst i hela Tyskland under sommaren med kollektivtrafiken. Den franska staden Dunkirk har infört avgiftsfri kollektivtrafik för invånarna. Resandet i kollektivtrafiken har ökat med 60 procent på vardagar och det dubbla på helgerna. 48 procent av "nyåkarna" har lämnat bilen till förmån för bussen, skriver [Dagens ETC](https://www.etc.se/inrikes/kollektivtrafik).

Att omvandla bilkörfält, infarter och gator i städer till gång- och cykelbanor, kollektivtrafikkörfält eller annan användning har potential att minska miljöpåverkan från trafik redan på kort sikt enligt [Naturvårdsverket](https://www.naturvardsverket.se/amnesomraden/klimatomstallningen/omraden/klimatet-och-transporterna/omvandling-av-ohallbara-trafikleder-och-stadsdelar/). Det går snabbare och är ofta billigare att genomföra än att bygga ny trafikinfrastruktur för dessa färdsätt.

[Forskare på Lunds universitet](https://www.forskning.se/2022/05/17/basta-satten-minska-biltrafiken-stader/) gick igenom 800 vetenskapliga studier för att ta reda på vilka åtgärder för att minska biltrafiken i städerna fungerat bäst.

En väldigt effektiv åtgärd verkar vara trängselavgifter. Efter att städer som London, Milano, Stockholm och Göteborg införde trängselavgift minskade trafiken i stadskärnan med mellan 12 och 33 procent. Forskarna presenterade kombolösningar med både morot och piska. 12 åtgärder presenterades:

1. **Trängselavgift**. Bilister betalar en avgift för att komma in i stadskärnan. Intäkterna från trängselavgifterna kan användas för att finansiera kollektivtrafiken eller annan hållbar transportinfrastruktur
2. **Parkering och trafikreglering**. Minska antalet parkeringsplatser eller ersätt parkeringsplatser med cykelbanor/gångbanor i stan, och inför bilfria gator
3. **Områden med begränsad trafik**. Förbud av biltrafik i vissa delar av staden (förutom för boende)
4. **Mobilitetstjänst för pendlare**. 
5. **Parkeringsavgift på arbetsplatsen**. Kampanj för att låta anställda åka gratis i kollektivtrafiken och pendelbussar till och från arbetsplatsen. Detta gav **37 %** minskning av bilpendlare.
6. **Reseplanering på arbetsplatsen**. Parkeringsavgift på arbetsplatsen. Parkeringsintäkterna används för att finansiera kollektivtrafiken. Anställda som slutar köra bil och åker kollektivt belönas. 
7. **Reseplanering för studenter och universitetsanställda**. Begränsade parkeringsmöjligheter på campus. Rabatterat pris på kollektivtrafik. Reserådgivning och förbättrad infrastruktur för att få studenter och personal att cykla, promenera eller åka kollektivt.
8. **Mobilitetstjänster för universitet**. Gratis kollektivtrafik och bättre pendlingsmöjligheter för studenter. Detta gav **24 %** minskad bilanvändning bland studenter
9. **Bildelning**. Möjlighet till bildelning i nära anslutning till arbetsplatser och bostadsområden
10. **Skolreseplanering**. Reserådgivning och aktiviteter för att få elever och föräldrar att gå/cykla/samåka till skolan
11. **Personlig reseplanering**. Subventionerad kollektivtrafik. Lokal reserådgivning för att få fler att gå, cykla eller åka kollektivtrafik (rabatterad)
12. **App för hållbar mobilitet**. Belöning för de som använder hållbara transportmedel såsom cykel och kollektivtrafik. **73%** av appanvändarna rapporterade minskad bilanvändning

Enligt en rapport som IVL tagit fram på uppdrag av Världsnaturfonden WWF så [måste biltrafiken minska med 30% till 2030](https://www.wwf.se/pressmeddelande/ny-rapport-biltrafiken-maste-minska-med-30-procent-till-2030-3502449/). Några av rapportens rekommendationer för att nå 70 % utsläppsminskning till 2030 är följande:

* Anpassa nationella och regionala infrastrukturplaner till hållbart och minskat bilresande. Satsa på mer transporteffektiv stadsplanering som främjar gång-, cykel- och kollektivtrafik.
* Förstärk bonus-malus för att ställa om till mer klimatsmarta bilar och fordons- och drivmedelsskatter som styr inköpen mot fordon med låg förbrukning av drivmedel och el.
* Satsa på infrastruktur för laddfordon.
* Sänk hastigheter på vägar och öka kontroller av hastighetsöverträdelser.
* Minska parkeringssubventioner och mängden parkeringsplatser vid nybyggnation.
* Inför åtgärder som främjar bilpooler, biluthyrning och bildelning.
* Inför fler miljözoner i städer för att styra mot el.
* Flytta långväga godstransporter till sjöfart och järnväg, samt transporter inom staden från dieseldrivna distributionslastbilar till ellastcyklar/små eldrivna godsfordon.
* Genomför prioriterade satsningar på kollektivtrafik, gärna i kombination med statligt stöd till kommuner (stadsmiljöavtal) och klimatmål på kommunal nivå (stadstrafikmål).
* Öka investeringar i järnvägsinfrastruktur och satsningar på tågresande som kan flytta bilresande till och mellan städer till tåg.
* Driv på i EU för skärpta regelverk för fordons koldioxidutsläpp och att viss andel av bilföretagens försäljning ska vara laddbar eller ha motsvarande klimatprestanda.
* Gradvis övergång till km-skatt för lätta och tunga fordon som styr mot laddbara fordon och differentieras efter körning stad/land, trängsel m.m.

# Teknikutvecklingen räcker inte, politik krävs

Jag hoppas att med det här blogginlägget kunnat visa vikten av att vi verkligen måste få ner utsläppen från resor och transporter, att det är olika förutsättningar beroende på var man bor, att elektrifiering av transportsektorn är viktig men räcker inte som enskild åtgärd, att det finns tydliga politiska förslag färdiga som ligger färdiga för alla politiker att driva.