---
title: Skrivandet 2024
image: blogging.jpg
image_description: Bild av <a href="https://pixabay.com/users/aagraphics-27946333/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=7252438">David Jones</a> från <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=7252438">Pixabay</a>
description: Om mitt bloggande, skrivande och annat under 2024
date: 2024-12-28 14:00
---

Då är snart 2024 slut och jag har lust att summera mitt skrivande lite. Varför? För att det är kul! Och för att jag för typ ett år sen om [nyårslöfte kring skrivande](/blogga-mera/). Fokus där var på att blogga och interagera med fler vänsterfolk som bloggar. Jag skrev också om NaNoWriMo under november som jag återkommer till.

Jag skrev följande om min målsättning : 

> Mitt nyårslöfte (nja, ambition snarare) är att blogga minst en gång per vecka. Men då undantar jag storhelger och sommar. Så med andra ord, de flesta vanliga jobbveckor så tänkte jag skriva minst ett blogginlägg i snitt. Så ungefär 35-40 inlägg under året borde det bli om jag skippar juni, juli, augusti och någon storhelg.

**Alltså, målet var: 35-40 inlägg under 2024.**

Men det är mycket mer som har hänt under 2024! Jag har börjat skriva krönikor för [ETC Nyhetsmagasin](https://www.etc.se/av/samuel-skaanberg). (Jag publicerar krönikorna efter någin vecka här på bloggen). Och jag har tagit tjänstledigt från och med i augusti för att [läsa en skrivarkurs på ett år](/skrivarkursen-startad/).

## Målet om skrivande

Men vi börjar med den här bloggen. Om jag bara räknar antalet blogginlägg som är markerade med 2024 i datumet:

```
cat source/_posts/*|grep "date: 2024"|wc -l
```

...då blir det 39. Av dem är 13 återpubliceringar av krönikor. Så om jag räknar med även krönikorna så har jag nått målet vilket är kul!

Men jag har också skapat en ny sida, [text.bitar.se](https://text.bitar.se/) där jag lagt upp flera av mina alster från skrivarkursen. Där har jag gjort 24 inlägg.

Mål har jag för att det är kul. Och jag tror att sätta upp ett rätt rimligt mål under året har sporrat mig lite till att skriva. Och nu kan man lugnt säga att jag skrivit mer :) Ett väldigt kul skrivarår!

## Några utvalda texter

Om jag ska välja ut några texter från den här bloggen (inklusive krönikor) så blir det nog:

* [Krönika - Så hotar ljudboken jämlikheten](/ljudboken-jamlikheten/)
* [Maktanalys för kommunikation och sociala medier](/socmed-strategi/)
* [Krönika - Vänsterpreppa - så är du förberedd när krisen kommer](/vansterpreppa-digitalt/)
* [Cyberdeck för att skriva i solen](/cyberdeck/)
* [Chat Control handlar om massövervakning av vanligt folk](/chat-control-massovervakning/)
* [Krönika - Hacka din chefs övervakning](/hacka-din-chef/)
* [Rösta för socialistisk klimatpolitik och ockupera sen industrin](/rosta-och-ockupera/)
* [Vänsterns svar på klimatkrisen, en välbehövlig boost](/vansterns-svar-pa-klimatkrisen/)
* [Länkparty - Chat Control i limbo, Right To Repair, Google lyssnar, deadline för IPv4-död i Tjeckien, AI-skit, open source sökmotor](/lankparty-v7/)
* [Länkparty - Anonyma betalningar, möjligheter och begräsningar med AI/LLM, mindre datainsamling för Googles tjänster pga DMA](/lankparty-ai/)
* [Mysigt med Mastodon](/mysigt-med-mastodon/)

Och om jag väljer ut några texter från text.bitar.se får det bli:

Dikter:

* [Längtan](https://text.bitar.se/langtan/)
* [Tillsammans kan vi orka mer](https://text.bitar.se/ge-inte-upp/)
* [När vinden blåste svalt](https://text.bitar.se/naturen/)
* [Framtidens skymning](https://text.bitar.se/framtidens-skymning/)
* [Samma jord - olika världar](https://text.bitar.se/diktsamling-samma-jord/)
* [Åtta haikus](https://text.bitar.se/haikus/)
* [Haikus om folkhögskolan](https://text.bitar.se/folkis-haikus/)
* [Dagsvers om Oxfam](https://text.bitar.se/dagsvers-oxfam/)
* [Dagsvers om lagens moral](https://text.bitar.se/dagsvers-suv/)
* [Dagsvers om valet i USA](https://text.bitar.se/dagsvers-usa-valet/)
* [Fladdermusen](https://text.bitar.se/fladdermus/)

Noveller:

* [Svalorna flyger lågt](https://text.bitar.se/svalorna-flyger-lagt/)
* [Britta](https://text.bitar.se/personbeskrivning-britta/)
* [Brinnande sängar](https://text.bitar.se/brinnande-sangar/)
* [Färger](https://text.bitar.se/farger/)
* [Ensamheten](https://text.bitar.se/ensamheten/)

## Andra bloggare och författare

Jag har haft stor glädje av andra bloggare och författare. Självklart på kursen i kreativt skrivande. Men det är nog på Mastodon/Fediversum som jag gläds mest. Följande kamrater har jag stor glädje av att läsa:

* httpster / Jesper på [turist.blog](https://www.turist.blog). Bloggar intressant om teknik och politik. Skriver även intressanta krönikor på [ETC Nyhetsmagasin](https://www.etc.se/av/jesper-nilsson).
* Myra på [ETC Nyhetsmagasin](https://www.etc.se/av/myra-aahbeck-oehrman). Hon skriver om teknik och politik och har insyn i delar av teknikvärlden som inte jag har.
* Gnomvid på [Laia Odo-institutet](https://gnomvid.se/). Skriver också intressant om teknik och politik, speciellt vänsterpolitik. Kul också med en vänsterpartist som bloggar flitigt! Gnomvid är dessutom den av oss som varit flitigast med "länkparty-blogginlägg" vilket jag uppskattar!
* Den vita drogen / Laserjesus / Linus på [laserjesus.se](https://laserjesus.se/). Jag uppskattar speciellt novellerna.
* Jonathan på [prepp.fritext.org](https://prepp.fritext.org/blog/). Bloggar mycket om frihet, gemenskap och beredskap i gemenskap.
* Anders på [Svenssons Nyheter](https://blog.zaramis.se/). Den vänsterbloggare i Sverige som är i särklass mest aktiv. Och bloggar väldigt mycket och bra om Mastodon och Fedivsersum och om vikten av att vänstern äger sina egna medier och kommunikationskanaler.
* Rasmus på [Copyriot](https://copyriot.se/). Bloggar om mycket olika saker. Mycket politik men även om teknik, musik och annat.
* [Det glada tjugotalet](https://detgladatjugotalet.se/) är också en favorit.

Några utvalda texter:

* [Veckans länkar: Hur ska vänstern lämna Twitter/X?](https://www.turist.blog/2024-11-24-veckans-lankar/) av httpster / Jesper
* [Om att bygga publik, nätverk och räckvidd](https://www.turist.blog/2024-07-25-om-att-bygga-publik-natverk-och-rackvidd/) av httpster / Jesper
* [Mitt möte med bläck-oligarkerna](https://www.etc.se/kronika/mitt-moete-med-blaeck-oligarkerna) av Myra
* [Medborgargarden i rosa förpackning](https://www.etc.se/kronika/medborgargarden-i-rosa-foerpackning) av Myra
* [Vi måste sluta spela de högerextremas spel](https://www.etc.se/kronika/vi-maaste-sluta-spela-de-hoegerextremas-spel) av Myra
* [Knutpunkt, 2024-08-03. Om känsligt språk, olika slags tillväxt, teknologiers syfte, Asien och kålsallad.](https://gnomvid.se/2024/08/03/knutpunkt-2024-08-03-om-kansligt-sprak-olika-slags-tillvaxt-teknologiers-syfte-asien-och-kalsallad/) av Gnomvid
* [Knutpunkt, 2024-06-09. Den verkliga rörelsen. EU, Vänsterns partiprogram, Vårdstrejk, antiimperialism, medelklassen och potatisodling.](https://gnomvid.se/2024/06/09/knutpunkt-2024-06-09-den-verkliga-rorelsen-eu-vansterns-partiprogram-vardstrejk-anti-imperialism-medelklassen-och-potatisodling/) av Gnomvid 
* [A Book Called Death](https://laserjesus.se/a-book-called-death/) av Laserjesus. En novell på engelska denna gång
* [Grindset](https://laserjesus.se/grindset/) av Laserjesus. En novell på svenska
* [De vuxna i rummet](https://laserjesus.se/de-vuxna-i-rummet/) av Laserjesus.
* [Buen Vivir – Goda liv i gemenskap. En positiv vision för Sociala rörelser.](https://prepp.fritext.org/blog/goda-liv/buen-vivir-goda-liv-i-gemenskap-en-positiv-vision-for-sociala-rorelser/) av Jonathan.
* [Beredd att möta lynchmobben? Motståndskraft under en pogrom](https://prepp.fritext.org/blog/flykt/beredd-att-mota-lynchmobben-motstandskraft-under-en-pogrom/) av Jonathan
* [Ingen äger Fediversum](https://blog.zaramis.se/2024/11/01/ingen-ager-fediversum/) av Anders
* [Ett tillhåll för minoriteter och nördar – Fediversums historia](https://blog.zaramis.se/2024/08/18/ett-tillhall-for-minoriteter-och-nordar-fediversums-historia/) av Anders
* [Sociala medier och fria programvaror](https://blog.zaramis.se/2024/12/18/sociala-medier-och-fria-programvaror/) av Anders
* [Vänsterns poddar](https://blog.zaramis.se/2024/12/15/vansterns-poddar/) av Anders
* [#2 (maj 2024): självförsörjningsmått, realistisk musik, neurodiversitet, Kant…](https://copyriot.se/2-maj-2024-sjalvforsorjning-realistisk-musik-neurodiversitet/) av Rasmus
* [Om The Pirate Bay, internet och världen bortom tangentbordet](https://detgladatjugotalet.se/2024/12/11/om-the-pirate-bay-internet-och-varlden-bortom-tangentbordet/) av/på Det glada tjugotalet

## 2025 då?

Ja, vad händer under 2025? Just nu har jag inga planer på att sätta upp några konkreta siffermål för skrivandet. Bloggandet flöt på bra och det var kul. Teknikkrönikorna likaså och det har varit väldigt roligt och en lärorik process.

Till våren så ska vi på skrivarkursen pyssla med mer projektarbete. Så jag lutar åt att börja på en kortare roman. Vi får se hur det blir med den saken i slutändan men det känns hur som helst väldigt roligt! Så det projektet kommer nog ta det mesta av skrivarkrafterna skulle jag tro. Utöver det kanske det blir några teknikkrönikor och något enstaka blogginlägg.

Hoppas ni har uppskattat mina blogginlägg! Jag har hur som helst uppskattat att skriva dem :)

