---
title: Vänsterns svar på klimatkrisen, en välbehövlig boost
image: vansterns-svar.jpg
description: När högerpolitiker världen över går till attack mot klimatpolitiken är det upp till vänstern att driva på för en snabb och rättvis omställning
date: 2024-03-04 18:00
---

Jag blev väldigt glad när jag hörde att Jens Holm och Jonas Sjöstedt höll på med ett bokprojekt, en antologi med olika fokus på hur vänsterns svar på klimatkrisen kan se ut. Boksläppet var på ABF och lokalen fullsatt. Det var tydligt att många gick och törstade efter en sådan här bok.

Bokens namn är kort och gott: Vänsterns svar på klimatkrisen.

Jens Holm har sedan tidigare skrivit två böcker i ämnet. 

Man kan köpa boken här:

* [Verbals förlag (som gett ut den)](https://verbalforlag.se/bocker/vansterns-svar-pa-klimatkrisen/)
* [Adlibris](https://www.adlibris.com/se/bok/vansterns-svar-pa-klimatkrisen-9789189524552)
* [Bokus](https://www.bokus.com/bok/9789189524552/vansterns-svar-pa-klimatkrisen/)

## De olika kapitlena

Här nedan följer en kort sammanfattning av de olika kapitlena

### Shora Esmailian - I Syd är klimatkrisen redan här

Shora är författare och journalist. Hon skriver om hur klimatkrisen redan drabbar en stor del av världen. Miljoner människor flyr årligen sina hem på grund av översvämningar i Kina, Indien och Pakistan. De små önationerna drabbas hårt på grund av stigande havsnivåhöjningar.

Även i den rika delen av världen drabbas man av klimatförändringar. Som i den stora skogsbranden på Hawai. Och även i delar av Europa.

Med klimatkrisen kommer klimatflyktingar. Klimatkris är dock inte en erkänd anledning till att få asyl. Men vi behöver öppna upp våra gränser i takt med att klimatkrisen drabbar fler.

### Andrea Andersson-Tay - Klimatet, livsstilen och de superrika

Andrea är riksdagsledamot för Vänsterpartiet och miljöpolitisk talesperson. Hon skriver om hur klimatpåverkan är väldigt olika mellan länder men också inom befolkningen. Rika personer släpper ut allra mest.

Andrea tar upp rapporter från Oxfam som beskriver hur ojämlikt fördelat klimatusläppen är, och även i Sverige, något jag [bloggat om här](/klimat-sektorer/). Den rikaste tiondelen i Sverige stod för 23% av utsläppen i Sverige år 1990-2019.

Andrea tar upp två felaktiga synsätt gällande detta:

1. Fokus på enskilda individers utsläpp och vad man kan göra som enskild person för att få ner utsläppen. Om alla tar sitt ansvar så löser vi klimatkrisen
2. Genom ny teknik kommer utsläppen minska. Vi kommer inte behöva göra avkall på vår livsstil i princip alls

Båda synsätten har stora brister vilket hon beskriver i sitt kapitel.

### Lars Henriksson - Klimatet, jobben och fackföreningsrörelsen

Lars har under sitt yrkesliv jobbat som bilarbetare och varit fackligt aktiv. Han ägnar sitt kapitel om att skriva om varför fackföreningsrörelsen måste kroka arm med miljörörelsen. Han ifrågasätter även den dominerande synen på vikten av tillväxt som funnits (och fortfarande finns) i fackföreningsrörelsen

Lars beskriver också att industrin kan ställa om och att arbetare kan gå i bräschen för detta. Han tar upp exempel där arbetare tagit över makten över sina arbetsplatser för att ställa om produktionen som Lucas Aerospace i Storbrittanien som producerade krigsmateriel och GKN-fabriken i Florens som producerar bildelar.

### Annika Lillemets - Klimatet, tillväxten och arbetstiden

Annika är civilingenjör inom teknisk fysik och elektroteknik och var tidigare riksdagsledemot för Miljöpartiet. Hon är numera medlem i Vänsterpartiet. Hon ägnar sitt kapitel för att kritisera tillväxtssamhället och arbetshetsen.

Både Keynes och Bertrand Russell såg framför sig en minskad arbetstid på 15-20 timmar i veckan.

Hon kritiserar begreppen "grön tillväxt" och visar på att hittils har energi- och resurseffektiviseringar ätits upp av ökad konsumtion, något som drivits på av lönearbete. EU-parlamentets miljöutskott uttalade 2010 att det vore klokt ur miljöperspektiv att växla produktionsökningar i minskad arbetstid.

Att minska arbetstiden skulle inte heller vara orimligt dyrt. Enligt en uträkning från Katalys skulle en minskning av arbetstiden från 40 timmar till 35 timmar i veckan vara i samma storleksordning som kostnadern för RUT- och ROT-avdrag.

### Elin Segerlind - Klimatet, skogen och jordbruket

Elin har tidigare varit riksdagsedamot för Vänsterpartiet och representerat Vänsterpartiet i miljö- och jordbruksutskottet. Hon skriver om att klimatkrisen och förslusten av biologisk mångfald hör samman. När det kommer till förlust av biologisk mångfald är det markanvändning som är det främsta hotet, dvs skogsbruk och jordbruk.

Vi behöver skydda mycket mer skog än vad som görs idag. EU har tagit beslut om att 30 procent av land- och havsrområden ska skyddas till 2030. Det är långt ifrån Sveriges dryga 11 procent idag.

Stora jordbruksstöd från EU gynnar storjordbruken iställen för de mindre. 80 procent av stöden går till den rikaste femtedelen av markägare.

Hon pekar också på att vi måste vända på perspektivet gällande skogsbruk. Det kan inte vara skogsindustrins behov av råvara som ska styra uttaget, utan skogens möjlighet att bidra med råvara som ska avgöra hur mycket skog som kan avverkas.

### Jens Holm - Efter köttet

Jens Holm är tidigare riksdagsledamot för Vänsterpartiet och var klimat- och miljöpolitisk talesperson. Han ägnar sitt kapitel åt hållbar livsmedelsproduktion.

Sedan 1950 har köttkonsumtionen femfaldigats. I Sverige används 70 procent av vår åkermark till djurfoder. Djuren lider, sjukdomar sprid och miljö- och klimatpåverkan är stor från animalieproduktionen. Det effektivaste sättet att råda bot på djurindustriernas negativa effekter är att minska köttkonsumtionen.

Jens jämför med hur skatter, styrmedel, regleringar, informationskampanjer och förändrade normer kan ha stor påverkan. Han pekar på hur rökningen har minskat och att det numera är självklart att man inte röker på krogen (vilket många tänkte var helt otänkbart för några årtionden sen), hur alkoholkonsumtionen ligger på en hållbarare nivå till följd av skatter och systembolag och att biltrafiken i Stockholms stad har minskat med 40 procent de senaste 20 åren.

Han presenterar några förslag på lösningar: sätta ett mål om att minska köttkonsumtionen med 40% per capita, att inte tillåta nyetablering av djurfabriker, ställa krav på att djur i djurindustrin måste kunna leva ut sina naturliga beteenden, fasa ut de stora EU-subventioner som ger pengar till djurindustrin och istället satsa de pengar på att öka de växtbaserade alternativen.

### Kajsa Fredholm - Vänsterns lösningar måste vara gemensamma

Kajsa är riksdagsledamot för Vänsterpartiet och klimatpolitisk talesperson. Hon ägnar sitt kapitel att sammanfatta vad Vänsterpartiets nuvarande klimat- och miljöpolitik är.

Hon pekar på att vänsterns lösningar måste vara gemensamma och tilltala breda grupper för att kunna realiseras. Högerns lösningar förlitar sig på marknaden och individuella insatser för att nå klimatmålen. Vänsterns klimatpolitik är kollektiv, materiell och jämlik med det offentliga som motor för förändring.

Kajsa understryker vikten av att hålla sig inom den koldioxidbudget vi har för att klara klimatmålen. Hon förklarar också varför Vänsterpartiets fokus på industri och transporter är så viktiga, för att de står för runt 2/3 av utsläppen.

Hon förklarar varför det kan vara en poäng att satsa på tekniska lösningar och varför det kan vara vanskligt att satsa för mycket på beteendeförändringar.

### Deniz Butros - Företagspolitik från vänster

Deniz Butros har jobbat med klimat- och miljöfrågor i både privat och offentlig sektor och har och har haft flera förtroendeuppdrag för Vänsterpartiet. I sitt kapitel skriver hon om företagspolitik.

Hon slår fast att de stora utsläppen idag kommer från industrin, jord- och lantbruk och transporter, det vill säga från företagen. Vänstern bör utforma en tydligare företagspolitik som ser till att sätta tydligare regler och förväntan på företag samtidigt som man tar vara på de positiva krafter inom företagsvärlden. Hon lyfter företagsnätverket Hagainitiativet som hade som mål att minska sina utsläpp med 40% till 2020. Det finns andra initiativ som Fossilfritt Sverige.

Att avfärda dessa initiativ från vänstern blir konstigt när de många gånger vågar göra mer radikala förändringar än vad politiken föreslår.

Vänstern bör sätta upp gröna marknadsregler för företagen att förhålla sig till. Högern har länge haft monopol att driva företagspolitik men vänstern har alla möjligheter att göra samma sak fast mycket bättre.

### Malin Björk - Vänsterns klimatkamp i EU

Malin Björn är EU-parlamentariker för Vänsterpartiet sedan 2014.

Vänsterpartiet har sedan Sveriges inträde i EU 1995 arbetat för att förbättra EU:s klimat- och miljöpolitik. Men samtidigt som EU kan föra fram bra klimat- och miljöåtgärder så hindrar man andra bra initiativ. T.ex. så skjuter EU till stora subventioner till klimatskadlig verksamhet (exempelvis köttproduktion, flygplatser och motorvägar) och EU:s regelverk hindrar att medlemsländer går före med progressiv klimat- och miljöpolitik som backas upp av statsstöd för att de skulle störa EU:s inre marknad.

Coronapandemin visade dock att det går att göra undantag för EU:s hårda budgetregler. Eftersom vi befinner oss i ett klimatnödläge så bör undantag också kunna göras för klimatinvesteringar.

Vänsterpartiet arbetar med andra systerpartiet i EU för att driva en mer ambitiös klimat- och miljöpolitik. 

### Max Andersson - Klimatet och handelsavtalen

Max Andersson har varit aktiv i arbetet mot handelsavtalen CETA och TTIP. Han har suttit i riksdagen och EU-parlamentet för Miljöpartiet men är nu aktiv i Vänsterpartiet.

Handelsavtal används ofta för att förhindra progressiva förslag. Ett tydligt exempel på hur handelsavtal kan användas för att förhindra progressiv lagstifning är investeringsskydd eller ISDS. Det gör det möjligt för företag att stämma stater som ändrar sina lagar och som på något sätt kan missgynna företagens verksamhet. Det finns gott om exempel om hur företag vunnit stämningar mot stater. Men bara blotta risken gör att stater ofta tvekar inför att införa mer progressiv lagstiftning.

Det finns en rad handelsavtal som påverkar internationell politik. Några exempel är ECT, NAFTA och CETA. Genom hård kamp har progressiva rörelser kämpat emot och stoppat ACTA och TTIP.

### Gertrud Ingelman - Klimatrörelsen är nyckeln till förändring

Gertrud Ingelman har länge varit aktiv i Vänsterpartiet och i klimat- och miljörörelsen. Hon sitter för närvarande i Vänsterpartiets klimat- och miljönätverk och i Partistyrelsen.

Vilket är bäst? Modiga klimataktivister som genom civil olydnad skapar uppmärksamhet eller den trögare parlamentariska vägen? Gertrud landar i att bägge behövs.

Gertrud skriver om hur klimataktivism och parlamentariskt arbete kan samverka i en positiv spiral. Hon skriver om hur klimataktioner som ställer krav som folk i allmänhet kan hålla med om är extra viktiga. Och hur viktigt det är att kunna sammankoppla en aktion med en plats. Hon tar upp klimatkampanjen Stoppa Preemraff och hur det gick att stoppa utbyggnaden genom stor folklig mobilisering.

Hon skriver om etablerade miljöorganisationer som Naturskyddsföreningen, WWF, Greenpeace och Jordens Vänner och om nyare organisationer och nätverk som Fridays For Future, Skydda skogen, Återställ våtmarkerna och Extinction Rebellion. Gertrud tar också upp några exempel på när klimataktivism och partiarbete och parlamentarism samverkat på ett positivt sätt.

### Linda Snecker - Patriarkatet får inte sabba klimatomställningen (igen)

Linda Snecker är riksdagsledamot för Vänsterpartiet och partiets trafikpolitiska talesperson.

Linda skriver om hur elmotorn hade goda förutsättningar att bli standardmotorn i bilen runt sekelskiftet 1900. Den var tyst och luktfri och marknadsfördes därför till kvinnor. Laddningsstationer för elbilar hade redan börjat byggas vid köpecentrum och andra platser. Men när Henry Ford 1909 skapade T-Forden och kunde massproducera den billigt så började bensinmotorn ta över. (Hans fru fick dock en elbil eftersom den ansågs passa kvinnor bättre).

Linda resonerar kring hur normer kring maskulint och feminimt påverkar samhällsutvecklingen. Hade elbilen kunnat slå igenom runt 1900 om normerna kring resande hade sett annorlunda ut?

### Rikard Warlenius - Allt måste förändras för att förbli detsamma

Rikard Hjort Warlenius är lektor i humanekologi. Han har suttit i Stockholms stadsfullmäktige för Vänsterpartiet. Han har även skrivit böcker om ekomarxism.

Rikard skriver om hur extremvärme gör att kroppens metod att reglera kroppstemperaturen genom svettning sätts ur spel. Han tar upp "cli-fi-boken" Ministry for the future som Kim Stanley Robinsons skrivit. Den beskriver en tänkt värmebölja 2025 i Indien drabbar samhället. Hur elkraftverk slås ut och hur miljontals människor dör i värmeböljan. En sådan framtid är långt ifrån skrämselpropagand utan tvärtom en realistisk framtidsbild. I boken så testas en rad klimatstrategier för att få ner utsläppen och minska växthuseffekten.

Han resonerar kring hur man kan använda potentialen i vänsterpopulismen för att få igenom en rättvis klimatomställning, en omställning som kan få stöd av folkflertalet. Det är en liten elit som tjänar pengar på sakernas tillstånd medan den absoluta majoriteten drabbas av klimatkrisen.

### Jonas Sjöstedt - Klimatet och socialismen

Jonas Sjösted är tidigare partiordförande för Vänsterpartiet. Han har tidigare suttit i riksdagen och EU-parlamentet för Vänsterpartiet. Han är inför EU-valet 2024 Vänsterpartiets listetta.

Jonas presenterar en bred bild av vilken klimatpolitik som behövs. Han menar att gammal politik måste omprövas, att vi måste få en ny syn på ekonomisk tillväxt, att även politiska åtgärder behövs som kan vara svårsmälta för många väljare (som minskad köttkonsumtion och flygande).

Han beskriver hur kapitalismen i sin grund är ohållbar. Den kräver ständig expansion med överkonsumtion, överproduktion och tillväxt. Han tar upp boken Capital in the antropocene av den japanska filosofprofessorn Kohei Saito som har lusläst Karl Marx och hur Marx har skrivit om kapitalismens påverkan på miljön.

Evig tillväxt är inte möjlig på en planet med begränsade resurser.

Jonas skriver om hur man visst kan använda prismekanismer och marknadskrafter för att bidra till klimatomställningen. Men det kommer inte räcka. Det krävs även mycket ingrepp som går på tvärs mot marknadslogiken. Staten måste ta ett större ansvar och styra mer. Fossila bränslen kommer behöva fasas ut och förbjudas på många områden.

Och högern kommer aldrig kunna driva en riktig klimatpolitik som kan klara av krisen eftersom omställningen kräver att den elit som har sina privelegier fråntas dem. Och det kommer högern aldrig göras, de vill inte begränsa den globala överklassen.

Jonas Sjöstedt skriver en snarlik (men kortare) text i debattartikeln [Klimatfrågan är en konflikt mellan höger och vänster](https://www.etc.se/debatt/klimatfraagan-aer-en-konflikt-mellan-hoeger-och-vaenster).