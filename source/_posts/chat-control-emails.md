---
title: Maila och kräv svar om Chat Control från M, KD och L
image: email-megaphone.jpg
image_description: Bild av <a href="https://pixabay.com/users/mohamed_hassan-5229782/">Mohamed Hassan</a> från <a href="https://pixabay.com/">Pixabay</a>
description: C, V, MP och SD har tagit ställning mot Chat Control och massövervakning visar rapport från Kamratdataföreningen Konstellationen. M, KD och L som tidigare uttryckt kritik duckar nu frågorna. Det är dags att de svarar. Här finns mallar för epost att använda samt kontaktuppgifter.
date: 2024-09-23 11:00
---

**C, V, MP och SD har tagit ställning mot Chat Control och massövervakning visar rapport från Kamratdataföreningen Konstellationen. M, KD och L som tidigare uttryckt kritik duckar nu frågorna. Det är dags att de svarar. Här finns mallar för epost att använda samt kontaktuppgifter.**

Kamratdataföreningen Konstellationen skickade ut en enkät med frågor till samtliga riksdagspartier om deras inställning till Chat Control, övervakning och personlig integritet. [Svaren presenterades sen i en rapport](https://konstellationen.org/2024/09/19/chatcontrol-rapport/) där föreningen samtidigt går igenom kritiken mot Chat Control från civilsamhälle, forskare och akademiker samt presenterar partiernas historiska agerande i frågan.

Efter rapporten skrev Kamratdataföreningen Konstellationen en [debattartikel](https://www.etc.se/debatt/varfoer-vill-m-l-kd-och-s-inte-prata-om-chat-control)

> C, V, MP och SD svarade på enkäten. De svarade alla "ja" på våra frågor och har därmed tagit tydligt ställning mot Chat Control och massövervakning.
> S, M, KD och L svarade inte på enkäten trots flera påminnelser. Vi upplever att partierna duckar för frågorna. Vi är oroade över att de inte vill svara på om de anser att privat kommunikation är en mänsklig rättighet, vilket det är enligt Europakonventionen, EU-stadgan och FN:s deklaration om mänskliga rättigheter.
>
> Det är dags att sluta tala med kluven tunga.

Enligt rapporten och enligt kampanjsajten [ChatControl.se](https://chatcontrol.se/status/) så har M, KD och L tidigare uttryckt kritik. Men partiet agerar inkonsekvent. Därför behöver EU-parlamentarikerna tydliggöra om de fortfarande står fast vid sin kritik.

Det vore verkligen bra om många kunde ställa dessa frågor till EU-parlamentariker för M, KD och L:

1. Är du motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står du bakom EU-parlamentets kompromissförslag?
3. Tycker du att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker du att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle du säga nej till förslaget?
6. Anser du att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser du att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

## Kristdemokraternas kritik i EU-parlamentet

Kristdemokraterna representeras av Alice Teodorescu Måwe. Hon har tidigare skrivit blogginläggen ["Chat control" hör inte hemma i rättsstaten](https://www.aliceteodorescu.se/p/chat-control-hor-inte-hemma-i-rattsstaten) och [Varför inte en kamera i sovrummet också?](https://www.aliceteodorescu.se/p/varfor-inte-en-kamera-i-sovrummet). Även efter EU-valet skrev hon [kritiskt om Chat Control på Twitter/X (4 september)](https://x.com/alicemedce/status/1831342333863891427)

> Fullspäckad dag också idag som bland annat innehållit intervju med Ekot och möten i flera av mina utskott. Kommissionär Ylva Johansson besökte Libe nu på eftermiddagen och dessvärre fick jag ingen taltid idag. Men jag hade förberett en fråga, på tema chat control. 
>
> Det är för mig helt uppenbart att behovet att komma åt sexualbrott mot barn är enormt. Men mycket tyder på att personer som det så kallade Chat control förslaget är tänkt att träffa inte kommunicerar på de plattformar som avses, varför risken är stor att de som ägnar sig åt barnövergrepp ändå skulle flyta under radarn medan den stora laglydiga massan, som inte har något att dölja, skulle utsättas för en oproportionerlig övervakning av sin dagliga digitala korrespondens. 
>
> Frågan är således inte huruvida det behövs effektivare medel för att komma åt pedofiler, det gör det helt uppenbart, utan om hur förslaget ska utformas för att på ett rättssäkert, effektivt och ändamålsenligt vis identifiera och döma fler pedofiler - utan att man samtidigt inför massövervakning, med alla de konsekvenser och risker som det medför, av oskyldiga människor.

Alice Teodorescu Måwes mail är alice.teodorescumawe@europarl.europa.eu

### Exempel på mail till Alice Teodorescu Måwe (KD)

Hej Alice,

Jag har läst om din kritik mot Chat Control, bland annat det här inlägget du skrev på X 4 september 2024: https://x.com/alicemedce/status/1831342333863891427.

Men än är frågan inte avgjord och flera som tidigare varit kritiska har blivit luddiga och otydliga. Jag undrar om du kan svara på följande frågor:

1. Är du motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står du bakom EU-parlamentets kompromissförslag?
3. Tycker du att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker du att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle du säga nej till förslaget?
6. Anser du att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser du att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Med vänlig hälsning

## Moderaternas kritik i EU-parlamentet

Moderaternas toppkandidat var Tomas Tobé och han har uttryck kritik vid flera tillfällen:

* [Moderaterna i EU säger nej till Chat control: ”Massövervakning”](https://www.aftonbladet.se/nyheter/a/oneXE0/m-i-eu-sager-nej-till-chat-control-gar-emot-regeringen)
* [Inlägg på Twitter/X](https://x.com/tomastobe/status/1648254012213403650) 18 april 2023
* [Inlägg på Twitter/X](https://x.com/tomastobe/status/1791039640633254381) 16 maj 2024

Tomas Tobé mail är tomas.tobe@europarl.europa.eu

### Exempel på mail till Tomas Tobé (M)

Hej Tomas,

Jag har läst om din kritik mot Chat Control:

https://www.aftonbladet.se/nyheter/a/oneXE0/m-i-eu-sager-nej-till-chat-control-gar-emot-regeringen

https://x.com/tomastobe/status/1791039640633254381

Men än är frågan inte avgjord och flera som tidigare varit kritiska har blivit luddiga och otydliga. Jag undrar om du kan svara på följande frågor:

1. Är du motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står du bakom EU-parlamentets kompromissförslag?
3. Tycker du att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker du att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle du säga nej till förslaget?
6. Anser du att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser du att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Med vänlig hälsning


## Liberalernas kritik i EU-parlamentet

Liberalernas representant i EU-parlamentet är Karin Karlsbro. 

Hon har tidigare skrivit på [Facebook](https://www.facebook.com/karlsbroliberal/posts/pfbid02CKHivHcrBkwnWJCNTyJLWvGTS1GSLnjTU1rNetUoGqnH4BWxSYTcmHj6fNdS5exel)

> NEJ TILL CHAT CONTROL 
>
> Min liberala röst i Europaparlamentet kommer jag att använda till att säga nej till EU-kommissionens lagförslag som kommit att kallas “Chat Control” i dess nuvarande form som ligger på parlamentets bord. 
Att bekämpa brottslighet, i synnerhet de allra grövsta vidriga brott man kan tänka sig och där brottsoffren är barn, är en av samhällets viktigaste uppgifter. Det är angeläget och nödvändigt att vi har gemensamt EU-arbete för att förebygga och bekämpa brott där barn är offer för grova sexualbrott, också online. 
>
> Men som delar av förslaget till lagstiftningen “Combating child sexual abuse online” nu är utformat så går ingreppen i den personliga integriteten alltför långt. Särskilt som det finns frågetecken kring de långtgående metodernas faktiska effektivitet för att uppnå syftet. Det är olyckligt och måste förändras.
>
> Rätten till personlig integritet är idag på många platser i världen allvarligt hotad, eller obefintlig. De tekniska möjligheterna till att övervaka människors privatliv växer samtidigt i snabb takt. I den verkligheten får vi i EU inte begå misstaget att i kampen för en mycket god sak, bortse från konsekvenserna och själva slå in på en väg som undergräver mänskliga fri- och rättigheter.

Hon beskrev inför EU-valet sin hållning till Chat Control i en [enkät från Tidningsutgivarna](https://tu.se/standpunkt/pressfrihet/bred-enighet-om-pressfrihet-bland-svenska-eu-kandidater/)

> Rätten till integritet ska vara stark – gentemot både staten och privata företag. Ibland måste integriteten vägas mot andra viktiga intressen, som arbetet mot terrorism eller grova brott mot barn. Integritetsfrågorna måste alltid analyseras noggrant när politiska förslag tas fram. Så har inte fallet varit vid framtagandet av det förslag till Child Sexual Abuse Material-förordning som i debatten nu har kommit att kallas för “chat control”. Bland annat därför har vi sagt nej till förslaget som kommissionen presenterade och det ursprungliga förslaget är därför inte längre aktuellt.

Karin Karlsbro säger alltså nej till EU-kommissionens ursprungliga förslag men säger att det inte längre är aktuellt vilket hon ju har rätt i. Därför är det extra intressant att veta hur hon ställer sig till frågan generellt, frågan om massövervakning och att bryta kryptering för att skanna efter material.

Karin Karlsbro mail är karin.karlsbro@europarl.europa.eu


### Exempel på mail till Karin Karlsbro (L)

Hej Karin,

Jag har läst om din kritik mot Chat Control:

https://www.facebook.com/karlsbroliberal/posts/pfbid02CKHivHcrBkwnWJCNTyJLWvGTS1GSLnjTU1rNetUoGqnH4BWxSYTcmHj6fNdS5exel

https://tu.se/standpunkt/pressfrihet/bred-enighet-om-pressfrihet-bland-svenska-eu-kandidater/

Men än är frågan inte avgjord och flera som tidigare varit kritiska har blivit luddiga och otydliga.

Som du säger så EU-kommissionens ursprungliga förslag inte längre aktuellt. Men flera av förslagen som lagts fram skiljer sig inte speciellt mycket från EU-kommissionens ursprungliga version och är mer likt det än vad de är lika EU-parlamentets förslag.

Jag undrar om du kan svara på följande frågor:

1. Är du motståndare till EU-kommissionens förslag som kallas Chat Control 2.0?
2. Står du bakom EU-parlamentets kompromissförslag?
3. Tycker du att övervakning och skanning bara ska vara riktad mot misstänkta, och inte generell mot alla användare?
4. Tycker du att totalsträckskrypterad kommunikation ska fortsätta vara tillåten och att man inte ska tvinga tjänster eller appar att kringå krypteringen?
5. Om Belgiens kompromissförslag (eller väldigt snarlikt förslag) skulle komma upp igen i EU-parlamentet, riksdag eller regering, skulle du säga nej till förslaget?
6. Anser du att privat kommunikation är (eller bör vara) en mänsklig rättighet?
7. Anser du att det finns en risk/sannolikhet för ett sluttande plan med Chat Control så att mer än CSAM-material skannas efter i framtiden?

Med vänlig hälsning

## Publicera era svar

Får ni svar från EU-parlamentarikerna så publicera gärna svaren på era bloggar och sociala medier.

Ni får väldigt gärna också vidarebefordra svaren till mig på samuel@bitar.se. Jag kanske publicerar dem på min blogg.