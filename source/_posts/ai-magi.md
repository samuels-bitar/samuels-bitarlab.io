---
title: Krönika - Är AI magi – eller något för vänstern?
image: abstract.jpg
description: En krönika om AI, open source och om fördelar och nackdelar med AI jag skrev i ETC Nyhetsmagasin
date: 2024-02-19 09:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/aer-ai-magi-eller-naagot-foer-vaenstern).

**Chattbottar är väldigt bra på att veta vad för slags text som följer efter annan text men kan inte resonera, bara härma och hallucinera; om den inte vet har den inga problem att hitta på, likt en akademiker på LSD.**

AI-utvecklingen exploderade under 2023 och blev känd för allmänheten genom ChatGPT, en chattbot man kunde skriva till och få läskigt bra svar. Det kändes som att det var en tänkande varelse på andra sidan. Men så var aldrig fallet.

Tekniken handlar om sannolikheter, statistik och träningsdata. Massvis med träningsdata. Chattbottar är väldigt bra på att veta vad för slags text som följer efter annan text men kan inte resonera, bara härma och hallucinera; om den inte vet har den inga problem att hitta på, likt en akademiker på LSD.

En av revolutionerna i AI-utvecklingen det senaste året är att flera av dessa språkmodeller är släppta fria så att vem som helst kan använda sig av dem. Mozilla (organisationen bakom webbläsaren Firefox) har exempelvis släppt vad de kallar [Llamafile](https://future.mozilla.org/blog/introducing-llamafile/), en lokal open source-AI som kan köras på din egen dator utan nätverksuppkoppling.

Jag testade Llamafile. Det var fascinerande att se att min dator kan spotta ur sig samma slags svar som ChatGPT kan göra. Och det är bra, för det tar bort magin från AI. Dessa nya typer av lokal AI gör också att tekniken blir mer tillgänglig.

New York Times [stämde](https://edition.cnn.com/2023/12/27/tech/new-york-times-sues-openai-microsoft/index.html) nyligen OpenAI för upphovsrättsbrott eftersom det hade blivit tydligt att tidningens artiklar utan tillstånd användes i träningen av språkmodellen ChatGPT. Gamla frågor får nu nya dimensioner. Är det okej att använda upphovsrättskyddat material för att träna en chattrobot? Eller måste ett företag i så fall betala licensavgifter? Och i så fall, är det bara storföretagen som kommer kunna utveckla språkmodeller?

Oavsett vad man tycker om AI, språkmodeller och maskininlärning så kommer det inte försvinna i första taget. Vill vi att storföretagen ska bestämma hur vi använder tekniken och hur den regleras? [Eller ska det vara upp till oss?](https://www.arbetaren.se/2024/01/23/kontroll-over-tekniken-nyckelfraga-for-framtiden/) [Sådana](https://www.turist.blog/2024-01-27-veckans-lankar/) [frågor](https://blog.zaramis.se/2024/01/28/agandet-och-kontrollen-over-teknologin-pa-natet/) har glädjande nog börjat diskuteras inom den svenska vänstern. [Vilka fördelar finns?](https://www.flamman.se/artificiell-intelligens-banar-vag-for-socialismen/) Vilka [risker?](https://gnomvid.se/2024/01/26/knutpunkt-2024-01-26-gaza-ekologisk-hushallning-samt-ais-politiska-ekonomi-och-faktiska-hot/) Vad kan ens AI göra?

Börja med att installera Llamafile på din dator och testa – om så bara för att avmystifiera AI.

