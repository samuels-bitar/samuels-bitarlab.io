---
title: Jakten på den fria bloggen
image: treasure.jpg
description: Några tankar om att hitta en blogglösning som är fri gratis och enkel att använda
date: 2023-07-29 16:00
---

Jag tror på bloggens potential!

Ja, jag gör faktiskt det. Det är så himla bra att kunna skriva texter som enkelt kan kommas åt på en enkel hemsida som inte kräver inloggning. Det fiffiga med en blogg är ju också att man kan skriva så långt (eller kort) man vill och sen länka till sitt blogginlägg i mailutskick eller i sociala medier.

Just ja, kan vara värt att säga ett en blogg är ju bara en vanlig hemsida. Det som kanske är speciellt med en blogg är att man kontinuerligt uppdaterar den med inlägg om ditt och datt. Kanske också att en blogg är lite mer personlig, eller att det är ett nätverk eller förening bakom. När slutar en blogg vara en blogg och vara något annat? Jag vet inte. Men med blogg så menar jag helt enkelt en hemsida som kontinuerligt uppdateras med inlägg.

## Fler behöver äga sina egna plattformar

Vi var några i [Kamratdataföreningen Konstellationen](https://konstellationen.org) som snackade i vår gemensamma gruppchatt om att det var väldigt många riksdagspolitiker som bara kommunicerar via centraliserade och proprietära lösningar som Twitter eller Facebook. Det är till och med så illa att när jag mailade den liberala EU-parlamentarikern Karin Karlsbro för att fråga om hennes inställning till [massövervakningsförslaget Chat Control](https://konstellationen.org/2023/05/20/demo-chatcontrol-tal/) så fick jag svaret:

> Tack för ditt mejl! Du kan läsa Karins ställningstagande om Chat Control här: https://www.facebook.com/karlsbroliberal/posts/pfbid08Hh1SURHWhgvBmHtHiWakmPDJzfsu1peHTodSFfazQLvR2QRNvsJvjvrBFgoAQ5rl

Ja, till och med folkvalda EU-parlamentariker med politiska sekreterare kan inte ens kommunicera sina politiska ställningstagande via egna hemsidor. Och det ser ut som att de flesta politikerna från vänster till höger inte har egna kommunikationskanaler.

Okej, det var en lång rant. Men poängen är att jag tror att Konstellationen har en roll att fylla här genom att göra det lättare för progressiva att komma igång med sina egna kanaler. Först och främst tänker jag på blogg/hemsida och nyhetsbrev. (Men bra också om man har närvaro på t.ex. Mastodon).

Det finns många sätt att starta en blogg. Antingen betalar man för hela kalaset eller så försöker man göra så gratis man bara kan. Det är några aspekter att tänka på:

## Faktorer att tänka på

* **Domännamn**. Det är bra om man äger sitt egen domännamn. Jag t.ex. har bitar.se (och alla subdomäner) som jag äger personligen. Det är bra för just nu pekar samuels.bitar.se mot [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) som hostar själva hemsidan. Om jag av någon anledning vill byta till något annat så kan jag göra det. Men då måste jag ha kontroll över domännamnet. Om jag hade använt gratisdomänen man får av Gitlab (samuelsbitar.gitlab.io) så får jag problem om jag vill byta till annan plats. Jag använder [Njalla](https://njal.la/) för mina domäner.
* **Hosting**. Det räcker inte med ett domännamn. Ens hemsida/blogg måste "bo" någonstans också. Som sagt, den här bloggen "bor" på Gitlab Pages som är gratis för statiska hemsidor. Det som är fiffigt med Gitlab Pages är att det är gratis att koppla ihop med ett domännamn. Så jag har kopplat ihop mitt domännamn samuels.bitar.se med Gitlab Pages.
* **Kostnad för domännamn**. Kostar domännamnet pengar? Ja, i nästan alla fall. En .se-domän på Njalla kostar 30 euro per år och en .org eller .net kostar 15 euro per år. [Man kan kolla här för priser](https://njal.la/domains/). Men Konstellationen har en tjänst för sina medlemmar (som vi inte kommunicerat så mycket kring) som vi kallar [Fritext.org](https://fritext.org/). Det innebär att alla medlemmar kan få en subdomän gratis under domänen fritext.org. I vårt exempel så ville Alice ha en hemsida så hon skapade en på [Codeberg Pages](https://codeberg.page/) (som Gitlab Pages) och fick en subdomän under fritext.org: https://alice.fritext.org/
* **Kostnad för hosting**. Det går att få gratis hosting. Oftast så är det om man godkänner reklam på sidan eller så är det hårda resursbegränsningar. Några ställen som är gratis är [Codeberg Pages](https://codeberg.page/), [Gitlab Pages](https://gitlab.com/pages), [Github Pages](https://pages.github.com/), [Alwaysdata](https://www.alwaysdata.com/en/).
* **Enkelt att använda**. Om man är webbutvecklare (som jag själv) så kan det vara enkelt och smidigt att hacka ihop sin egen hemsida med en static site generator (SSG) som Hexo eller Jekyll och hosta den på t.ex. Gitlab Pages eller Codeberg Pages. Men om man är lite ny på det hela så kanske Wordpress eller andra webbgränssnitt kan funka
* **Kunna lägga upp bilder**. Det finns exempel på gratis bloggar som är väldigt smidiga men som inte har stöd för bilder, t.ex. WriteFreely. Jag skapade en testblogg här på https://write.bz/samuel. [Här är ett blogginlägg på den bloggen](https://write.bz/samuel/forsta-blogginlagget-med-write-freely).

## Det enklaste sättet - gratis

Det enklaste sättet att komma igång är nog att starta en gratis wordpressida.

Jag skapade för länge sen en hemsida som heter https://samuelslillamatkasse.wordpress.com. Den använde jag för att skriva om vegansk mat.

Några nackdelar:

* Det skjuts in reklam på sidan
* Man äger inte domänen. Så om folk känner till ens domännamn så "sitter man fast". Man kan visserligen göra en redirect av gamla domännamnet till det nya. Det går ju oftas.

![Skärmdump av reklam på hemsidan](./images/screendump-ads.png)*Skärmdump av reklam på hemsidan. Jag älskar ju katter men vill inte ha reklam i blogginlägget även om det är med katt*

## Det smidigaste sättet - kostar pengar

Att bara betala för ett domännamn och hosting är det absolut enklaste. Antingen kan man betala allt till wordpress.com. Eller så kan man betala för ett ett webbhotell hos t.ex. [Loopia](https://www.loopia.se/), [Miss Hosting](https://misshosting.se/) eller [Inleed](https://inleed.se/). Där kan man köpa både domännamn och webbhotell. Man kan även använda själva plattformen Wordpress, alltså programvaran, även om det inte alls är kopplat till wordpress.com.

När det gäller hemsidan till [Tvärnit Södertörn](https://tvarnitsodertorn.se/) (som jag startade) så köpte jag domän hos Loopia men köpte hosting direkt på Wordpress.com.

## Enkelt och gratis men inga bilder

Jag har som sagt testat WriteFreely som är en bloggplattform som finns på Fediversum, dvs den finns på samma nätverk som Mastodon, Pixelfed, Peertube, Lemmy och kbin.

Här är min sida: https://write.bz/samuel/. Och [här är ett blogginlägg](https://write.bz/samuel/forsta-blogginlagget-med-write-freely).

Funkar väldigt smidigt och bra om man bara vill skriva text.

Nackdel:

* Man har inte en egen domän. Man har inte ens ha en egen subdomän
* Man kan inte ladda upp bilder. (Man kan ladda upp bilderna på andra ställen och länka men det skapar nog mer problem)

## Gratis och utan reklam - kan vara krångligt för vissa

Jag har några bloggar som jag kör:

* https://samuels.bitar.se - Den här bloggen. Körs på Gitlab pages med egen domän.
* https://konstellationen.org - Kamratdataföreningen Konstellationens blogg/hemsida. Körs på egen domän
* https://alice.fritext.org - Testhemsida som körs på Codeberg Pages med gratis subdomän under fritext.org.

Alla dessa är så kallade static site generators (SSG) och innebär att det inte finns en databas (som det gör med Wordpress) och att man inte kan logga in på sidan och skapa inlägg där. Istället så skriver man inlägg i text-filer som sedan automatiskt görs om din gamla hederliga html-filer.

![Skärmdump av hur det ser ut när jag skapar blogginlägg](./images/vscode-samuelsbitar.png)*Skärmdump av hur det ser ut när jag skapar blogginlägg*

Alla dessa sparas/hanteras i git som är ett program som många utvecklare använder för att spara kod. Så för de som inte pysslar med sådant till vardags kanske det kan vara en uppförsbacke.

Några kända static site generators är:

* [Hexo](https://hexo.io/)
* [Jekyll](https://jekyllrb.com/)
* [Hugo](https://gohugo.io/)

Det finns [många static site generators](https://jamstack.org/generators/) att välja på.

## Gratis hosting men utan domännamn

En annan hemsida jag gjort är https://samas.alwaysdata.net. Det är en väldigt enkel php-sida där jag sparar steg från min stegräknare (ja, jag är nörd). Jag har det på [Alwaysdata](https://www.alwaysdata.com/en/) där det finns gratiskonton med upp till 100 MB lagring. Det är inte så mycket lagring men för mycket små projekt räcker det gott. Det enda jag saknar är att jag lyckas inte koppla min domännamn dit.

## Gratis hosting OCH med domännamn

Det är det här som jag har letat som bara den efter! Och till slut har jag fått lite tips!

Per Axbom skrev [ett blogginlägg](https://axbom.com/25y/) som han tipsade mig om. Han tar upp ungefär samma sak som jag gör i det här blogginlägget. Och han har faktiskt hittat gratis hosting, nämligen hos [Free Hosting])https://www.freehosting.com/free-hosting.html).

Några features för den gratis hostingen är:

* 100% free hosting for lifetime
* Hosting for your own domain name
* Unmetered bandwidth
* Linux / Apache / PHP / MySQL

Det här är alltså något jag måste testa.

Några andra som jag har kikat på men inte lyckats med är:

* [000webhost](https://www.000webhost.com) - När jag skulle registrera konto fick jag "Please use an email address from a reputable email provider (like GMail or Outlook)."


## Vad finns det mer för grejer?

Jag är mest inne på static site generator-sidor. Dels för att det är säkrare, ingen kan logga in på siten. Men jag skulle behöva ett webbgränssnitt som är enkelt att använda. Jag blev tipsad on [Lichen](https://lichen.sensorstation.co/) som är just det:

> Lichen is designed to hit the sweet spot between WordPress and manually editing HTML. A technically-inclined person can easily set up many Lichen sites for friends with minimal effort without having to worry about the technical burden of maintaining a database-driven CMS.

Det behöver man hosta själv då. Men man hade kunnat hosta Lichen på t.ex. Always data och att man sen (automatiskt) publicerar till t.ex. Gitlab Pages eller Codeberg Pages.

Jag fick också tips på att man kunde ha ett så kallat Headless CMS som [Contentful](https://www.contentful.com/) och att man gör om det sedan till statiska html-filer med någon hook. Det finns fria alternativ till Contentful som t.ex. [Strapi](https://strapi.io/), [Cockpit](https://strapi.io/), [SilverStripe](https://www.silverstripe.com/cloud-hosting/pricing/) och [Sanity](https://www.sanity.io/pricing). Dessvärre har bara Sanity en gratisversion, alla andra kostar pengar.

Det finns också en najs hemsida som heter [Free For Developers](https://free-for.dev/) som listar flera olika ställen med gratisversioner av tjänster. Det var där jag hittade Alwaysdata t.ex.

Sen så kan man göra som jag själv gör ibland, att använda den inbyggda Web IDE:n för Gitlab (eller för Github eller annan git provider). Men då dyker det upp lite fler filer och man måste interagera med git. För många är det kanske läskigt och stökigt men för många andra så går de nog rätt bra.

En annan grej man kan köra är att köra en CDN över en S3-bucket, alltså att ha en "online-katalog" som körs om till en webbplats. Amazon är ett skitföretag så använd inte Amazons S3-buckets. Men här är några alternativ till S3-bucket och CDN:er:

* Digital ocean har [spaces](https://www.digitalocean.com/products/spaces) (alltså S3-buckets) med stöd för [CDN](https://docs.digitalocean.com/products/spaces/how-to/enable-cdn/)
* Scaleway har [object storage](https://www.scaleway.com/en/object-storage/) (alltså S3-buckets) med stöd för att [koppla domännamn](https://www.scaleway.com/en/docs/tutorials/s3-customize-url-cname/)
* Bunny har [storage](https://bunny.net/storage/) som även har [stöd för sftp](https://bunny.net/blog/introducing-edge-storage-sftp-support-s3-next/) och har även [CDN](https://bunny.net/cdn/).

Jag har även skapat en site med [Netlify](https://www.netlify.com/). Där kan man ladda upp kataloger som görs om till hemsidor. När man laddar upp katalogen så skapades en hemsida med den här url:en: https://papaya-yeot-96ba81.netlify.app. Man kan också koppla en egen domän. Så jag testade att göra det, man måste fippla lite med inställningar i sin DNS, men det gick snabbt. Då kan jag komma åt den så här: https://netlify-drop.bitar.se. Så det går att använda sådana här deploymentsiter men det är nog alltid smidigare att använda git direkt. Speciellt om man ska fortsätta att ändra sin sida, vilket man ju gör med en blogg.

Jag trillade över sidan [telegra.ph](https://telegra.ph) där man kan skapa blogginlägg direkt på sidan utan inloggning. [Här är ett blogginlägg jag skapat](https://telegra.ph/H%C3%A4r-%C3%A4r-ett-exempel-07-29).

Sen finns det en drös med olika färdiga tjänster. Vissa gratis, andra kostar:

* [micro.blog](https://micro.blog/) - Kostar pengar
* [Ghost](https://ghost.org/) - Open source. Finns som betalversion men man kan hosta själv
* [Bearblog.dev](https://bearblog.dev) - Väldigt enkel bloggplattform. Finns som gratisversion. Men om man t.ex. vill lägga upp bilder behöver man uppgrader till pro. [Här är ett blogginlägg jag gjort](https://samuel.bearblog.dev/forsta-blogginlagg/).
* [Blot](https://blot.im/) - "Blot turns a folder into a website"
* [Write.as](https://write.as) - Betalversion av [WriteFreely](https://writefreely.org/)

## För mig eller för andra?

För min egen del är jag rätt nöjd med setupen för den här bloggen. Det enda som vore mer najs är om jag kunde lägga upp nya inlägg via ett webbinterface så jag kan göra det från mobilen. Jag kan rent teoretiskt göra det via Gitlabs Web IDE men det hade varit meckigt med git och var filer ska ligga. Då tror jag mer på att fixa igång [Lichen](https://lichen.sensorstation.co/). Men för att andra (som inte är webbutvecklare eller tekniskt intresserade) så vill jag kunna tipsa om något enklare sätt att komma igång än att behöva introducera git och sånt. Markdown är fine svårighetsgrad tänker jag. Så på det sättet är WriteFreely ett bra första steg.

Har du andra förslag på andra bra tjänster eller andra bra setups? Maila mig på samuel@bitar.se eller [skriv till mig på Mastodon](https://social.spejset.org/@samuel)!

Eller skulle du vilja komma igång med en blogg men inte vet hur så kontakta mig gärna då också.

Jag tänker att när jag landat i något bra förslag så skriver jag någon guide på [Konstellationens wiki](https://wiki.konstellationen.org/).

## Event lördag 19 augusti i Stockholm

Just ja, vi i Konstellationen anordnar ett event lördag 19 augusti. [Bortom Big Tech sociala medier - hur kan vänstern vara online? ](https://konstellationen.org/2023/07/05/mastodonfest-rss-blogg/). Då kommer vi pratat bland annat om bloggande, Mastodon och mailstrategi.