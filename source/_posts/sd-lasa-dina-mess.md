---
title: Krönika - Är du redo för att SD ska läsa dina mess?
image: cctv-surveillance-camera.jpg
image_description: Bild av Joseph Mucira från <a href="https://pixabay.com/photos/cctv-surveillance-camera-cctv-7267551/">Pixabay</a>
description: En krönika om varför massövervakning är en dålig idé
date: 2024-05-19 21:00
---

Här kommer en krönika jag skrev i [ETC Nyhetsmagasin](https://www.etc.se/kronika/vill-du-att-sd-ska-laesa-dina-mess).


**Vi vet inte vilka regeringar vi får i framtiden. Känns det fortfarande som en bra idé med massövervakning av all kommunikation med en SD-minister vid spakarna?**

Vindkraftsföretaget trodde att de blivit hackade. IT-säkerhetsexperten de tog in såg att det stämde. Deras datorer hade blivit infekterade av skadlig kod som användes för att utvinna kryptovaluta. Hackarna hade dessutom installerat säkerhetsuppdateringar för operativsystemet för att undvika att andra hackare skulle ta sig in. IT-säkerhetsexperten tog fram rekommendationer för hur man kunde ta bort den skadliga koden på ett säkert sätt men IT-systemet fungerade faktiskt bättre efter att de blivit hackade. Så företaget tackade för hjälpen och lät hackarna vara kvar.

Historien berättades i ett [avsnitt](https://darknetdiaries.com/episode/22/) av podcasten Darknet Diaries. Nu i april kunde vi läsa om hur en [bakdörr installerats i verktyget xz Utils](https://en.wikipedia.org/wiki/XZ_Utils_backdoor). Om det inte hade [upptäckts i tid](https://www.theguardian.com/global/2024/apr/02/techscape-linux-cyber-attack) så kunde miljontals Linux-servrar fått den bakdörren installerad. Men kanske att man skulle släppa in hackarna ändå? De kanske har bättre koll på mina system än vad jag själv har? Och antagligen kommer de bara använda mina servrar väldigt sällan för att göra överbelastningsattacker och det kommer nog inte drabba mig. Nej, självklart skulle vi rensa bort virus, bakdörrar och annan skadlig kod som hackare installerar.

Massövervakningsförslaget Chat Control 2.0 från EU-kommissionen skulle innebära att i princip varje kommunikationstjänst måste installera en bakdörr för att avlyssna all kommunikation. EU-parlamentets alla partigrupper var kritiska och ställde sig eniga bakom ett [motförslag som tydligt sade nej](https://www.etc.se/utrikes/eu-parlamentet-roestade-nej-till-kritiserade-chat-control) till de kränkande övervakningsdelarna i förslaget. Men det belgiska ordförandeskapet i EU verkar gå på kommissionens linje, [enligt ett läckt dokument](https://www.patrick-breyer.de/en/full-chat-control-proposal-leaked-attack-on-digital-privacy-of-correspondence-and-secure-encryption/).

Liksom vi inte vill att hackare ska ha tillgång till våra system (även om de skulle sköta säkerhetsuppdateringar) bör vi inte tillåta att det installeras bakdörrar i våra telefoner och datorer av EU eller av staten (även om de säger att det handlar om vår säkerhet).

Vi vet inte vilka regeringar vi får i framtiden. Känns det fortfarande som en bra idé med massövervakning av all kommunikation med en SD-minister vid spakarna?