---
title: Klägget granskar korrupta politiker och lobbyister
image: klagget-kristersson.png
description: I går lanserade Skiftet det partipolitiskt oberoende granskningsinstitutet Klägget
date: 2024-01-19 09:00
---

Igår så släppte [Skiftet](https://skiftet.org) en riktigt rolig satsning: [klagget.nu](https://klagget.nu/)!

Granskningsinstitutet Klägget granskar korruptionen inom politiken och lobbyismen:

> Klägget är platsen där gränsen mellan politiken, påverkan och storbolagen upplöses. Varje dag ser vi hur tidigare politiker går från att representera folket till att bli betalda lobbyister för hemliga kunder.

Det är Robin Zachari och Max Andersson från Skiftet som driver sajten. De skrev en [debattartikel i Dagens ETC](https://www.etc.se/debatt/saa-stoppar-vi-klaeggets-framvaext-tillsammans) där de presenterade den nya satsningen:

> Sverige styrs idag till stor del av ett klägg av personer som vandrar mellan olika maktcentrum och säljer ut sina väljares värderingar för pengar. Klägget är platsen där gränsen mellan politik, påverkan och storbolagen upplöses, och den rundar demokratin.
>
> Klägget är grogrund för korruption och det fräter sönder vårt demokratiska samhälle. Låt oss ta ett tydligt exempel: vinst i välfärden. Svenska folket vill inte ha vinstdrift i skolan. En majoritet av samtliga partiers väljare, utom Moderaterna, är mot detta. Varför händer då inget? Jo, storföretagens lobbyister höll en massa möten med Sverigedemokraterna, bjöd dem på fina middagar och inlemmade SD i klägget. Innan middagarna var SD mot vinsterna och efteråt var de för. En av ägarna till Internationella Engelska Skolan Hans Bergström lobbade redan 2014 på SD för att skulle fälla regeringen, och därmed skydda välfärdsbolagens vinster. Nu har vi en regering som bildats för att göra just detta.

De har så klart redan ett gäng artiklar på sidan:

* [Så blev SD en del av klägget](https://klagget.nu/2024/01/18/sa-blev-sd-en-del-av-klagget/)
* [Ska lobbyister verkligen vara programledare i SVT?](https://klagget.nu/2024/01/17/ska-lobbyister-verkligen-vara-programledare-i-svt/)
* [Ingerö fick sparken efter anklagelse om sexuellt ofredande – blev lobbyist!](https://klagget.nu/2024/01/11/ingero-fick-sparken-efter-anklagelse-om-sexuellt-ofredande-blev-lobbyist/)
* [Kärnkraftslobbyist blir kärnkraftsgeneral](https://klagget.nu/2024/01/08/karnkraftslobbyist-blir-karnkraftsgeneral/)
* [Vapenlobbyisten Löfven: “Jag är inte lobbyist”](https://klagget.nu/2024/01/04/vapenlobbyisten-lofven-jag-ar-inte-lobbyist/)
* [Sommarstugan är såld – istället ska statsministern varva ner på lobbyist-herrgård](https://klagget.nu/2024/01/03/sommarstugan-ar-sald-istallet-ska-statsministern-varva-ner-pa-lobbyist-herrgard/)
* [Statsministerns superlobbyist svartbyggde trädäck – åkte fast](https://klagget.nu/2024/01/03/statsministerns-superlobbyist-svartbyggde-tradack-akte-fast/)
* [Elon Musk söker fackkrossarlobbyist](https://klagget.nu/2024/01/03/elon-musk-soker-fackkrossarlobbyist/)
* [Då: sångare i Muf:s sågade vallåt – Nu: skollobbyist och skolpolitiker](https://klagget.nu/2023/12/12/da-sangare-i-mufs-sagade-vallat-nu-skollobbyist-och-skolpolitiker/)
* [S-politiker blir djävulsadvokat på moderaternas lobby-firma](https://klagget.nu/2023/11/13/s-politiker-blir-djavulsadvokat-pa-moderaternas-lobby-firma/)

## Enkelt, snyggt och slagkraftigt

Det första jag slås av är att hemsidan är snygg och enkel! Budskapet är enkelt men slagkraftigt. Bilderna är riktigt snygga och man blir sugen på att läsa.

![Bilder till artiklarna](./images/klagget-bilder.png)*Bilder till artiklarna*

Bilden till [Så blev SD en del av klägget](https://klagget.nu/2024/01/18/sa-blev-sd-en-del-av-klagget/) ser ut så här:

![Bild till SD-artikel](./images/klagget-sd-artikel.jpg)*Bild till SD-artikel*

Visst blir man pepp av att läsa den!

## Folkbildande och kunskapsbank

En sak jag uppskattar är att de har taggat upp organisationer som t.ex. Kreab så när de nämns i en artikel (t.ex. [denna](https://klagget.nu/2023/11/13/s-politiker-blir-djavulsadvokat-pa-moderaternas-lobby-firma/)) så fälls en liten faktaruta ut där det står kort om Kreab och [länk till sida](https://klagget.nu/organisationer/kreab/) där man kan läsa mer.

![Kreab-ruta](./images/kreab.png)*Kreabruta*

## Nyhetsbrev, RSS och Mastodon gör det möjligt att bryta sig loss från big tech

Det första man möts på hemsidan är en box där man kan skriva in sin e-post för att få del av nyhetsbrevet. Vidare så finns möjligheten att följa nyheterna via RSS och Mastodon vilket gör att man blir mindre beroende av big tech sociala medier. Det är fantastiskt bra! Och något som fler borde följa!

Jag har tidigare bloggat om vikten av att [bryta sig loss från big tech sociala medier](/bryta-sig-loss/).

Vänsterbloggaren Anders Svensson skrev i [Magasin Konkret en bra artikel](https://magasinetkonkret.se/vanstern-forlorade-initiativet-pa-natet/) om hur vänstern gick från att vara dominerande på nätet under 00-talet med egna bloggar och forum till att sugas upp av de sociala mediernas lockande algoritmplattformar. Men han skriver också om motkulturen som nu formas från vänstern igen och föreslår även konkret programvara och slår ett stort slag för fediversum.

Jag var med och grundade [Kamratatdaföreningen Konstellation](https://konstellationen.org) (och är ordförande) som syftar just till att hjälpa till att bryta sig loss från big tech. Därför driftar föreningen [mastodoninstansen Spejset](https://social.spejset.org) så att man kan ha sociala medier som inte ägs av drivs av ett enda vinstdrivande företag. Man kan läsa mer om varför jag tycker Mastodon är så bra i blogginlägget [Mysigt med Mastodon](/mysigt-med-mastodon/).

Konstellationen utvecklade och driver även mediaprojektet [Mediakollen](https://mediakollen.org/) som samlar nyheter från vänstertidningar, vänsterbloggar och vänsterpoddar (med hjälp av RSS) och presenterar på en sida. Alltså som ett slag Omni.se fast för vänstern. Självklart är Kläggets RSS-flöde med på Mediakollen!

## Peppigt och välbehövligt med Klägget

Klägget är en välbehövlig satsning! Frågan om korruption mellan politiker, lobbyister och storföretag är viktig och korruptionen måste granskas. Men Skiftets lansering av Klägget visar också på hur vi i den breda vänstern bör agera för att sätta dagordningen och få till en förändring i samhället: mer verkstad och mindre snack!

Nåja, en del av verkstaden ÄR snack. Exempelvis så vill jag få igång en bloggande vänster igen. Men det är en stor skillnad på att skriva texter på sin blogg och länka till andra vänsterbloggar, vänstertidningar och vänsterpoddar än att klaga slött på Facebook.

Så STORT TACK till Skiftet för denna viktiga satsning och energiboost!