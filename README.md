# samuels-bitar.gitlab.io

Samuels bitar website.

It is deployed at [gitlab pages](https://samuels-bitar.gitlab.io). Later also on [samuels.bitar.se](samuels.bitar.se).

The setup from scratch was:

```
npm install hexo --save
npm install hexo-generator-feed --save
# Setup the PATH so we can use hexo directly
nano ~/.bashrc
# Add to the end: PATH="$PATH:./node_modules/.bin"
source ~/.bashrc
hexo init blog
rm -rf node_modules/ package-lock.json
mv blog/* .
rm -rf blog
hexo server
```
